﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;
using System.IO;
using UnityEngine.XR.ARFoundation;




public class galleryImporter : Singleton<galleryImporter>
{

    string baseUrl;
    string projectsUrl;
    string getprojectUrl;

    public static string currentId;
    public bool startit = false;
    public bool isCollapsible;

    public bool isFloorReady = false;

    public AudioSource ad;

    public Transform originalParent;

    public Color green;
    public Color violet;

    public Material mat;

    public GameObject UIHelper;

    public GameObject currentArSession;

    public GameObject arSessionPref;

    public GameObject XImage;

    public Slider sd;

    

    public TextMeshProUGUI tapToPlace;

    public GameObject hotSpot;

    [Tooltip("this is the prefab containing the projects thumbnail for 2 projects")]
    public GameObject panelPref;

    [Tooltip("this is the prefab for the collapsible")]
    public GameObject collapsiblePref;

    [Tooltip("json for debugging")]
    public string json;

    [Tooltip("json for debugging")]
    public string testJson;

    [Tooltip("how many projects yet to download")]
    public int toLoad;

    [Tooltip("are we downloading now or not")]
    public bool isWaiting = false;

    [Tooltip("the loading gameobject activated when isWaiting == true")]
    public GameObject loading;

    [Tooltip("this is the custom button that comes with every portal usially reservation")]
    public Button customBtn;
    public TextMeshProUGUI customBtnText;
    public Button cta;

    public portalsJson pj, totalPj;

    public IEnumerator Start()
    {
        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/thumbnail");
        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/blurredThumbnail");
        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/images360");
        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/bundles");


        //originalParent = transform.parent;

        baseUrl = cashing.baseUrl;

        projectsUrl = cashing.projectsUrl;

        getprojectUrl = cashing.getprojectUrl;


        totalPj = new portalsJson();

        loading.SetActive(true);

        yield return StartCoroutine(cashing.getPortalsJson());


        //totalPj = JsonUtility.FromJson<portalsJson>(json);

        pj = totalPj;
        Debug.Log(totalPj.count());

        /*foreach (Transform child in sceneHolder.instance.transform)
        {
            child.parent = transform;
        }*/

        transform.parent = originalParent;

        /*foreach (Transform child in sceneHolder.instance.transform)
        {
            DestroyImmediate(child.gameObject);
        }*/

        //print("starting");

        if (transform.childCount == 0)
        {
            StartCoroutine(spawnGallery());
        }


        if(sceneHolder.instance.isDeep)
        {
            Debug.Log("heeerreee");
            deepLinkPortal(sceneHolder.instance.id);
        }
        yield return null;
    }

    public void search(string input)
    {
        //print("searching for '" + input + "'");

        /*if(input != "")
        {
            portalsJson temp = new portalsJson();

            foreach (var p in totalPj.portals)
            {
                if (p.name.ToUpper().Contains(input.ToUpper()) || p.tagContains(input.ToUpper()) )
                {
                    temp.portals.Add(p);
                }
            }

            pj = temp;
        }
        else
        {
            pj = totalPj;
        }*/

        if (input != "")
        {
            portalsJson temp = new portalsJson();
            foreach (portal p in totalPj.result)
            {
                portal pT = p;
                Debug.Log(p._id);
                bool added = false;

                foreach (panorama pano in p.panoramas)
                {
                    if (!added)
                    {
                        
                        if (p.name.ToUpper().Contains(input.ToUpper()))
                        { 
                        temp.result.Add(pT);
                            added = true;
                            //pT.panoramas.Add(pano);
                            
                        }
                    }
                    else
                    {
                         if (p.name.ToUpper().Contains(input.ToUpper()))
                        {
                        //pT.panoramas.Add(pano);
                        }
                    }
                }
            }

            pj = temp;
        }
        else
        {
            pj = totalPj;
        }

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        //print("results found are " + pj.portals.Count);
        StopAllCoroutines();
        StartCoroutine(spawnGallery());
    }

    public IEnumerator spawnGallery()
    {
        //yield return new WaitForSeconds(.1f);

        //print("spawning gallery");


        //mat.SetColor("_TexTintColor", violet);


        //toLoad = 10;
        Texture2D t;
        if (isCollapsible)
        {
            int countryCount = 0;
            foreach (portal p in pj.result)
            {
                if (p.origin == AuthManager.origine)
                {
                    GameObject countryG = Instantiate(collapsiblePref, transform);

                    //countryG.GetComponent<collapsibleList>().updateBrothers();

                    countryG.GetComponentInChildren<TextMeshProUGUI>().text = p.name.ToUpper();

                    //GetComponent<RectTransform>().sizeDelta += new Vector2(0, 110);

                    //countryG.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -countryCount * 100);

                    int count = 0;


                    foreach (panorama pano in p.panoramas)
                    {
                        //if (!textureHolder.panoramas.ContainsKey(pano._id))
                        //    textureHolder.panoramas.Add(pano._id, pano.defaultPanoramaThumb);
                        //if (!textureHolder.thumbnailes.TryGetValue(pano._id, out t))
                        //{
                            yield return StartCoroutine(cashing.download(pano.defaultPanoramaThumb, pano._id, false));
                            textureHolder.thumbnailes.TryGetValue(pano._id, out t);
                        //}
                        
                        //textureHolder.thumbnailes.TryGetValue(pano._id, out b);

                        bool isZero = countryCount == 0;
                        Debug.Log(p.cta);
                        addButton(pano.name, t, t, pano._id, count, countryG, p.cta, p.buttonLabel, p.name, false);
                        count++;
                    }
                    //toLoad--;

                    //yield return new WaitForSeconds(.1f);

                    //loading.SetActive(toLoad >= 0);

                    //yield return new WaitUntil(() => toLoad >= 0);



                    countryCount++;
                    if (countryCount > 6)
                        transform.parent.parent.Find("filter").Find("searchBar").GetComponent<Animator>().enabled = true;
                }
            }
            //GetComponentInChildren<Toggle>().isOn = true;
        }
        else
        {
            print("non collapsible");
            int countryCount = 0;
            int count = 0;
            Debug.Log(pj.result.Count);
            foreach (portal p in pj.result)
            {
                Debug.Log(p._id);
                Debug.Log(p.name);
                Debug.Log(p.panoramas);
                Debug.Log(p.defaultPanorama);
                Debug.Log(p.origin);
                Debug.Log(p.country);

                int views = 0;
                if (p.origin == AuthManager.origine)
                {
                    print("xyz");
                    yield return StartCoroutine(cashing.download(p.defaultPanorama, p.panoramas[0]._id));

                    //yield return StartCoroutine(cashing.download(p.defaultPanorama, p.panoramas[0]._id, true));
                    //textureHolder.thumbnailes.TryGetValue(p.panoramas[0]._id, out b);

                    foreach (panorama pano in p.panoramas)
                    {
                        if (!textureHolder.panoramas.ContainsKey(pano._id))
                            textureHolder.panoramas.Add(pano._id, pano.defaultPanoramaThumb);

                        //yield return StartCoroutine(cashing.download(pano.defaultPanoramaThumb, pano._id));


                        //yield return StartCoroutine(cashing.download(pano.defaultPanorama, pano._id, true));
                        views++;
                    }
                    foreach (location loc in p.panoramas[0].locations)
                    {
                        yield return StartCoroutine(cashing.download(textureHolder.panoramas[loc.url], loc.url));
                    }
                    textureHolder.thumbnailes.TryGetValue(p.panoramas[0]._id, out t);
                    if (t)
                        addButtonNC(p.country, t, t, p._id, count, p.name, views);
                    count++;
                    //toLoad--;

                    //yield return new WaitForSeconds(.1f);

                    //loading.SetActive(toLoad >= 0);

                    //yield return new WaitUntil(() => toLoad >= 0);



                    countryCount++;
                    if (countryCount > 6)
                        transform.parent.parent.Find("filter").Find("searchBar").GetComponent<Animator>().enabled = true;
                }
            }
        }

        loading.SetActive(false);
    }


    /// <summary>
    /// manages the adding of a project's button to the gallery
    /// </summary>
    public void addButtonNC(string name, Texture2D texture, Texture2D blurred, string id, int order, string countryName, int views)
    {
        //print("adding " + name);

        int itemsPerLine = 2;
        int dist = 510;

        if (order % itemsPerLine == 0)
        {

            GameObject tt = Instantiate(panelPref, transform);
            tt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -(order / itemsPerLine) * dist - 100);
            //print(-(order / itemsPerLine) * 209);
            GetComponent<RectTransform>().sizeDelta = new Vector2(792, (order / itemsPerLine + 1) * dist);

        }

        GameObject temp = transform.GetChild(order / itemsPerLine).GetChild(order % itemsPerLine).gameObject;

        temp.name = id;

        temp.GetComponentInChildren<TextMeshProUGUI>().text = name;
        temp.GetComponentsInChildren<TextMeshProUGUI>()[1].text = countryName;
        temp.GetComponentInChildren<RawImage>().texture = blurred;

        //temp.transform.GetChild(0).GetChild(2).gameObject.SetActive();
        temp.GetComponentsInChildren<RawImage>()[1].texture = texture;

        //temp.transform.GetChild(0).GetChild(3).gameObject.SetActive(type.ToUpper() == "ASSETBUNDLE");

        temp.transform.GetChild(0).GetChild(4).gameObject.SetActive(true);
        if (views != 1)
        {
            temp.transform.GetChild(0).GetChild(8).gameObject.SetActive(true);
            temp.transform.GetChild(0).GetChild(8).GetComponentInChildren<TextMeshProUGUI>().text = views.ToString();
        }
        else
            temp.transform.GetChild(0).GetChild(8).gameObject.SetActive(false);

        temp.GetComponentInChildren<RawImage>().GetComponent<RectTransform>().sizeDelta = fitOut360(texture);
        temp.GetComponentsInChildren<RawImage>()[1].GetComponent<RectTransform>().sizeDelta = fitIn360(texture);

        temp.GetComponentInChildren<Button>().onClick.AddListener(delegate { StartCoroutine(setPortalnc(id)); });

        temp.SetActive(true);
    }


    /// <summary>
    /// manages the adding of a project's button to the gallery
    /// </summary>
    public void addButton(string name, Texture2D texture, Texture2D blurred, string id, int order, GameObject collapse,string ctabutton, string ctalabel, string countryName, bool isZero = false)
    {
        print("adding " + ctabutton);

        int itemsPerLine = 2;
        int dist = 510;

        if (order % itemsPerLine == 0)
        {
            GameObject tt = Instantiate(panelPref, collapse.transform);
            tt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -(order / itemsPerLine) * dist - 100);
            //print(-(order / itemsPerLine) * 209);
            //GetComponent<RectTransform>().sizeDelta = new Vector2(792, (order / itemsPerLine + 1) * dist);

        }

        collapse.GetComponent<collapsibleList>().maxHeight = (order / itemsPerLine + 1) * dist + 100;

        if (isZero)
        {
            collapse.GetComponent<RectTransform>().sizeDelta = new Vector2(792, (order / itemsPerLine + 1) * dist + 100);
        }

        GameObject temp = collapse.transform.GetChild(order / itemsPerLine + 1).GetChild(order % itemsPerLine).gameObject;

        temp.name = id.ToString();

        temp.GetComponentInChildren<TextMeshProUGUI>().text = name;
        temp.GetComponentsInChildren<TextMeshProUGUI>()[1].text = countryName;
        //temp.GetComponentInChildren<RawImage>().texture = blurred;
        temp.GetComponentsInChildren<RawImage>()[1].texture = texture;

        //temp.transform.GetChild(0).GetChild(2).gameObject.SetActive(type.ToUpper() == "VIDEO");

        //temp.transform.GetChild(0).GetChild(3).gameObject.SetActive(type.ToUpper() == "ASSETBUNDLE");

        temp.transform.GetChild(0).GetChild(4).gameObject.SetActive(true);
        temp.transform.GetChild(0).GetChild(8).gameObject.SetActive(false);
        //temp.GetComponentInChildren<RawImage>().GetComponent<RectTransform>().sizeDelta = fitOut360(texture);
        temp.GetComponentsInChildren<RawImage>()[1].GetComponent<RectTransform>().sizeDelta = fitIn360(texture);

        temp.GetComponentInChildren<Button>().onClick.AddListener(delegate { StartCoroutine(setPortal(id, ctabutton, ctalabel)) ; });

        temp.SetActive(true);
    }


    /// <summary>
    /// used for loading a portal by deeplink connected to deeplink.cs by taskbar.cs
    /// </summary>
    public void deepLinkPortal(string id)
    {
        StartCoroutine(setPortalnc(id, true));
        sceneHolder.instance.isDeep = false;
    }


    /// <summary>
    /// downloads the portal content and make it ready to be useable
    /// </summary>
    public IEnumerator setPortal(string id,string ctabutton, string ctalabel, bool isDeepLink = false)
    {
        print(ctalabel);
        if (isDeepLink)
        {
            yield return null;
        }

        isFloorReady = false;

        sceneHolder.instance.id = id.ToString();

        //mat.SetColor("_TexTintColor", violet);

        currentId = id.ToString();
        XImage.SetActive(true);
        sd.gameObject.SetActive(true);

        if(PlayerPrefs.GetInt("isFR") == 1)
        {
            tapToPlace.text = Fr.waitForSound;
        }
        else
        {
            tapToPlace.text = En.waitForSound;
        }

        //print("Veuillez attendre le chargement et le signal sonore.");



        if (currentArSession)
            DestroyImmediate(currentArSession);


        //loading.SetActive(true);
        panorama mj = new panorama();
        location loc = new location();
        //yield return StartCoroutine(cashing.getPortal(id));

        string pJs;

        if (textureHolder.mainJsons.TryGetValue(id, out pJs))
        {
            mj = JsonUtility.FromJson<panorama>(pJs);
        }
        else
        {
            sd.gameObject.SetActive(false);
            yield break;
        }

        //mj = JsonUtility.FromJson<mainJson>(testJson);
        customBtnText.text = ctalabel;
        cta.onClick.RemoveAllListeners();
        Debug.Log(PlayerPrefs.GetString("email"));
        cta.onClick.AddListener(delegate { Application.OpenURL(ctabutton + "?umail=" + PlayerPrefs.GetString("email")); });
        cta.gameObject.SetActive(true);

        customBtn.onClick.RemoveAllListeners();
        customBtn.onClick.AddListener(delegate { Application.OpenURL(ctabutton+"?umail="+PlayerPrefs.GetString("email")); });
        customBtn.gameObject.SetActive(true);

        if (transform.parent.parent.GetComponent<Animator>().enabled)
            transform.parent.parent.GetComponent<Animator>().Play("galleryDisappear");

        UIHelper.SetActive(true);

        //reset arSession
        currentArSession = Instantiate(arSessionPref);


        transform.parent = sceneHolder.instance.transform;

        FindObjectOfType<ARPlaneManager>().enabled = true;

        PlaceMultipleObjectsOnPlane ppp = FindObjectOfType<PlaceMultipleObjectsOnPlane>();

        GameObject temp = ppp.placedPrefab;

        ppp.type = "IMAGE";

        ppp.ableToPlace = false;

        //if (mj.background.type.ToUpper() == "VIDEO")
        //{

        //    temp.GetComponentInChildren<VideoPlayer>().enabled = true;
        //    temp.GetComponentInChildren<VideoPlayer>().url = baseUrl + mj.background.url;

        //}
        //else if (mj.background.type.ToUpper() == "IMAGE")
        //{

        print("_____________________________________________");
        print(mj.defaultPanorama);

        yield return StartCoroutine(cashing.download360(JsonUtility.FromJson<panorama>(textureHolder.mainJsons[id]).defaultPanorama, id, sd));
        
        foreach (location l in mj.locations)
        {

            yield return StartCoroutine(cashing.download(textureHolder.panoramas[l.url], id, false));
          
        }
        textureHolder.images360.TryGetValue(id, out Texture2D t);

        if (!t)
        {
            sceneManager.errorReset();
        }

        ppp.texture = t;

        ppp.ableToPlace = true;
        sd.value = 1;
        sd.gameObject.SetActive(false);

        ARPlaneManager ap = FindObjectOfType<ARPlaneManager>();

        //mat.SetColor("_TexTintColor", green);

        //foreach (var plane in ap.trackables)
        //    plane.GetComponent<Renderer>().material.SetColor("_TexTintColor", green);


        while (!isFloorReady)
        {
            yield return null;
        }

        ad.Play();

        isFloorReady = false;


        if(PlayerPrefs.GetInt("isFR") == 1)
        {
            tapToPlace.text = Fr.tapToPlace;
        }
        else
        {
            tapToPlace.text = En.tapToPlace;
        }

        //print("Appuyez pour positionner votre porte");

        XImage.SetActive(false);

    }

    public IEnumerator setPortalnc(string id, bool isDeepLink = false)
    {
        if (isDeepLink)
        {
            yield return null;
        }

        isFloorReady = false;

        sceneHolder.instance.id = id;

        //mat.SetColor("_TexTintColor", violet);

        currentId = id;
        XImage.SetActive(true);
        sd.gameObject.SetActive(true);

        if (PlayerPrefs.GetInt("isFR") == 1)
        {
            tapToPlace.text = Fr.waitForSound;
        }
        else
        {
            tapToPlace.text = En.waitForSound;
        }

        //print("Veuillez attendre le chargement et le signal sonore.");



        if (currentArSession)
            DestroyImmediate(currentArSession);


        //loading.SetActive(true);
        portal mj = new portal(id);

        //yield return StartCoroutine(cashing.getPortal(id));

        string pJs;

        Debug.Log(textureHolder.portalJsons.TryGetValue(id, out pJs));
        if (textureHolder.portalJsons.TryGetValue(id, out pJs))
        {
            mj = JsonUtility.FromJson<portal>(pJs);
        }
        else
        {
            sd.gameObject.SetActive(false);
            yield break;
        }

        //mj = JsonUtility.FromJson<mainJson>(testJson);

        customBtnText.text = mj.buttonLabel;
        cta.onClick.RemoveAllListeners();
        Debug.Log(PlayerPrefs.GetString("email"));
        cta.onClick.AddListener(delegate { Application.OpenURL(mj.cta + "?umail=" + PlayerPrefs.GetString("email")); });
        cta.gameObject.SetActive(true);

        customBtn.onClick.RemoveAllListeners();
        customBtn.onClick.AddListener(delegate { Application.OpenURL(mj.cta + "?umail=" + PlayerPrefs.GetString("email")); });
        customBtn.gameObject.SetActive(true);

        if (transform.parent.parent.GetComponent<Animator>().enabled)
            transform.parent.parent.GetComponent<Animator>().Play("galleryDisappear");

        UIHelper.SetActive(true);

        //reset arSession
        currentArSession = Instantiate(arSessionPref);


        transform.parent = sceneHolder.instance.transform;

        FindObjectOfType<ARPlaneManager>().enabled = true;

        PlaceMultipleObjectsOnPlane ppp = FindObjectOfType<PlaceMultipleObjectsOnPlane>();

        GameObject temp = ppp.placedPrefab;

        ppp.type = "IMAGE";

        ppp.ableToPlace = false;

        //if (mj.background.type.ToUpper() == "VIDEO")
        //{

        //    temp.GetComponentInChildren<VideoPlayer>().enabled = true;
        //    temp.GetComponentInChildren<VideoPlayer>().url = baseUrl + mj.background.url;

        //}
        //else if (mj.background.type.ToUpper() == "IMAGE")
        //{

        print("_____________________________________________");
        print(mj.defaultPanorama);
        ChangePano cp = new ChangePano();
        yield return StartCoroutine(cashing.download360(mj.panoramas[0].defaultPanorama, mj.panoramas[0]._id, sd));
        GameObject port = new GameObject(id);
        ppp.locations = mj.panoramas[0].locations;
        foreach (location l in mj.panoramas[0].locations)
        {
            yield return StartCoroutine(cashing.download(textureHolder.panoramas[l.url], l.url));
        }
        textureHolder.images360.TryGetValue(mj.panoramas[0]._id, out Texture2D t);

        if (!t)
        {
            sceneManager.errorReset();
        }

        ppp.texture = t;

        ppp.ableToPlace = true;
        sd.value = 1;
        sd.gameObject.SetActive(false);

        ARPlaneManager ap = FindObjectOfType<ARPlaneManager>();

        //mat.SetColor("_TexTintColor", green);

        //foreach (var plane in ap.trackables)
        //    plane.GetComponent<Renderer>().material.SetColor("_TexTintColor", green);


        while (!isFloorReady)
        {
            yield return null;
        }

        ad.Play();

        isFloorReady = false;


        if(PlayerPrefs.GetInt("isFR") == 1)
        {
            tapToPlace.text = Fr.tapToPlace;
        }
        else
        {
            tapToPlace.text = En.tapToPlace;
        }

        //print("Appuyez pour positionner votre porte");

        XImage.SetActive(false);

    }

   
    /// <summary>
    /// fits the thumbnail in the button rectangle
    /// </summary>
    Vector2 fitIn360(Texture2D t)
    {
        Vector2 temp = new Vector2();

        if (t.width > t.height)
        {
            temp.x = 380;
            temp.y = (float)t.height / ((float)t.width / 380);
            //temp.y = 160;
        }
        else
        {

            temp.y = 480;
            temp.x = (float)t.width / ((float)t.height / 480);

            //temp.y = (float)t.height / ((float)t.width / 160);
            //temp.x = 160;
        }

        temp.y = (int)temp.y;

        temp.x = (int)temp.x;

        return temp;
    }

    /// <summary>
    /// covers the blurredThumbnail in the button rectangle
    /// </summary>
    Vector2 fitOut360(Texture2D t)
    {
        Vector2 temp = new Vector2();

        if (t.width < t.height)
        {
            temp.x = 360;
            temp.y = (float)t.height / ((float)t.width / 360);
            //temp.y = 160;
        }
        else
        {

            temp.y = 460;
            temp.x = (float)t.width / ((float)t.height / 460);

            //temp.y = (float)t.height / ((float)t.width / 160);
            //temp.x = 160;
        }

        temp.y = (int)temp.y;

        temp.x = (int)temp.x;

        return temp;
    }

    /// <summary>
    /// updates everytime gallery gets scrolled and checks if we need to load more
    /// </summary>
    public void ff(Vector2 x)
    {
        if (GetComponent<RectTransform>().sizeDelta.y - GetComponent<RectTransform>().anchoredPosition.y < 2000)
        {
            if (!isWaiting)
            {
                toLoad = 8;
                isWaiting = true;
            }
        }
        else
        {
            isWaiting = false;
        }
    }

   
    

}