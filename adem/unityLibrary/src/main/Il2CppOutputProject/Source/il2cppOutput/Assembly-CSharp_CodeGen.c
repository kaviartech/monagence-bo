﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void request::.ctor(System.String)
extern void request__ctor_m3465B691D7F2F23A8CC54490EEB50B8F220E3777 (void);
// 0x00000002 System.Int32 Customizer::get_ImageCount()
extern void Customizer_get_ImageCount_m9A24772F85B81CC0DE4B9C46867991AFC3CC22CB (void);
// 0x00000003 System.Void Customizer::set_ImageCount(System.Int32)
extern void Customizer_set_ImageCount_m2B2853BED33B92966956CC4839CA99B0730EB764 (void);
// 0x00000004 System.Void Customizer::Start()
extern void Customizer_Start_mBAF79BFBC3149D847D5A018556A0DEB59A9D26D3 (void);
// 0x00000005 System.Void Customizer::Update()
extern void Customizer_Update_mC1BB61D9E8E9693369CDAFF1AC7BDF5DA54E3301 (void);
// 0x00000006 System.Void Customizer::treat()
extern void Customizer_treat_mD09CFB83D5DDEC6CBA79CD36963EC4F5897D60BC (void);
// 0x00000007 UnityEngine.Vector2 Customizer::convertPosition(System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector2)
extern void Customizer_convertPosition_mE828013ED406CB981ABBC26A3869A8DD2B978636 (void);
// 0x00000008 UnityEngine.Color Customizer::toColor(System.String,System.Single)
extern void Customizer_toColor_m69A765CCC1778175A95ACBDF32996892294EDE8F (void);
// 0x00000009 UnityEngine.Sprite Customizer::SpriteFromTexture2D(UnityEngine.Texture2D)
extern void Customizer_SpriteFromTexture2D_m22F3C9A2323B75948C6D2742A32BBC374BD830CE (void);
// 0x0000000A System.Void Customizer::showContent()
extern void Customizer_showContent_m3889AB77995B481136B58C4AD766A2D0B2EBC893 (void);
// 0x0000000B System.Void Customizer::.ctor()
extern void Customizer__ctor_m86CDD261C9ECC286DA11192707346A62360111ED (void);
// 0x0000000C System.Void Customizer::<Start>b__10_0(System.String)
extern void Customizer_U3CStartU3Eb__10_0_mFC7C8E187BD7C36F36DEE117E2391927A0FE8B81 (void);
// 0x0000000D System.Void Customizer::<treat>b__12_0(UnityEngine.Texture2D)
extern void Customizer_U3CtreatU3Eb__12_0_m735B07CBFF747B143A745027920E8072099590F5 (void);
// 0x0000000E System.Void Customizer/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m07C60FA481F681395CDA3C2F7D35D06D811D54CB (void);
// 0x0000000F System.Void Customizer/<>c__DisplayClass12_0::<treat>b__1(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass12_0_U3CtreatU3Eb__1_mDB501A88F27D66E6242B21847356272E0F677CB2 (void);
// 0x00000010 System.Collections.IEnumerator Fetch::downloadText(System.String,System.String,System.Action`1<System.String>)
extern void Fetch_downloadText_mE26F63971A466CC2ACBA2F607A8CE2ABD78D7329 (void);
// 0x00000011 System.Collections.IEnumerator Fetch::downloadGetText(System.String,System.Action`1<System.String>)
extern void Fetch_downloadGetText_m85B48FDD4F8BA4CFE5CB6D7B2EAC94D1081C9224 (void);
// 0x00000012 System.Collections.IEnumerator Fetch::downloadImage(System.String,System.String,System.Action`1<UnityEngine.Texture2D>,System.Action`1<System.String>)
extern void Fetch_downloadImage_m292D9436BBD741ADD1159ACD372308448898E126 (void);
// 0x00000013 System.Void Fetch::saveCashe(UnityEngine.Texture2D,System.String)
extern void Fetch_saveCashe_mDA0B5425E4B9B50CC0A3E6A1BA6E47F779C7A68F (void);
// 0x00000014 System.String Fetch::checkCashe(System.String)
extern void Fetch_checkCashe_m10336D4CD9DD90DAFC89B385CE75071833FEAD63 (void);
// 0x00000015 System.Collections.IEnumerator Fetch::downloadText(System.String,System.String,System.Action`1<System.String>,System.Action`1<System.String>)
extern void Fetch_downloadText_m1D7AEDECA401D81E0FE0B80242A6F11B1244CBC6 (void);
// 0x00000016 System.Void Fetch::.ctor()
extern void Fetch__ctor_m1DDFF51693A8A2AA73E6473720801492F9B21C97 (void);
// 0x00000017 System.Void Fetch/<downloadText>d__0::.ctor(System.Int32)
extern void U3CdownloadTextU3Ed__0__ctor_m8B1F338D399BBDFB1577FE3078058A0C0ECEB996 (void);
// 0x00000018 System.Void Fetch/<downloadText>d__0::System.IDisposable.Dispose()
extern void U3CdownloadTextU3Ed__0_System_IDisposable_Dispose_m023B93775875DDA5FC56B136B53B39BE3476C3E2 (void);
// 0x00000019 System.Boolean Fetch/<downloadText>d__0::MoveNext()
extern void U3CdownloadTextU3Ed__0_MoveNext_mC6FAD378F34B9A13C8DF94E0F54ED5495A397FFA (void);
// 0x0000001A System.Void Fetch/<downloadText>d__0::<>m__Finally1()
extern void U3CdownloadTextU3Ed__0_U3CU3Em__Finally1_m71DB5A3CCC8D9140579DB21B71A789FA0378C8FC (void);
// 0x0000001B System.Object Fetch/<downloadText>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D70CD4D54E4A073567F9044DB5EBA58485BCE69 (void);
// 0x0000001C System.Void Fetch/<downloadText>d__0::System.Collections.IEnumerator.Reset()
extern void U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_Reset_m5C6238E1F272A88B022EF1D5BD33E1A2F4E54E57 (void);
// 0x0000001D System.Object Fetch/<downloadText>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_get_Current_mF8C7FC36607E131889A9A9AED8331CE043370F57 (void);
// 0x0000001E System.Void Fetch/<downloadGetText>d__1::.ctor(System.Int32)
extern void U3CdownloadGetTextU3Ed__1__ctor_m48C2E8D4A6E69416525D1D10ECA4357A1F709158 (void);
// 0x0000001F System.Void Fetch/<downloadGetText>d__1::System.IDisposable.Dispose()
extern void U3CdownloadGetTextU3Ed__1_System_IDisposable_Dispose_mEAD4E07760EC46CD000717CF865BA36785AF7C4B (void);
// 0x00000020 System.Boolean Fetch/<downloadGetText>d__1::MoveNext()
extern void U3CdownloadGetTextU3Ed__1_MoveNext_mB861E290678693DED1E32083A0F02B1587DCCFE8 (void);
// 0x00000021 System.Void Fetch/<downloadGetText>d__1::<>m__Finally1()
extern void U3CdownloadGetTextU3Ed__1_U3CU3Em__Finally1_mFC32DE6F4A3C654D84BC8C98E0696E20200778AF (void);
// 0x00000022 System.Object Fetch/<downloadGetText>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadGetTextU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4723D0018EAA7B6A932E0937FA85661288EA808F (void);
// 0x00000023 System.Void Fetch/<downloadGetText>d__1::System.Collections.IEnumerator.Reset()
extern void U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_Reset_mBFBD2050CD5EBB6324D256BBDC9A43484B739600 (void);
// 0x00000024 System.Object Fetch/<downloadGetText>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_get_Current_m6BC8285F42611EBF06A5F784D34952F7FF2CB13C (void);
// 0x00000025 System.Void Fetch/<downloadImage>d__2::.ctor(System.Int32)
extern void U3CdownloadImageU3Ed__2__ctor_m46565F438AEA7CC91C53F58D968DA3C585F70003 (void);
// 0x00000026 System.Void Fetch/<downloadImage>d__2::System.IDisposable.Dispose()
extern void U3CdownloadImageU3Ed__2_System_IDisposable_Dispose_mE5405D9D37A7D2F1D164A3B8C86D3284E4E91B16 (void);
// 0x00000027 System.Boolean Fetch/<downloadImage>d__2::MoveNext()
extern void U3CdownloadImageU3Ed__2_MoveNext_mAE4AB7C9532EFFED9DF3B2AF172856E823A86571 (void);
// 0x00000028 System.Void Fetch/<downloadImage>d__2::<>m__Finally1()
extern void U3CdownloadImageU3Ed__2_U3CU3Em__Finally1_m06CE2DED9FFD0B37C5DC83DBB8340ABF579F5C18 (void);
// 0x00000029 System.Object Fetch/<downloadImage>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadImageU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1E5DE562F1972533B57AD4C08E9003DF3FB8E44 (void);
// 0x0000002A System.Void Fetch/<downloadImage>d__2::System.Collections.IEnumerator.Reset()
extern void U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_Reset_m6AEBE2D189CBBD69C57BB6B363D4B1CBFE11EF06 (void);
// 0x0000002B System.Object Fetch/<downloadImage>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_get_Current_m44621CCBB0A8DF7A54D323974919F4424397F182 (void);
// 0x0000002C System.Void Fetch/<downloadText>d__6::.ctor(System.Int32)
extern void U3CdownloadTextU3Ed__6__ctor_m736D4A1F1D30BB619CC8AF938713E9B93B0C75F8 (void);
// 0x0000002D System.Void Fetch/<downloadText>d__6::System.IDisposable.Dispose()
extern void U3CdownloadTextU3Ed__6_System_IDisposable_Dispose_m0C927DFD368CFE1E6F31DDF44039D8BD71B8229F (void);
// 0x0000002E System.Boolean Fetch/<downloadText>d__6::MoveNext()
extern void U3CdownloadTextU3Ed__6_MoveNext_mBAC7F8ADC7A3CAEFE82E8F377FB79D36C4245229 (void);
// 0x0000002F System.Void Fetch/<downloadText>d__6::<>m__Finally1()
extern void U3CdownloadTextU3Ed__6_U3CU3Em__Finally1_m1D788943C3D14002BF5A937AFC798DE27C96D9BB (void);
// 0x00000030 System.Object Fetch/<downloadText>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadTextU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m597DBB3BF363A420B77DB60D2B4D574445409836 (void);
// 0x00000031 System.Void Fetch/<downloadText>d__6::System.Collections.IEnumerator.Reset()
extern void U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_Reset_m0751DCDC13928AA5D907AC7B36516C65D707DF14 (void);
// 0x00000032 System.Object Fetch/<downloadText>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_get_Current_m4DF213FF52A5A077F31FF7451F20153D024682C5 (void);
// 0x00000033 System.Void Asset::.ctor()
extern void Asset__ctor_m5E0FACFA7CAE61B737CB9E6EFB0B08DFD33F8D99 (void);
// 0x00000034 System.Void Shape::.ctor()
extern void Shape__ctor_mBB98685051DAF006098FABCC1A2F5A3797DC1852 (void);
// 0x00000035 System.Void Page::.ctor()
extern void Page__ctor_mC8763F85034F7230967ACCDDFD98204CD50A3451 (void);
// 0x00000036 System.Void Project::.ctor()
extern void Project__ctor_m0444A6D59081677990AA152E3C8C2E87349FF3A6 (void);
// 0x00000037 System.Void CustomizationResponse::.ctor()
extern void CustomizationResponse__ctor_mD43C4A534DFCE2BFD84A3E748C2209188B46F224 (void);
// 0x00000038 System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m0AAD92A3D682C1DFC1B5630585550B6CAFBAFE32 (void);
// 0x00000039 System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m4761377095817FA81908BC43FD08EF600F6CC231 (void);
// 0x0000003A System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_m7FD9AC4C30D2F12A0A30CC3CAD9EC6C17B07C45D (void);
// 0x0000003B System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_mEE0AFFC028F876DE841312FEB8CA15FEAD105FE1 (void);
// 0x0000003C System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m51213AC9D2DBD97C93CC41330F65D74661D5DC81 (void);
// 0x0000003D System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m354D58D0B29B40FE6D3A6689590E271D9282323F (void);
// 0x0000003E System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m241EDA47E1C4DECA8CCBC61D8DF6EC300546CDF0 (void);
// 0x0000003F System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_mA27A238E88D5FBA54D4DDFE588080DD41B449A4C (void);
// 0x00000040 System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mF5C9F857AC060614B46B9359B3F0859FBC309DF3 (void);
// 0x00000041 System.Void BigFileTransfer::Awake()
extern void BigFileTransfer_Awake_m3DEF7001822DA6F451CE793EB0A096679E8EB156 (void);
// 0x00000042 System.Void BigFileTransfer::Update()
extern void BigFileTransfer_Update_mFD49BFED4143A3914104F9CB4F9F041B9EF90F9D (void);
// 0x00000043 System.Void BigFileTransfer::WriteAllBytesAsync(System.Byte[],System.String,BigFileTransfer/OnWriteComplete)
extern void BigFileTransfer_WriteAllBytesAsync_m203535D4F5FFD755FC3A766D57A86FCEA792BD61 (void);
// 0x00000044 System.Collections.IEnumerator BigFileTransfer::NowCheckForResourceUnload(System.String)
extern void BigFileTransfer_NowCheckForResourceUnload_m5EB71F677981F73F4F3775E68423D6059B0AF4FB (void);
// 0x00000045 System.Void BigFileTransfer::SaveDataThreaded(System.String)
extern void BigFileTransfer_SaveDataThreaded_mA5C5FDB58E4A1F7A2AB18E5EEE93BBC1C726DD70 (void);
// 0x00000046 System.Void BigFileTransfer::SaveDataTaskThreaded(System.String)
extern void BigFileTransfer_SaveDataTaskThreaded_mE6BBB22CAF0CB940232EFA25C06BA14CF02362FA (void);
// 0x00000047 System.Void BigFileTransfer::CallCB(System.String,System.Boolean)
extern void BigFileTransfer_CallCB_m95B9E0802B30608309E607A043BD72E63368DB71 (void);
// 0x00000048 System.Void BigFileTransfer::.ctor()
extern void BigFileTransfer__ctor_m15D9B6FA79B456BA0F46ED784A1C8AA6CAF12042 (void);
// 0x00000049 System.Void BigFileTransfer/OnWriteComplete::.ctor(System.Object,System.IntPtr)
extern void OnWriteComplete__ctor_m5E4DDE4A6D08C1ECB6F6100EC1FAB7FD6C7F9ED2 (void);
// 0x0000004A System.Void BigFileTransfer/OnWriteComplete::Invoke(System.Boolean)
extern void OnWriteComplete_Invoke_mB117DE7BD5013CF9D387CA6B0412B9561DEB16BB (void);
// 0x0000004B System.IAsyncResult BigFileTransfer/OnWriteComplete::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void OnWriteComplete_BeginInvoke_m90B2DA8CA82E504522789525ACC015BA48DD7DA4 (void);
// 0x0000004C System.Void BigFileTransfer/OnWriteComplete::EndInvoke(System.IAsyncResult)
extern void OnWriteComplete_EndInvoke_m6CD14295AE8ADDDD20D989E5397C6352509A97E4 (void);
// 0x0000004D System.Void BigFileTransfer/<NowCheckForResourceUnload>d__5::.ctor(System.Int32)
extern void U3CNowCheckForResourceUnloadU3Ed__5__ctor_m4810B14070C5216BCA0F1F217A7B3653BC66F5B1 (void);
// 0x0000004E System.Void BigFileTransfer/<NowCheckForResourceUnload>d__5::System.IDisposable.Dispose()
extern void U3CNowCheckForResourceUnloadU3Ed__5_System_IDisposable_Dispose_mDA95B5C816AEC937D165CF6D263C7085959E6A2F (void);
// 0x0000004F System.Boolean BigFileTransfer/<NowCheckForResourceUnload>d__5::MoveNext()
extern void U3CNowCheckForResourceUnloadU3Ed__5_MoveNext_mE46648EBD7BCC6DA137B5AAE98433A652E2A8FB8 (void);
// 0x00000050 System.Object BigFileTransfer/<NowCheckForResourceUnload>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD8597AAA4D7CC5CCC1EA88DCA8417825EAE7C2F (void);
// 0x00000051 System.Void BigFileTransfer/<NowCheckForResourceUnload>d__5::System.Collections.IEnumerator.Reset()
extern void U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_Reset_m61B8907EC6A8728FEB290A78E71268521E2E7EAB (void);
// 0x00000052 System.Object BigFileTransfer/<NowCheckForResourceUnload>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_get_Current_m47DEB1A7423358FEA2F80C95D805C376FB593CC6 (void);
// 0x00000053 System.Void BigFileTransfer/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m587A46B8D133929D37561EAA49B7D464BB4CA5B1 (void);
// 0x00000054 System.Void BigFileTransfer/<>c__DisplayClass10_0::<SaveDataThreaded>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CSaveDataThreadedU3Eb__0_mAA7E7571A76648C83BA1CCAF074FDDB008C728B3 (void);
// 0x00000055 System.Void En::.ctor()
extern void En__ctor_m9972465DE60A76F3037CB864D80A8CF56D200757 (void);
// 0x00000056 System.Void En::.cctor()
extern void En__cctor_mEBCDF74FF01CC362892A9345712C4E06D6C16C6F (void);
// 0x00000057 System.String ExtensionMethodes::RemoveQuotes(System.String)
extern void ExtensionMethodes_RemoveQuotes_m9A717CF268B56CC0E5DE3D67272B856909B8E166 (void);
// 0x00000058 System.String ExtensionMethodes::ToJSON(System.String)
extern void ExtensionMethodes_ToJSON_mCA4FCBE50520BBEE0533C4BB26710BAC73021462 (void);
// 0x00000059 System.Void ExtensionMethodes::Invoke(UnityEngine.MonoBehaviour,System.Action,System.Single)
extern void ExtensionMethodes_Invoke_m0A97AA69FF14FC349F826EA8CB5E22F7E958A50D (void);
// 0x0000005A System.Collections.IEnumerator ExtensionMethodes::InvokeRoutine(System.Action,System.Single)
extern void ExtensionMethodes_InvokeRoutine_m68D13618A04F1101FC57EB9B7F6A6240EF47E027 (void);
// 0x0000005B System.Void ExtensionMethodes::fetchText(UnityEngine.MonoBehaviour,System.String,System.String,System.Action`1<System.String>)
extern void ExtensionMethodes_fetchText_m5BC095AD33A5648A3A1F5A8DCC2966C70EC96D45 (void);
// 0x0000005C System.Void ExtensionMethodes::getText(UnityEngine.MonoBehaviour,System.String,System.Action`1<System.String>)
extern void ExtensionMethodes_getText_m1C9828B2FB479308B4245F59087EAF0C4232B7C2 (void);
// 0x0000005D System.Void ExtensionMethodes::fetchText(UnityEngine.MonoBehaviour,System.String,System.String,System.Action`1<System.String>,System.Action`1<System.String>)
extern void ExtensionMethodes_fetchText_m41CC23C150225080757EB0305E55CC9F747D79C3 (void);
// 0x0000005E System.Void ExtensionMethodes::fetchImage(UnityEngine.MonoBehaviour,System.String,System.Action`1<UnityEngine.Texture2D>,System.Action`1<System.String>)
extern void ExtensionMethodes_fetchImage_mFFAD95B615A556F8FAA456813FF7C0F4A4ECE4EC (void);
// 0x0000005F System.Void ExtensionMethodes::fit(UnityEngine.UI.RawImage,System.Int32,System.Int32)
extern void ExtensionMethodes_fit_m6CE160463F7C2D2A0E3CE704DB5D2A45E48768F9 (void);
// 0x00000060 System.Void ExtensionMethodes::fitIn(UnityEngine.UI.RawImage,System.Int32,System.Int32)
extern void ExtensionMethodes_fitIn_m098C6C9472334448B87968472E68FC0E7EB7A7F7 (void);
// 0x00000061 System.Void ExtensionMethodes/<InvokeRoutine>d__3::.ctor(System.Int32)
extern void U3CInvokeRoutineU3Ed__3__ctor_mDE74617B2BB991AC518479ADBD4FC449F2CF8713 (void);
// 0x00000062 System.Void ExtensionMethodes/<InvokeRoutine>d__3::System.IDisposable.Dispose()
extern void U3CInvokeRoutineU3Ed__3_System_IDisposable_Dispose_m6ADC93DE5909213D7180E3892F23EEF1391A085D (void);
// 0x00000063 System.Boolean ExtensionMethodes/<InvokeRoutine>d__3::MoveNext()
extern void U3CInvokeRoutineU3Ed__3_MoveNext_m4EAF175A2FC58F5D2337F27CB612175920E443CC (void);
// 0x00000064 System.Object ExtensionMethodes/<InvokeRoutine>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInvokeRoutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DB589A82DEB3980CD467F5210816729B6332E2E (void);
// 0x00000065 System.Void ExtensionMethodes/<InvokeRoutine>d__3::System.Collections.IEnumerator.Reset()
extern void U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_Reset_m1054613E0F54ADCCA9F9B985EEF2D99D23816070 (void);
// 0x00000066 System.Object ExtensionMethodes/<InvokeRoutine>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_get_Current_mCD45E7F0EF779A679CC0267BB47747283483AD63 (void);
// 0x00000067 System.Void Fr::.ctor()
extern void Fr__ctor_m67A01EA3CFCB8910B267D09BEBC46D9B60A15014 (void);
// 0x00000068 System.Void Fr::.cctor()
extern void Fr__cctor_mE12E837B374D924116AA5E5C6A22E0A6855A0E54 (void);
// 0x00000069 System.String Lang::choose(System.String,System.String)
extern void Lang_choose_m763C64A4E4D7FD22E9FAD0E164FABA74E659DF16 (void);
// 0x0000006A System.Void Lang::.ctor()
extern void Lang__ctor_mCE2F385716D35E9A682DCF321C127924677B99DC (void);
// 0x0000006B System.Collections.IEnumerator LeaderBoard::Start()
extern void LeaderBoard_Start_m662CE1902678089ED1725BD705C2D55569FA2717 (void);
// 0x0000006C System.Void LeaderBoard::.ctor()
extern void LeaderBoard__ctor_mFA9C06024BBB4E24403E2AD790398E1CCF52C94A (void);
// 0x0000006D System.Void LeaderBoard/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mF8C0169B77CF687227A3B20DA547A55DB023A5BA (void);
// 0x0000006E System.Void LeaderBoard/<>c__DisplayClass1_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m2390D2ABE3F0E1F7127E5A02CF1B995C743EE6EE (void);
// 0x0000006F System.Void LeaderBoard/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m3678519FEB2A830B52694CC1A8F920BC498ACD37 (void);
// 0x00000070 System.Void LeaderBoard/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_mFAE344D106FD947608664C6EAA75C92211E6D1F8 (void);
// 0x00000071 System.Boolean LeaderBoard/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m91C31E301F8AA9095C0F27F6BE0B6EFA5C39E93A (void);
// 0x00000072 System.Void LeaderBoard/<Start>d__1::<>m__Finally1()
extern void U3CStartU3Ed__1_U3CU3Em__Finally1_m2155FD071FECB130DF888301994791996BA97CCA (void);
// 0x00000073 System.Object LeaderBoard/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B7D1CA1CFA697529F4FA22E71C7E4B74771A002 (void);
// 0x00000074 System.Void LeaderBoard/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m6C97C235FF601436BEEE231C487C46F7E51D8E1F (void);
// 0x00000075 System.Object LeaderBoard/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mAA56FDC02462F6064F0966239B3399EC2E3FE7CD (void);
// 0x00000076 UnityEngine.GameObject PlaceMultipleObjectsOnPlane::get_placedPrefab()
extern void PlaceMultipleObjectsOnPlane_get_placedPrefab_mF30814062D1103F3CBF52DBE4EF5B8699F471AAD (void);
// 0x00000077 System.Void PlaceMultipleObjectsOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void PlaceMultipleObjectsOnPlane_set_placedPrefab_m99A4FD1A33F645E357B0A062A338BB67E7B5ED3C (void);
// 0x00000078 UnityEngine.GameObject PlaceMultipleObjectsOnPlane::get_spawnedObject()
extern void PlaceMultipleObjectsOnPlane_get_spawnedObject_m6E6DC83FA54E6A49680A69B3D087119A2F6010CF (void);
// 0x00000079 System.Void PlaceMultipleObjectsOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void PlaceMultipleObjectsOnPlane_set_spawnedObject_m101BBFC64960985CF1A5BC47C788741012A02728 (void);
// 0x0000007A System.Void PlaceMultipleObjectsOnPlane::add_onPlacedObject(System.Action)
extern void PlaceMultipleObjectsOnPlane_add_onPlacedObject_m03FFE730BC4665310982D88817DD2F01CAFE4DB8 (void);
// 0x0000007B System.Void PlaceMultipleObjectsOnPlane::remove_onPlacedObject(System.Action)
extern void PlaceMultipleObjectsOnPlane_remove_onPlacedObject_m903CF85547E4B7FA691C36447C003F5FC41A54A5 (void);
// 0x0000007C System.Void PlaceMultipleObjectsOnPlane::Awake()
extern void PlaceMultipleObjectsOnPlane_Awake_m7B98B03191D41539EC8B524F5CC746A069380F72 (void);
// 0x0000007D System.Void PlaceMultipleObjectsOnPlane::Update()
extern void PlaceMultipleObjectsOnPlane_Update_m1D53B5D4D0F9858D32E97342EF45397FEE6B0BED (void);
// 0x0000007E System.Void PlaceMultipleObjectsOnPlane::.ctor()
extern void PlaceMultipleObjectsOnPlane__ctor_m4593FA1117FA14F18171B62A47CB4B5E35CCF981 (void);
// 0x0000007F System.Void PlaceMultipleObjectsOnPlane::.cctor()
extern void PlaceMultipleObjectsOnPlane__cctor_mFF9A32602D844813D277B71234AD5A1038E15BF7 (void);
// 0x00000080 UnityEngine.GameObject PlaceOnPlane::get_placedPrefab()
extern void PlaceOnPlane_get_placedPrefab_mFF491A996FB1370DF94BAA02291EF06ADD19BEBC (void);
// 0x00000081 System.Void PlaceOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void PlaceOnPlane_set_placedPrefab_m36395BA83344E138FE2CE24CE0B213AE83B4A181 (void);
// 0x00000082 UnityEngine.GameObject PlaceOnPlane::get_spawnedObject()
extern void PlaceOnPlane_get_spawnedObject_m3EEB260F2583438D88633A58DB2A0E7C9FC9853E (void);
// 0x00000083 System.Void PlaceOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void PlaceOnPlane_set_spawnedObject_m2FCF31C894AF24CB5E36FA21620911FC41270E7D (void);
// 0x00000084 System.Void PlaceOnPlane::Awake()
extern void PlaceOnPlane_Awake_m07F667A8D11C4C52B0D6E80EC6BFBD3A6DCC9F4C (void);
// 0x00000085 System.Boolean PlaceOnPlane::TryGetTouchPosition(UnityEngine.Vector2&)
extern void PlaceOnPlane_TryGetTouchPosition_m3A154C1C41BB6BDED6BDDAC542AD3B1B0C06E033 (void);
// 0x00000086 System.Void PlaceOnPlane::Update()
extern void PlaceOnPlane_Update_mB0B5CF33955B4E0C22674FEC89BDFAFF9031D878 (void);
// 0x00000087 System.Void PlaceOnPlane::.ctor()
extern void PlaceOnPlane__ctor_m6A163443FEBA846E4B12847076EC8482911BDAFC (void);
// 0x00000088 System.Void PlaceOnPlane::.cctor()
extern void PlaceOnPlane__cctor_m179DE5DFC70769680B7A879566D59E759EBB62AA (void);
// 0x00000089 UnityEngine.UI.Text PlaneDetectionController::get_togglePlaneDetectionText()
extern void PlaneDetectionController_get_togglePlaneDetectionText_m653DCAF71188ED390D7C3262A340264851058DF3 (void);
// 0x0000008A System.Void PlaneDetectionController::set_togglePlaneDetectionText(UnityEngine.UI.Text)
extern void PlaneDetectionController_set_togglePlaneDetectionText_mD749209D5E6473CAE65DAE42CF74390DA8614B1F (void);
// 0x0000008B System.Void PlaneDetectionController::TogglePlaneDetection()
extern void PlaneDetectionController_TogglePlaneDetection_m5F3F97C553B68B64F7FE08CFECCD7260B3B72843 (void);
// 0x0000008C System.Void PlaneDetectionController::SetAllPlanesActive(System.Boolean)
extern void PlaneDetectionController_SetAllPlanesActive_m8C0CCC2235D2F7B0EE5C6DA4B7C3BE69F5321B25 (void);
// 0x0000008D System.Void PlaneDetectionController::Awake()
extern void PlaneDetectionController_Awake_m5D161C3A0ED5E8FF1035B1BB3F69FF1D767FDA9A (void);
// 0x0000008E System.Void PlaneDetectionController::.ctor()
extern void PlaneDetectionController__ctor_m33E62EAD0CA6CB93196A504646793B990311FC9F (void);
// 0x0000008F System.Void Singleton`1::Awake()
// 0x00000090 System.Void Singleton`1::.ctor()
// 0x00000091 System.Void FadePlaneOnBoundaryChange::OnEnable()
extern void FadePlaneOnBoundaryChange_OnEnable_m9A290421CF90C45F885B14F3E1DA888BFD27A340 (void);
// 0x00000092 System.Void FadePlaneOnBoundaryChange::OnDisable()
extern void FadePlaneOnBoundaryChange_OnDisable_m0C62572D4B470C313E0B203F6AE948865F395825 (void);
// 0x00000093 System.Void FadePlaneOnBoundaryChange::Update()
extern void FadePlaneOnBoundaryChange_Update_mBB620E21C1BE2FE4EF8F45D1C14EBE6C5579F8A8 (void);
// 0x00000094 System.Void FadePlaneOnBoundaryChange::PlaneOnBoundaryChanged(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void FadePlaneOnBoundaryChange_PlaneOnBoundaryChanged_mF362091F455EDBF9437C0B672D7CE83ED4CD20CC (void);
// 0x00000095 System.Void FadePlaneOnBoundaryChange::.ctor()
extern void FadePlaneOnBoundaryChange__ctor_m6625F1DF41CF81E94037383DFBEDE2FB0237096E (void);
// 0x00000096 UnityEngine.XR.ARFoundation.ARCameraManager UIManager::get_cameraManager()
extern void UIManager_get_cameraManager_mCBC76B9FAD2889BDF675423F41E0D8F45A4D9B9B (void);
// 0x00000097 System.Void UIManager::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void UIManager_set_cameraManager_mE84105B8732CF3F5AF6A7D0D431E071026EC8B40 (void);
// 0x00000098 UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::get_planeManager()
extern void UIManager_get_planeManager_mC295F7015EC0AFA6BF017218A3D50FBB5F8EC93B (void);
// 0x00000099 System.Void UIManager::set_planeManager(UnityEngine.XR.ARFoundation.ARPlaneManager)
extern void UIManager_set_planeManager_m00D2D98C5B74891D8CE5F847317C28A7B4A992B5 (void);
// 0x0000009A UnityEngine.Animator UIManager::get_moveDeviceAnimation()
extern void UIManager_get_moveDeviceAnimation_mCEBB895CDD2C29B0E5E876812574CB239095AC86 (void);
// 0x0000009B System.Void UIManager::set_moveDeviceAnimation(UnityEngine.Animator)
extern void UIManager_set_moveDeviceAnimation_mEF7CB4D795D5D4135F27BE428D348BAF300D844B (void);
// 0x0000009C UnityEngine.Animator UIManager::get_tapToPlaceAnimation()
extern void UIManager_get_tapToPlaceAnimation_m0285215E48E9D5A0E45DB14351D359B849BA1BBB (void);
// 0x0000009D System.Void UIManager::set_tapToPlaceAnimation(UnityEngine.Animator)
extern void UIManager_set_tapToPlaceAnimation_m626FD1527F2416C37546E95C2974B0F567CFC74F (void);
// 0x0000009E System.Void UIManager::OnEnable()
extern void UIManager_OnEnable_mAFE10D24F19B379487455D635E4A3C65D4B64240 (void);
// 0x0000009F System.Void UIManager::OnDisable()
extern void UIManager_OnDisable_mBAED7246344396BA861ADE4603C663A9ECFBCDEF (void);
// 0x000000A0 System.Void UIManager::FrameChanged(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void UIManager_FrameChanged_mF4EDFEC89AA3118757706CC79E22A42ABE245030 (void);
// 0x000000A1 System.Boolean UIManager::PlanesFound()
extern void UIManager_PlanesFound_m4815E9F0871E527DBBCD7A190B7727D65D482622 (void);
// 0x000000A2 System.Void UIManager::PlacedObject()
extern void UIManager_PlacedObject_m984A9FFFEE0AD12C887C1F0C67B8C259A1888E40 (void);
// 0x000000A3 System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x000000A4 System.Void UIManager::.cctor()
extern void UIManager__cctor_mEEEBEC7EE33138DE838943638C73F7592DDBE3B8 (void);
// 0x000000A5 UnityEngine.Texture2D cashing::LoadJPG(System.String)
extern void cashing_LoadJPG_m297FF062BE657C8A5C24E746346B7C6AE8216B87 (void);
// 0x000000A6 System.Collections.IEnumerator cashing::LoadAssetBundle(System.String,System.Int32,System.Int32,UnityEngine.UI.Slider)
extern void cashing_LoadAssetBundle_m2A8F39B091A6E2678A513F029728116BAD6B321A (void);
// 0x000000A7 System.Collections.IEnumerator cashing::load360InGameMode(System.Int32,System.Int32)
extern void cashing_load360InGameMode_mAC10C32153021F0DE28F29DAD37800D5DEC44469 (void);
// 0x000000A8 gMJson cashing::loadGameModeJson()
extern void cashing_loadGameModeJson_mD2C7A0F9A806D7D5E944D49E0CB9A0E8356084A7 (void);
// 0x000000A9 System.Collections.IEnumerator cashing::getAllTps(System.Collections.Generic.List`1<mainJson>)
extern void cashing_getAllTps_m63B27A953D9BBF10760CE224ED695E490CFD1050 (void);
// 0x000000AA System.Collections.IEnumerator cashing::download360(System.String,System.Int32,System.Int32,UnityEngine.UI.Slider)
extern void cashing_download360_m0A0CEDC96DF65F9AC90CADE6C1BAC75D9D196E87 (void);
// 0x000000AB System.Collections.IEnumerator cashing::downloadForGameMode(System.String,System.Int32,System.Int32,System.Int32,UnityEngine.UI.Slider)
extern void cashing_downloadForGameMode_mA8EF232F20FF4C0B9DE8F25580DC2185B7F6494A (void);
// 0x000000AC System.Collections.IEnumerator cashing::download360ForGameMode(System.String,System.Int32,System.Int32,System.Int32,UnityEngine.UI.Slider)
extern void cashing_download360ForGameMode_m2D430260CD0B1CE0E80849BDB2C623BDD57963F0 (void);
// 0x000000AD System.Collections.IEnumerator cashing::download(System.String,System.Int32,System.Int32,System.Boolean)
extern void cashing_download_mE46059585947898E3F841CBF2E20DCF0CBCB3F42 (void);
// 0x000000AE System.Collections.IEnumerator cashing::getPortalsJson()
extern void cashing_getPortalsJson_m36A6E9ECB959C23FE3F2AC3BDC4ED6738B842700 (void);
// 0x000000AF System.Collections.IEnumerator cashing::getPortal(System.Int32)
extern void cashing_getPortal_m44629A7892D19B1D30F933394D2E9C82A23211F9 (void);
// 0x000000B0 System.Collections.IEnumerator cashing::getGameModeJson()
extern void cashing_getGameModeJson_m6E1164B53A6B983E1CB7A91FB0254D141C878B68 (void);
// 0x000000B1 System.Void cashing::SaveData(System.String,System.String)
extern void cashing_SaveData_m083A5C4629AEF8AE098715AD7B5CA2918520E0E7 (void);
// 0x000000B2 System.String cashing::LoadData(System.String)
extern void cashing_LoadData_m3ED87876A5C955FC3B4201CDCA2C94FE2E66430C (void);
// 0x000000B3 System.Void cashing::.ctor()
extern void cashing__ctor_m9EBACAE916558B194318B78ADFA03C0799FBB883 (void);
// 0x000000B4 System.Void cashing::.cctor()
extern void cashing__cctor_m629F36D102D8D6255F6560871D36146ABF44BBBC (void);
// 0x000000B5 System.Void cashing/<LoadAssetBundle>d__11::.ctor(System.Int32)
extern void U3CLoadAssetBundleU3Ed__11__ctor_m27DB2E4150ED40D54B6D7AC09A233248B2892300 (void);
// 0x000000B6 System.Void cashing/<LoadAssetBundle>d__11::System.IDisposable.Dispose()
extern void U3CLoadAssetBundleU3Ed__11_System_IDisposable_Dispose_m757415C4F7CCEF82DC8DD2BB460FFD3483BCF4A4 (void);
// 0x000000B7 System.Boolean cashing/<LoadAssetBundle>d__11::MoveNext()
extern void U3CLoadAssetBundleU3Ed__11_MoveNext_m24B35E51DAF44C74FAC89B511ECF521452E36908 (void);
// 0x000000B8 System.Object cashing/<LoadAssetBundle>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75089F44DE96500C0BF6967D14008FBF15EDDD24 (void);
// 0x000000B9 System.Void cashing/<LoadAssetBundle>d__11::System.Collections.IEnumerator.Reset()
extern void U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mCC75770FBA53EB5CFA7D41DDD37C694BC01FBD84 (void);
// 0x000000BA System.Object cashing/<LoadAssetBundle>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m6C541003B1786239D2669D83C3568C39A97D13DB (void);
// 0x000000BB System.Void cashing/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mB1D9B95DAFB0862B3CB47DCB4E30FAE50A8F3A25 (void);
// 0x000000BC System.Void cashing/<>c__DisplayClass12_0::<load360InGameMode>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass12_0_U3Cload360InGameModeU3Eb__0_m8FFC2C7B3A8981390314DA720A2F59CF0E40091B (void);
// 0x000000BD System.Void cashing/<load360InGameMode>d__12::.ctor(System.Int32)
extern void U3Cload360InGameModeU3Ed__12__ctor_mE83D806E0EE2815CAB01D8B62CF1E7FBB6A2BC36 (void);
// 0x000000BE System.Void cashing/<load360InGameMode>d__12::System.IDisposable.Dispose()
extern void U3Cload360InGameModeU3Ed__12_System_IDisposable_Dispose_mD73B9A03551E2E0B1DDE124768B4637E36901413 (void);
// 0x000000BF System.Boolean cashing/<load360InGameMode>d__12::MoveNext()
extern void U3Cload360InGameModeU3Ed__12_MoveNext_mFE86DE9D3623D814EE58EE41E953A01AA4F42693 (void);
// 0x000000C0 System.Void cashing/<load360InGameMode>d__12::<>m__Finally1()
extern void U3Cload360InGameModeU3Ed__12_U3CU3Em__Finally1_m8E6DF178B666E4C984D268AA7D498E7EF9CB0BDB (void);
// 0x000000C1 System.Object cashing/<load360InGameMode>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cload360InGameModeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFE70ED4FAF646F8C999092EBA78F42076C88CEC (void);
// 0x000000C2 System.Void cashing/<load360InGameMode>d__12::System.Collections.IEnumerator.Reset()
extern void U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_Reset_m6108BAF5C6A492E6A5B33A9FE215256428726EFA (void);
// 0x000000C3 System.Object cashing/<load360InGameMode>d__12::System.Collections.IEnumerator.get_Current()
extern void U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_get_Current_mCFAF5DAA51544C6913BA7C2CBB0CB047B5635D45 (void);
// 0x000000C4 System.Void cashing/<getAllTps>d__14::.ctor(System.Int32)
extern void U3CgetAllTpsU3Ed__14__ctor_m51508F758A4EE470759F88A90E33C10CA651C4CA (void);
// 0x000000C5 System.Void cashing/<getAllTps>d__14::System.IDisposable.Dispose()
extern void U3CgetAllTpsU3Ed__14_System_IDisposable_Dispose_m59A4443BEAFEC7E72871F04F73ABE41CBB20953D (void);
// 0x000000C6 System.Boolean cashing/<getAllTps>d__14::MoveNext()
extern void U3CgetAllTpsU3Ed__14_MoveNext_mCF04E6F15D13548DD5852D393EF4E0D2DACE08D5 (void);
// 0x000000C7 System.Void cashing/<getAllTps>d__14::<>m__Finally1()
extern void U3CgetAllTpsU3Ed__14_U3CU3Em__Finally1_mCBAE1809D44A070A97850CD013AEC896D56811C4 (void);
// 0x000000C8 System.Void cashing/<getAllTps>d__14::<>m__Finally2()
extern void U3CgetAllTpsU3Ed__14_U3CU3Em__Finally2_m7A6D54C36292BF9E4CBDCC0426553DC0EBFBCC4C (void);
// 0x000000C9 System.Object cashing/<getAllTps>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgetAllTpsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3280A4DC4A2D656206EF0199368AA999FC3CA53D (void);
// 0x000000CA System.Void cashing/<getAllTps>d__14::System.Collections.IEnumerator.Reset()
extern void U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_Reset_mC2E810E082B85571FA8F14A2EE0F9BEAF1020D10 (void);
// 0x000000CB System.Object cashing/<getAllTps>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_get_Current_m94A816C63B45183CBAE8C2BFCB9A4AB1F458BAF1 (void);
// 0x000000CC System.Void cashing/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m5EA7980BA558D9C99CB313C535760DE5FDC715ED (void);
// 0x000000CD System.Void cashing/<>c__DisplayClass15_0::<download360>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3Cdownload360U3Eb__0_m7DE33E3A8DE91D784B01BFB85FF851A4F87C2D05 (void);
// 0x000000CE System.Void cashing/<>c__DisplayClass15_1::.ctor()
extern void U3CU3Ec__DisplayClass15_1__ctor_m748BD68187FB39CECAC059F24E8901004D66F369 (void);
// 0x000000CF System.Void cashing/<>c__DisplayClass15_1::<download360>b__1()
extern void U3CU3Ec__DisplayClass15_1_U3Cdownload360U3Eb__1_m92CBF2C7FA68346AABDC480D36BF3AA35EED0916 (void);
// 0x000000D0 System.Void cashing/<>c__DisplayClass15_2::.ctor()
extern void U3CU3Ec__DisplayClass15_2__ctor_mF6B970CAFFCFEC2E9762959C1E8F9111F067376F (void);
// 0x000000D1 System.Void cashing/<>c__DisplayClass15_2::<download360>b__2(System.Boolean)
extern void U3CU3Ec__DisplayClass15_2_U3Cdownload360U3Eb__2_m9408AF4ACFC8BC480EA105807539C5F14321F9FF (void);
// 0x000000D2 System.Void cashing/<>c::.cctor()
extern void U3CU3Ec__cctor_mDE213E0F57BF186318E686CC6AD90971C61BA152 (void);
// 0x000000D3 System.Void cashing/<>c::.ctor()
extern void U3CU3Ec__ctor_m369369D83D1D26E1E208A9FB0EC00580E435E7FE (void);
// 0x000000D4 System.Void cashing/<>c::<download360>b__15_3()
extern void U3CU3Ec_U3Cdownload360U3Eb__15_3_mDE5B4F54FC6BBA343173E62AE3807261B7426643 (void);
// 0x000000D5 System.Void cashing/<>c::<downloadForGameMode>b__16_3()
extern void U3CU3Ec_U3CdownloadForGameModeU3Eb__16_3_m4E6A64CAD71D7F5DFAF96B9BCC473056D16E04E4 (void);
// 0x000000D6 System.Void cashing/<>c::<download360ForGameMode>b__17_3()
extern void U3CU3Ec_U3Cdownload360ForGameModeU3Eb__17_3_m1E675618580E5AB27184D4F3D112E270B6E7E8B6 (void);
// 0x000000D7 System.Void cashing/<>c::<download>b__18_2()
extern void U3CU3Ec_U3CdownloadU3Eb__18_2_mD5111E100BDB131D54D541FF1662BE14661CBCC1 (void);
// 0x000000D8 System.Void cashing/<>c::<getPortalsJson>b__19_2()
extern void U3CU3Ec_U3CgetPortalsJsonU3Eb__19_2_m5F32E167D6F570CAF2A4DFC9F399A7061945AEAE (void);
// 0x000000D9 System.Void cashing/<>c::<getPortalsJson>b__19_3()
extern void U3CU3Ec_U3CgetPortalsJsonU3Eb__19_3_m3505633961C18A45E3A269E70D7924B741687692 (void);
// 0x000000DA System.Void cashing/<>c::<getPortal>b__20_4()
extern void U3CU3Ec_U3CgetPortalU3Eb__20_4_mD816592E34CE805B6C7A8FB7BE7F0709245277E7 (void);
// 0x000000DB System.Void cashing/<>c::<getGameModeJson>b__21_2()
extern void U3CU3Ec_U3CgetGameModeJsonU3Eb__21_2_mC4A052FF6FEE61D7087E4EE081E105FFD6786A16 (void);
// 0x000000DC System.Void cashing/<download360>d__15::.ctor(System.Int32)
extern void U3Cdownload360U3Ed__15__ctor_mFB7F1D6BC6F63CC8129E1EDD9F042D86A2C247A4 (void);
// 0x000000DD System.Void cashing/<download360>d__15::System.IDisposable.Dispose()
extern void U3Cdownload360U3Ed__15_System_IDisposable_Dispose_m33E0DEAF9648839BD56911E0484ACFB603D4B02E (void);
// 0x000000DE System.Boolean cashing/<download360>d__15::MoveNext()
extern void U3Cdownload360U3Ed__15_MoveNext_mF86203CE112F823939E80CCB5918A46FCF72BCFE (void);
// 0x000000DF System.Void cashing/<download360>d__15::<>m__Finally1()
extern void U3Cdownload360U3Ed__15_U3CU3Em__Finally1_mECB1020734D77DEDE5BDAFE9D3728897A7B90745 (void);
// 0x000000E0 System.Void cashing/<download360>d__15::<>m__Finally2()
extern void U3Cdownload360U3Ed__15_U3CU3Em__Finally2_mE8ADDE0C284ABC444119E8C8499E76E3AB621950 (void);
// 0x000000E1 System.Object cashing/<download360>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cdownload360U3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m691B920495544D18CEEFCE8127C290181909C4AA (void);
// 0x000000E2 System.Void cashing/<download360>d__15::System.Collections.IEnumerator.Reset()
extern void U3Cdownload360U3Ed__15_System_Collections_IEnumerator_Reset_m1B4C4BAB956CDCFB14DE8386FE2D17A088CF4463 (void);
// 0x000000E3 System.Object cashing/<download360>d__15::System.Collections.IEnumerator.get_Current()
extern void U3Cdownload360U3Ed__15_System_Collections_IEnumerator_get_Current_mA5E9CEDC5B9EB29BB4F38156CA7D4B4B07B5022C (void);
// 0x000000E4 System.Void cashing/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m5201024652BD3259E6854FBB87AA8707DF84C4AB (void);
// 0x000000E5 System.Void cashing/<>c__DisplayClass16_0::<downloadForGameMode>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CdownloadForGameModeU3Eb__0_m96D9D7A4C9107134340D7717C2FC87ABB40B18BE (void);
// 0x000000E6 System.Void cashing/<>c__DisplayClass16_1::.ctor()
extern void U3CU3Ec__DisplayClass16_1__ctor_mA24204E9C53670035C9ACD385F7FB4E7FB4941B8 (void);
// 0x000000E7 System.Void cashing/<>c__DisplayClass16_1::<downloadForGameMode>b__1()
extern void U3CU3Ec__DisplayClass16_1_U3CdownloadForGameModeU3Eb__1_mD59017FE98537AE0F17B410F6ACD82905DA34C42 (void);
// 0x000000E8 System.Void cashing/<>c__DisplayClass16_2::.ctor()
extern void U3CU3Ec__DisplayClass16_2__ctor_m7C3219803A1B42E29D4ED0647C0EEDE6CC88ACA4 (void);
// 0x000000E9 System.Void cashing/<>c__DisplayClass16_2::<downloadForGameMode>b__2(System.Boolean)
extern void U3CU3Ec__DisplayClass16_2_U3CdownloadForGameModeU3Eb__2_m6C9F66AD94950290EA29357BDA4109A3EF817719 (void);
// 0x000000EA System.Void cashing/<downloadForGameMode>d__16::.ctor(System.Int32)
extern void U3CdownloadForGameModeU3Ed__16__ctor_m472E02B641AFDF72C75D3725D7126F85EFC9BD53 (void);
// 0x000000EB System.Void cashing/<downloadForGameMode>d__16::System.IDisposable.Dispose()
extern void U3CdownloadForGameModeU3Ed__16_System_IDisposable_Dispose_m0EA57873FAD8370AE52B273CA4EFE711BDF18933 (void);
// 0x000000EC System.Boolean cashing/<downloadForGameMode>d__16::MoveNext()
extern void U3CdownloadForGameModeU3Ed__16_MoveNext_m41C89490821135292017F0253FDBFBDF5EACD166 (void);
// 0x000000ED System.Void cashing/<downloadForGameMode>d__16::<>m__Finally1()
extern void U3CdownloadForGameModeU3Ed__16_U3CU3Em__Finally1_m0F54C5EE8816B4C30AE1F19A2440DFFA9B71CD3A (void);
// 0x000000EE System.Object cashing/<downloadForGameMode>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadForGameModeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m358F85E1553EB93E39939F33714E1BBE1136BB70 (void);
// 0x000000EF System.Void cashing/<downloadForGameMode>d__16::System.Collections.IEnumerator.Reset()
extern void U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_Reset_mA8F421E36E6A9C9EB392365B7788722EC3B6AA08 (void);
// 0x000000F0 System.Object cashing/<downloadForGameMode>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_get_Current_mADB9FC2B5CD54319C4C552D6E54FCB88F6E25DEB (void);
// 0x000000F1 System.Void cashing/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mF7A1E899ED83CB5B4EDE4BC19C5980AA469CE48F (void);
// 0x000000F2 System.Void cashing/<>c__DisplayClass17_0::<download360ForGameMode>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3Cdownload360ForGameModeU3Eb__0_m84E295F68C4A5FD0DA97F69BF73DA4B2EC9C2989 (void);
// 0x000000F3 System.Void cashing/<>c__DisplayClass17_1::.ctor()
extern void U3CU3Ec__DisplayClass17_1__ctor_m946B519649780C1A8623591E60FA20C33C7887C9 (void);
// 0x000000F4 System.Void cashing/<>c__DisplayClass17_1::<download360ForGameMode>b__1()
extern void U3CU3Ec__DisplayClass17_1_U3Cdownload360ForGameModeU3Eb__1_mAD2D2A85BCF8A6FF85139FA876056990ED8EEBFE (void);
// 0x000000F5 System.Void cashing/<>c__DisplayClass17_2::.ctor()
extern void U3CU3Ec__DisplayClass17_2__ctor_m4CA7EFB26DA2B3231803BB17E5AFE518FC4011D6 (void);
// 0x000000F6 System.Void cashing/<>c__DisplayClass17_2::<download360ForGameMode>b__2(System.Boolean)
extern void U3CU3Ec__DisplayClass17_2_U3Cdownload360ForGameModeU3Eb__2_m90F4F2683A2101479ACD05C38310BEDDD9CD2977 (void);
// 0x000000F7 System.Void cashing/<download360ForGameMode>d__17::.ctor(System.Int32)
extern void U3Cdownload360ForGameModeU3Ed__17__ctor_mE9557100C0BA840F6F03FE0EEEFDC692ED8A6011 (void);
// 0x000000F8 System.Void cashing/<download360ForGameMode>d__17::System.IDisposable.Dispose()
extern void U3Cdownload360ForGameModeU3Ed__17_System_IDisposable_Dispose_m5DE4CA4615F57DF289C548A1631ACEE10BB11894 (void);
// 0x000000F9 System.Boolean cashing/<download360ForGameMode>d__17::MoveNext()
extern void U3Cdownload360ForGameModeU3Ed__17_MoveNext_m9C89BA81D195208BC710FB0C316EED1F14C38963 (void);
// 0x000000FA System.Void cashing/<download360ForGameMode>d__17::<>m__Finally1()
extern void U3Cdownload360ForGameModeU3Ed__17_U3CU3Em__Finally1_mF2471BE9DA31EDA96910B4EE83AB0B0D2E16A790 (void);
// 0x000000FB System.Object cashing/<download360ForGameMode>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cdownload360ForGameModeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC17E8C51DAC6D1212F7BA907F67C8030B1E3CE36 (void);
// 0x000000FC System.Void cashing/<download360ForGameMode>d__17::System.Collections.IEnumerator.Reset()
extern void U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_Reset_mBBDB9EC2A4D88EED8B85216A582E109A4BBD31C9 (void);
// 0x000000FD System.Object cashing/<download360ForGameMode>d__17::System.Collections.IEnumerator.get_Current()
extern void U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_get_Current_m7FE0DBAF0EF8B6FD7A6D2D39C1BD3BAB76C6DF49 (void);
// 0x000000FE System.Void cashing/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mD7548752190C9F6007DEED586E95B687E4B87EEA (void);
// 0x000000FF System.Void cashing/<>c__DisplayClass18_0::<download>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CdownloadU3Eb__0_mE0EEEBE3BC70018207E9AF6686A922EF1B62B96C (void);
// 0x00000100 System.Void cashing/<>c__DisplayClass18_1::.ctor()
extern void U3CU3Ec__DisplayClass18_1__ctor_m75C0E5176EB2BD79CB3FC5EEBF387C3D0FBF9326 (void);
// 0x00000101 System.Void cashing/<>c__DisplayClass18_1::<download>b__1()
extern void U3CU3Ec__DisplayClass18_1_U3CdownloadU3Eb__1_m057C04819B2328AB96234CE559A186BC16AE92EE (void);
// 0x00000102 System.Void cashing/<download>d__18::.ctor(System.Int32)
extern void U3CdownloadU3Ed__18__ctor_m43E1E27480854B1FE2DB6B6DEAE884B7FBEA7134 (void);
// 0x00000103 System.Void cashing/<download>d__18::System.IDisposable.Dispose()
extern void U3CdownloadU3Ed__18_System_IDisposable_Dispose_mA29A63F245B20473233D4D16D370D84417CF9617 (void);
// 0x00000104 System.Boolean cashing/<download>d__18::MoveNext()
extern void U3CdownloadU3Ed__18_MoveNext_m00F4A3B4AC20FA2A8479FA86BAD8167152758175 (void);
// 0x00000105 System.Void cashing/<download>d__18::<>m__Finally1()
extern void U3CdownloadU3Ed__18_U3CU3Em__Finally1_m17CB1C024A64603F3AB5F9F8C74407C8ED9AF401 (void);
// 0x00000106 System.Object cashing/<download>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB4E2FC6B956A6AF4568DE51640B3ACC4BABE482 (void);
// 0x00000107 System.Void cashing/<download>d__18::System.Collections.IEnumerator.Reset()
extern void U3CdownloadU3Ed__18_System_Collections_IEnumerator_Reset_m946ACD78A5CA4F8B7E98BCE82ED7E3D9C97CAD3B (void);
// 0x00000108 System.Object cashing/<download>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadU3Ed__18_System_Collections_IEnumerator_get_Current_m81787FF7DF119D442C3C3291F65A91B2A1271E18 (void);
// 0x00000109 System.Void cashing/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m13E1B0EC07BAA4B24EAE9C7916F3A71B55024175 (void);
// 0x0000010A System.Void cashing/<>c__DisplayClass19_0::<getPortalsJson>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CgetPortalsJsonU3Eb__0_m1671AD71287DA82AB584E26B7C74F34121F4CCBE (void);
// 0x0000010B System.Void cashing/<>c__DisplayClass19_1::.ctor()
extern void U3CU3Ec__DisplayClass19_1__ctor_m3D593E1F39EEA553490C02B7A8017942AD44CBEB (void);
// 0x0000010C System.Void cashing/<>c__DisplayClass19_1::<getPortalsJson>b__1()
extern void U3CU3Ec__DisplayClass19_1_U3CgetPortalsJsonU3Eb__1_m48D15DBB36BF2BBD53450906ECC5360E20D3A3EE (void);
// 0x0000010D System.Void cashing/<getPortalsJson>d__19::.ctor(System.Int32)
extern void U3CgetPortalsJsonU3Ed__19__ctor_m0D8998FC0BB840571494BFCF5E5059EC16CB430B (void);
// 0x0000010E System.Void cashing/<getPortalsJson>d__19::System.IDisposable.Dispose()
extern void U3CgetPortalsJsonU3Ed__19_System_IDisposable_Dispose_m5C0DD30947E713AF6E1C124D915F9D9AF4D625CE (void);
// 0x0000010F System.Boolean cashing/<getPortalsJson>d__19::MoveNext()
extern void U3CgetPortalsJsonU3Ed__19_MoveNext_m440080B68D46A98D4BF8D7F8CE63498762AF5D72 (void);
// 0x00000110 System.Void cashing/<getPortalsJson>d__19::<>m__Finally1()
extern void U3CgetPortalsJsonU3Ed__19_U3CU3Em__Finally1_m3970B00A2C9BE7F4EA1DE3420438545C0D335AD7 (void);
// 0x00000111 System.Void cashing/<getPortalsJson>d__19::<>m__Finally2()
extern void U3CgetPortalsJsonU3Ed__19_U3CU3Em__Finally2_mFE99E8A705B59C52EBF76B0761116E01681DD32D (void);
// 0x00000112 System.Void cashing/<getPortalsJson>d__19::<>m__Finally3()
extern void U3CgetPortalsJsonU3Ed__19_U3CU3Em__Finally3_mAB396157DC60021464EFCAAC1197C5E5FAC4E322 (void);
// 0x00000113 System.Object cashing/<getPortalsJson>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgetPortalsJsonU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AF10A11810A70141E8CE7B926EEB339D650B226 (void);
// 0x00000114 System.Void cashing/<getPortalsJson>d__19::System.Collections.IEnumerator.Reset()
extern void U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_Reset_mE676442194F6699FC2B720F79D01A1F6EE85086A (void);
// 0x00000115 System.Object cashing/<getPortalsJson>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_get_Current_m530301696820C06A9957D466F8AD0B3FC86A74A1 (void);
// 0x00000116 System.Void cashing/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m536FF7040331A502EDAC44C130186770023EC2A1 (void);
// 0x00000117 System.Void cashing/<>c__DisplayClass20_0::<getPortal>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CgetPortalU3Eb__0_mFB3453D16449B1CFF682BE4EF14F91BD02404E67 (void);
// 0x00000118 System.Void cashing/<>c__DisplayClass20_1::.ctor()
extern void U3CU3Ec__DisplayClass20_1__ctor_mF1443FCDD2C982948C8D90A655EC4EEB3BC36B20 (void);
// 0x00000119 System.Void cashing/<>c__DisplayClass20_1::<getPortal>b__1()
extern void U3CU3Ec__DisplayClass20_1_U3CgetPortalU3Eb__1_m101CCD50B4CA7683EB3B34CED585AC13F1D3648C (void);
// 0x0000011A System.Void cashing/<>c__DisplayClass20_2::.ctor()
extern void U3CU3Ec__DisplayClass20_2__ctor_m79841F1B14EEE8D0F2C68E9F18BB5F50861A9EBD (void);
// 0x0000011B System.Void cashing/<>c__DisplayClass20_2::<getPortal>b__2()
extern void U3CU3Ec__DisplayClass20_2_U3CgetPortalU3Eb__2_m6730F384E114CA31B8C096C5FB1013E4A3C3E369 (void);
// 0x0000011C System.Void cashing/<>c__DisplayClass20_3::.ctor()
extern void U3CU3Ec__DisplayClass20_3__ctor_m9C1D130EEF572C67A7D93C32A02C728419AC851F (void);
// 0x0000011D System.Void cashing/<>c__DisplayClass20_3::<getPortal>b__3()
extern void U3CU3Ec__DisplayClass20_3_U3CgetPortalU3Eb__3_m33AA51A6EAF53A0E3F573AF29547A3BC3366828C (void);
// 0x0000011E System.Void cashing/<getPortal>d__20::.ctor(System.Int32)
extern void U3CgetPortalU3Ed__20__ctor_m575CD131084A10603A0E610EF658BFB9091E420B (void);
// 0x0000011F System.Void cashing/<getPortal>d__20::System.IDisposable.Dispose()
extern void U3CgetPortalU3Ed__20_System_IDisposable_Dispose_mA6058E0DB176DDBEF05040D7F4D9BA588BCB094E (void);
// 0x00000120 System.Boolean cashing/<getPortal>d__20::MoveNext()
extern void U3CgetPortalU3Ed__20_MoveNext_mD09B3AB3070758D567A2B2DE5375D245F62B696F (void);
// 0x00000121 System.Void cashing/<getPortal>d__20::<>m__Finally1()
extern void U3CgetPortalU3Ed__20_U3CU3Em__Finally1_m458379EAA73BCBC71AC2CBF6EA5A9C92442491B0 (void);
// 0x00000122 System.Object cashing/<getPortal>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgetPortalU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E4907A063AF1F0C4A0A3F4DE4520126AB94AE79 (void);
// 0x00000123 System.Void cashing/<getPortal>d__20::System.Collections.IEnumerator.Reset()
extern void U3CgetPortalU3Ed__20_System_Collections_IEnumerator_Reset_m4095F38A748F61E9F7A2CAE7B8140FF0D95F408A (void);
// 0x00000124 System.Object cashing/<getPortal>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CgetPortalU3Ed__20_System_Collections_IEnumerator_get_Current_mDD7D091DBC38241704DCE7A16B08A60F7ACF54F6 (void);
// 0x00000125 System.Void cashing/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mE7E149879660855C361E0FE84D2936CE4F6FD7C6 (void);
// 0x00000126 System.Void cashing/<>c__DisplayClass21_0::<getGameModeJson>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CgetGameModeJsonU3Eb__0_mB12D958BB33E51F81BB8B5D848FA42B4A78ED0D2 (void);
// 0x00000127 System.Void cashing/<>c__DisplayClass21_1::.ctor()
extern void U3CU3Ec__DisplayClass21_1__ctor_mB997491DA8522C159A6685ADF274AB03C77407DE (void);
// 0x00000128 System.Void cashing/<>c__DisplayClass21_1::<getGameModeJson>b__1()
extern void U3CU3Ec__DisplayClass21_1_U3CgetGameModeJsonU3Eb__1_mB9752A2254D6A70C27227F7C5C0658D708DE457F (void);
// 0x00000129 System.Void cashing/<getGameModeJson>d__21::.ctor(System.Int32)
extern void U3CgetGameModeJsonU3Ed__21__ctor_m33BA62505B1495742109388FDD4AD47D59299713 (void);
// 0x0000012A System.Void cashing/<getGameModeJson>d__21::System.IDisposable.Dispose()
extern void U3CgetGameModeJsonU3Ed__21_System_IDisposable_Dispose_m2BA528F86EB7740587B9DE8992F9ABEA4AE6F1E6 (void);
// 0x0000012B System.Boolean cashing/<getGameModeJson>d__21::MoveNext()
extern void U3CgetGameModeJsonU3Ed__21_MoveNext_mE3CACF84A193C8BD6D1ED053FA5AAFEE9C0B6A33 (void);
// 0x0000012C System.Void cashing/<getGameModeJson>d__21::<>m__Finally1()
extern void U3CgetGameModeJsonU3Ed__21_U3CU3Em__Finally1_mCCAF4992522CDF2BC1DD29141F213678D90D7526 (void);
// 0x0000012D System.Object cashing/<getGameModeJson>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgetGameModeJsonU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3682D310CCEBCC6227CEDEF4937C0FE58C4C0D15 (void);
// 0x0000012E System.Void cashing/<getGameModeJson>d__21::System.Collections.IEnumerator.Reset()
extern void U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_Reset_m5B4D96E2D62CA34C2DF12D1FB947F73C91C10D6B (void);
// 0x0000012F System.Object cashing/<getGameModeJson>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_get_Current_m6C4AECAF32F3DF93C95D46238555A5F7671913DA (void);
// 0x00000130 System.Void clickTag::clickPhoto()
extern void clickTag_clickPhoto_mDE87CBD6549B26D4F58DDFFEF88B2B061698F57A (void);
// 0x00000131 System.Void clickTag::click3D()
extern void clickTag_click3D_mE913A971F5F5A0645A78B989D8F61B944450F870 (void);
// 0x00000132 System.Void clickTag::clickVideo()
extern void clickTag_clickVideo_mEFCC0AA122B1D43D7BCEED6794F6CD68176C04A1 (void);
// 0x00000133 System.Void clickTag::.ctor()
extern void clickTag__ctor_m0D97B92A65CADD05C2C2D7875AC34DF88CD31EAD (void);
// 0x00000134 System.Void clickTag/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD5255203E0B5ECA9602203E03C5FE6263FEAB250 (void);
// 0x00000135 System.Void clickTag/<>c__DisplayClass0_0::<clickPhoto>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CclickPhotoU3Eb__0_m61479DC4D441CBB99491595C089920024A7E95AE (void);
// 0x00000136 System.Void collapsibleList::Start()
extern void collapsibleList_Start_m363413DE99F9481A60181A9C995D3D10982001B5 (void);
// 0x00000137 System.Void collapsibleList::goTo(System.Boolean)
extern void collapsibleList_goTo_m9729A978942C3D424BE2155B8B7B81BB26857377 (void);
// 0x00000138 System.Collections.IEnumerator collapsibleList::expandCollapse(System.Boolean)
extern void collapsibleList_expandCollapse_mADFAD1573901F55E3251D47EA2ACAB29C6AC731E (void);
// 0x00000139 System.Void collapsibleList::.ctor()
extern void collapsibleList__ctor_m4AC041173186CDEE3492FE09837C55F820CDCF99 (void);
// 0x0000013A System.Void collapsibleList/<expandCollapse>d__5::.ctor(System.Int32)
extern void U3CexpandCollapseU3Ed__5__ctor_mBFBDFACBF40F3A6AC86C6232A962D8D5A460733F (void);
// 0x0000013B System.Void collapsibleList/<expandCollapse>d__5::System.IDisposable.Dispose()
extern void U3CexpandCollapseU3Ed__5_System_IDisposable_Dispose_m426B430EA932152626ED4F5A729047211B853E06 (void);
// 0x0000013C System.Boolean collapsibleList/<expandCollapse>d__5::MoveNext()
extern void U3CexpandCollapseU3Ed__5_MoveNext_m916FE1316995E2FBE1C0D421D13C5325D32EDA45 (void);
// 0x0000013D System.Object collapsibleList/<expandCollapse>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CexpandCollapseU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B073DE4ACAC5B582EE2DC2358E6453237C630F2 (void);
// 0x0000013E System.Void collapsibleList/<expandCollapse>d__5::System.Collections.IEnumerator.Reset()
extern void U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_Reset_mF6D7DFDB099383114F4D246A1157C185A59D76CD (void);
// 0x0000013F System.Object collapsibleList/<expandCollapse>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_get_Current_m8D4E549664F10A3B58AFA4921925EB42C6F57E8C (void);
// 0x00000140 System.Void debugger::Start()
extern void debugger_Start_m99932BEE820F7BB9709D80B0F99BB3F8544A48F3 (void);
// 0x00000141 System.Void debugger::updateCullMask(System.String)
extern void debugger_updateCullMask_mDF8A77E2168EC7EADFB57D641E46931795FD6F03 (void);
// 0x00000142 System.Void debugger::updateStencil(System.String)
extern void debugger_updateStencil_mB4D1AF9F123178901F2D6C17C7145E5A795A7681 (void);
// 0x00000143 System.Void debugger::.ctor()
extern void debugger__ctor_mA2E574938E73A95860B39B6507321A0C05F6CF45 (void);
// 0x00000144 System.Void deepLink::Start()
extern void deepLink_Start_mF733917446A85B560493030E3063AE7FE2A04B8A (void);
// 0x00000145 System.Void deepLink::Instance_LinkActivated(ImaginationOverflow.UniversalDeepLinking.LinkActivation)
extern void deepLink_Instance_LinkActivated_mE880AD8D9ACC9FD246CDB389C744766BFB630BA7 (void);
// 0x00000146 System.Void deepLink::OnDestroy()
extern void deepLink_OnDestroy_mA60DB9014F7314B884769A1D517994352466DA2D (void);
// 0x00000147 System.Void deepLink::.ctor()
extern void deepLink__ctor_m4693A61A052497F71B40D365A1D4E82351AAAAF0 (void);
// 0x00000148 System.Void errorHandler::Start()
extern void errorHandler_Start_mCC31E5C3E9B92EE43275E1629F51BA9BB970E1A9 (void);
// 0x00000149 UnityEngine.UI.Button errorHandler::showError(System.String,System.String)
extern void errorHandler_showError_m15FCD0CFE38D19BE0A2240E27A8005C52B6F3324 (void);
// 0x0000014A UnityEngine.UI.Button errorHandler::showHelp()
extern void errorHandler_showHelp_m0FCF7FDD8D65B61CB58490E710B58F6E0883AE3C (void);
// 0x0000014B UnityEngine.UI.Button errorHandler::showInfo(System.String,System.String)
extern void errorHandler_showInfo_mDFDA75035938347BD688A7B2824D2BE0BA41CD33 (void);
// 0x0000014C UnityEngine.UI.Button errorHandler::showDone(System.String,System.String)
extern void errorHandler_showDone_mBB2D48AEC67F1A6D7E68EECE0FC5529DA5FFE677 (void);
// 0x0000014D System.Void errorHandler::.ctor()
extern void errorHandler__ctor_mB81548B71F7F8DC4D04C7D809DBAF7197586A8AB (void);
// 0x0000014E System.Void firstLast::ff(UnityEngine.Vector2)
extern void firstLast_ff_m838443D64A8ABBA8AA33F009DF98FCCA339A0013 (void);
// 0x0000014F System.Void firstLast::Start()
extern void firstLast_Start_mC25988C61747753138AAD8DECFA2B48EC61AF711 (void);
// 0x00000150 System.Void firstLast::.ctor()
extern void firstLast__ctor_mCC9CD0D285363E3A7B3805D91ACC32D5D8942E74 (void);
// 0x00000151 System.Collections.IEnumerator galleryImporter::Start()
extern void galleryImporter_Start_m0C28E1792CF565FA829FFCDA4787A28F23C99D30 (void);
// 0x00000152 System.Void galleryImporter::search(System.String)
extern void galleryImporter_search_m5596E16A8E86E05C84D4E48AC96C3CBECAEAC3D0 (void);
// 0x00000153 System.Collections.IEnumerator galleryImporter::spawnGallery()
extern void galleryImporter_spawnGallery_mB8E622DDCEBFAB6D3D1462282F3104D8D42EB698 (void);
// 0x00000154 System.Void galleryImporter::addButtonNC(System.String,UnityEngine.Texture2D,UnityEngine.Texture2D,System.Int32,System.Int32,System.String,System.String)
extern void galleryImporter_addButtonNC_m21DF0F549E07767F44C1ACC23EC8DB7A05E6FC94 (void);
// 0x00000155 System.Void galleryImporter::addButton(System.String,UnityEngine.Texture2D,UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.GameObject,System.String,System.String,System.Boolean)
extern void galleryImporter_addButton_m7BBFB9246430CD926A5E03FC5C67B90C8BA13375 (void);
// 0x00000156 System.Void galleryImporter::deepLinkPortal(System.Int32)
extern void galleryImporter_deepLinkPortal_m7BF4779CD3003283ACA0B3C4CE29A02CC217E4C3 (void);
// 0x00000157 System.Collections.IEnumerator galleryImporter::setPortal(System.Int32,System.Boolean)
extern void galleryImporter_setPortal_m03AA7011878B44152E20186188DD20F05379CF1B (void);
// 0x00000158 UnityEngine.Vector2 galleryImporter::fitIn360(UnityEngine.Texture2D)
extern void galleryImporter_fitIn360_m9D3FFF51D8CECA29AC6ED9A42D2A68B675B878EF (void);
// 0x00000159 UnityEngine.Vector2 galleryImporter::fitOut360(UnityEngine.Texture2D)
extern void galleryImporter_fitOut360_m8CC60B2FF529A8FDA8A3A64946773BFED2594C69 (void);
// 0x0000015A System.Void galleryImporter::ff(UnityEngine.Vector2)
extern void galleryImporter_ff_m1A171CD132731B0A4A1E751B7334982A1A72DD15 (void);
// 0x0000015B System.Void galleryImporter::.ctor()
extern void galleryImporter__ctor_m27B904304DFD84F18E04A557D6387013E4397BB9 (void);
// 0x0000015C System.Void galleryImporter/<Start>d__27::.ctor(System.Int32)
extern void U3CStartU3Ed__27__ctor_m0B8811B30EB600364DF6C15D193DCAC339F6B626 (void);
// 0x0000015D System.Void galleryImporter/<Start>d__27::System.IDisposable.Dispose()
extern void U3CStartU3Ed__27_System_IDisposable_Dispose_mE8D87EB2F4514AEDAB6555362B72FC96A2E0EDB4 (void);
// 0x0000015E System.Boolean galleryImporter/<Start>d__27::MoveNext()
extern void U3CStartU3Ed__27_MoveNext_m7766E3EEFA14B50CB49CC2ACF8EBADBEC02874DC (void);
// 0x0000015F System.Object galleryImporter/<Start>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA2DBFD21ADBAF930E826FFD85CAB9195E8665E5 (void);
// 0x00000160 System.Void galleryImporter/<Start>d__27::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__27_System_Collections_IEnumerator_Reset_m4029AE5DF84AD76E8097B4F4B258F88374A49793 (void);
// 0x00000161 System.Object galleryImporter/<Start>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__27_System_Collections_IEnumerator_get_Current_m882972B1CA3F6ADD16C5533231020519B1F64E2C (void);
// 0x00000162 System.Void galleryImporter/<spawnGallery>d__29::.ctor(System.Int32)
extern void U3CspawnGalleryU3Ed__29__ctor_m0193ADC12AEB1D701A614D2B10A9F265EE4A9992 (void);
// 0x00000163 System.Void galleryImporter/<spawnGallery>d__29::System.IDisposable.Dispose()
extern void U3CspawnGalleryU3Ed__29_System_IDisposable_Dispose_m7FEBF588D867DC10A5585017993C635B8DAB45D7 (void);
// 0x00000164 System.Boolean galleryImporter/<spawnGallery>d__29::MoveNext()
extern void U3CspawnGalleryU3Ed__29_MoveNext_mF0607F1BFBD70F7C262995CE7E2EFCA5EFCC4AC4 (void);
// 0x00000165 System.Void galleryImporter/<spawnGallery>d__29::<>m__Finally1()
extern void U3CspawnGalleryU3Ed__29_U3CU3Em__Finally1_m1ADCB9C0C82FF0AA64E0C2343D2C22447B295A4F (void);
// 0x00000166 System.Void galleryImporter/<spawnGallery>d__29::<>m__Finally2()
extern void U3CspawnGalleryU3Ed__29_U3CU3Em__Finally2_mC02F00088687054BDD2B02C1C973B134B79E5CC0 (void);
// 0x00000167 System.Void galleryImporter/<spawnGallery>d__29::<>m__Finally3()
extern void U3CspawnGalleryU3Ed__29_U3CU3Em__Finally3_m20297ECB59BB3C5A1E9B9E4EF763DE1387506ED9 (void);
// 0x00000168 System.Void galleryImporter/<spawnGallery>d__29::<>m__Finally4()
extern void U3CspawnGalleryU3Ed__29_U3CU3Em__Finally4_m733DCF0488ED30E9ED0284276E2AF32728A450A0 (void);
// 0x00000169 System.Object galleryImporter/<spawnGallery>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CspawnGalleryU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4871A28B2322CB8FAB15A72BD66A58EEDD6B9629 (void);
// 0x0000016A System.Void galleryImporter/<spawnGallery>d__29::System.Collections.IEnumerator.Reset()
extern void U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_Reset_m32DC4030997459632CE3C216C330990BDF25503E (void);
// 0x0000016B System.Object galleryImporter/<spawnGallery>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_get_Current_m96EFD1F62A0244F072445F09F66F821B22D8ED79 (void);
// 0x0000016C System.Void galleryImporter/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m429E3B4FFF518E99BA28FE59FF9361A2302A6272 (void);
// 0x0000016D System.Void galleryImporter/<>c__DisplayClass30_0::<addButtonNC>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CaddButtonNCU3Eb__0_m1FF396902F0828141E4FE5D3D5829F027B44DF8F (void);
// 0x0000016E System.Void galleryImporter/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m59CF9F17B52D376185429BFAC0880A5E93247ECC (void);
// 0x0000016F System.Void galleryImporter/<>c__DisplayClass31_0::<addButton>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CaddButtonU3Eb__0_m0D671515FD03573A0E1C8D335205419239670431 (void);
// 0x00000170 System.Void galleryImporter/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mA13E96F3F1C16426E2F389D1FD2FD53107EC5B0A (void);
// 0x00000171 System.Void galleryImporter/<>c__DisplayClass33_0::<setPortal>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CsetPortalU3Eb__0_mD3B68B9047191F63CB7DB4F9E5E1A65CCF10D23B (void);
// 0x00000172 System.Void galleryImporter/<setPortal>d__33::.ctor(System.Int32)
extern void U3CsetPortalU3Ed__33__ctor_m58AD80E68A85B07032DC3D27D26BC188D3E43D5C (void);
// 0x00000173 System.Void galleryImporter/<setPortal>d__33::System.IDisposable.Dispose()
extern void U3CsetPortalU3Ed__33_System_IDisposable_Dispose_m21A4150C350D623D1C5F6F9DF5A90F622A82D797 (void);
// 0x00000174 System.Boolean galleryImporter/<setPortal>d__33::MoveNext()
extern void U3CsetPortalU3Ed__33_MoveNext_mCCCB64C30ABBE37988A00C6A2379894D21029C88 (void);
// 0x00000175 System.Object galleryImporter/<setPortal>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetPortalU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34134D144A2E3435E70851F744F08B6F705AC206 (void);
// 0x00000176 System.Void galleryImporter/<setPortal>d__33::System.Collections.IEnumerator.Reset()
extern void U3CsetPortalU3Ed__33_System_Collections_IEnumerator_Reset_mAA9424DFE9D8706A426B724705BC890DA5550A97 (void);
// 0x00000177 System.Object galleryImporter/<setPortal>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CsetPortalU3Ed__33_System_Collections_IEnumerator_get_Current_mD5F908FBA910960F3A26A4BA1D9AC922314F8305 (void);
// 0x00000178 System.Collections.IEnumerator gameManager::Start()
extern void gameManager_Start_m169616D522718461F2522FC60796972765513C0B (void);
// 0x00000179 System.Collections.IEnumerator gameManager::setPortal(mainJson)
extern void gameManager_setPortal_mA087AAC5911FA190935E3F15A8E68127F2563FDC (void);
// 0x0000017A System.Collections.IEnumerator gameManager::setPortalNoReload(mainJson)
extern void gameManager_setPortalNoReload_mC693A3202E474BD7084B3925328C1A45C44597B3 (void);
// 0x0000017B System.Collections.IEnumerator gameManager::next()
extern void gameManager_next_mADD0A4EA4D45B942DE04516A3311E13C6AC6BC14 (void);
// 0x0000017C System.Collections.IEnumerator gameManager::sendLeaderBoard(UnityEngine.UI.Button,System.String,System.String)
extern void gameManager_sendLeaderBoard_mA350466B6838C990593636057E0F360174E3A11C (void);
// 0x0000017D System.Void gameManager::.ctor()
extern void gameManager__ctor_mEC3390C453AE10969E79C65755A3BECB98211356 (void);
// 0x0000017E System.Void gameManager/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m9700207C136BBD5CB21EDAF05F98512E2C113FD0 (void);
// 0x0000017F System.Void gameManager/<>c__DisplayClass21_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CStartU3Eb__0_m8DF771ABE58A010BB92BFDBA5FB639499DB1C854 (void);
// 0x00000180 System.Void gameManager/<Start>d__21::.ctor(System.Int32)
extern void U3CStartU3Ed__21__ctor_mEF2B87CF03BF250F61C1E22922C890B32917C866 (void);
// 0x00000181 System.Void gameManager/<Start>d__21::System.IDisposable.Dispose()
extern void U3CStartU3Ed__21_System_IDisposable_Dispose_m3BC66AD8F8AE7802BCFD4D98362AA5305A338CC7 (void);
// 0x00000182 System.Boolean gameManager/<Start>d__21::MoveNext()
extern void U3CStartU3Ed__21_MoveNext_m275F122A7009062EFDBD33A0D4219A18AB042EDA (void);
// 0x00000183 System.Object gameManager/<Start>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5258F4F2C9E7DD1733DE60CE2DEC9597FF26A25 (void);
// 0x00000184 System.Void gameManager/<Start>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m145FD8392C86D200899A2C4599867A8D891F6DBC (void);
// 0x00000185 System.Object gameManager/<Start>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_m5D6592FF1D9F9E1BC91BAF232D12E12097694C82 (void);
// 0x00000186 System.Void gameManager/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m3719F66F9C4E415059098F273FD28094C351FA95 (void);
// 0x00000187 System.Void gameManager/<>c__DisplayClass22_0::<setPortal>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CsetPortalU3Eb__0_m3395A9660661B7894CB8D999C2893F93097C6082 (void);
// 0x00000188 System.Boolean gameManager/<>c__DisplayClass22_0::<setPortal>b__1()
extern void U3CU3Ec__DisplayClass22_0_U3CsetPortalU3Eb__1_mD130D202A49F2AB4FD85E3DCD1D4177C572C383B (void);
// 0x00000189 System.Void gameManager/<setPortal>d__22::.ctor(System.Int32)
extern void U3CsetPortalU3Ed__22__ctor_m14C3F1881692CB63AC92768EC611CDEC88C8E3D2 (void);
// 0x0000018A System.Void gameManager/<setPortal>d__22::System.IDisposable.Dispose()
extern void U3CsetPortalU3Ed__22_System_IDisposable_Dispose_m933F60E78613B8F77909F38934408D65DACC7212 (void);
// 0x0000018B System.Boolean gameManager/<setPortal>d__22::MoveNext()
extern void U3CsetPortalU3Ed__22_MoveNext_m3E3501AD49696AC0BBF1E1D418D1EBC2FF645DFE (void);
// 0x0000018C System.Object gameManager/<setPortal>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetPortalU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D2B7D2355AC39A635454CAAEA2C9F29E38D34D0 (void);
// 0x0000018D System.Void gameManager/<setPortal>d__22::System.Collections.IEnumerator.Reset()
extern void U3CsetPortalU3Ed__22_System_Collections_IEnumerator_Reset_m8194E63ADEDFADC0621E20A60D8968E09281954A (void);
// 0x0000018E System.Object gameManager/<setPortal>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CsetPortalU3Ed__22_System_Collections_IEnumerator_get_Current_mA9DE23C794BA2DEC3072A3E1B64A4624B705BB71 (void);
// 0x0000018F System.Void gameManager/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m40C44B7BBB71767A3AABA65C5D556067C04760B9 (void);
// 0x00000190 System.Void gameManager/<>c__DisplayClass23_0::<setPortalNoReload>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CsetPortalNoReloadU3Eb__0_m998020E137FE77C2C6B481B1DCA3CA250C72F323 (void);
// 0x00000191 System.Void gameManager/<setPortalNoReload>d__23::.ctor(System.Int32)
extern void U3CsetPortalNoReloadU3Ed__23__ctor_m1767EFC32956AAAF6F7D60E14EE15C19F411BF49 (void);
// 0x00000192 System.Void gameManager/<setPortalNoReload>d__23::System.IDisposable.Dispose()
extern void U3CsetPortalNoReloadU3Ed__23_System_IDisposable_Dispose_m48415DE3EB47BCF40947029C0FDA0AC0071B4C09 (void);
// 0x00000193 System.Boolean gameManager/<setPortalNoReload>d__23::MoveNext()
extern void U3CsetPortalNoReloadU3Ed__23_MoveNext_m2883FDBE265AB075F2CE38E8B9CFFB9537F1B8AE (void);
// 0x00000194 System.Object gameManager/<setPortalNoReload>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetPortalNoReloadU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45321B203FA4AE4176FE9E7778990D99605467AC (void);
// 0x00000195 System.Void gameManager/<setPortalNoReload>d__23::System.Collections.IEnumerator.Reset()
extern void U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_Reset_m36BC0AA0642EFFC8B82559416D679FD080578627 (void);
// 0x00000196 System.Object gameManager/<setPortalNoReload>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_get_Current_mED9BFBAF66CB09E82ECA7BBE6F9E3F531487D95D (void);
// 0x00000197 System.Void gameManager/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m63334B167D0BF3BD2E349FE6205E168E9599D683 (void);
// 0x00000198 System.Void gameManager/<>c__DisplayClass24_0::<next>b__1(System.String)
extern void U3CU3Ec__DisplayClass24_0_U3CnextU3Eb__1_mAF3647D10EE173A42F91BB3A8918E6622C6F44B8 (void);
// 0x00000199 System.Void gameManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m826676E45D743A7A47AB77387EC55261DC928D64 (void);
// 0x0000019A System.Void gameManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m5BB8FA177096356EB97A75CA4E1D89C9312FD626 (void);
// 0x0000019B System.Void gameManager/<>c::<next>b__24_0()
extern void U3CU3Ec_U3CnextU3Eb__24_0_mB3D5F1D0A1BC3FF998305B351650D29F437AD350 (void);
// 0x0000019C System.Void gameManager/<next>d__24::.ctor(System.Int32)
extern void U3CnextU3Ed__24__ctor_m17D0A0EC8F3115BFF62CB09AEB60A1FCB4EA556E (void);
// 0x0000019D System.Void gameManager/<next>d__24::System.IDisposable.Dispose()
extern void U3CnextU3Ed__24_System_IDisposable_Dispose_m4B20891703F191AB5B860B84215319B9F809E975 (void);
// 0x0000019E System.Boolean gameManager/<next>d__24::MoveNext()
extern void U3CnextU3Ed__24_MoveNext_m949B4D5730D5B713438C34AB2D63129538C2D31D (void);
// 0x0000019F System.Object gameManager/<next>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CnextU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m919363BEC99F1629E5E9B975AFC36E11299A47FA (void);
// 0x000001A0 System.Void gameManager/<next>d__24::System.Collections.IEnumerator.Reset()
extern void U3CnextU3Ed__24_System_Collections_IEnumerator_Reset_mD787BBEEDCF6BDA102082176386C6821C7BF4CA3 (void);
// 0x000001A1 System.Object gameManager/<next>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CnextU3Ed__24_System_Collections_IEnumerator_get_Current_m3EBF0F869F7F8E21C2FD3227D29BFEFB85A013CE (void);
// 0x000001A2 System.Void gameManager/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mE1DE6A41DD2C5AD754519923AE00AC255C9C16E8 (void);
// 0x000001A3 System.Void gameManager/<>c__DisplayClass25_0::<sendLeaderBoard>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CsendLeaderBoardU3Eb__0_m62FD01347CA076A3F2069C06BEB510DF3BB2E020 (void);
// 0x000001A4 System.Void gameManager/<>c__DisplayClass25_1::.ctor()
extern void U3CU3Ec__DisplayClass25_1__ctor_m5B585A2EF60F714DD86493F2B319DF386EDCF2FC (void);
// 0x000001A5 System.Void gameManager/<>c__DisplayClass25_1::<sendLeaderBoard>b__1()
extern void U3CU3Ec__DisplayClass25_1_U3CsendLeaderBoardU3Eb__1_m366EB63B42F3807F79FEC32BE6CD2FFF1C00830A (void);
// 0x000001A6 System.Void gameManager/<>c__DisplayClass25_2::.ctor()
extern void U3CU3Ec__DisplayClass25_2__ctor_m56151A09B3AE322E4D891E95B19FC65F8E48A089 (void);
// 0x000001A7 System.Void gameManager/<>c__DisplayClass25_2::<sendLeaderBoard>b__2()
extern void U3CU3Ec__DisplayClass25_2_U3CsendLeaderBoardU3Eb__2_m93285107A1DC9156F33370BE0905CB4F881716B9 (void);
// 0x000001A8 System.Void gameManager/<>c__DisplayClass25_3::.ctor()
extern void U3CU3Ec__DisplayClass25_3__ctor_mDBFE04FAB325D9E811E0D6C3822D474329A508F7 (void);
// 0x000001A9 System.Void gameManager/<>c__DisplayClass25_3::<sendLeaderBoard>b__3()
extern void U3CU3Ec__DisplayClass25_3_U3CsendLeaderBoardU3Eb__3_m233DA85A8CA9BC13A5FDAE6D87A66B7C39FAE3D0 (void);
// 0x000001AA System.Void gameManager/<sendLeaderBoard>d__25::.ctor(System.Int32)
extern void U3CsendLeaderBoardU3Ed__25__ctor_m3135E302BFD2C0C3D21A122EA1D2C1D9901B629D (void);
// 0x000001AB System.Void gameManager/<sendLeaderBoard>d__25::System.IDisposable.Dispose()
extern void U3CsendLeaderBoardU3Ed__25_System_IDisposable_Dispose_m24270614E4514553D51DC65691B789672AA2A984 (void);
// 0x000001AC System.Boolean gameManager/<sendLeaderBoard>d__25::MoveNext()
extern void U3CsendLeaderBoardU3Ed__25_MoveNext_m9C8936764FD78D2C25087579261E3094B4AF5AEC (void);
// 0x000001AD System.Void gameManager/<sendLeaderBoard>d__25::<>m__Finally1()
extern void U3CsendLeaderBoardU3Ed__25_U3CU3Em__Finally1_mD0926A9A17621F154E6E8F8200BED450B06F8A78 (void);
// 0x000001AE System.Object gameManager/<sendLeaderBoard>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsendLeaderBoardU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1064BB8870C65E12FCAF6E50F53AA409F9552585 (void);
// 0x000001AF System.Void gameManager/<sendLeaderBoard>d__25::System.Collections.IEnumerator.Reset()
extern void U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_Reset_m66A7DDDD5C34A611D7AAB4166A61B80028048A4E (void);
// 0x000001B0 System.Object gameManager/<sendLeaderBoard>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_get_Current_m0A31FDCB2696350455C3F79BFDC9779F3B43F6DD (void);
// 0x000001B1 System.Void gameMode::getReady()
extern void gameMode_getReady_m14A23C55801E7F363008BE44D455BBFD74675D4A (void);
// 0x000001B2 System.Void gameMode::Start()
extern void gameMode_Start_m152E8D48AB1D55E5E0DB06CB258DFB7B5C45C5D6 (void);
// 0x000001B3 System.Void gameMode::askCam()
extern void gameMode_askCam_mE69233F0213F5C44B121A848271A570650B69553 (void);
// 0x000001B4 System.Collections.IEnumerator gameMode::download()
extern void gameMode_download_m3657B9D533F61FABB22FCD3EAAC82F1158E801E4 (void);
// 0x000001B5 System.Collections.IEnumerator gameMode::downloadHidden()
extern void gameMode_downloadHidden_m68D4C21E769238439ACE96C78F8F9070758F471D (void);
// 0x000001B6 System.Void gameMode::.ctor()
extern void gameMode__ctor_mA47AFDF0941418DE94212B577A247BDE980973EE (void);
// 0x000001B7 System.Void gameMode/<download>d__5::.ctor(System.Int32)
extern void U3CdownloadU3Ed__5__ctor_m48EEE33F5E91228F9519B94A0F1A574DB8B87977 (void);
// 0x000001B8 System.Void gameMode/<download>d__5::System.IDisposable.Dispose()
extern void U3CdownloadU3Ed__5_System_IDisposable_Dispose_m45936B0F78F3A28CFEC857966FA9B4C0B5C11B05 (void);
// 0x000001B9 System.Boolean gameMode/<download>d__5::MoveNext()
extern void U3CdownloadU3Ed__5_MoveNext_m874309E06A86C7F97B8C471F3C4F795AB44E6DE6 (void);
// 0x000001BA System.Void gameMode/<download>d__5::<>m__Finally1()
extern void U3CdownloadU3Ed__5_U3CU3Em__Finally1_m5158681611D35E5010A5796F709F0954A7CA9D18 (void);
// 0x000001BB System.Object gameMode/<download>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE5F967C2CF51C2616C0784F560DEC048EC9E4A (void);
// 0x000001BC System.Void gameMode/<download>d__5::System.Collections.IEnumerator.Reset()
extern void U3CdownloadU3Ed__5_System_Collections_IEnumerator_Reset_m457EFAE4B023CED9721AEB88FB9E758FC46B8DC0 (void);
// 0x000001BD System.Object gameMode/<download>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadU3Ed__5_System_Collections_IEnumerator_get_Current_m397E0633C764549FDAB0F322F0B1ACECE9443A91 (void);
// 0x000001BE System.Void gameMode/<downloadHidden>d__6::.ctor(System.Int32)
extern void U3CdownloadHiddenU3Ed__6__ctor_m2810B2657357FEF21FC927248373AE1845FB39D6 (void);
// 0x000001BF System.Void gameMode/<downloadHidden>d__6::System.IDisposable.Dispose()
extern void U3CdownloadHiddenU3Ed__6_System_IDisposable_Dispose_mC3C65E624E782F46E034947EB42B4F362E764688 (void);
// 0x000001C0 System.Boolean gameMode/<downloadHidden>d__6::MoveNext()
extern void U3CdownloadHiddenU3Ed__6_MoveNext_mE4EDCF8D3B1F435EA1E6AACFE1E026282D153FD2 (void);
// 0x000001C1 System.Void gameMode/<downloadHidden>d__6::<>m__Finally1()
extern void U3CdownloadHiddenU3Ed__6_U3CU3Em__Finally1_m2BD2AA4192036E8044BCA5D45B6CA29AF43A15DB (void);
// 0x000001C2 System.Object gameMode/<downloadHidden>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdownloadHiddenU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E52325C5905BE8D6C05233270C82E5CB7805FC (void);
// 0x000001C3 System.Void gameMode/<downloadHidden>d__6::System.Collections.IEnumerator.Reset()
extern void U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_Reset_m46369049429C9A0C7EF2FE276441BAE764F80E6B (void);
// 0x000001C4 System.Object gameMode/<downloadHidden>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_get_Current_m6EF7A2146BA4DC6BC2877876AC97DE5FC20F5DFD (void);
// 0x000001C5 System.Void IListExtensions::Shuffle(System.Collections.Generic.IList`1<T>)
// 0x000001C6 System.Void leaderB::.ctor()
extern void leaderB__ctor_m2DDA98A195EBF2FABAA61A6823B9B35F0983E949 (void);
// 0x000001C7 System.Void leader::.ctor()
extern void leader__ctor_mEF513645400A122038B7786F5B902087F2417C32 (void);
// 0x000001C8 System.Int32 portalsJson::count()
extern void portalsJson_count_m452467821CFDE8D5A7AFD060A598477F2F0432EE (void);
// 0x000001C9 System.Void portalsJson::append(portalsJson)
extern void portalsJson_append_m4AE2CC1769BAB89B9E9523127701E16774A6F87C (void);
// 0x000001CA System.Void portalsJson::.ctor()
extern void portalsJson__ctor_m8F34B5AD31B61F387F85BA7E88780C900950263C (void);
// 0x000001CB System.Void gMJson::.ctor()
extern void gMJson__ctor_m7DD91B25A42B92CF16C5E50DBA9D0868F86A5DD3 (void);
// 0x000001CC System.Void teleportBtn::.ctor()
extern void teleportBtn__ctor_mC17E9F3E75ECD2E9B06391BB144054500B3EA28F (void);
// 0x000001CD System.Void country::.ctor(System.String)
extern void country__ctor_m1C3B25F995505BCFD46DBFE58AD8DD5269AC4E14 (void);
// 0x000001CE country country::exists(System.Collections.Generic.List`1<country>)
extern void country_exists_mA6E88FC7DF524A1BF5D457D07AA470B65E3D6ED6 (void);
// 0x000001CF System.Boolean portal::tagContains(System.String)
extern void portal_tagContains_m38BCC7D2690CBD4E1B5F7EA12C6B825CC49977CF (void);
// 0x000001D0 System.Void portal::.ctor()
extern void portal__ctor_mE918CCDCADE9C997F9494BC5B5CD09CD4193D0D7 (void);
// 0x000001D1 System.Void tag::.ctor()
extern void tag__ctor_m94647E303F0E2C917724D3EB68E18DA4D5B7D9BA (void);
// 0x000001D2 System.Void mainJson::.ctor()
extern void mainJson__ctor_mBBEE59A30B4D1D05F72437271FA6C2062838E2C8 (void);
// 0x000001D3 System.Void assetBundle::.ctor()
extern void assetBundle__ctor_mE61585BF10F2226F913C63F422686C33A785CD17 (void);
// 0x000001D4 System.Void Background::.ctor()
extern void Background__ctor_mEC9A50E3A2C20E886BD7FA5D93F73FF0CC9E7B38 (void);
// 0x000001D5 System.Void CustomBtn::.ctor()
extern void CustomBtn__ctor_m833507B8E9404AA62E37898E0DC8712A5462B7FD (void);
// 0x000001D6 System.Void sceneObject::.ctor()
extern void sceneObject__ctor_m88F02F38FE7C06A1569FC831DA07120A1FD9C980 (void);
// 0x000001D7 System.Void v3::.ctor(UnityEngine.Vector3)
extern void v3__ctor_m21C46DFCDBFD2D34ACB40CE016D8B2DD42D5B2D5 (void);
// 0x000001D8 UnityEngine.Vector3 v3::toVector3()
extern void v3_toVector3_mEE68C5134A262C7909F617ACB1F838EDBBF53F35 (void);
// 0x000001D9 System.Void v4::.ctor(UnityEngine.Quaternion)
extern void v4__ctor_m73C2E94BCACEBBF5C507A9CA768D830957E30E18 (void);
// 0x000001DA UnityEngine.Quaternion v4::toQuaternion()
extern void v4_toQuaternion_m8D1D147EE19BE5BD40BFC721CE01C5E16AE10DAE (void);
// 0x000001DB System.Void mainMenuLang::Start()
extern void mainMenuLang_Start_m1BF155356F8CE69BA905A6DF66EFCB43B87AC497 (void);
// 0x000001DC System.Void mainMenuLang::.ctor()
extern void mainMenuLang__ctor_mBEBF2597889D7989F8FC8ED2A4FE46FCD8CC9B04 (void);
// 0x000001DD System.Void nativeBack::Update()
extern void nativeBack_Update_m8E972C0FF180F64578CF600221562C2BEAAF1220 (void);
// 0x000001DE System.Void nativeBack::.ctor()
extern void nativeBack__ctor_m59E2F2FDB9C8927F1207B638949E7ECC3C1E2740 (void);
// 0x000001DF System.Void config::.ctor()
extern void config__ctor_mF553B308AF044B5BD97531BF44BA946BAB14BE97 (void);
// 0x000001E0 System.Void nativeGalleryImporter::Awake()
extern void nativeGalleryImporter_Awake_mA0DFE326F5F6ECA416334DB062CDF863AB410334 (void);
// 0x000001E1 System.Boolean nativeGalleryImporter::getIntentData()
extern void nativeGalleryImporter_getIntentData_m5C85743AC49AD9A0B2ABF18BBFAEAD7BAA58CDC7 (void);
// 0x000001E2 System.Boolean nativeGalleryImporter::CreatePushClass(UnityEngine.AndroidJavaClass)
extern void nativeGalleryImporter_CreatePushClass_mD43F2E609D8FB5B465AEDD7026FE1CE5883F1A20 (void);
// 0x000001E3 UnityEngine.AndroidJavaObject nativeGalleryImporter::GetExtras(UnityEngine.AndroidJavaObject)
extern void nativeGalleryImporter_GetExtras_mFD30AE1CA6174A9A4C586B219314C0A39FCD129E (void);
// 0x000001E4 System.String nativeGalleryImporter::GetProperty(UnityEngine.AndroidJavaObject,System.String)
extern void nativeGalleryImporter_GetProperty_mD4F4F406D72DF56E728C21AE94AEFC104C2A4141 (void);
// 0x000001E5 System.Void nativeGalleryImporter::pop()
extern void nativeGalleryImporter_pop_m519F9E3C9AF89DDC9418CE0BAF64AE21E6243281 (void);
// 0x000001E6 System.Void nativeGalleryImporter::Start()
extern void nativeGalleryImporter_Start_m9591FEA5E299B90674227E498D1745210E590482 (void);
// 0x000001E7 System.Void nativeGalleryImporter::customizeScene(System.String)
extern void nativeGalleryImporter_customizeScene_m8DE1DD113EF5109A4634126BF6277F0F3118F654 (void);
// 0x000001E8 System.Void nativeGalleryImporter::getImage(System.Int32)
extern void nativeGalleryImporter_getImage_mB01507D32DBDD1C1508A83B0DB2C5CBB76173D8E (void);
// 0x000001E9 System.Void nativeGalleryImporter::placeObjs()
extern void nativeGalleryImporter_placeObjs_m7A7D888D7387B7DEB8A08C0A0DD796BDF66B727A (void);
// 0x000001EA System.Void nativeGalleryImporter::PickImage(System.Int32,UnityEngine.UI.RawImage)
extern void nativeGalleryImporter_PickImage_mA1F5869686E599051457A56E70FA3025064F24FD (void);
// 0x000001EB System.Collections.IEnumerator nativeGalleryImporter::setPortal(System.Int32)
extern void nativeGalleryImporter_setPortal_m2380CD9BD029464024B834CD3C723FA8ABB123DE (void);
// 0x000001EC System.Void nativeGalleryImporter::OnApplicationFocus(System.Boolean)
extern void nativeGalleryImporter_OnApplicationFocus_m114D1D4EE4D20E61582A1AFDF88A1506E41071CE (void);
// 0x000001ED System.Void nativeGalleryImporter::.ctor()
extern void nativeGalleryImporter__ctor_m124FF8CDFC027B76E21DB5DB55394D9B9334359A (void);
// 0x000001EE System.Void nativeGalleryImporter::<customizeScene>b__26_0(UnityEngine.Texture2D)
extern void nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_0_m270B1D1CE3829824E010011625AF105A74A3AEFB (void);
// 0x000001EF System.Void nativeGalleryImporter::<customizeScene>b__26_1(UnityEngine.Texture2D)
extern void nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_1_mCD2790B37EE115F46C41F7D49FAA6C58710523C3 (void);
// 0x000001F0 System.Void nativeGalleryImporter::<customizeScene>b__26_2(UnityEngine.Texture2D)
extern void nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_2_mA1A540D526EC3EACC52D115C32751D307497D7E0 (void);
// 0x000001F1 System.Void nativeGalleryImporter/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mB9AED20E29665515E0794496EEA9DA9F9EA2671C (void);
// 0x000001F2 System.Void nativeGalleryImporter/<>c__DisplayClass29_0::<PickImage>b__0(System.String)
extern void U3CU3Ec__DisplayClass29_0_U3CPickImageU3Eb__0_m06D959E4CB6B960D9C93DF06256134A62591E283 (void);
// 0x000001F3 System.Void nativeGalleryImporter/<setPortal>d__30::.ctor(System.Int32)
extern void U3CsetPortalU3Ed__30__ctor_m6C8E4E232B9E6DF5BF9CF28A383DF2B314A3142C (void);
// 0x000001F4 System.Void nativeGalleryImporter/<setPortal>d__30::System.IDisposable.Dispose()
extern void U3CsetPortalU3Ed__30_System_IDisposable_Dispose_mB0CD9C5D764E231E71230E223E2D19C07EA09BDF (void);
// 0x000001F5 System.Boolean nativeGalleryImporter/<setPortal>d__30::MoveNext()
extern void U3CsetPortalU3Ed__30_MoveNext_mDF8484A088A22B58A40AAD997EDE3F06DDDBDB18 (void);
// 0x000001F6 System.Object nativeGalleryImporter/<setPortal>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetPortalU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1EDC811A1D4AFAFCA0CDC184534C02C70A2699C (void);
// 0x000001F7 System.Void nativeGalleryImporter/<setPortal>d__30::System.Collections.IEnumerator.Reset()
extern void U3CsetPortalU3Ed__30_System_Collections_IEnumerator_Reset_mA2BB601112A640FE30489C0E4E93B407E5075C22 (void);
// 0x000001F8 System.Object nativeGalleryImporter/<setPortal>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CsetPortalU3Ed__30_System_Collections_IEnumerator_get_Current_m6D35ECB1F8B9011F1C5D29BE07833F759381CD0B (void);
// 0x000001F9 System.Void picker::.ctor()
extern void picker__ctor_mE68096A12C2F1FFC98A8297F3A0733E871C0F83B (void);
// 0x000001FA System.Void placeholderText::Start()
extern void placeholderText_Start_mE7FA10541B01A0FB2E9EEEE55BF1028FBD95345B (void);
// 0x000001FB System.Void placeholderText::.ctor()
extern void placeholderText__ctor_mAA576B0B3F01053C4E3F36D30D5FA4CAD81FDBBD (void);
// 0x000001FC System.Void portalManager::Start()
extern void portalManager_Start_m083B147C95122A2394CB84B394EBDED40F2AA98A (void);
// 0x000001FD System.Void portalManager::OnTriggerStay(UnityEngine.Collider)
extern void portalManager_OnTriggerStay_m905CE2E1A4C64B17A1B0BB3F0B567EC82C21CA61 (void);
// 0x000001FE System.Void portalManager::OnTriggerEnter(UnityEngine.Collider)
extern void portalManager_OnTriggerEnter_m71045601F1A6F966C7039BC2DB930BBD942C1736 (void);
// 0x000001FF System.Void portalManager::.ctor()
extern void portalManager__ctor_m07CA661474091E3FEB264920AB07A585164B7DBB (void);
// 0x00000200 System.Void sceneHolder::.ctor()
extern void sceneHolder__ctor_m8F0D2AD8662C21CDBA86F48FB9D49822498E4D31 (void);
// 0x00000201 System.Void sceneManager::reset()
extern void sceneManager_reset_m934FB0704EA91EB2183C1E36D4A10211CF7B1B3C (void);
// 0x00000202 System.Void sceneManager::back()
extern void sceneManager_back_mB3859AF40C833FEA7226F4E0C9577037AEE9B673 (void);
// 0x00000203 System.Void sceneManager::resetCustom()
extern void sceneManager_resetCustom_mEFD341A18CAE74F8193C13E847E7978603E77242 (void);
// 0x00000204 System.Void sceneManager::gamer()
extern void sceneManager_gamer_mCAA365EC63FD6CD4F8E34745F8693B16D874065A (void);
// 0x00000205 System.Void sceneManager::errorReset()
extern void sceneManager_errorReset_m781C46FDBE6BB73FA8749E13495EEE7BEE5193E1 (void);
// 0x00000206 System.Void sceneManager::refresh()
extern void sceneManager_refresh_m3B856ABD94582AF48D2ACAB6D15E7D5ADB12A8CE (void);
// 0x00000207 System.Void sceneManager::resetS()
extern void sceneManager_resetS_mEE3C16A114B652C6AF87095ABD6D1BE1402B53AB (void);
// 0x00000208 System.Void sceneManager::share()
extern void sceneManager_share_m805A592776854880865365E6BD1D3C15A19824FF (void);
// 0x00000209 System.Void sceneManager::.ctor()
extern void sceneManager__ctor_m65855833EBD363DE1C424AA42209E331D454B5CC (void);
// 0x0000020A System.Boolean scoreManager::get_IsCounting()
extern void scoreManager_get_IsCounting_mAD2CA929AB8A9D6EFB8CEC3C4C76E1C360C7D5B2 (void);
// 0x0000020B System.Void scoreManager::set_IsCounting(System.Boolean)
extern void scoreManager_set_IsCounting_m25C152E8E8499F825E5A3E580A66207727B0E005 (void);
// 0x0000020C System.Boolean scoreManager::startSound(System.Boolean)
extern void scoreManager_startSound_mEF9D29E6A1C29A83A8438400337FF804624DD688 (void);
// 0x0000020D System.Void scoreManager::OnMouseDown()
extern void scoreManager_OnMouseDown_m870320DF27BD54773236D9FC6D298518CD5CD3A2 (void);
// 0x0000020E System.Void scoreManager::Update()
extern void scoreManager_Update_m427CAD9463091B684BC768495413A2C9F8DE80E0 (void);
// 0x0000020F System.Void scoreManager::.ctor()
extern void scoreManager__ctor_m34D3F568976B68881D634D07FBFDC625BCA44162 (void);
// 0x00000210 System.Void switchCollapse::Start()
extern void switchCollapse_Start_mEDF15B893B3E97E4457BA12B7E74F381EBE70944 (void);
// 0x00000211 System.Void switchCollapse::Collapse()
extern void switchCollapse_Collapse_mF6AFE8B4FAC098729BF517F00720E706121C640A (void);
// 0x00000212 System.Void switchCollapse::.ctor()
extern void switchCollapse__ctor_m7C2C4656F7D0A0EE4ECBCC452780414FA6EFDCB8 (void);
// 0x00000213 System.Void taskbar::search(System.String)
extern void taskbar_search_mB378D901D8F93411A90BDBCEC741A1CFEC342A08 (void);
// 0x00000214 System.Void taskbar::Awake()
extern void taskbar_Awake_m29C7E4F7728ED49B064FD6451B974FB88FF98E48 (void);
// 0x00000215 System.Void taskbar::Start()
extern void taskbar_Start_m751D53A8E37301A310363B3E77269F663572FC82 (void);
// 0x00000216 System.Void taskbar::pop()
extern void taskbar_pop_m676549B4256134D756B8BBA0A735EC904AA2C09B (void);
// 0x00000217 System.Void taskbar::OnApplicationPause(System.Boolean)
extern void taskbar_OnApplicationPause_m8BD4C8CE8CB74B8766207A47A01B178449AB51FC (void);
// 0x00000218 System.Void taskbar::OnApplicationFocus(System.Boolean)
extern void taskbar_OnApplicationFocus_m1D742F6C709D3242E270296D28242D6B18D86542 (void);
// 0x00000219 System.Void taskbar::.ctor()
extern void taskbar__ctor_mEDDEE9D493B1230608444E7ED605B3E4C0C877EB (void);
// 0x0000021A System.Void taskbarForGameMode::changeTo(UnityEngine.Texture2D)
extern void taskbarForGameMode_changeTo_m42ADDE005C9552DF45E7A253EE0CADCED2B124A4 (void);
// 0x0000021B System.Void taskbarForGameMode::Start()
extern void taskbarForGameMode_Start_mF0C2C95D363441453EFDD21BE9C4A229D2DC06A6 (void);
// 0x0000021C System.Void taskbarForGameMode::pop()
extern void taskbarForGameMode_pop_m08F1CC9B13FC433CB95194720A216AE8E4EBC50F (void);
// 0x0000021D System.Void taskbarForGameMode::OnApplicationFocus(System.Boolean)
extern void taskbarForGameMode_OnApplicationFocus_m0D9B8D950531D3A7C9863BC0E4AB95C5F0FC2A65 (void);
// 0x0000021E System.Void taskbarForGameMode::.ctor()
extern void taskbarForGameMode__ctor_mAC20D9A56C5277590F5BF98860DD92BBBB66BF63 (void);
// 0x0000021F System.Void testingBehaviour::.ctor()
extern void testingBehaviour__ctor_m4D0CDB0E7D4701C13CD5F1E40ABDB3E7D7D07AB8 (void);
// 0x00000220 UnityEngine.GameObject textureHolder::get_AB()
extern void textureHolder_get_AB_m75503E8FCAE4E93DC6F75A1762C73C9A3DB17066 (void);
// 0x00000221 System.Void textureHolder::set_AB(UnityEngine.GameObject)
extern void textureHolder_set_AB_mB626A57B3C891E41608A837373E7CE850F1FB859 (void);
// 0x00000222 System.Void textureHolder::fix(UnityEngine.GameObject)
extern void textureHolder_fix_m9737529288B4BBF5416859A913902B950E53CD78 (void);
// 0x00000223 System.Void textureHolder::.ctor()
extern void textureHolder__ctor_mC926D4E816FB392F5481A5168E30773928E1E7E1 (void);
// 0x00000224 System.Void textureHolder::.cctor()
extern void textureHolder__cctor_m04A2362A0FDF9007C1CC2FB4282F4BBF7DF9BF78 (void);
// 0x00000225 System.Void playAnim::play(System.String)
extern void playAnim_play_mD6C72B9D7D70DD0AF73244801A95AC14949066CB (void);
// 0x00000226 System.Void playAnim::.ctor()
extern void playAnim__ctor_mBD2CC7E837A1C76C40229260B37BAB1C8499AA26 (void);
// 0x00000227 System.Int32 DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_CurrentPanel()
extern void SimpleScrollSnap_get_CurrentPanel_mCC5EC8648BB72DDE5C99DE4739320C82F0F64470 (void);
// 0x00000228 System.Int32 DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_TargetPanel()
extern void SimpleScrollSnap_get_TargetPanel_m25CB62F655CFA42AB0292E3ED9819842F2934566 (void);
// 0x00000229 System.Int32 DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_NearestPanel()
extern void SimpleScrollSnap_get_NearestPanel_mD6F4F8774EDB107F730AF1D4C8D4FBC8459C342D (void);
// 0x0000022A System.Int32 DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_NumberOfPanels()
extern void SimpleScrollSnap_get_NumberOfPanels_m78E9EB00525451B77A5A40FF59264F7306EFCAB6 (void);
// 0x0000022B UnityEngine.UI.ScrollRect DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_ScrollRect()
extern void SimpleScrollSnap_get_ScrollRect_mE4ED2804D0BE037AAE76F36A048C2844C2398EF3 (void);
// 0x0000022C UnityEngine.RectTransform DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_Content()
extern void SimpleScrollSnap_get_Content_m5A9291CFDE58C26A1327C04D6291B10127B82A92 (void);
// 0x0000022D UnityEngine.RectTransform DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_Viewport()
extern void SimpleScrollSnap_get_Viewport_m8AE2716DDDAC6D9BE4830593D614E4820B4FC0BB (void);
// 0x0000022E UnityEngine.GameObject[] DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_Panels()
extern void SimpleScrollSnap_get_Panels_mEE7C882105D47FC6832F8AA8C59D6F56AC987C1E (void);
// 0x0000022F UnityEngine.UI.Toggle[] DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::get_Toggles()
extern void SimpleScrollSnap_get_Toggles_m68B3D8EECE325FDB7AE3F9E41EFAC83FB2D7584C (void);
// 0x00000230 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::Start()
extern void SimpleScrollSnap_Start_m3D6B9310F2EDECED686EBD9EF48B6111B5E332C7 (void);
// 0x00000231 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::Update()
extern void SimpleScrollSnap_Update_mAF5F9322EA4327CB9F5DA1084A45497391FCF595 (void);
// 0x00000232 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void SimpleScrollSnap_OnPointerDown_mCDDAE2E5F08BA64C7FAA0FF4895273E45878A5CD (void);
// 0x00000233 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void SimpleScrollSnap_OnBeginDrag_mCCCC09ADA3A817AE458403FF5C20650B872EBA86 (void);
// 0x00000234 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void SimpleScrollSnap_OnDrag_mF90C27F0E84F6930D7D90ED4D2A4314BE3552E87 (void);
// 0x00000235 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void SimpleScrollSnap_OnEndDrag_mBFED0F3AB8851C9E762C718AE30068922299B5EB (void);
// 0x00000236 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void SimpleScrollSnap_OnPointerUp_mE6C8EF647587B151FAACB7011B676ECEF4A69C64 (void);
// 0x00000237 System.Boolean DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::Validate()
extern void SimpleScrollSnap_Validate_mAE8841A17B04DAA58AD5F331295F1B11A34BB3FC (void);
// 0x00000238 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::Setup(System.Boolean)
extern void SimpleScrollSnap_Setup_mEDBE0C985FA5C848F7AD641CBD068363B2673D0E (void);
// 0x00000239 UnityEngine.Vector2 DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::DisplacementFromCenter(UnityEngine.Vector2)
extern void SimpleScrollSnap_DisplacementFromCenter_m90B7622CB7ECAB872641150825732A68CE2157B9 (void);
// 0x0000023A System.Int32 DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::DetermineNearestPanel()
extern void SimpleScrollSnap_DetermineNearestPanel_mE2B5A7C78F30EF6CC163258F1116294034C5633B (void);
// 0x0000023B System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::SelectTargetPanel()
extern void SimpleScrollSnap_SelectTargetPanel_m510BA969F490F81363ED5A0A47E57FEDE8AB52D2 (void);
// 0x0000023C System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::SnapToTargetPanel()
extern void SimpleScrollSnap_SnapToTargetPanel_m67107EE40C9C006EB2FE3FD8F0128D01DEA818F2 (void);
// 0x0000023D System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnSelectingAndSnapping()
extern void SimpleScrollSnap_OnSelectingAndSnapping_m40225CC288101B05F48DE9D136071AD511484B0C (void);
// 0x0000023E System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnInfiniteScrolling()
extern void SimpleScrollSnap_OnInfiniteScrolling_m78F13037007C1ADAB07D15B74A9257C5528B7268 (void);
// 0x0000023F System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnTransitionEffects()
extern void SimpleScrollSnap_OnTransitionEffects_m4E4A14E55A4CEE6AD4EA602BDD6A14A345982C8C (void);
// 0x00000240 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::OnSwipeGestures()
extern void SimpleScrollSnap_OnSwipeGestures_m655C228F042E1645D3D6247A917FF6F3F42D0737 (void);
// 0x00000241 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::GoToPanel(System.Int32)
extern void SimpleScrollSnap_GoToPanel_mCF45C7E388B917C01C9BA91FA55DB00798CB43E2 (void);
// 0x00000242 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::GoToPreviousPanel()
extern void SimpleScrollSnap_GoToPreviousPanel_m1D59A398191245B80211B53ED674162D59399D19 (void);
// 0x00000243 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::GoToNextPanel()
extern void SimpleScrollSnap_GoToNextPanel_m40B209D86FA4212F5719BCB2568A7E603466C930 (void);
// 0x00000244 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::AddToFront(UnityEngine.GameObject)
extern void SimpleScrollSnap_AddToFront_m4235E5545FD9AA39C05839DF317D09472212560C (void);
// 0x00000245 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::AddToBack(UnityEngine.GameObject)
extern void SimpleScrollSnap_AddToBack_mCD99433469A2D18F9A1A045A41DE3DD852F44F38 (void);
// 0x00000246 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::Add(UnityEngine.GameObject,System.Int32)
extern void SimpleScrollSnap_Add_m72CBA1ED8A29C6719D37B0B573CBAA863964E761 (void);
// 0x00000247 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::RemoveFromFront()
extern void SimpleScrollSnap_RemoveFromFront_m1C37FDA00889204EAF7706D573CAEF1B6479080B (void);
// 0x00000248 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::RemoveFromBack()
extern void SimpleScrollSnap_RemoveFromBack_m35E271D3E49DFE4C70F5B2430142673FDCA38B17 (void);
// 0x00000249 System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::Remove(System.Int32)
extern void SimpleScrollSnap_Remove_mDD0EE00A08E0CD562E11DD7223F35D6F24279D49 (void);
// 0x0000024A System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::AddVelocity(UnityEngine.Vector2)
extern void SimpleScrollSnap_AddVelocity_mDD69CAA821FC1A69D59BDEF8DB4BAD33FB0B0126 (void);
// 0x0000024B System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap::.ctor()
extern void SimpleScrollSnap__ctor_mC0629B573BA873F4997CA742C39CD2014904C426 (void);
// 0x0000024C System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_mAAC41ED533EA9EE78896566F3E4F3A3551721C15 (void);
// 0x0000024D System.Void DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap/<>c__DisplayClass77_0::<Setup>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass77_0_U3CSetupU3Eb__0_m181DD579666233F022CF2645C3A8A3D84030AA66 (void);
// 0x0000024E System.String DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::get_Label()
extern void TransitionEffect_get_Label_m4AB3775D78A1A51618D4A96EF8CE712411FDA98B (void);
// 0x0000024F System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::set_Label(System.String)
extern void TransitionEffect_set_Label_m71D57E08620AFBA00E627DA61F3AD0B510950B59 (void);
// 0x00000250 System.Single DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::get_MinValue()
extern void TransitionEffect_get_MinValue_m0379F7CF8098042455A61291EB5C1B8C2D14446D (void);
// 0x00000251 System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::set_MinValue(System.Single)
extern void TransitionEffect_set_MinValue_m6FE1E7F46B1EBC7CA2ED849DD28CA541CE86017A (void);
// 0x00000252 System.Single DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::get_MaxValue()
extern void TransitionEffect_get_MaxValue_m86D5260E5ECAB96EC0707875858A701C9791499B (void);
// 0x00000253 System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::set_MaxValue(System.Single)
extern void TransitionEffect_set_MaxValue_m8EA7812145C61109C2346E629A9BBBCC0AFBCBF0 (void);
// 0x00000254 System.Single DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::get_MinDisplacement()
extern void TransitionEffect_get_MinDisplacement_mBF11AD093821E581F1A50D94CD4CAA5A8AB40174 (void);
// 0x00000255 System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::set_MinDisplacement(System.Single)
extern void TransitionEffect_set_MinDisplacement_mEBA9ED7A5F5E9F41C3047F35D69A32F749AF7B61 (void);
// 0x00000256 System.Single DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::get_MaxDisplacement()
extern void TransitionEffect_get_MaxDisplacement_mCEA4D663D0E462E11CA542947C37FB89A2181758 (void);
// 0x00000257 System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::set_MaxDisplacement(System.Single)
extern void TransitionEffect_set_MaxDisplacement_m1788D0FE93B137139B125727B8790039CB5FCD0B (void);
// 0x00000258 UnityEngine.AnimationCurve DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::get_Function()
extern void TransitionEffect_get_Function_m9930C0BC2541378A943FBF08ACE8409FDD5617A2 (void);
// 0x00000259 System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::set_Function(UnityEngine.AnimationCurve)
extern void TransitionEffect_set_Function_m55CB820A33BAFB18C2F7379EB9316A78804810A7 (void);
// 0x0000025A System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::.ctor(System.String,System.Single,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap)
extern void TransitionEffect__ctor_mA4738E77E13C0FD89F6104748DB96BF4910596B2 (void);
// 0x0000025B System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::SetDefaultValues(System.Single,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve)
extern void TransitionEffect_SetDefaultValues_mFF2E440FB958FB3B432E1A8DA4AB870DEF4CBEA7 (void);
// 0x0000025C System.Void DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::Reset()
extern void TransitionEffect_Reset_mFB8DF968AF24E4AC2619CAC418096ACB7257327A (void);
// 0x0000025D System.Single DanielLochner.Assets.SimpleScrollSnap.TransitionEffect::GetValue(System.Single)
extern void TransitionEffect_GetValue_m40AF086414C3A9F225DA899712F939F04AF9549E (void);
// 0x0000025E System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[606] = 
{
	request__ctor_m3465B691D7F2F23A8CC54490EEB50B8F220E3777,
	Customizer_get_ImageCount_m9A24772F85B81CC0DE4B9C46867991AFC3CC22CB,
	Customizer_set_ImageCount_m2B2853BED33B92966956CC4839CA99B0730EB764,
	Customizer_Start_mBAF79BFBC3149D847D5A018556A0DEB59A9D26D3,
	Customizer_Update_mC1BB61D9E8E9693369CDAFF1AC7BDF5DA54E3301,
	Customizer_treat_mD09CFB83D5DDEC6CBA79CD36963EC4F5897D60BC,
	Customizer_convertPosition_mE828013ED406CB981ABBC26A3869A8DD2B978636,
	Customizer_toColor_m69A765CCC1778175A95ACBDF32996892294EDE8F,
	Customizer_SpriteFromTexture2D_m22F3C9A2323B75948C6D2742A32BBC374BD830CE,
	Customizer_showContent_m3889AB77995B481136B58C4AD766A2D0B2EBC893,
	Customizer__ctor_m86CDD261C9ECC286DA11192707346A62360111ED,
	Customizer_U3CStartU3Eb__10_0_mFC7C8E187BD7C36F36DEE117E2391927A0FE8B81,
	Customizer_U3CtreatU3Eb__12_0_m735B07CBFF747B143A745027920E8072099590F5,
	U3CU3Ec__DisplayClass12_0__ctor_m07C60FA481F681395CDA3C2F7D35D06D811D54CB,
	U3CU3Ec__DisplayClass12_0_U3CtreatU3Eb__1_mDB501A88F27D66E6242B21847356272E0F677CB2,
	Fetch_downloadText_mE26F63971A466CC2ACBA2F607A8CE2ABD78D7329,
	Fetch_downloadGetText_m85B48FDD4F8BA4CFE5CB6D7B2EAC94D1081C9224,
	Fetch_downloadImage_m292D9436BBD741ADD1159ACD372308448898E126,
	Fetch_saveCashe_mDA0B5425E4B9B50CC0A3E6A1BA6E47F779C7A68F,
	Fetch_checkCashe_m10336D4CD9DD90DAFC89B385CE75071833FEAD63,
	Fetch_downloadText_m1D7AEDECA401D81E0FE0B80242A6F11B1244CBC6,
	Fetch__ctor_m1DDFF51693A8A2AA73E6473720801492F9B21C97,
	U3CdownloadTextU3Ed__0__ctor_m8B1F338D399BBDFB1577FE3078058A0C0ECEB996,
	U3CdownloadTextU3Ed__0_System_IDisposable_Dispose_m023B93775875DDA5FC56B136B53B39BE3476C3E2,
	U3CdownloadTextU3Ed__0_MoveNext_mC6FAD378F34B9A13C8DF94E0F54ED5495A397FFA,
	U3CdownloadTextU3Ed__0_U3CU3Em__Finally1_m71DB5A3CCC8D9140579DB21B71A789FA0378C8FC,
	U3CdownloadTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D70CD4D54E4A073567F9044DB5EBA58485BCE69,
	U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_Reset_m5C6238E1F272A88B022EF1D5BD33E1A2F4E54E57,
	U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_get_Current_mF8C7FC36607E131889A9A9AED8331CE043370F57,
	U3CdownloadGetTextU3Ed__1__ctor_m48C2E8D4A6E69416525D1D10ECA4357A1F709158,
	U3CdownloadGetTextU3Ed__1_System_IDisposable_Dispose_mEAD4E07760EC46CD000717CF865BA36785AF7C4B,
	U3CdownloadGetTextU3Ed__1_MoveNext_mB861E290678693DED1E32083A0F02B1587DCCFE8,
	U3CdownloadGetTextU3Ed__1_U3CU3Em__Finally1_mFC32DE6F4A3C654D84BC8C98E0696E20200778AF,
	U3CdownloadGetTextU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4723D0018EAA7B6A932E0937FA85661288EA808F,
	U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_Reset_mBFBD2050CD5EBB6324D256BBDC9A43484B739600,
	U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_get_Current_m6BC8285F42611EBF06A5F784D34952F7FF2CB13C,
	U3CdownloadImageU3Ed__2__ctor_m46565F438AEA7CC91C53F58D968DA3C585F70003,
	U3CdownloadImageU3Ed__2_System_IDisposable_Dispose_mE5405D9D37A7D2F1D164A3B8C86D3284E4E91B16,
	U3CdownloadImageU3Ed__2_MoveNext_mAE4AB7C9532EFFED9DF3B2AF172856E823A86571,
	U3CdownloadImageU3Ed__2_U3CU3Em__Finally1_m06CE2DED9FFD0B37C5DC83DBB8340ABF579F5C18,
	U3CdownloadImageU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1E5DE562F1972533B57AD4C08E9003DF3FB8E44,
	U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_Reset_m6AEBE2D189CBBD69C57BB6B363D4B1CBFE11EF06,
	U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_get_Current_m44621CCBB0A8DF7A54D323974919F4424397F182,
	U3CdownloadTextU3Ed__6__ctor_m736D4A1F1D30BB619CC8AF938713E9B93B0C75F8,
	U3CdownloadTextU3Ed__6_System_IDisposable_Dispose_m0C927DFD368CFE1E6F31DDF44039D8BD71B8229F,
	U3CdownloadTextU3Ed__6_MoveNext_mBAC7F8ADC7A3CAEFE82E8F377FB79D36C4245229,
	U3CdownloadTextU3Ed__6_U3CU3Em__Finally1_m1D788943C3D14002BF5A937AFC798DE27C96D9BB,
	U3CdownloadTextU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m597DBB3BF363A420B77DB60D2B4D574445409836,
	U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_Reset_m0751DCDC13928AA5D907AC7B36516C65D707DF14,
	U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_get_Current_m4DF213FF52A5A077F31FF7451F20153D024682C5,
	Asset__ctor_m5E0FACFA7CAE61B737CB9E6EFB0B08DFD33F8D99,
	Shape__ctor_mBB98685051DAF006098FABCC1A2F5A3797DC1852,
	Page__ctor_mC8763F85034F7230967ACCDDFD98204CD50A3451,
	Project__ctor_m0444A6D59081677990AA152E3C8C2E87349FF3A6,
	CustomizationResponse__ctor_mD43C4A534DFCE2BFD84A3E748C2209188B46F224,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m0AAD92A3D682C1DFC1B5630585550B6CAFBAFE32,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m4761377095817FA81908BC43FD08EF600F6CC231,
	ARFeatheredPlaneMeshVisualizer_Awake_m7FD9AC4C30D2F12A0A30CC3CAD9EC6C17B07C45D,
	ARFeatheredPlaneMeshVisualizer_OnEnable_mEE0AFFC028F876DE841312FEB8CA15FEAD105FE1,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m51213AC9D2DBD97C93CC41330F65D74661D5DC81,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m354D58D0B29B40FE6D3A6689590E271D9282323F,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m241EDA47E1C4DECA8CCBC61D8DF6EC300546CDF0,
	ARFeatheredPlaneMeshVisualizer__ctor_mA27A238E88D5FBA54D4DDFE588080DD41B449A4C,
	ARFeatheredPlaneMeshVisualizer__cctor_mF5C9F857AC060614B46B9359B3F0859FBC309DF3,
	BigFileTransfer_Awake_m3DEF7001822DA6F451CE793EB0A096679E8EB156,
	BigFileTransfer_Update_mFD49BFED4143A3914104F9CB4F9F041B9EF90F9D,
	BigFileTransfer_WriteAllBytesAsync_m203535D4F5FFD755FC3A766D57A86FCEA792BD61,
	BigFileTransfer_NowCheckForResourceUnload_m5EB71F677981F73F4F3775E68423D6059B0AF4FB,
	BigFileTransfer_SaveDataThreaded_mA5C5FDB58E4A1F7A2AB18E5EEE93BBC1C726DD70,
	BigFileTransfer_SaveDataTaskThreaded_mE6BBB22CAF0CB940232EFA25C06BA14CF02362FA,
	BigFileTransfer_CallCB_m95B9E0802B30608309E607A043BD72E63368DB71,
	BigFileTransfer__ctor_m15D9B6FA79B456BA0F46ED784A1C8AA6CAF12042,
	OnWriteComplete__ctor_m5E4DDE4A6D08C1ECB6F6100EC1FAB7FD6C7F9ED2,
	OnWriteComplete_Invoke_mB117DE7BD5013CF9D387CA6B0412B9561DEB16BB,
	OnWriteComplete_BeginInvoke_m90B2DA8CA82E504522789525ACC015BA48DD7DA4,
	OnWriteComplete_EndInvoke_m6CD14295AE8ADDDD20D989E5397C6352509A97E4,
	U3CNowCheckForResourceUnloadU3Ed__5__ctor_m4810B14070C5216BCA0F1F217A7B3653BC66F5B1,
	U3CNowCheckForResourceUnloadU3Ed__5_System_IDisposable_Dispose_mDA95B5C816AEC937D165CF6D263C7085959E6A2F,
	U3CNowCheckForResourceUnloadU3Ed__5_MoveNext_mE46648EBD7BCC6DA137B5AAE98433A652E2A8FB8,
	U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD8597AAA4D7CC5CCC1EA88DCA8417825EAE7C2F,
	U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_Reset_m61B8907EC6A8728FEB290A78E71268521E2E7EAB,
	U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_get_Current_m47DEB1A7423358FEA2F80C95D805C376FB593CC6,
	U3CU3Ec__DisplayClass10_0__ctor_m587A46B8D133929D37561EAA49B7D464BB4CA5B1,
	U3CU3Ec__DisplayClass10_0_U3CSaveDataThreadedU3Eb__0_mAA7E7571A76648C83BA1CCAF074FDDB008C728B3,
	En__ctor_m9972465DE60A76F3037CB864D80A8CF56D200757,
	En__cctor_mEBCDF74FF01CC362892A9345712C4E06D6C16C6F,
	ExtensionMethodes_RemoveQuotes_m9A717CF268B56CC0E5DE3D67272B856909B8E166,
	ExtensionMethodes_ToJSON_mCA4FCBE50520BBEE0533C4BB26710BAC73021462,
	ExtensionMethodes_Invoke_m0A97AA69FF14FC349F826EA8CB5E22F7E958A50D,
	ExtensionMethodes_InvokeRoutine_m68D13618A04F1101FC57EB9B7F6A6240EF47E027,
	ExtensionMethodes_fetchText_m5BC095AD33A5648A3A1F5A8DCC2966C70EC96D45,
	ExtensionMethodes_getText_m1C9828B2FB479308B4245F59087EAF0C4232B7C2,
	ExtensionMethodes_fetchText_m41CC23C150225080757EB0305E55CC9F747D79C3,
	ExtensionMethodes_fetchImage_mFFAD95B615A556F8FAA456813FF7C0F4A4ECE4EC,
	ExtensionMethodes_fit_m6CE160463F7C2D2A0E3CE704DB5D2A45E48768F9,
	ExtensionMethodes_fitIn_m098C6C9472334448B87968472E68FC0E7EB7A7F7,
	U3CInvokeRoutineU3Ed__3__ctor_mDE74617B2BB991AC518479ADBD4FC449F2CF8713,
	U3CInvokeRoutineU3Ed__3_System_IDisposable_Dispose_m6ADC93DE5909213D7180E3892F23EEF1391A085D,
	U3CInvokeRoutineU3Ed__3_MoveNext_m4EAF175A2FC58F5D2337F27CB612175920E443CC,
	U3CInvokeRoutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DB589A82DEB3980CD467F5210816729B6332E2E,
	U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_Reset_m1054613E0F54ADCCA9F9B985EEF2D99D23816070,
	U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_get_Current_mCD45E7F0EF779A679CC0267BB47747283483AD63,
	Fr__ctor_m67A01EA3CFCB8910B267D09BEBC46D9B60A15014,
	Fr__cctor_mE12E837B374D924116AA5E5C6A22E0A6855A0E54,
	Lang_choose_m763C64A4E4D7FD22E9FAD0E164FABA74E659DF16,
	Lang__ctor_mCE2F385716D35E9A682DCF321C127924677B99DC,
	LeaderBoard_Start_m662CE1902678089ED1725BD705C2D55569FA2717,
	LeaderBoard__ctor_mFA9C06024BBB4E24403E2AD790398E1CCF52C94A,
	U3CU3Ec__DisplayClass1_0__ctor_mF8C0169B77CF687227A3B20DA547A55DB023A5BA,
	U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m2390D2ABE3F0E1F7127E5A02CF1B995C743EE6EE,
	U3CStartU3Ed__1__ctor_m3678519FEB2A830B52694CC1A8F920BC498ACD37,
	U3CStartU3Ed__1_System_IDisposable_Dispose_mFAE344D106FD947608664C6EAA75C92211E6D1F8,
	U3CStartU3Ed__1_MoveNext_m91C31E301F8AA9095C0F27F6BE0B6EFA5C39E93A,
	U3CStartU3Ed__1_U3CU3Em__Finally1_m2155FD071FECB130DF888301994791996BA97CCA,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B7D1CA1CFA697529F4FA22E71C7E4B74771A002,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m6C97C235FF601436BEEE231C487C46F7E51D8E1F,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mAA56FDC02462F6064F0966239B3399EC2E3FE7CD,
	PlaceMultipleObjectsOnPlane_get_placedPrefab_mF30814062D1103F3CBF52DBE4EF5B8699F471AAD,
	PlaceMultipleObjectsOnPlane_set_placedPrefab_m99A4FD1A33F645E357B0A062A338BB67E7B5ED3C,
	PlaceMultipleObjectsOnPlane_get_spawnedObject_m6E6DC83FA54E6A49680A69B3D087119A2F6010CF,
	PlaceMultipleObjectsOnPlane_set_spawnedObject_m101BBFC64960985CF1A5BC47C788741012A02728,
	PlaceMultipleObjectsOnPlane_add_onPlacedObject_m03FFE730BC4665310982D88817DD2F01CAFE4DB8,
	PlaceMultipleObjectsOnPlane_remove_onPlacedObject_m903CF85547E4B7FA691C36447C003F5FC41A54A5,
	PlaceMultipleObjectsOnPlane_Awake_m7B98B03191D41539EC8B524F5CC746A069380F72,
	PlaceMultipleObjectsOnPlane_Update_m1D53B5D4D0F9858D32E97342EF45397FEE6B0BED,
	PlaceMultipleObjectsOnPlane__ctor_m4593FA1117FA14F18171B62A47CB4B5E35CCF981,
	PlaceMultipleObjectsOnPlane__cctor_mFF9A32602D844813D277B71234AD5A1038E15BF7,
	PlaceOnPlane_get_placedPrefab_mFF491A996FB1370DF94BAA02291EF06ADD19BEBC,
	PlaceOnPlane_set_placedPrefab_m36395BA83344E138FE2CE24CE0B213AE83B4A181,
	PlaceOnPlane_get_spawnedObject_m3EEB260F2583438D88633A58DB2A0E7C9FC9853E,
	PlaceOnPlane_set_spawnedObject_m2FCF31C894AF24CB5E36FA21620911FC41270E7D,
	PlaceOnPlane_Awake_m07F667A8D11C4C52B0D6E80EC6BFBD3A6DCC9F4C,
	PlaceOnPlane_TryGetTouchPosition_m3A154C1C41BB6BDED6BDDAC542AD3B1B0C06E033,
	PlaceOnPlane_Update_mB0B5CF33955B4E0C22674FEC89BDFAFF9031D878,
	PlaceOnPlane__ctor_m6A163443FEBA846E4B12847076EC8482911BDAFC,
	PlaceOnPlane__cctor_m179DE5DFC70769680B7A879566D59E759EBB62AA,
	PlaneDetectionController_get_togglePlaneDetectionText_m653DCAF71188ED390D7C3262A340264851058DF3,
	PlaneDetectionController_set_togglePlaneDetectionText_mD749209D5E6473CAE65DAE42CF74390DA8614B1F,
	PlaneDetectionController_TogglePlaneDetection_m5F3F97C553B68B64F7FE08CFECCD7260B3B72843,
	PlaneDetectionController_SetAllPlanesActive_m8C0CCC2235D2F7B0EE5C6DA4B7C3BE69F5321B25,
	PlaneDetectionController_Awake_m5D161C3A0ED5E8FF1035B1BB3F69FF1D767FDA9A,
	PlaneDetectionController__ctor_m33E62EAD0CA6CB93196A504646793B990311FC9F,
	NULL,
	NULL,
	FadePlaneOnBoundaryChange_OnEnable_m9A290421CF90C45F885B14F3E1DA888BFD27A340,
	FadePlaneOnBoundaryChange_OnDisable_m0C62572D4B470C313E0B203F6AE948865F395825,
	FadePlaneOnBoundaryChange_Update_mBB620E21C1BE2FE4EF8F45D1C14EBE6C5579F8A8,
	FadePlaneOnBoundaryChange_PlaneOnBoundaryChanged_mF362091F455EDBF9437C0B672D7CE83ED4CD20CC,
	FadePlaneOnBoundaryChange__ctor_m6625F1DF41CF81E94037383DFBEDE2FB0237096E,
	UIManager_get_cameraManager_mCBC76B9FAD2889BDF675423F41E0D8F45A4D9B9B,
	UIManager_set_cameraManager_mE84105B8732CF3F5AF6A7D0D431E071026EC8B40,
	UIManager_get_planeManager_mC295F7015EC0AFA6BF017218A3D50FBB5F8EC93B,
	UIManager_set_planeManager_m00D2D98C5B74891D8CE5F847317C28A7B4A992B5,
	UIManager_get_moveDeviceAnimation_mCEBB895CDD2C29B0E5E876812574CB239095AC86,
	UIManager_set_moveDeviceAnimation_mEF7CB4D795D5D4135F27BE428D348BAF300D844B,
	UIManager_get_tapToPlaceAnimation_m0285215E48E9D5A0E45DB14351D359B849BA1BBB,
	UIManager_set_tapToPlaceAnimation_m626FD1527F2416C37546E95C2974B0F567CFC74F,
	UIManager_OnEnable_mAFE10D24F19B379487455D635E4A3C65D4B64240,
	UIManager_OnDisable_mBAED7246344396BA861ADE4603C663A9ECFBCDEF,
	UIManager_FrameChanged_mF4EDFEC89AA3118757706CC79E22A42ABE245030,
	UIManager_PlanesFound_m4815E9F0871E527DBBCD7A190B7727D65D482622,
	UIManager_PlacedObject_m984A9FFFEE0AD12C887C1F0C67B8C259A1888E40,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	UIManager__cctor_mEEEBEC7EE33138DE838943638C73F7592DDBE3B8,
	cashing_LoadJPG_m297FF062BE657C8A5C24E746346B7C6AE8216B87,
	cashing_LoadAssetBundle_m2A8F39B091A6E2678A513F029728116BAD6B321A,
	cashing_load360InGameMode_mAC10C32153021F0DE28F29DAD37800D5DEC44469,
	cashing_loadGameModeJson_mD2C7A0F9A806D7D5E944D49E0CB9A0E8356084A7,
	cashing_getAllTps_m63B27A953D9BBF10760CE224ED695E490CFD1050,
	cashing_download360_m0A0CEDC96DF65F9AC90CADE6C1BAC75D9D196E87,
	cashing_downloadForGameMode_mA8EF232F20FF4C0B9DE8F25580DC2185B7F6494A,
	cashing_download360ForGameMode_m2D430260CD0B1CE0E80849BDB2C623BDD57963F0,
	cashing_download_mE46059585947898E3F841CBF2E20DCF0CBCB3F42,
	cashing_getPortalsJson_m36A6E9ECB959C23FE3F2AC3BDC4ED6738B842700,
	cashing_getPortal_m44629A7892D19B1D30F933394D2E9C82A23211F9,
	cashing_getGameModeJson_m6E1164B53A6B983E1CB7A91FB0254D141C878B68,
	cashing_SaveData_m083A5C4629AEF8AE098715AD7B5CA2918520E0E7,
	cashing_LoadData_m3ED87876A5C955FC3B4201CDCA2C94FE2E66430C,
	cashing__ctor_m9EBACAE916558B194318B78ADFA03C0799FBB883,
	cashing__cctor_m629F36D102D8D6255F6560871D36146ABF44BBBC,
	U3CLoadAssetBundleU3Ed__11__ctor_m27DB2E4150ED40D54B6D7AC09A233248B2892300,
	U3CLoadAssetBundleU3Ed__11_System_IDisposable_Dispose_m757415C4F7CCEF82DC8DD2BB460FFD3483BCF4A4,
	U3CLoadAssetBundleU3Ed__11_MoveNext_m24B35E51DAF44C74FAC89B511ECF521452E36908,
	U3CLoadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75089F44DE96500C0BF6967D14008FBF15EDDD24,
	U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mCC75770FBA53EB5CFA7D41DDD37C694BC01FBD84,
	U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m6C541003B1786239D2669D83C3568C39A97D13DB,
	U3CU3Ec__DisplayClass12_0__ctor_mB1D9B95DAFB0862B3CB47DCB4E30FAE50A8F3A25,
	U3CU3Ec__DisplayClass12_0_U3Cload360InGameModeU3Eb__0_m8FFC2C7B3A8981390314DA720A2F59CF0E40091B,
	U3Cload360InGameModeU3Ed__12__ctor_mE83D806E0EE2815CAB01D8B62CF1E7FBB6A2BC36,
	U3Cload360InGameModeU3Ed__12_System_IDisposable_Dispose_mD73B9A03551E2E0B1DDE124768B4637E36901413,
	U3Cload360InGameModeU3Ed__12_MoveNext_mFE86DE9D3623D814EE58EE41E953A01AA4F42693,
	U3Cload360InGameModeU3Ed__12_U3CU3Em__Finally1_m8E6DF178B666E4C984D268AA7D498E7EF9CB0BDB,
	U3Cload360InGameModeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFE70ED4FAF646F8C999092EBA78F42076C88CEC,
	U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_Reset_m6108BAF5C6A492E6A5B33A9FE215256428726EFA,
	U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_get_Current_mCFAF5DAA51544C6913BA7C2CBB0CB047B5635D45,
	U3CgetAllTpsU3Ed__14__ctor_m51508F758A4EE470759F88A90E33C10CA651C4CA,
	U3CgetAllTpsU3Ed__14_System_IDisposable_Dispose_m59A4443BEAFEC7E72871F04F73ABE41CBB20953D,
	U3CgetAllTpsU3Ed__14_MoveNext_mCF04E6F15D13548DD5852D393EF4E0D2DACE08D5,
	U3CgetAllTpsU3Ed__14_U3CU3Em__Finally1_mCBAE1809D44A070A97850CD013AEC896D56811C4,
	U3CgetAllTpsU3Ed__14_U3CU3Em__Finally2_m7A6D54C36292BF9E4CBDCC0426553DC0EBFBCC4C,
	U3CgetAllTpsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3280A4DC4A2D656206EF0199368AA999FC3CA53D,
	U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_Reset_mC2E810E082B85571FA8F14A2EE0F9BEAF1020D10,
	U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_get_Current_m94A816C63B45183CBAE8C2BFCB9A4AB1F458BAF1,
	U3CU3Ec__DisplayClass15_0__ctor_m5EA7980BA558D9C99CB313C535760DE5FDC715ED,
	U3CU3Ec__DisplayClass15_0_U3Cdownload360U3Eb__0_m7DE33E3A8DE91D784B01BFB85FF851A4F87C2D05,
	U3CU3Ec__DisplayClass15_1__ctor_m748BD68187FB39CECAC059F24E8901004D66F369,
	U3CU3Ec__DisplayClass15_1_U3Cdownload360U3Eb__1_m92CBF2C7FA68346AABDC480D36BF3AA35EED0916,
	U3CU3Ec__DisplayClass15_2__ctor_mF6B970CAFFCFEC2E9762959C1E8F9111F067376F,
	U3CU3Ec__DisplayClass15_2_U3Cdownload360U3Eb__2_m9408AF4ACFC8BC480EA105807539C5F14321F9FF,
	U3CU3Ec__cctor_mDE213E0F57BF186318E686CC6AD90971C61BA152,
	U3CU3Ec__ctor_m369369D83D1D26E1E208A9FB0EC00580E435E7FE,
	U3CU3Ec_U3Cdownload360U3Eb__15_3_mDE5B4F54FC6BBA343173E62AE3807261B7426643,
	U3CU3Ec_U3CdownloadForGameModeU3Eb__16_3_m4E6A64CAD71D7F5DFAF96B9BCC473056D16E04E4,
	U3CU3Ec_U3Cdownload360ForGameModeU3Eb__17_3_m1E675618580E5AB27184D4F3D112E270B6E7E8B6,
	U3CU3Ec_U3CdownloadU3Eb__18_2_mD5111E100BDB131D54D541FF1662BE14661CBCC1,
	U3CU3Ec_U3CgetPortalsJsonU3Eb__19_2_m5F32E167D6F570CAF2A4DFC9F399A7061945AEAE,
	U3CU3Ec_U3CgetPortalsJsonU3Eb__19_3_m3505633961C18A45E3A269E70D7924B741687692,
	U3CU3Ec_U3CgetPortalU3Eb__20_4_mD816592E34CE805B6C7A8FB7BE7F0709245277E7,
	U3CU3Ec_U3CgetGameModeJsonU3Eb__21_2_mC4A052FF6FEE61D7087E4EE081E105FFD6786A16,
	U3Cdownload360U3Ed__15__ctor_mFB7F1D6BC6F63CC8129E1EDD9F042D86A2C247A4,
	U3Cdownload360U3Ed__15_System_IDisposable_Dispose_m33E0DEAF9648839BD56911E0484ACFB603D4B02E,
	U3Cdownload360U3Ed__15_MoveNext_mF86203CE112F823939E80CCB5918A46FCF72BCFE,
	U3Cdownload360U3Ed__15_U3CU3Em__Finally1_mECB1020734D77DEDE5BDAFE9D3728897A7B90745,
	U3Cdownload360U3Ed__15_U3CU3Em__Finally2_mE8ADDE0C284ABC444119E8C8499E76E3AB621950,
	U3Cdownload360U3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m691B920495544D18CEEFCE8127C290181909C4AA,
	U3Cdownload360U3Ed__15_System_Collections_IEnumerator_Reset_m1B4C4BAB956CDCFB14DE8386FE2D17A088CF4463,
	U3Cdownload360U3Ed__15_System_Collections_IEnumerator_get_Current_mA5E9CEDC5B9EB29BB4F38156CA7D4B4B07B5022C,
	U3CU3Ec__DisplayClass16_0__ctor_m5201024652BD3259E6854FBB87AA8707DF84C4AB,
	U3CU3Ec__DisplayClass16_0_U3CdownloadForGameModeU3Eb__0_m96D9D7A4C9107134340D7717C2FC87ABB40B18BE,
	U3CU3Ec__DisplayClass16_1__ctor_mA24204E9C53670035C9ACD385F7FB4E7FB4941B8,
	U3CU3Ec__DisplayClass16_1_U3CdownloadForGameModeU3Eb__1_mD59017FE98537AE0F17B410F6ACD82905DA34C42,
	U3CU3Ec__DisplayClass16_2__ctor_m7C3219803A1B42E29D4ED0647C0EEDE6CC88ACA4,
	U3CU3Ec__DisplayClass16_2_U3CdownloadForGameModeU3Eb__2_m6C9F66AD94950290EA29357BDA4109A3EF817719,
	U3CdownloadForGameModeU3Ed__16__ctor_m472E02B641AFDF72C75D3725D7126F85EFC9BD53,
	U3CdownloadForGameModeU3Ed__16_System_IDisposable_Dispose_m0EA57873FAD8370AE52B273CA4EFE711BDF18933,
	U3CdownloadForGameModeU3Ed__16_MoveNext_m41C89490821135292017F0253FDBFBDF5EACD166,
	U3CdownloadForGameModeU3Ed__16_U3CU3Em__Finally1_m0F54C5EE8816B4C30AE1F19A2440DFFA9B71CD3A,
	U3CdownloadForGameModeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m358F85E1553EB93E39939F33714E1BBE1136BB70,
	U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_Reset_mA8F421E36E6A9C9EB392365B7788722EC3B6AA08,
	U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_get_Current_mADB9FC2B5CD54319C4C552D6E54FCB88F6E25DEB,
	U3CU3Ec__DisplayClass17_0__ctor_mF7A1E899ED83CB5B4EDE4BC19C5980AA469CE48F,
	U3CU3Ec__DisplayClass17_0_U3Cdownload360ForGameModeU3Eb__0_m84E295F68C4A5FD0DA97F69BF73DA4B2EC9C2989,
	U3CU3Ec__DisplayClass17_1__ctor_m946B519649780C1A8623591E60FA20C33C7887C9,
	U3CU3Ec__DisplayClass17_1_U3Cdownload360ForGameModeU3Eb__1_mAD2D2A85BCF8A6FF85139FA876056990ED8EEBFE,
	U3CU3Ec__DisplayClass17_2__ctor_m4CA7EFB26DA2B3231803BB17E5AFE518FC4011D6,
	U3CU3Ec__DisplayClass17_2_U3Cdownload360ForGameModeU3Eb__2_m90F4F2683A2101479ACD05C38310BEDDD9CD2977,
	U3Cdownload360ForGameModeU3Ed__17__ctor_mE9557100C0BA840F6F03FE0EEEFDC692ED8A6011,
	U3Cdownload360ForGameModeU3Ed__17_System_IDisposable_Dispose_m5DE4CA4615F57DF289C548A1631ACEE10BB11894,
	U3Cdownload360ForGameModeU3Ed__17_MoveNext_m9C89BA81D195208BC710FB0C316EED1F14C38963,
	U3Cdownload360ForGameModeU3Ed__17_U3CU3Em__Finally1_mF2471BE9DA31EDA96910B4EE83AB0B0D2E16A790,
	U3Cdownload360ForGameModeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC17E8C51DAC6D1212F7BA907F67C8030B1E3CE36,
	U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_Reset_mBBDB9EC2A4D88EED8B85216A582E109A4BBD31C9,
	U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_get_Current_m7FE0DBAF0EF8B6FD7A6D2D39C1BD3BAB76C6DF49,
	U3CU3Ec__DisplayClass18_0__ctor_mD7548752190C9F6007DEED586E95B687E4B87EEA,
	U3CU3Ec__DisplayClass18_0_U3CdownloadU3Eb__0_mE0EEEBE3BC70018207E9AF6686A922EF1B62B96C,
	U3CU3Ec__DisplayClass18_1__ctor_m75C0E5176EB2BD79CB3FC5EEBF387C3D0FBF9326,
	U3CU3Ec__DisplayClass18_1_U3CdownloadU3Eb__1_m057C04819B2328AB96234CE559A186BC16AE92EE,
	U3CdownloadU3Ed__18__ctor_m43E1E27480854B1FE2DB6B6DEAE884B7FBEA7134,
	U3CdownloadU3Ed__18_System_IDisposable_Dispose_mA29A63F245B20473233D4D16D370D84417CF9617,
	U3CdownloadU3Ed__18_MoveNext_m00F4A3B4AC20FA2A8479FA86BAD8167152758175,
	U3CdownloadU3Ed__18_U3CU3Em__Finally1_m17CB1C024A64603F3AB5F9F8C74407C8ED9AF401,
	U3CdownloadU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB4E2FC6B956A6AF4568DE51640B3ACC4BABE482,
	U3CdownloadU3Ed__18_System_Collections_IEnumerator_Reset_m946ACD78A5CA4F8B7E98BCE82ED7E3D9C97CAD3B,
	U3CdownloadU3Ed__18_System_Collections_IEnumerator_get_Current_m81787FF7DF119D442C3C3291F65A91B2A1271E18,
	U3CU3Ec__DisplayClass19_0__ctor_m13E1B0EC07BAA4B24EAE9C7916F3A71B55024175,
	U3CU3Ec__DisplayClass19_0_U3CgetPortalsJsonU3Eb__0_m1671AD71287DA82AB584E26B7C74F34121F4CCBE,
	U3CU3Ec__DisplayClass19_1__ctor_m3D593E1F39EEA553490C02B7A8017942AD44CBEB,
	U3CU3Ec__DisplayClass19_1_U3CgetPortalsJsonU3Eb__1_m48D15DBB36BF2BBD53450906ECC5360E20D3A3EE,
	U3CgetPortalsJsonU3Ed__19__ctor_m0D8998FC0BB840571494BFCF5E5059EC16CB430B,
	U3CgetPortalsJsonU3Ed__19_System_IDisposable_Dispose_m5C0DD30947E713AF6E1C124D915F9D9AF4D625CE,
	U3CgetPortalsJsonU3Ed__19_MoveNext_m440080B68D46A98D4BF8D7F8CE63498762AF5D72,
	U3CgetPortalsJsonU3Ed__19_U3CU3Em__Finally1_m3970B00A2C9BE7F4EA1DE3420438545C0D335AD7,
	U3CgetPortalsJsonU3Ed__19_U3CU3Em__Finally2_mFE99E8A705B59C52EBF76B0761116E01681DD32D,
	U3CgetPortalsJsonU3Ed__19_U3CU3Em__Finally3_mAB396157DC60021464EFCAAC1197C5E5FAC4E322,
	U3CgetPortalsJsonU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AF10A11810A70141E8CE7B926EEB339D650B226,
	U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_Reset_mE676442194F6699FC2B720F79D01A1F6EE85086A,
	U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_get_Current_m530301696820C06A9957D466F8AD0B3FC86A74A1,
	U3CU3Ec__DisplayClass20_0__ctor_m536FF7040331A502EDAC44C130186770023EC2A1,
	U3CU3Ec__DisplayClass20_0_U3CgetPortalU3Eb__0_mFB3453D16449B1CFF682BE4EF14F91BD02404E67,
	U3CU3Ec__DisplayClass20_1__ctor_mF1443FCDD2C982948C8D90A655EC4EEB3BC36B20,
	U3CU3Ec__DisplayClass20_1_U3CgetPortalU3Eb__1_m101CCD50B4CA7683EB3B34CED585AC13F1D3648C,
	U3CU3Ec__DisplayClass20_2__ctor_m79841F1B14EEE8D0F2C68E9F18BB5F50861A9EBD,
	U3CU3Ec__DisplayClass20_2_U3CgetPortalU3Eb__2_m6730F384E114CA31B8C096C5FB1013E4A3C3E369,
	U3CU3Ec__DisplayClass20_3__ctor_m9C1D130EEF572C67A7D93C32A02C728419AC851F,
	U3CU3Ec__DisplayClass20_3_U3CgetPortalU3Eb__3_m33AA51A6EAF53A0E3F573AF29547A3BC3366828C,
	U3CgetPortalU3Ed__20__ctor_m575CD131084A10603A0E610EF658BFB9091E420B,
	U3CgetPortalU3Ed__20_System_IDisposable_Dispose_mA6058E0DB176DDBEF05040D7F4D9BA588BCB094E,
	U3CgetPortalU3Ed__20_MoveNext_mD09B3AB3070758D567A2B2DE5375D245F62B696F,
	U3CgetPortalU3Ed__20_U3CU3Em__Finally1_m458379EAA73BCBC71AC2CBF6EA5A9C92442491B0,
	U3CgetPortalU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E4907A063AF1F0C4A0A3F4DE4520126AB94AE79,
	U3CgetPortalU3Ed__20_System_Collections_IEnumerator_Reset_m4095F38A748F61E9F7A2CAE7B8140FF0D95F408A,
	U3CgetPortalU3Ed__20_System_Collections_IEnumerator_get_Current_mDD7D091DBC38241704DCE7A16B08A60F7ACF54F6,
	U3CU3Ec__DisplayClass21_0__ctor_mE7E149879660855C361E0FE84D2936CE4F6FD7C6,
	U3CU3Ec__DisplayClass21_0_U3CgetGameModeJsonU3Eb__0_mB12D958BB33E51F81BB8B5D848FA42B4A78ED0D2,
	U3CU3Ec__DisplayClass21_1__ctor_mB997491DA8522C159A6685ADF274AB03C77407DE,
	U3CU3Ec__DisplayClass21_1_U3CgetGameModeJsonU3Eb__1_mB9752A2254D6A70C27227F7C5C0658D708DE457F,
	U3CgetGameModeJsonU3Ed__21__ctor_m33BA62505B1495742109388FDD4AD47D59299713,
	U3CgetGameModeJsonU3Ed__21_System_IDisposable_Dispose_m2BA528F86EB7740587B9DE8992F9ABEA4AE6F1E6,
	U3CgetGameModeJsonU3Ed__21_MoveNext_mE3CACF84A193C8BD6D1ED053FA5AAFEE9C0B6A33,
	U3CgetGameModeJsonU3Ed__21_U3CU3Em__Finally1_mCCAF4992522CDF2BC1DD29141F213678D90D7526,
	U3CgetGameModeJsonU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3682D310CCEBCC6227CEDEF4937C0FE58C4C0D15,
	U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_Reset_m5B4D96E2D62CA34C2DF12D1FB947F73C91C10D6B,
	U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_get_Current_m6C4AECAF32F3DF93C95D46238555A5F7671913DA,
	clickTag_clickPhoto_mDE87CBD6549B26D4F58DDFFEF88B2B061698F57A,
	clickTag_click3D_mE913A971F5F5A0645A78B989D8F61B944450F870,
	clickTag_clickVideo_mEFCC0AA122B1D43D7BCEED6794F6CD68176C04A1,
	clickTag__ctor_m0D97B92A65CADD05C2C2D7875AC34DF88CD31EAD,
	U3CU3Ec__DisplayClass0_0__ctor_mD5255203E0B5ECA9602203E03C5FE6263FEAB250,
	U3CU3Ec__DisplayClass0_0_U3CclickPhotoU3Eb__0_m61479DC4D441CBB99491595C089920024A7E95AE,
	collapsibleList_Start_m363413DE99F9481A60181A9C995D3D10982001B5,
	collapsibleList_goTo_m9729A978942C3D424BE2155B8B7B81BB26857377,
	collapsibleList_expandCollapse_mADFAD1573901F55E3251D47EA2ACAB29C6AC731E,
	collapsibleList__ctor_m4AC041173186CDEE3492FE09837C55F820CDCF99,
	U3CexpandCollapseU3Ed__5__ctor_mBFBDFACBF40F3A6AC86C6232A962D8D5A460733F,
	U3CexpandCollapseU3Ed__5_System_IDisposable_Dispose_m426B430EA932152626ED4F5A729047211B853E06,
	U3CexpandCollapseU3Ed__5_MoveNext_m916FE1316995E2FBE1C0D421D13C5325D32EDA45,
	U3CexpandCollapseU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B073DE4ACAC5B582EE2DC2358E6453237C630F2,
	U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_Reset_mF6D7DFDB099383114F4D246A1157C185A59D76CD,
	U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_get_Current_m8D4E549664F10A3B58AFA4921925EB42C6F57E8C,
	debugger_Start_m99932BEE820F7BB9709D80B0F99BB3F8544A48F3,
	debugger_updateCullMask_mDF8A77E2168EC7EADFB57D641E46931795FD6F03,
	debugger_updateStencil_mB4D1AF9F123178901F2D6C17C7145E5A795A7681,
	debugger__ctor_mA2E574938E73A95860B39B6507321A0C05F6CF45,
	deepLink_Start_mF733917446A85B560493030E3063AE7FE2A04B8A,
	deepLink_Instance_LinkActivated_mE880AD8D9ACC9FD246CDB389C744766BFB630BA7,
	deepLink_OnDestroy_mA60DB9014F7314B884769A1D517994352466DA2D,
	deepLink__ctor_m4693A61A052497F71B40D365A1D4E82351AAAAF0,
	errorHandler_Start_mCC31E5C3E9B92EE43275E1629F51BA9BB970E1A9,
	errorHandler_showError_m15FCD0CFE38D19BE0A2240E27A8005C52B6F3324,
	errorHandler_showHelp_m0FCF7FDD8D65B61CB58490E710B58F6E0883AE3C,
	errorHandler_showInfo_mDFDA75035938347BD688A7B2824D2BE0BA41CD33,
	errorHandler_showDone_mBB2D48AEC67F1A6D7E68EECE0FC5529DA5FFE677,
	errorHandler__ctor_mB81548B71F7F8DC4D04C7D809DBAF7197586A8AB,
	firstLast_ff_m838443D64A8ABBA8AA33F009DF98FCCA339A0013,
	firstLast_Start_mC25988C61747753138AAD8DECFA2B48EC61AF711,
	firstLast__ctor_mCC9CD0D285363E3A7B3805D91ACC32D5D8942E74,
	galleryImporter_Start_m0C28E1792CF565FA829FFCDA4787A28F23C99D30,
	galleryImporter_search_m5596E16A8E86E05C84D4E48AC96C3CBECAEAC3D0,
	galleryImporter_spawnGallery_mB8E622DDCEBFAB6D3D1462282F3104D8D42EB698,
	galleryImporter_addButtonNC_m21DF0F549E07767F44C1ACC23EC8DB7A05E6FC94,
	galleryImporter_addButton_m7BBFB9246430CD926A5E03FC5C67B90C8BA13375,
	galleryImporter_deepLinkPortal_m7BF4779CD3003283ACA0B3C4CE29A02CC217E4C3,
	galleryImporter_setPortal_m03AA7011878B44152E20186188DD20F05379CF1B,
	galleryImporter_fitIn360_m9D3FFF51D8CECA29AC6ED9A42D2A68B675B878EF,
	galleryImporter_fitOut360_m8CC60B2FF529A8FDA8A3A64946773BFED2594C69,
	galleryImporter_ff_m1A171CD132731B0A4A1E751B7334982A1A72DD15,
	galleryImporter__ctor_m27B904304DFD84F18E04A557D6387013E4397BB9,
	U3CStartU3Ed__27__ctor_m0B8811B30EB600364DF6C15D193DCAC339F6B626,
	U3CStartU3Ed__27_System_IDisposable_Dispose_mE8D87EB2F4514AEDAB6555362B72FC96A2E0EDB4,
	U3CStartU3Ed__27_MoveNext_m7766E3EEFA14B50CB49CC2ACF8EBADBEC02874DC,
	U3CStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA2DBFD21ADBAF930E826FFD85CAB9195E8665E5,
	U3CStartU3Ed__27_System_Collections_IEnumerator_Reset_m4029AE5DF84AD76E8097B4F4B258F88374A49793,
	U3CStartU3Ed__27_System_Collections_IEnumerator_get_Current_m882972B1CA3F6ADD16C5533231020519B1F64E2C,
	U3CspawnGalleryU3Ed__29__ctor_m0193ADC12AEB1D701A614D2B10A9F265EE4A9992,
	U3CspawnGalleryU3Ed__29_System_IDisposable_Dispose_m7FEBF588D867DC10A5585017993C635B8DAB45D7,
	U3CspawnGalleryU3Ed__29_MoveNext_mF0607F1BFBD70F7C262995CE7E2EFCA5EFCC4AC4,
	U3CspawnGalleryU3Ed__29_U3CU3Em__Finally1_m1ADCB9C0C82FF0AA64E0C2343D2C22447B295A4F,
	U3CspawnGalleryU3Ed__29_U3CU3Em__Finally2_mC02F00088687054BDD2B02C1C973B134B79E5CC0,
	U3CspawnGalleryU3Ed__29_U3CU3Em__Finally3_m20297ECB59BB3C5A1E9B9E4EF763DE1387506ED9,
	U3CspawnGalleryU3Ed__29_U3CU3Em__Finally4_m733DCF0488ED30E9ED0284276E2AF32728A450A0,
	U3CspawnGalleryU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4871A28B2322CB8FAB15A72BD66A58EEDD6B9629,
	U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_Reset_m32DC4030997459632CE3C216C330990BDF25503E,
	U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_get_Current_m96EFD1F62A0244F072445F09F66F821B22D8ED79,
	U3CU3Ec__DisplayClass30_0__ctor_m429E3B4FFF518E99BA28FE59FF9361A2302A6272,
	U3CU3Ec__DisplayClass30_0_U3CaddButtonNCU3Eb__0_m1FF396902F0828141E4FE5D3D5829F027B44DF8F,
	U3CU3Ec__DisplayClass31_0__ctor_m59CF9F17B52D376185429BFAC0880A5E93247ECC,
	U3CU3Ec__DisplayClass31_0_U3CaddButtonU3Eb__0_m0D671515FD03573A0E1C8D335205419239670431,
	U3CU3Ec__DisplayClass33_0__ctor_mA13E96F3F1C16426E2F389D1FD2FD53107EC5B0A,
	U3CU3Ec__DisplayClass33_0_U3CsetPortalU3Eb__0_mD3B68B9047191F63CB7DB4F9E5E1A65CCF10D23B,
	U3CsetPortalU3Ed__33__ctor_m58AD80E68A85B07032DC3D27D26BC188D3E43D5C,
	U3CsetPortalU3Ed__33_System_IDisposable_Dispose_m21A4150C350D623D1C5F6F9DF5A90F622A82D797,
	U3CsetPortalU3Ed__33_MoveNext_mCCCB64C30ABBE37988A00C6A2379894D21029C88,
	U3CsetPortalU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34134D144A2E3435E70851F744F08B6F705AC206,
	U3CsetPortalU3Ed__33_System_Collections_IEnumerator_Reset_mAA9424DFE9D8706A426B724705BC890DA5550A97,
	U3CsetPortalU3Ed__33_System_Collections_IEnumerator_get_Current_mD5F908FBA910960F3A26A4BA1D9AC922314F8305,
	gameManager_Start_m169616D522718461F2522FC60796972765513C0B,
	gameManager_setPortal_mA087AAC5911FA190935E3F15A8E68127F2563FDC,
	gameManager_setPortalNoReload_mC693A3202E474BD7084B3925328C1A45C44597B3,
	gameManager_next_mADD0A4EA4D45B942DE04516A3311E13C6AC6BC14,
	gameManager_sendLeaderBoard_mA350466B6838C990593636057E0F360174E3A11C,
	gameManager__ctor_mEC3390C453AE10969E79C65755A3BECB98211356,
	U3CU3Ec__DisplayClass21_0__ctor_m9700207C136BBD5CB21EDAF05F98512E2C113FD0,
	U3CU3Ec__DisplayClass21_0_U3CStartU3Eb__0_m8DF771ABE58A010BB92BFDBA5FB639499DB1C854,
	U3CStartU3Ed__21__ctor_mEF2B87CF03BF250F61C1E22922C890B32917C866,
	U3CStartU3Ed__21_System_IDisposable_Dispose_m3BC66AD8F8AE7802BCFD4D98362AA5305A338CC7,
	U3CStartU3Ed__21_MoveNext_m275F122A7009062EFDBD33A0D4219A18AB042EDA,
	U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5258F4F2C9E7DD1733DE60CE2DEC9597FF26A25,
	U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m145FD8392C86D200899A2C4599867A8D891F6DBC,
	U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_m5D6592FF1D9F9E1BC91BAF232D12E12097694C82,
	U3CU3Ec__DisplayClass22_0__ctor_m3719F66F9C4E415059098F273FD28094C351FA95,
	U3CU3Ec__DisplayClass22_0_U3CsetPortalU3Eb__0_m3395A9660661B7894CB8D999C2893F93097C6082,
	U3CU3Ec__DisplayClass22_0_U3CsetPortalU3Eb__1_mD130D202A49F2AB4FD85E3DCD1D4177C572C383B,
	U3CsetPortalU3Ed__22__ctor_m14C3F1881692CB63AC92768EC611CDEC88C8E3D2,
	U3CsetPortalU3Ed__22_System_IDisposable_Dispose_m933F60E78613B8F77909F38934408D65DACC7212,
	U3CsetPortalU3Ed__22_MoveNext_m3E3501AD49696AC0BBF1E1D418D1EBC2FF645DFE,
	U3CsetPortalU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D2B7D2355AC39A635454CAAEA2C9F29E38D34D0,
	U3CsetPortalU3Ed__22_System_Collections_IEnumerator_Reset_m8194E63ADEDFADC0621E20A60D8968E09281954A,
	U3CsetPortalU3Ed__22_System_Collections_IEnumerator_get_Current_mA9DE23C794BA2DEC3072A3E1B64A4624B705BB71,
	U3CU3Ec__DisplayClass23_0__ctor_m40C44B7BBB71767A3AABA65C5D556067C04760B9,
	U3CU3Ec__DisplayClass23_0_U3CsetPortalNoReloadU3Eb__0_m998020E137FE77C2C6B481B1DCA3CA250C72F323,
	U3CsetPortalNoReloadU3Ed__23__ctor_m1767EFC32956AAAF6F7D60E14EE15C19F411BF49,
	U3CsetPortalNoReloadU3Ed__23_System_IDisposable_Dispose_m48415DE3EB47BCF40947029C0FDA0AC0071B4C09,
	U3CsetPortalNoReloadU3Ed__23_MoveNext_m2883FDBE265AB075F2CE38E8B9CFFB9537F1B8AE,
	U3CsetPortalNoReloadU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45321B203FA4AE4176FE9E7778990D99605467AC,
	U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_Reset_m36BC0AA0642EFFC8B82559416D679FD080578627,
	U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_get_Current_mED9BFBAF66CB09E82ECA7BBE6F9E3F531487D95D,
	U3CU3Ec__DisplayClass24_0__ctor_m63334B167D0BF3BD2E349FE6205E168E9599D683,
	U3CU3Ec__DisplayClass24_0_U3CnextU3Eb__1_mAF3647D10EE173A42F91BB3A8918E6622C6F44B8,
	U3CU3Ec__cctor_m826676E45D743A7A47AB77387EC55261DC928D64,
	U3CU3Ec__ctor_m5BB8FA177096356EB97A75CA4E1D89C9312FD626,
	U3CU3Ec_U3CnextU3Eb__24_0_mB3D5F1D0A1BC3FF998305B351650D29F437AD350,
	U3CnextU3Ed__24__ctor_m17D0A0EC8F3115BFF62CB09AEB60A1FCB4EA556E,
	U3CnextU3Ed__24_System_IDisposable_Dispose_m4B20891703F191AB5B860B84215319B9F809E975,
	U3CnextU3Ed__24_MoveNext_m949B4D5730D5B713438C34AB2D63129538C2D31D,
	U3CnextU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m919363BEC99F1629E5E9B975AFC36E11299A47FA,
	U3CnextU3Ed__24_System_Collections_IEnumerator_Reset_mD787BBEEDCF6BDA102082176386C6821C7BF4CA3,
	U3CnextU3Ed__24_System_Collections_IEnumerator_get_Current_m3EBF0F869F7F8E21C2FD3227D29BFEFB85A013CE,
	U3CU3Ec__DisplayClass25_0__ctor_mE1DE6A41DD2C5AD754519923AE00AC255C9C16E8,
	U3CU3Ec__DisplayClass25_0_U3CsendLeaderBoardU3Eb__0_m62FD01347CA076A3F2069C06BEB510DF3BB2E020,
	U3CU3Ec__DisplayClass25_1__ctor_m5B585A2EF60F714DD86493F2B319DF386EDCF2FC,
	U3CU3Ec__DisplayClass25_1_U3CsendLeaderBoardU3Eb__1_m366EB63B42F3807F79FEC32BE6CD2FFF1C00830A,
	U3CU3Ec__DisplayClass25_2__ctor_m56151A09B3AE322E4D891E95B19FC65F8E48A089,
	U3CU3Ec__DisplayClass25_2_U3CsendLeaderBoardU3Eb__2_m93285107A1DC9156F33370BE0905CB4F881716B9,
	U3CU3Ec__DisplayClass25_3__ctor_mDBFE04FAB325D9E811E0D6C3822D474329A508F7,
	U3CU3Ec__DisplayClass25_3_U3CsendLeaderBoardU3Eb__3_m233DA85A8CA9BC13A5FDAE6D87A66B7C39FAE3D0,
	U3CsendLeaderBoardU3Ed__25__ctor_m3135E302BFD2C0C3D21A122EA1D2C1D9901B629D,
	U3CsendLeaderBoardU3Ed__25_System_IDisposable_Dispose_m24270614E4514553D51DC65691B789672AA2A984,
	U3CsendLeaderBoardU3Ed__25_MoveNext_m9C8936764FD78D2C25087579261E3094B4AF5AEC,
	U3CsendLeaderBoardU3Ed__25_U3CU3Em__Finally1_mD0926A9A17621F154E6E8F8200BED450B06F8A78,
	U3CsendLeaderBoardU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1064BB8870C65E12FCAF6E50F53AA409F9552585,
	U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_Reset_m66A7DDDD5C34A611D7AAB4166A61B80028048A4E,
	U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_get_Current_m0A31FDCB2696350455C3F79BFDC9779F3B43F6DD,
	gameMode_getReady_m14A23C55801E7F363008BE44D455BBFD74675D4A,
	gameMode_Start_m152E8D48AB1D55E5E0DB06CB258DFB7B5C45C5D6,
	gameMode_askCam_mE69233F0213F5C44B121A848271A570650B69553,
	gameMode_download_m3657B9D533F61FABB22FCD3EAAC82F1158E801E4,
	gameMode_downloadHidden_m68D4C21E769238439ACE96C78F8F9070758F471D,
	gameMode__ctor_mA47AFDF0941418DE94212B577A247BDE980973EE,
	U3CdownloadU3Ed__5__ctor_m48EEE33F5E91228F9519B94A0F1A574DB8B87977,
	U3CdownloadU3Ed__5_System_IDisposable_Dispose_m45936B0F78F3A28CFEC857966FA9B4C0B5C11B05,
	U3CdownloadU3Ed__5_MoveNext_m874309E06A86C7F97B8C471F3C4F795AB44E6DE6,
	U3CdownloadU3Ed__5_U3CU3Em__Finally1_m5158681611D35E5010A5796F709F0954A7CA9D18,
	U3CdownloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE5F967C2CF51C2616C0784F560DEC048EC9E4A,
	U3CdownloadU3Ed__5_System_Collections_IEnumerator_Reset_m457EFAE4B023CED9721AEB88FB9E758FC46B8DC0,
	U3CdownloadU3Ed__5_System_Collections_IEnumerator_get_Current_m397E0633C764549FDAB0F322F0B1ACECE9443A91,
	U3CdownloadHiddenU3Ed__6__ctor_m2810B2657357FEF21FC927248373AE1845FB39D6,
	U3CdownloadHiddenU3Ed__6_System_IDisposable_Dispose_mC3C65E624E782F46E034947EB42B4F362E764688,
	U3CdownloadHiddenU3Ed__6_MoveNext_mE4EDCF8D3B1F435EA1E6AACFE1E026282D153FD2,
	U3CdownloadHiddenU3Ed__6_U3CU3Em__Finally1_m2BD2AA4192036E8044BCA5D45B6CA29AF43A15DB,
	U3CdownloadHiddenU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E52325C5905BE8D6C05233270C82E5CB7805FC,
	U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_Reset_m46369049429C9A0C7EF2FE276441BAE764F80E6B,
	U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_get_Current_m6EF7A2146BA4DC6BC2877876AC97DE5FC20F5DFD,
	NULL,
	leaderB__ctor_m2DDA98A195EBF2FABAA61A6823B9B35F0983E949,
	leader__ctor_mEF513645400A122038B7786F5B902087F2417C32,
	portalsJson_count_m452467821CFDE8D5A7AFD060A598477F2F0432EE,
	portalsJson_append_m4AE2CC1769BAB89B9E9523127701E16774A6F87C,
	portalsJson__ctor_m8F34B5AD31B61F387F85BA7E88780C900950263C,
	gMJson__ctor_m7DD91B25A42B92CF16C5E50DBA9D0868F86A5DD3,
	teleportBtn__ctor_mC17E9F3E75ECD2E9B06391BB144054500B3EA28F,
	country__ctor_m1C3B25F995505BCFD46DBFE58AD8DD5269AC4E14,
	country_exists_mA6E88FC7DF524A1BF5D457D07AA470B65E3D6ED6,
	portal_tagContains_m38BCC7D2690CBD4E1B5F7EA12C6B825CC49977CF,
	portal__ctor_mE918CCDCADE9C997F9494BC5B5CD09CD4193D0D7,
	tag__ctor_m94647E303F0E2C917724D3EB68E18DA4D5B7D9BA,
	mainJson__ctor_mBBEE59A30B4D1D05F72437271FA6C2062838E2C8,
	assetBundle__ctor_mE61585BF10F2226F913C63F422686C33A785CD17,
	Background__ctor_mEC9A50E3A2C20E886BD7FA5D93F73FF0CC9E7B38,
	CustomBtn__ctor_m833507B8E9404AA62E37898E0DC8712A5462B7FD,
	sceneObject__ctor_m88F02F38FE7C06A1569FC831DA07120A1FD9C980,
	v3__ctor_m21C46DFCDBFD2D34ACB40CE016D8B2DD42D5B2D5,
	v3_toVector3_mEE68C5134A262C7909F617ACB1F838EDBBF53F35,
	v4__ctor_m73C2E94BCACEBBF5C507A9CA768D830957E30E18,
	v4_toQuaternion_m8D1D147EE19BE5BD40BFC721CE01C5E16AE10DAE,
	mainMenuLang_Start_m1BF155356F8CE69BA905A6DF66EFCB43B87AC497,
	mainMenuLang__ctor_mBEBF2597889D7989F8FC8ED2A4FE46FCD8CC9B04,
	nativeBack_Update_m8E972C0FF180F64578CF600221562C2BEAAF1220,
	nativeBack__ctor_m59E2F2FDB9C8927F1207B638949E7ECC3C1E2740,
	config__ctor_mF553B308AF044B5BD97531BF44BA946BAB14BE97,
	nativeGalleryImporter_Awake_mA0DFE326F5F6ECA416334DB062CDF863AB410334,
	nativeGalleryImporter_getIntentData_m5C85743AC49AD9A0B2ABF18BBFAEAD7BAA58CDC7,
	nativeGalleryImporter_CreatePushClass_mD43F2E609D8FB5B465AEDD7026FE1CE5883F1A20,
	nativeGalleryImporter_GetExtras_mFD30AE1CA6174A9A4C586B219314C0A39FCD129E,
	nativeGalleryImporter_GetProperty_mD4F4F406D72DF56E728C21AE94AEFC104C2A4141,
	nativeGalleryImporter_pop_m519F9E3C9AF89DDC9418CE0BAF64AE21E6243281,
	nativeGalleryImporter_Start_m9591FEA5E299B90674227E498D1745210E590482,
	nativeGalleryImporter_customizeScene_m8DE1DD113EF5109A4634126BF6277F0F3118F654,
	nativeGalleryImporter_getImage_mB01507D32DBDD1C1508A83B0DB2C5CBB76173D8E,
	nativeGalleryImporter_placeObjs_m7A7D888D7387B7DEB8A08C0A0DD796BDF66B727A,
	nativeGalleryImporter_PickImage_mA1F5869686E599051457A56E70FA3025064F24FD,
	nativeGalleryImporter_setPortal_m2380CD9BD029464024B834CD3C723FA8ABB123DE,
	nativeGalleryImporter_OnApplicationFocus_m114D1D4EE4D20E61582A1AFDF88A1506E41071CE,
	nativeGalleryImporter__ctor_m124FF8CDFC027B76E21DB5DB55394D9B9334359A,
	nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_0_m270B1D1CE3829824E010011625AF105A74A3AEFB,
	nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_1_mCD2790B37EE115F46C41F7D49FAA6C58710523C3,
	nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_2_mA1A540D526EC3EACC52D115C32751D307497D7E0,
	U3CU3Ec__DisplayClass29_0__ctor_mB9AED20E29665515E0794496EEA9DA9F9EA2671C,
	U3CU3Ec__DisplayClass29_0_U3CPickImageU3Eb__0_m06D959E4CB6B960D9C93DF06256134A62591E283,
	U3CsetPortalU3Ed__30__ctor_m6C8E4E232B9E6DF5BF9CF28A383DF2B314A3142C,
	U3CsetPortalU3Ed__30_System_IDisposable_Dispose_mB0CD9C5D764E231E71230E223E2D19C07EA09BDF,
	U3CsetPortalU3Ed__30_MoveNext_mDF8484A088A22B58A40AAD997EDE3F06DDDBDB18,
	U3CsetPortalU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1EDC811A1D4AFAFCA0CDC184534C02C70A2699C,
	U3CsetPortalU3Ed__30_System_Collections_IEnumerator_Reset_mA2BB601112A640FE30489C0E4E93B407E5075C22,
	U3CsetPortalU3Ed__30_System_Collections_IEnumerator_get_Current_m6D35ECB1F8B9011F1C5D29BE07833F759381CD0B,
	picker__ctor_mE68096A12C2F1FFC98A8297F3A0733E871C0F83B,
	placeholderText_Start_mE7FA10541B01A0FB2E9EEEE55BF1028FBD95345B,
	placeholderText__ctor_mAA576B0B3F01053C4E3F36D30D5FA4CAD81FDBBD,
	portalManager_Start_m083B147C95122A2394CB84B394EBDED40F2AA98A,
	portalManager_OnTriggerStay_m905CE2E1A4C64B17A1B0BB3F0B567EC82C21CA61,
	portalManager_OnTriggerEnter_m71045601F1A6F966C7039BC2DB930BBD942C1736,
	portalManager__ctor_m07CA661474091E3FEB264920AB07A585164B7DBB,
	sceneHolder__ctor_m8F0D2AD8662C21CDBA86F48FB9D49822498E4D31,
	sceneManager_reset_m934FB0704EA91EB2183C1E36D4A10211CF7B1B3C,
	sceneManager_back_mB3859AF40C833FEA7226F4E0C9577037AEE9B673,
	sceneManager_resetCustom_mEFD341A18CAE74F8193C13E847E7978603E77242,
	sceneManager_gamer_mCAA365EC63FD6CD4F8E34745F8693B16D874065A,
	sceneManager_errorReset_m781C46FDBE6BB73FA8749E13495EEE7BEE5193E1,
	sceneManager_refresh_m3B856ABD94582AF48D2ACAB6D15E7D5ADB12A8CE,
	sceneManager_resetS_mEE3C16A114B652C6AF87095ABD6D1BE1402B53AB,
	sceneManager_share_m805A592776854880865365E6BD1D3C15A19824FF,
	sceneManager__ctor_m65855833EBD363DE1C424AA42209E331D454B5CC,
	scoreManager_get_IsCounting_mAD2CA929AB8A9D6EFB8CEC3C4C76E1C360C7D5B2,
	scoreManager_set_IsCounting_m25C152E8E8499F825E5A3E580A66207727B0E005,
	scoreManager_startSound_mEF9D29E6A1C29A83A8438400337FF804624DD688,
	scoreManager_OnMouseDown_m870320DF27BD54773236D9FC6D298518CD5CD3A2,
	scoreManager_Update_m427CAD9463091B684BC768495413A2C9F8DE80E0,
	scoreManager__ctor_m34D3F568976B68881D634D07FBFDC625BCA44162,
	switchCollapse_Start_mEDF15B893B3E97E4457BA12B7E74F381EBE70944,
	switchCollapse_Collapse_mF6AFE8B4FAC098729BF517F00720E706121C640A,
	switchCollapse__ctor_m7C2C4656F7D0A0EE4ECBCC452780414FA6EFDCB8,
	taskbar_search_mB378D901D8F93411A90BDBCEC741A1CFEC342A08,
	taskbar_Awake_m29C7E4F7728ED49B064FD6451B974FB88FF98E48,
	taskbar_Start_m751D53A8E37301A310363B3E77269F663572FC82,
	taskbar_pop_m676549B4256134D756B8BBA0A735EC904AA2C09B,
	taskbar_OnApplicationPause_m8BD4C8CE8CB74B8766207A47A01B178449AB51FC,
	taskbar_OnApplicationFocus_m1D742F6C709D3242E270296D28242D6B18D86542,
	taskbar__ctor_mEDDEE9D493B1230608444E7ED605B3E4C0C877EB,
	taskbarForGameMode_changeTo_m42ADDE005C9552DF45E7A253EE0CADCED2B124A4,
	taskbarForGameMode_Start_mF0C2C95D363441453EFDD21BE9C4A229D2DC06A6,
	taskbarForGameMode_pop_m08F1CC9B13FC433CB95194720A216AE8E4EBC50F,
	taskbarForGameMode_OnApplicationFocus_m0D9B8D950531D3A7C9863BC0E4AB95C5F0FC2A65,
	taskbarForGameMode__ctor_mAC20D9A56C5277590F5BF98860DD92BBBB66BF63,
	testingBehaviour__ctor_m4D0CDB0E7D4701C13CD5F1E40ABDB3E7D7D07AB8,
	textureHolder_get_AB_m75503E8FCAE4E93DC6F75A1762C73C9A3DB17066,
	textureHolder_set_AB_mB626A57B3C891E41608A837373E7CE850F1FB859,
	textureHolder_fix_m9737529288B4BBF5416859A913902B950E53CD78,
	textureHolder__ctor_mC926D4E816FB392F5481A5168E30773928E1E7E1,
	textureHolder__cctor_m04A2362A0FDF9007C1CC2FB4282F4BBF7DF9BF78,
	playAnim_play_mD6C72B9D7D70DD0AF73244801A95AC14949066CB,
	playAnim__ctor_mBD2CC7E837A1C76C40229260B37BAB1C8499AA26,
	SimpleScrollSnap_get_CurrentPanel_mCC5EC8648BB72DDE5C99DE4739320C82F0F64470,
	SimpleScrollSnap_get_TargetPanel_m25CB62F655CFA42AB0292E3ED9819842F2934566,
	SimpleScrollSnap_get_NearestPanel_mD6F4F8774EDB107F730AF1D4C8D4FBC8459C342D,
	SimpleScrollSnap_get_NumberOfPanels_m78E9EB00525451B77A5A40FF59264F7306EFCAB6,
	SimpleScrollSnap_get_ScrollRect_mE4ED2804D0BE037AAE76F36A048C2844C2398EF3,
	SimpleScrollSnap_get_Content_m5A9291CFDE58C26A1327C04D6291B10127B82A92,
	SimpleScrollSnap_get_Viewport_m8AE2716DDDAC6D9BE4830593D614E4820B4FC0BB,
	SimpleScrollSnap_get_Panels_mEE7C882105D47FC6832F8AA8C59D6F56AC987C1E,
	SimpleScrollSnap_get_Toggles_m68B3D8EECE325FDB7AE3F9E41EFAC83FB2D7584C,
	SimpleScrollSnap_Start_m3D6B9310F2EDECED686EBD9EF48B6111B5E332C7,
	SimpleScrollSnap_Update_mAF5F9322EA4327CB9F5DA1084A45497391FCF595,
	SimpleScrollSnap_OnPointerDown_mCDDAE2E5F08BA64C7FAA0FF4895273E45878A5CD,
	SimpleScrollSnap_OnBeginDrag_mCCCC09ADA3A817AE458403FF5C20650B872EBA86,
	SimpleScrollSnap_OnDrag_mF90C27F0E84F6930D7D90ED4D2A4314BE3552E87,
	SimpleScrollSnap_OnEndDrag_mBFED0F3AB8851C9E762C718AE30068922299B5EB,
	SimpleScrollSnap_OnPointerUp_mE6C8EF647587B151FAACB7011B676ECEF4A69C64,
	SimpleScrollSnap_Validate_mAE8841A17B04DAA58AD5F331295F1B11A34BB3FC,
	SimpleScrollSnap_Setup_mEDBE0C985FA5C848F7AD641CBD068363B2673D0E,
	SimpleScrollSnap_DisplacementFromCenter_m90B7622CB7ECAB872641150825732A68CE2157B9,
	SimpleScrollSnap_DetermineNearestPanel_mE2B5A7C78F30EF6CC163258F1116294034C5633B,
	SimpleScrollSnap_SelectTargetPanel_m510BA969F490F81363ED5A0A47E57FEDE8AB52D2,
	SimpleScrollSnap_SnapToTargetPanel_m67107EE40C9C006EB2FE3FD8F0128D01DEA818F2,
	SimpleScrollSnap_OnSelectingAndSnapping_m40225CC288101B05F48DE9D136071AD511484B0C,
	SimpleScrollSnap_OnInfiniteScrolling_m78F13037007C1ADAB07D15B74A9257C5528B7268,
	SimpleScrollSnap_OnTransitionEffects_m4E4A14E55A4CEE6AD4EA602BDD6A14A345982C8C,
	SimpleScrollSnap_OnSwipeGestures_m655C228F042E1645D3D6247A917FF6F3F42D0737,
	SimpleScrollSnap_GoToPanel_mCF45C7E388B917C01C9BA91FA55DB00798CB43E2,
	SimpleScrollSnap_GoToPreviousPanel_m1D59A398191245B80211B53ED674162D59399D19,
	SimpleScrollSnap_GoToNextPanel_m40B209D86FA4212F5719BCB2568A7E603466C930,
	SimpleScrollSnap_AddToFront_m4235E5545FD9AA39C05839DF317D09472212560C,
	SimpleScrollSnap_AddToBack_mCD99433469A2D18F9A1A045A41DE3DD852F44F38,
	SimpleScrollSnap_Add_m72CBA1ED8A29C6719D37B0B573CBAA863964E761,
	SimpleScrollSnap_RemoveFromFront_m1C37FDA00889204EAF7706D573CAEF1B6479080B,
	SimpleScrollSnap_RemoveFromBack_m35E271D3E49DFE4C70F5B2430142673FDCA38B17,
	SimpleScrollSnap_Remove_mDD0EE00A08E0CD562E11DD7223F35D6F24279D49,
	SimpleScrollSnap_AddVelocity_mDD69CAA821FC1A69D59BDEF8DB4BAD33FB0B0126,
	SimpleScrollSnap__ctor_mC0629B573BA873F4997CA742C39CD2014904C426,
	U3CU3Ec__DisplayClass77_0__ctor_mAAC41ED533EA9EE78896566F3E4F3A3551721C15,
	U3CU3Ec__DisplayClass77_0_U3CSetupU3Eb__0_m181DD579666233F022CF2645C3A8A3D84030AA66,
	TransitionEffect_get_Label_m4AB3775D78A1A51618D4A96EF8CE712411FDA98B,
	TransitionEffect_set_Label_m71D57E08620AFBA00E627DA61F3AD0B510950B59,
	TransitionEffect_get_MinValue_m0379F7CF8098042455A61291EB5C1B8C2D14446D,
	TransitionEffect_set_MinValue_m6FE1E7F46B1EBC7CA2ED849DD28CA541CE86017A,
	TransitionEffect_get_MaxValue_m86D5260E5ECAB96EC0707875858A701C9791499B,
	TransitionEffect_set_MaxValue_m8EA7812145C61109C2346E629A9BBBCC0AFBCBF0,
	TransitionEffect_get_MinDisplacement_mBF11AD093821E581F1A50D94CD4CAA5A8AB40174,
	TransitionEffect_set_MinDisplacement_mEBA9ED7A5F5E9F41C3047F35D69A32F749AF7B61,
	TransitionEffect_get_MaxDisplacement_mCEA4D663D0E462E11CA542947C37FB89A2181758,
	TransitionEffect_set_MaxDisplacement_m1788D0FE93B137139B125727B8790039CB5FCD0B,
	TransitionEffect_get_Function_m9930C0BC2541378A943FBF08ACE8409FDD5617A2,
	TransitionEffect_set_Function_m55CB820A33BAFB18C2F7379EB9316A78804810A7,
	TransitionEffect__ctor_mA4738E77E13C0FD89F6104748DB96BF4910596B2,
	TransitionEffect_SetDefaultValues_mFF2E440FB958FB3B432E1A8DA4AB870DEF4CBEA7,
	TransitionEffect_Reset_mFB8DF968AF24E4AC2619CAC418096ACB7257327A,
	TransitionEffect_GetValue_m40AF086414C3A9F225DA899712F939F04AF9549E,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
extern void request__ctor_m3465B691D7F2F23A8CC54490EEB50B8F220E3777_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000001, request__ctor_m3465B691D7F2F23A8CC54490EEB50B8F220E3777_AdjustorThunk },
};
static const int32_t s_InvokerIndices[606] = 
{
	2797,
	3329,
	2781,
	3397,
	3397,
	3397,
	309,
	1118,
	2123,
	3397,
	3397,
	2797,
	2797,
	3397,
	2797,
	4191,
	4551,
	3939,
	4789,
	5012,
	3939,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	3371,
	2820,
	3397,
	3397,
	3397,
	2721,
	2797,
	3397,
	5179,
	3397,
	3397,
	4397,
	2123,
	2797,
	2797,
	1654,
	3397,
	1649,
	2817,
	876,
	2797,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	5179,
	5012,
	5012,
	4399,
	4553,
	4044,
	4397,
	3804,
	4044,
	4388,
	4388,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	5179,
	4551,
	3397,
	3345,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3345,
	2797,
	3345,
	2797,
	5102,
	5102,
	3397,
	3397,
	3397,
	5179,
	3345,
	2797,
	3345,
	2797,
	3397,
	2314,
	3397,
	3397,
	5179,
	3345,
	2797,
	3397,
	2817,
	3397,
	3397,
	-1,
	-1,
	3397,
	3397,
	3397,
	2721,
	3397,
	3345,
	2797,
	3345,
	2797,
	3345,
	2797,
	3345,
	2797,
	3397,
	3397,
	2709,
	3368,
	3397,
	3397,
	5179,
	5012,
	3927,
	4537,
	5159,
	5012,
	3927,
	3697,
	3697,
	3928,
	5159,
	5009,
	5159,
	4789,
	5012,
	3397,
	5179,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	2817,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	2781,
	3397,
	3368,
	3397,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	2817,
	5179,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	2817,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	2817,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3397,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2817,
	2125,
	3397,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	5102,
	5102,
	3397,
	3397,
	2797,
	3397,
	3397,
	3397,
	4551,
	5159,
	4551,
	4551,
	3397,
	2843,
	3397,
	3397,
	3345,
	2797,
	3345,
	121,
	35,
	2781,
	1239,
	2576,
	2576,
	2843,
	3397,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	2781,
	3397,
	3368,
	3397,
	3397,
	3397,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3345,
	2123,
	2123,
	3345,
	861,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	3397,
	3368,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	3397,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	2797,
	5179,
	3397,
	3397,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3345,
	3345,
	3397,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	2781,
	3397,
	3368,
	3397,
	3345,
	3397,
	3345,
	-1,
	3397,
	3397,
	3329,
	2797,
	3397,
	3397,
	3397,
	2797,
	2123,
	2413,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2845,
	3394,
	2806,
	3353,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3368,
	2413,
	2123,
	1247,
	3397,
	3397,
	2797,
	2781,
	3397,
	1542,
	2120,
	2817,
	3397,
	2797,
	2797,
	2797,
	3397,
	2797,
	2781,
	3397,
	3368,
	3345,
	3397,
	3345,
	3397,
	3397,
	3397,
	3397,
	2797,
	2797,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	5179,
	3397,
	3397,
	3397,
	3397,
	3368,
	2817,
	2434,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2797,
	3397,
	3397,
	3397,
	2817,
	2817,
	3397,
	5102,
	3397,
	3397,
	2817,
	3397,
	3397,
	5159,
	5102,
	5102,
	3397,
	5179,
	2797,
	3397,
	3329,
	3329,
	3329,
	3329,
	3345,
	3345,
	3345,
	3345,
	3345,
	3397,
	3397,
	2797,
	2797,
	2797,
	2797,
	2797,
	3368,
	2817,
	2578,
	3329,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2781,
	3397,
	3397,
	2797,
	2797,
	1647,
	3397,
	3397,
	2781,
	2843,
	3397,
	3397,
	2817,
	3345,
	2797,
	3371,
	2820,
	3371,
	2820,
	3371,
	2820,
	3371,
	2820,
	3345,
	2797,
	132,
	399,
	3397,
	2544,
	4941,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000020, { 0, 2 } },
	{ 0x060001C5, { 2, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, 3349 },
	{ (Il2CppRGCTXDataType)2, 416 },
	{ (Il2CppRGCTXDataType)2, 1834 },
	{ (Il2CppRGCTXDataType)2, 2414 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	606,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
