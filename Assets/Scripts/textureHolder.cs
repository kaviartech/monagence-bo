﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class textureHolder : MonoBehaviour
{
    public static Dictionary<string, Texture2D> thumbnailes = new Dictionary<string, Texture2D>();
    public static Dictionary<string, Texture2D> blurredThumbnailes = new Dictionary<string, Texture2D>();
    public static Dictionary<string, Texture2D> images360 = new Dictionary<string, Texture2D>();
    public static Dictionary<string, Texture2D> tps = new Dictionary<string, Texture2D>();
    public static Dictionary<string, string> mainJsons = new Dictionary<string, string>();
    public static Dictionary<string, string> portalJsons = new Dictionary<string, string>();
    public static Dictionary<string, string> panoramas = new Dictionary<string, string>();

    //asset bundle
    private static GameObject aB;

    public static GameObject AB { get => aB; set => fix(value); }

    public static void fix(GameObject ff)
    {
        /*Shader sha = Shader.Find("Unlit/SponzaNormal");

        Renderer[] renderers = ff.GetComponentsInChildren<Renderer>();

        foreach (var rend in renderers)
        {
            Material[] materials = rend.sharedMaterials;

            foreach (var mat in materials)
            {
                mat.shader = sha;
                mat.SetInt("_stencilComp", (int)CompareFunction.Equal);
            }
        }*/

        aB = ff;
    }

}
