﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SpatialTracking;
using UnityEngine.XR.ARFoundation;

public class testingBehaviour : MonoBehaviour
{
#if UNITY_EDITOR

    //public PlaceMultipleObjectsOnPlane p;

    public ARRaycastManager ar;

    public ARPlaneManager ap;

    public ARSessionOrigin aSe;

    public TrackedPoseDriver td;

    public ARCameraManager ac;

    public ARCameraBackground ab;


    private void Awake()
    {
        //p.enabled = false;

        ar.enabled = false;

        ap.enabled = false;

        aSe.enabled = false;

        td.enabled = false;

        ac.enabled = false;

        ab.enabled = false;
    }

#endif
}
