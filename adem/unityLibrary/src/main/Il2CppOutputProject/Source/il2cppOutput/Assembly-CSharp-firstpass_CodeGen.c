﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DeeplinkingExample::Start()
extern void DeeplinkingExample_Start_mB208B0E17AB12D214E677AA2A9052CC1C7634BEF (void);
// 0x00000002 System.Void DeeplinkingExample::SetupUi()
extern void DeeplinkingExample_SetupUi_mBFFEB5D603531E38004C53B96EF030E798F37679 (void);
// 0x00000003 System.Void DeeplinkingExample::Instance_LinkActivated(ImaginationOverflow.UniversalDeepLinking.LinkActivation)
extern void DeeplinkingExample_Instance_LinkActivated_mC5D1E642CFF5796AE78C374A4753B2E49B18CB8F (void);
// 0x00000004 System.Void DeeplinkingExample::UpdateContentSize()
extern void DeeplinkingExample_UpdateContentSize_mD2AB1B435EAD56EC87952F17D479A39B1ED9E18B (void);
// 0x00000005 System.Void DeeplinkingExample::.ctor()
extern void DeeplinkingExample__ctor_mFF826F3D7796A05AAA827AEBCCE6D3ADAE337C3D (void);
// 0x00000006 System.Void OpenLinkBehaviour::Start()
extern void OpenLinkBehaviour_Start_mA5A2F437D7D9692A4C56315085F8490684FD7FD5 (void);
// 0x00000007 System.Void OpenLinkBehaviour::Open()
extern void OpenLinkBehaviour_Open_mBC671BDCAEC4F3027A1FC2DB3D895E6A70C3DEA1 (void);
// 0x00000008 System.Void OpenLinkBehaviour::.ctor()
extern void OpenLinkBehaviour__ctor_m2C7D95D6766DCE045609BBC6D1E5AF7EFF65DC4B (void);
// 0x00000009 UnityEngine.AndroidJavaClass NativeGallery::get_AJC()
extern void NativeGallery_get_AJC_m4307BF76B4FCEA5D1192219F6CCD30AC7BDD65EE (void);
// 0x0000000A UnityEngine.AndroidJavaObject NativeGallery::get_Context()
extern void NativeGallery_get_Context_mC358BF371761A22F9AEA0949C94D7198394CC562 (void);
// 0x0000000B System.String NativeGallery::get_TemporaryImagePath()
extern void NativeGallery_get_TemporaryImagePath_mD2C75BFC9F9266D5559E8832B58395ECD1C6747A (void);
// 0x0000000C System.String NativeGallery::get_SelectedImagePath()
extern void NativeGallery_get_SelectedImagePath_m5B63C2B54E2FB35402CF05A4388D8CF3EAA33187 (void);
// 0x0000000D System.String NativeGallery::get_SelectedVideoPath()
extern void NativeGallery_get_SelectedVideoPath_m1BA11E0F23FF6E51BB4904024B7ABA27FC52F8D3 (void);
// 0x0000000E System.String NativeGallery::get_SelectedAudioPath()
extern void NativeGallery_get_SelectedAudioPath_mA0D29026393823944B2E7BD6678015A5EFDC5477 (void);
// 0x0000000F NativeGallery/Permission NativeGallery::CheckPermission(System.Boolean)
extern void NativeGallery_CheckPermission_mA16DF5B8DA74D19804AD35B9CD40399CF01AD8D0 (void);
// 0x00000010 NativeGallery/Permission NativeGallery::RequestPermission(System.Boolean)
extern void NativeGallery_RequestPermission_m50048F915B1B5F7EDCB6CDD3E7A4DE83559BA8F1 (void);
// 0x00000011 System.Boolean NativeGallery::CanOpenSettings()
extern void NativeGallery_CanOpenSettings_mC870229E9DE701FC7CA10EA968F4B925CDC6027F (void);
// 0x00000012 System.Void NativeGallery::OpenSettings()
extern void NativeGallery_OpenSettings_m5CFBBF96B4100848BDBADF722E5883F9D221AEDC (void);
// 0x00000013 NativeGallery/Permission NativeGallery::SaveImageToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_m1854BDA6D74818B37783A0FC5EFE7154B18A11F7 (void);
// 0x00000014 NativeGallery/Permission NativeGallery::SaveImageToGallery(System.String,System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_m09061C20A16440BBC00AF77865EA923BF13C2F7F (void);
// 0x00000015 NativeGallery/Permission NativeGallery::SaveImageToGallery(UnityEngine.Texture2D,System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveImageToGallery_mA9EAB26EC648656A0A4C9C91B4F13B6DFE2C0B15 (void);
// 0x00000016 NativeGallery/Permission NativeGallery::SaveVideoToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveVideoToGallery_m926473B0A6E83134D3150A02201BC4FDED6CE6CB (void);
// 0x00000017 NativeGallery/Permission NativeGallery::SaveVideoToGallery(System.String,System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveVideoToGallery_mBB74ABCACE4415325C3E19BD071DE26048871AC7 (void);
// 0x00000018 NativeGallery/Permission NativeGallery::SaveAudioToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveAudioToGallery_mC6F9109673DCF15666858D294D113BC37612FC7B (void);
// 0x00000019 NativeGallery/Permission NativeGallery::SaveAudioToGallery(System.String,System.String,System.String,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveAudioToGallery_mC8D0A974F0343AC94A2CE263DBCD8834C0B3DAE1 (void);
// 0x0000001A System.Boolean NativeGallery::CanSelectMultipleFilesFromGallery()
extern void NativeGallery_CanSelectMultipleFilesFromGallery_mDDC4F8A801165FDB60FE11AFB18BAC81848C012D (void);
// 0x0000001B NativeGallery/Permission NativeGallery::GetImageFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
extern void NativeGallery_GetImageFromGallery_m88285EEC398873AA26EE11422DE1DCCD35D19027 (void);
// 0x0000001C NativeGallery/Permission NativeGallery::GetVideoFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
extern void NativeGallery_GetVideoFromGallery_mF747A8329A1D9D227E6A216C4E87696C21846DE1 (void);
// 0x0000001D NativeGallery/Permission NativeGallery::GetAudioFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
extern void NativeGallery_GetAudioFromGallery_m722E49AAF484423DA6B1B9C421F452724BD27081 (void);
// 0x0000001E NativeGallery/Permission NativeGallery::GetImagesFromGallery(NativeGallery/MediaPickMultipleCallback,System.String,System.String)
extern void NativeGallery_GetImagesFromGallery_mFD310667DE18263AF16161140A44D794C0971057 (void);
// 0x0000001F NativeGallery/Permission NativeGallery::GetVideosFromGallery(NativeGallery/MediaPickMultipleCallback,System.String,System.String)
extern void NativeGallery_GetVideosFromGallery_mABEF1891642369237C4FC020A47AB6BC1835B6A2 (void);
// 0x00000020 NativeGallery/Permission NativeGallery::GetAudiosFromGallery(NativeGallery/MediaPickMultipleCallback,System.String,System.String)
extern void NativeGallery_GetAudiosFromGallery_mF02197FEA79379AC4FECCC300D19BF4AC686D0E2 (void);
// 0x00000021 System.Boolean NativeGallery::IsMediaPickerBusy()
extern void NativeGallery_IsMediaPickerBusy_m1ECF19CC1C2CC61412415367CD6A4A0CCB2D49B1 (void);
// 0x00000022 NativeGallery/Permission NativeGallery::SaveToGallery(System.Byte[],System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveToGallery_mDEE93746747317233B168AEFCB9238136AC5777C (void);
// 0x00000023 NativeGallery/Permission NativeGallery::SaveToGallery(System.String,System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveToGallery_m5146791056EE9A7EF4943C910D29BE8204BF0DCF (void);
// 0x00000024 System.Void NativeGallery::SaveToGalleryInternal(System.String,System.String,NativeGallery/MediaType,NativeGallery/MediaSaveCallback)
extern void NativeGallery_SaveToGalleryInternal_m43CC2663F52F40D70185E0D0309EF3F58EED369B (void);
// 0x00000025 System.String NativeGallery::GetTemporarySavePath(System.String)
extern void NativeGallery_GetTemporarySavePath_m7F42CE60368F11FF35422DB8BC48C293B77A7F60 (void);
// 0x00000026 NativeGallery/Permission NativeGallery::GetMediaFromGallery(NativeGallery/MediaPickCallback,NativeGallery/MediaType,System.String,System.String)
extern void NativeGallery_GetMediaFromGallery_m0D534A4D397DEB9A558322FE6C15DCE0329647F5 (void);
// 0x00000027 NativeGallery/Permission NativeGallery::GetMultipleMediaFromGallery(NativeGallery/MediaPickMultipleCallback,NativeGallery/MediaType,System.String,System.String)
extern void NativeGallery_GetMultipleMediaFromGallery_m3F2D6AFE208A875BA46051972B0F751445E455D6 (void);
// 0x00000028 System.Byte[] NativeGallery::GetTextureBytes(UnityEngine.Texture2D,System.Boolean)
extern void NativeGallery_GetTextureBytes_m685923E43144B1B776841913E3C879E78A159E00 (void);
// 0x00000029 System.Byte[] NativeGallery::GetTextureBytesFromCopy(UnityEngine.Texture2D,System.Boolean)
extern void NativeGallery_GetTextureBytesFromCopy_m6CB6DE153F1F1FBEBBA10D170A97E9EEF94863E6 (void);
// 0x0000002A UnityEngine.Texture2D NativeGallery::LoadImageAtPath(System.String,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void NativeGallery_LoadImageAtPath_m5D84DE861AD6659942A2FFB4547A4343F266D225 (void);
// 0x0000002B NativeGallery/ImageProperties NativeGallery::GetImageProperties(System.String)
extern void NativeGallery_GetImageProperties_m9366D0A56B0DAE8EDE926AC0751C3D968ACB019D (void);
// 0x0000002C NativeGallery/VideoProperties NativeGallery::GetVideoProperties(System.String)
extern void NativeGallery_GetVideoProperties_m3C9D534463E7A815D7446E0B14A01B453D2E4B14 (void);
// 0x0000002D System.Void NativeGallery::.cctor()
extern void NativeGallery__cctor_mD4DE63A08384B2ABBD888116FB7365A0F1BF2878 (void);
// 0x0000002E System.Void NativeGallery/ImageProperties::.ctor(System.Int32,System.Int32,System.String,NativeGallery/ImageOrientation)
extern void ImageProperties__ctor_m8C0DA93333F55DFB00769AB1326E80AE7DB036ED (void);
// 0x0000002F System.Void NativeGallery/VideoProperties::.ctor(System.Int32,System.Int32,System.Int64,System.Single)
extern void VideoProperties__ctor_m45BABD4A9B46E4B193903730ACCE828941099219 (void);
// 0x00000030 System.Void NativeGallery/MediaSaveCallback::.ctor(System.Object,System.IntPtr)
extern void MediaSaveCallback__ctor_mC7C4CF0848485E6487508A2245B8AD8E9AB4BD58 (void);
// 0x00000031 System.Void NativeGallery/MediaSaveCallback::Invoke(System.String)
extern void MediaSaveCallback_Invoke_m9ED3F44F7DBF2B6CDF929AE4B0558AFF97CA614E (void);
// 0x00000032 System.IAsyncResult NativeGallery/MediaSaveCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MediaSaveCallback_BeginInvoke_m4AC39914705631D7BA632362371DF0AE4C87D1AC (void);
// 0x00000033 System.Void NativeGallery/MediaSaveCallback::EndInvoke(System.IAsyncResult)
extern void MediaSaveCallback_EndInvoke_mC5EBEC1BF0764B25073510A1B0E0ADF7B500F4A7 (void);
// 0x00000034 System.Void NativeGallery/MediaPickCallback::.ctor(System.Object,System.IntPtr)
extern void MediaPickCallback__ctor_m1A0C70520340FB94DCC10508482A9D4254E44CE5 (void);
// 0x00000035 System.Void NativeGallery/MediaPickCallback::Invoke(System.String)
extern void MediaPickCallback_Invoke_mB7C0436D54E925BD80E322FCB5103E318AEB28EE (void);
// 0x00000036 System.IAsyncResult NativeGallery/MediaPickCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MediaPickCallback_BeginInvoke_m7F831FCDF7B025C120491881F3C82F59EEF7043B (void);
// 0x00000037 System.Void NativeGallery/MediaPickCallback::EndInvoke(System.IAsyncResult)
extern void MediaPickCallback_EndInvoke_mFA394402B5E407D5E93B51CC451B4B73FD51FB98 (void);
// 0x00000038 System.Void NativeGallery/MediaPickMultipleCallback::.ctor(System.Object,System.IntPtr)
extern void MediaPickMultipleCallback__ctor_mDA8C6DBB4E0B07A2165043F0AA4ADC1495F3B826 (void);
// 0x00000039 System.Void NativeGallery/MediaPickMultipleCallback::Invoke(System.String[])
extern void MediaPickMultipleCallback_Invoke_m48CF72A34CD7D55174ECEDEECD0183C9A07BFC11 (void);
// 0x0000003A System.IAsyncResult NativeGallery/MediaPickMultipleCallback::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void MediaPickMultipleCallback_BeginInvoke_m5CAF498BF960F7CCCE46937D2456BF48AB1CA83B (void);
// 0x0000003B System.Void NativeGallery/MediaPickMultipleCallback::EndInvoke(System.IAsyncResult)
extern void MediaPickMultipleCallback_EndInvoke_m813DFDC23DAC035F742A0DDE3E060CAEF9822623 (void);
// 0x0000003C UnityEngine.AndroidJavaClass NativeShare::get_AJC()
extern void NativeShare_get_AJC_m197FE61BC4A165302E755354EF032A6709108F8B (void);
// 0x0000003D UnityEngine.AndroidJavaObject NativeShare::get_Context()
extern void NativeShare_get_Context_m39A676FA7DCBC61532C912022B4384352285C878 (void);
// 0x0000003E System.Void NativeShare::.ctor()
extern void NativeShare__ctor_mFE21ADAF78DA1B4411680EEFBE7A6AAD73C51B2D (void);
// 0x0000003F NativeShare NativeShare::SetSubject(System.String)
extern void NativeShare_SetSubject_mC0FEAC79F4711A48E8D3DC9846C1B145F53A01C6 (void);
// 0x00000040 NativeShare NativeShare::SetText(System.String)
extern void NativeShare_SetText_m9E497FA740AFFB72D9EBF7F50C5C84E885F6F063 (void);
// 0x00000041 NativeShare NativeShare::SetTitle(System.String)
extern void NativeShare_SetTitle_m2B263C832CF038BA4A01FCE0E6C74968917C2426 (void);
// 0x00000042 NativeShare NativeShare::SetTarget(System.String,System.String)
extern void NativeShare_SetTarget_m41D1AFE4C5A8C45B5588FD4C737D87A2F07DF851 (void);
// 0x00000043 NativeShare NativeShare::AddFile(System.String,System.String)
extern void NativeShare_AddFile_m16417849839656B79FF89477885835B1E441611B (void);
// 0x00000044 System.Void NativeShare::Share()
extern void NativeShare_Share_m951480527B58AF515483FD8C399CAA626B68F8FA (void);
// 0x00000045 System.Boolean NativeShare::TargetExists(System.String,System.String)
extern void NativeShare_TargetExists_mDBA768A0510668E5A798DC3E8C5CBE5A27AD9E29 (void);
// 0x00000046 System.Boolean NativeShare::FindTarget(System.String&,System.String&,System.String,System.String)
extern void NativeShare_FindTarget_mB158D836796C4765A36930B9FD31DC8CBD58C5E7 (void);
// 0x00000047 System.Void NativeShare::.cctor()
extern void NativeShare__cctor_mA577AE49A3BD242D7D3308AEB6A8F2145C29CB84 (void);
// 0x00000048 System.Void NativeGalleryNamespace.NGCallbackHelper::Awake()
extern void NGCallbackHelper_Awake_m28D007314C438D3874F56F5B836CB4736EE07FAB (void);
// 0x00000049 System.Void NativeGalleryNamespace.NGCallbackHelper::Update()
extern void NGCallbackHelper_Update_m9F6E97686653F22D62E5D95A8734787C174B86E3 (void);
// 0x0000004A System.Void NativeGalleryNamespace.NGCallbackHelper::CallOnMainThread(System.Action)
extern void NGCallbackHelper_CallOnMainThread_m935A8C0505A00CF267FD52FCC54D6213DB5EFEB5 (void);
// 0x0000004B System.Void NativeGalleryNamespace.NGCallbackHelper::.ctor()
extern void NGCallbackHelper__ctor_mB41651EAF80FC1E02EB5559D6C00E8BB6094D5FB (void);
// 0x0000004C System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::.ctor(NativeGallery/MediaPickCallback,NativeGallery/MediaPickMultipleCallback)
extern void NGMediaReceiveCallbackAndroid__ctor_m7DA10E449CF785C4B5DE7CAF5A67352C068F3582 (void);
// 0x0000004D System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::OnMediaReceived(System.String)
extern void NGMediaReceiveCallbackAndroid_OnMediaReceived_m2BD7C7D4D1F349E3F936A1DBED3B106F463A3A35 (void);
// 0x0000004E System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::OnMultipleMediaReceived(System.String)
extern void NGMediaReceiveCallbackAndroid_OnMultipleMediaReceived_m71C422152F3F22E7098899323BD70737BD8E1E9A (void);
// 0x0000004F System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::MediaReceiveCallback(System.String)
extern void NGMediaReceiveCallbackAndroid_MediaReceiveCallback_mA899EDF8FA2EDC60ADB313DE5C81CB9B77C0FDFF (void);
// 0x00000050 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::MediaReceiveMultipleCallback(System.String[])
extern void NGMediaReceiveCallbackAndroid_MediaReceiveMultipleCallback_m935837B6ABD452F4982EA0FFBCB1A58342A732C5 (void);
// 0x00000051 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m2686585843AF3B928C02F998C463D2A8AEFCC539 (void);
// 0x00000052 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0::<OnMediaReceived>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3COnMediaReceivedU3Eb__0_mF5DB9C317748F4BB3FB340AB29F0B66065114926 (void);
// 0x00000053 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mCB77E4385C4FFB3099C8E8D8D4AE7E743DD45F5D (void);
// 0x00000054 System.Void NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0::<OnMultipleMediaReceived>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3COnMultipleMediaReceivedU3Eb__0_m36962BA6DBD2F57C90B56CC8B3C10C3A2A133532 (void);
// 0x00000055 System.Int32 NativeGalleryNamespace.NGPermissionCallbackAndroid::get_Result()
extern void NGPermissionCallbackAndroid_get_Result_mE352376B3CA1BD57D1A5ABDB349FE269B42DEA30 (void);
// 0x00000056 System.Void NativeGalleryNamespace.NGPermissionCallbackAndroid::set_Result(System.Int32)
extern void NGPermissionCallbackAndroid_set_Result_mFCF6107883E0F8AED7BE8951B85AF2693F0EC950 (void);
// 0x00000057 System.Void NativeGalleryNamespace.NGPermissionCallbackAndroid::.ctor(System.Object)
extern void NGPermissionCallbackAndroid__ctor_mC8D10C3772B425957CA4DF08E0E874876C48AEF0 (void);
// 0x00000058 System.Void NativeGalleryNamespace.NGPermissionCallbackAndroid::OnPermissionResult(System.Int32)
extern void NGPermissionCallbackAndroid_OnPermissionResult_mFA6227690FA00A7ED3E4B43923B97E4B5B2A1F7C (void);
static Il2CppMethodPointer s_methodPointers[88] = 
{
	DeeplinkingExample_Start_mB208B0E17AB12D214E677AA2A9052CC1C7634BEF,
	DeeplinkingExample_SetupUi_mBFFEB5D603531E38004C53B96EF030E798F37679,
	DeeplinkingExample_Instance_LinkActivated_mC5D1E642CFF5796AE78C374A4753B2E49B18CB8F,
	DeeplinkingExample_UpdateContentSize_mD2AB1B435EAD56EC87952F17D479A39B1ED9E18B,
	DeeplinkingExample__ctor_mFF826F3D7796A05AAA827AEBCCE6D3ADAE337C3D,
	OpenLinkBehaviour_Start_mA5A2F437D7D9692A4C56315085F8490684FD7FD5,
	OpenLinkBehaviour_Open_mBC671BDCAEC4F3027A1FC2DB3D895E6A70C3DEA1,
	OpenLinkBehaviour__ctor_m2C7D95D6766DCE045609BBC6D1E5AF7EFF65DC4B,
	NativeGallery_get_AJC_m4307BF76B4FCEA5D1192219F6CCD30AC7BDD65EE,
	NativeGallery_get_Context_mC358BF371761A22F9AEA0949C94D7198394CC562,
	NativeGallery_get_TemporaryImagePath_mD2C75BFC9F9266D5559E8832B58395ECD1C6747A,
	NativeGallery_get_SelectedImagePath_m5B63C2B54E2FB35402CF05A4388D8CF3EAA33187,
	NativeGallery_get_SelectedVideoPath_m1BA11E0F23FF6E51BB4904024B7ABA27FC52F8D3,
	NativeGallery_get_SelectedAudioPath_mA0D29026393823944B2E7BD6678015A5EFDC5477,
	NativeGallery_CheckPermission_mA16DF5B8DA74D19804AD35B9CD40399CF01AD8D0,
	NativeGallery_RequestPermission_m50048F915B1B5F7EDCB6CDD3E7A4DE83559BA8F1,
	NativeGallery_CanOpenSettings_mC870229E9DE701FC7CA10EA968F4B925CDC6027F,
	NativeGallery_OpenSettings_m5CFBBF96B4100848BDBADF722E5883F9D221AEDC,
	NativeGallery_SaveImageToGallery_m1854BDA6D74818B37783A0FC5EFE7154B18A11F7,
	NativeGallery_SaveImageToGallery_m09061C20A16440BBC00AF77865EA923BF13C2F7F,
	NativeGallery_SaveImageToGallery_mA9EAB26EC648656A0A4C9C91B4F13B6DFE2C0B15,
	NativeGallery_SaveVideoToGallery_m926473B0A6E83134D3150A02201BC4FDED6CE6CB,
	NativeGallery_SaveVideoToGallery_mBB74ABCACE4415325C3E19BD071DE26048871AC7,
	NativeGallery_SaveAudioToGallery_mC6F9109673DCF15666858D294D113BC37612FC7B,
	NativeGallery_SaveAudioToGallery_mC8D0A974F0343AC94A2CE263DBCD8834C0B3DAE1,
	NativeGallery_CanSelectMultipleFilesFromGallery_mDDC4F8A801165FDB60FE11AFB18BAC81848C012D,
	NativeGallery_GetImageFromGallery_m88285EEC398873AA26EE11422DE1DCCD35D19027,
	NativeGallery_GetVideoFromGallery_mF747A8329A1D9D227E6A216C4E87696C21846DE1,
	NativeGallery_GetAudioFromGallery_m722E49AAF484423DA6B1B9C421F452724BD27081,
	NativeGallery_GetImagesFromGallery_mFD310667DE18263AF16161140A44D794C0971057,
	NativeGallery_GetVideosFromGallery_mABEF1891642369237C4FC020A47AB6BC1835B6A2,
	NativeGallery_GetAudiosFromGallery_mF02197FEA79379AC4FECCC300D19BF4AC686D0E2,
	NativeGallery_IsMediaPickerBusy_m1ECF19CC1C2CC61412415367CD6A4A0CCB2D49B1,
	NativeGallery_SaveToGallery_mDEE93746747317233B168AEFCB9238136AC5777C,
	NativeGallery_SaveToGallery_m5146791056EE9A7EF4943C910D29BE8204BF0DCF,
	NativeGallery_SaveToGalleryInternal_m43CC2663F52F40D70185E0D0309EF3F58EED369B,
	NativeGallery_GetTemporarySavePath_m7F42CE60368F11FF35422DB8BC48C293B77A7F60,
	NativeGallery_GetMediaFromGallery_m0D534A4D397DEB9A558322FE6C15DCE0329647F5,
	NativeGallery_GetMultipleMediaFromGallery_m3F2D6AFE208A875BA46051972B0F751445E455D6,
	NativeGallery_GetTextureBytes_m685923E43144B1B776841913E3C879E78A159E00,
	NativeGallery_GetTextureBytesFromCopy_m6CB6DE153F1F1FBEBBA10D170A97E9EEF94863E6,
	NativeGallery_LoadImageAtPath_m5D84DE861AD6659942A2FFB4547A4343F266D225,
	NativeGallery_GetImageProperties_m9366D0A56B0DAE8EDE926AC0751C3D968ACB019D,
	NativeGallery_GetVideoProperties_m3C9D534463E7A815D7446E0B14A01B453D2E4B14,
	NativeGallery__cctor_mD4DE63A08384B2ABBD888116FB7365A0F1BF2878,
	ImageProperties__ctor_m8C0DA93333F55DFB00769AB1326E80AE7DB036ED,
	VideoProperties__ctor_m45BABD4A9B46E4B193903730ACCE828941099219,
	MediaSaveCallback__ctor_mC7C4CF0848485E6487508A2245B8AD8E9AB4BD58,
	MediaSaveCallback_Invoke_m9ED3F44F7DBF2B6CDF929AE4B0558AFF97CA614E,
	MediaSaveCallback_BeginInvoke_m4AC39914705631D7BA632362371DF0AE4C87D1AC,
	MediaSaveCallback_EndInvoke_mC5EBEC1BF0764B25073510A1B0E0ADF7B500F4A7,
	MediaPickCallback__ctor_m1A0C70520340FB94DCC10508482A9D4254E44CE5,
	MediaPickCallback_Invoke_mB7C0436D54E925BD80E322FCB5103E318AEB28EE,
	MediaPickCallback_BeginInvoke_m7F831FCDF7B025C120491881F3C82F59EEF7043B,
	MediaPickCallback_EndInvoke_mFA394402B5E407D5E93B51CC451B4B73FD51FB98,
	MediaPickMultipleCallback__ctor_mDA8C6DBB4E0B07A2165043F0AA4ADC1495F3B826,
	MediaPickMultipleCallback_Invoke_m48CF72A34CD7D55174ECEDEECD0183C9A07BFC11,
	MediaPickMultipleCallback_BeginInvoke_m5CAF498BF960F7CCCE46937D2456BF48AB1CA83B,
	MediaPickMultipleCallback_EndInvoke_m813DFDC23DAC035F742A0DDE3E060CAEF9822623,
	NativeShare_get_AJC_m197FE61BC4A165302E755354EF032A6709108F8B,
	NativeShare_get_Context_m39A676FA7DCBC61532C912022B4384352285C878,
	NativeShare__ctor_mFE21ADAF78DA1B4411680EEFBE7A6AAD73C51B2D,
	NativeShare_SetSubject_mC0FEAC79F4711A48E8D3DC9846C1B145F53A01C6,
	NativeShare_SetText_m9E497FA740AFFB72D9EBF7F50C5C84E885F6F063,
	NativeShare_SetTitle_m2B263C832CF038BA4A01FCE0E6C74968917C2426,
	NativeShare_SetTarget_m41D1AFE4C5A8C45B5588FD4C737D87A2F07DF851,
	NativeShare_AddFile_m16417849839656B79FF89477885835B1E441611B,
	NativeShare_Share_m951480527B58AF515483FD8C399CAA626B68F8FA,
	NativeShare_TargetExists_mDBA768A0510668E5A798DC3E8C5CBE5A27AD9E29,
	NativeShare_FindTarget_mB158D836796C4765A36930B9FD31DC8CBD58C5E7,
	NativeShare__cctor_mA577AE49A3BD242D7D3308AEB6A8F2145C29CB84,
	NGCallbackHelper_Awake_m28D007314C438D3874F56F5B836CB4736EE07FAB,
	NGCallbackHelper_Update_m9F6E97686653F22D62E5D95A8734787C174B86E3,
	NGCallbackHelper_CallOnMainThread_m935A8C0505A00CF267FD52FCC54D6213DB5EFEB5,
	NGCallbackHelper__ctor_mB41651EAF80FC1E02EB5559D6C00E8BB6094D5FB,
	NGMediaReceiveCallbackAndroid__ctor_m7DA10E449CF785C4B5DE7CAF5A67352C068F3582,
	NGMediaReceiveCallbackAndroid_OnMediaReceived_m2BD7C7D4D1F349E3F936A1DBED3B106F463A3A35,
	NGMediaReceiveCallbackAndroid_OnMultipleMediaReceived_m71C422152F3F22E7098899323BD70737BD8E1E9A,
	NGMediaReceiveCallbackAndroid_MediaReceiveCallback_mA899EDF8FA2EDC60ADB313DE5C81CB9B77C0FDFF,
	NGMediaReceiveCallbackAndroid_MediaReceiveMultipleCallback_m935837B6ABD452F4982EA0FFBCB1A58342A732C5,
	U3CU3Ec__DisplayClass4_0__ctor_m2686585843AF3B928C02F998C463D2A8AEFCC539,
	U3CU3Ec__DisplayClass4_0_U3COnMediaReceivedU3Eb__0_mF5DB9C317748F4BB3FB340AB29F0B66065114926,
	U3CU3Ec__DisplayClass5_0__ctor_mCB77E4385C4FFB3099C8E8D8D4AE7E743DD45F5D,
	U3CU3Ec__DisplayClass5_0_U3COnMultipleMediaReceivedU3Eb__0_m36962BA6DBD2F57C90B56CC8B3C10C3A2A133532,
	NGPermissionCallbackAndroid_get_Result_mE352376B3CA1BD57D1A5ABDB349FE269B42DEA30,
	NGPermissionCallbackAndroid_set_Result_mFCF6107883E0F8AED7BE8951B85AF2693F0EC950,
	NGPermissionCallbackAndroid__ctor_mC8D10C3772B425957CA4DF08E0E874876C48AEF0,
	NGPermissionCallbackAndroid_OnPermissionResult_mFA6227690FA00A7ED3E4B43923B97E4B5B2A1F7C,
};
extern void ImageProperties__ctor_m8C0DA93333F55DFB00769AB1326E80AE7DB036ED_AdjustorThunk (void);
extern void VideoProperties__ctor_m45BABD4A9B46E4B193903730ACCE828941099219_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600002E, ImageProperties__ctor_m8C0DA93333F55DFB00769AB1326E80AE7DB036ED_AdjustorThunk },
	{ 0x0600002F, VideoProperties__ctor_m45BABD4A9B46E4B193903730ACCE828941099219_AdjustorThunk },
};
static const int32_t s_InvokerIndices[88] = 
{
	3397,
	3397,
	2797,
	3397,
	3397,
	3397,
	3397,
	3397,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	4942,
	4942,
	5168,
	5179,
	3849,
	3849,
	3849,
	3849,
	3849,
	3849,
	3849,
	5168,
	4121,
	4121,
	4121,
	4121,
	4121,
	4121,
	5168,
	3677,
	3677,
	4041,
	5012,
	3841,
	3841,
	4552,
	4552,
	3699,
	5121,
	5122,
	5179,
	664,
	662,
	1649,
	2797,
	861,
	2797,
	1649,
	2797,
	861,
	2797,
	1649,
	2797,
	861,
	2797,
	5159,
	5159,
	3397,
	2123,
	2123,
	2123,
	1247,
	1247,
	3397,
	4645,
	3959,
	5179,
	3397,
	3397,
	2797,
	3397,
	1650,
	2797,
	2797,
	2797,
	2797,
	3397,
	3397,
	3397,
	3397,
	3329,
	2781,
	2797,
	2781,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	88,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
