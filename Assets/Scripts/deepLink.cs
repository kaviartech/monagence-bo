﻿using ImaginationOverflow.UniversalDeepLinking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class deepLink : MonoBehaviour
{

    public galleryImporter gi;

    void Start()
    {
        DeepLinkManager.Instance.LinkActivated += Instance_LinkActivated;
    }

    private void Instance_LinkActivated(LinkActivation s)
    {
        sceneHolder.instance.isDeep = true;
        if (s.QueryString["id"] != null)
        {
            string id = s.QueryString["id"];
            print(id);

            sceneHolder.instance.id = id;
            if (SceneManager.GetActiveScene().buildIndex != 2)
                SceneManager.LoadScene(2);
        }
    }

    void OnDestroy()
    {
        DeepLinkManager.Instance.LinkActivated -= Instance_LinkActivated;
    }
}
