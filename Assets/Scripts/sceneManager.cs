﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class sceneManager : MonoBehaviour
{

    //string baseUrl = "https://mongence.herokuapp.com/api/portals/";
    public void reset()
    {
        if(galleryImporter.instance)
        galleryImporter.instance.StopAllCoroutines();
        SceneManager.LoadScene(2);
    }

    public void back()
    {
        SceneManager.LoadScene(1);
    }

    public void EditProfile()
    {
        SceneManager.LoadScene(3);
    }

    public void resetCustom()
    {
        SceneManager.LoadScene(1);
    }

    public void gamer()
    {
        if (galleryImporter.instance)
            galleryImporter.instance.StopAllCoroutines();
        SceneManager.LoadScene(3);
    }

    public static void errorReset()
    {
        if (galleryImporter.instance)
            galleryImporter.instance.StopAllCoroutines();
        SceneManager.LoadScene(1);
    }

    public void refresh()
    {
        if (galleryImporter.instance)
            galleryImporter.instance.StopAllCoroutines();

        sceneHolder.instance.loadWithId = true;

        FindObjectOfType<ARSession>().Reset();

        SceneManager.LoadScene(2);
    }
    public void resetS()
    {
        //SceneManager.LoadScene(0);
        Application.Quit();
    }

    public void share()
    {


        //string url = baseUrl + galleryImporter.currentId;
        string url = "https://qrco.de/bcgYtP";
        new NativeShare().SetTitle("MonAgence").SetSubject("je partage avec vous mon experience 360").SetText(url).Share();
    }
}
