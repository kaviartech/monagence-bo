﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class LeaderBoard : MonoBehaviour
{

    public string url = "https://gate.kaviar.app/projects/leaderBoard.php";

    IEnumerator Start()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
           Button temp = errorHandler.showError("leaderBoard", "if offline leaderBoard won't load");
           if (temp)
               temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });
            
        }
        else
        {
            using (UnityWebRequest www = UnityWebRequest.Post(url,""))
            {
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    string t = www.downloadHandler.text;

                    leaderB lb = JsonUtility.FromJson<leaderB>(t);

                    int i = 0;

                    foreach(var l in lb.leaders)
                    {
                        transform.GetChild(i).GetComponentsInChildren<TextMeshProUGUI>()[1].text = l.name;
                        transform.GetChild(i).GetComponentsInChildren<TextMeshProUGUI>()[0].text = l.score.Replace(" ","");
                        i++;
                    }

                }
            }
        }
    }

}
