﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class switchCollapse : MonoBehaviour
{
    public Sprite coll, norm;

    private void Start()
    {
        if (galleryImporter.instance.isCollapsible)
        {
            GetComponent<Image>().sprite = coll;
        }
        else
        {
            GetComponent<Image>().sprite = norm;
        }
    }

    public void Collapse()
    {
        galleryImporter.instance.isCollapsible = !galleryImporter.instance.isCollapsible;
        galleryImporter.instance.search("");
        if (galleryImporter.instance.isCollapsible)
        {
            GetComponent<Image>().sprite = coll;
        }
        else
        {
            GetComponent<Image>().sprite = norm;
        }
    }
    
}
