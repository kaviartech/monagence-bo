﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class mainMenuLang : MonoBehaviour
{

    public TextMeshProUGUI exploration, gamer, custom;

    void Start()
    {
        exploration.text = Lang.choose(En.exploaration, Fr.exploaration);
        gamer.text = Lang.choose(En.gamer, Fr.gamer);
        custom.text = Lang.choose(En.custom, Fr.custom);
    }

    
}
