﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class debugger : Singleton<debugger>
{

    public TextMeshProUGUI cullMask, stencil, cameraPos, spawnedPos, spawnedRot,  arSessionState;

    public bool isDebug;

    private void Start()
    {
        
        cullMask.gameObject.SetActive(isDebug);
        stencil.gameObject.SetActive(isDebug);
        cameraPos.gameObject.SetActive(isDebug);
        spawnedPos.gameObject.SetActive(isDebug);
        spawnedRot.gameObject.SetActive(isDebug);
        arSessionState.gameObject.SetActive(isDebug);


    }

    public static void updateCullMask(string value)
    {
        if(instance.isDebug)
        instance.cullMask.text = "cullMask : " + value;
    }

    public static void updateStencil(string value)
    {
        if(instance.isDebug)
        instance.stencil.text = "stencil : " + value;
    }
    public static void updateCamera(Vector3 value)
    {
        if (instance.isDebug)
            instance.cameraPos.text = "camera   : x :" + value.x + " y : " + value.y + " z : "+ value.z;
    }
    public static void updateSpawned(Vector3 value)
    {
        if (instance.isDebug)
            instance.spawnedPos.text = "Spawned   : x :" + value.x + " y : " + value.y + " z : " + value.z;
    }

    public static void updateSpawnedRot(Quaternion value)
    {
        if (instance.isDebug)
            instance.spawnedRot.text = "Spawned   : x :" + value.x + " y : " + value.y + " z : " + value.z;
    }

    public static void updateARState(string value)
    {
        if (instance.isDebug)
            instance.arSessionState.text = "AR State   : x :" + value;
    }
}
