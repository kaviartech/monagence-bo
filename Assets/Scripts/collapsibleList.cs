﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collapsibleList : MonoBehaviour
{
    
    public float minHeight = 100, maxHeight;

    public RectTransform rt;


    
    void Start()
    {
        rt = GetComponent<RectTransform>();

    }

    
    public void goTo(bool extended)
    {
        StopAllCoroutines();

        StartCoroutine(expandCollapse(extended));
    }

    IEnumerator expandCollapse(bool extend)
    {
        float targetValue = extend?maxHeight:minHeight;

        float extensionValue = extend ? maxHeight - minHeight : minHeight - maxHeight;

        transform.parent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, extensionValue);

        while (Mathf.Abs(rt.sizeDelta.y - targetValue) >= 1f)
        {
            rt.sizeDelta = Vector2.Lerp(rt.sizeDelta, new Vector2(792, targetValue),5* Time.deltaTime);

            yield return null;
        }
        rt.sizeDelta = new Vector2(792, targetValue);
    }

}
