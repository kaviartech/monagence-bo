﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sceneHolder : Singleton<sceneHolder>
{
    public string searchedValue;
    public string id;
    public bool loadWithId = false;
    public bool isDeep;
}
