//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.SceneManagement;

//public class FirebaseNotifier : MonoBehaviour
//{
//    private static FirebaseNotifier _instance;
//    public static FirebaseNotifier instance;
//    bool isOnAndroid = false;

//    void Awake()
//    {
//        if (_instance != null && _instance != this)
//        {
//            Destroy(gameObject);
//            return;
//        }
//        else
//        {
//            _instance = this;
//        }

//        instance = _instance;
//        DontDestroyOnLoad(this);

//#if UNITY_ANDROID
//        isOnAndroid = true;
//        Screen.fullScreen = false;
//#endif
//    }

//    public void Start()
//    {
//        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
//        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
//    }

//    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
//    {
//        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
//    }

//    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
//    {
//        UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
//    }


//    void OnApplicationPause(bool appPaused)
//    {
//        if (!isOnAndroid || Application.isEditor) { return; }

//        if (!appPaused)
//        {
//            //Returning to Application
//            Debug.Log("Application Resumed");
//            StartCoroutine(LoadSceneFromFCM());
//        }
//        else
//        {
//            //Leaving Application
//            Debug.Log("Application Paused");
//        }
//    }

//    IEnumerator LoadSceneFromFCM()
//    {
//        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
//        AndroidJavaObject curActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
//        AndroidJavaObject curIntent = curActivity.Call<AndroidJavaObject>("getIntent");

//        string sceneToLoad = curIntent.Call<string>("getStringExtra", "sceneToOpen");

//        Scene curScene = SceneManager.GetActiveScene();

//        if (!string.IsNullOrEmpty(sceneToLoad) && sceneToLoad != curScene.name)
//        {
//            // If the current scene is different than the intended scene to load,
//            // load the intended scene. This is to avoid reloading an already acive
//            // scene.
//            Debug.Log("Loading Scene: " + sceneToLoad);
//#if UNITY_ANDROID
//            Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
//#endif
//            Handheld.StartActivityIndicator();
//            yield return new WaitForSeconds(0f);

//            SceneManager.LoadScene(sceneToLoad);
//        }
//    }

//}
