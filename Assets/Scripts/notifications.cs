using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class notifications : MonoBehaviour
{
    public void Start()
    {
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        Debug.Log("Received a new message from: " + e.Message.Notification.ClickAction);
        Debug.Log("Received key event: " + e.Message.Data["event"]);
        sceneHolder.instance.isDeep = true;
        if (e.Message.Data.ContainsKey("id"))
        {
            string id = e.Message.Data["id"];
            print(id);

            sceneHolder.instance.id = id;
            if (SceneManager.GetActiveScene().buildIndex != 2)
                SceneManager.LoadScene(2);
        }
        
    }

}
