using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;

using UnityEngine.Rendering;




public class EventTexture : MonoBehaviour
{
    public string url;
    public bool raw;
    public bool img;
    public bool clip;
    public static bool mediaLoaded;
    // Start is called before the first frame update
    public void Awake()
    {
        mediaLoaded = false;
       
    }
    public IEnumerator Start()
    {
       yield return StartCoroutine(GetTexture());
    }

    public IEnumerator GetTexture()
    {

        if (!clip)
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();
            while (!www.isDone)
            {
                yield return www;
            }

            Texture myTexture = DownloadHandlerTexture.GetContent(www);
            if (raw)
                GetComponent<RawImage>().texture = myTexture;
            else if (img)
            {
                Texture2D tex = (Texture2D)myTexture;
                GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0, 0), .01f);
            }
            else
            {
                foreach (var mat in GetComponent<Renderer>().materials)
                {
                    mat.mainTexture = myTexture;
                }
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * (int)myTexture.height / myTexture.width);

            }
            www.Dispose();
            mediaLoaded = true;
        }
        else
        {
            AudioType audioType;
            if (url.Contains("mp3"))
                audioType = AudioType.MPEG;
            else
                audioType = AudioType.WAV;
            UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url,audioType);
            yield return www.SendWebRequest();
            while (!www.isDone)
            {
                yield return www;
            }
            AudioClip myClip = DownloadHandlerAudioClip.GetContent(www);
            Debug.Log(myClip);
            GetComponent<PlaceMultipleObjectsOnPlane>().clip = myClip;
            GetComponent<AudioSource>().clip = myClip;
            www.Dispose();
            mediaLoaded = true;
        }
    }
}
