﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class nativeBack : MonoBehaviour
{

    public bool isSceneOne = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isSceneOne)
            {
                Application.Quit();
            }
            else
            {
                SceneManager.LoadScene(0); 
            }
            
        }
    }



}
