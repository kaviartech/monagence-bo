﻿using System;
using System.Collections;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AuthManager : Singleton<AuthManager>
{
    [Space(20)]
    public bool isRegister;

    [Space(20)]
    [Header("Login")]
    [Space(30)]
    [SerializeField]
    private string userOrEmail;
    [SerializeField]
    private string password;
    [SerializeField]
    private bool remeberMe;
    [Space(10)]
    public TMP_InputField loginInput;

    public static responseData res;


    [Space(20)]
    [Header("Register")]
    [Space(30)]
    [SerializeField]
    private bool civility = true;
    [SerializeField]
    private string email;
    [SerializeField]
    private string origin;
    [SerializeField]
    private string firstName;
    [SerializeField]
    private string birthday;
    [SerializeField]
    private string lastName;
    [SerializeField]
    private string confirmRegisterPassword;
    [SerializeField]
    private bool termsAndConditions;
    [SerializeField]
    private string registerPassword;

    private DateTime selectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

    private string loginPath = "api/users/login";
    private string registerPath = "api/users/register";

    public string UserOrEmail { get => userOrEmail; set => userOrEmail = value; }
    public string Password { get => password; set => password = value; }
    public bool RemeberMe { get => remeberMe; set => remeberMe = value; }
    public bool Civility { get => civility; set => civility = value; }
    public string Email { get => email; set => email = value; }
    public string Origin { get => origin; set => origin = value; }
    public string FirstName { get => firstName; set => firstName = value; }
    public string LastName { get => lastName; set => lastName = value; }
    public string Birthday { get => birthday; set => birthday = value; }
    public string RegisterPassword { get => registerPassword; set => registerPassword = value; }
    public string ConfirmRegisterPassword { get => confirmRegisterPassword; set => confirmRegisterPassword = value; }
    public bool TermsAndConditions { get => termsAndConditions; set => termsAndConditions = value; }
    public DateTime SelectedDate
    {
        get
        {
            return selectedDate;
        }
        set
        {
            selectedDate = value;
        }
    }
    public static string origine = "monagence";
    //public string origine = "gate";
    //public string origine = "monagencetunis";

    public static Sprite profile;

   

    private IEnumerator Start()
    {
        if(PlayerPrefs.HasKey("rememberMe") && PlayerPrefs.HasKey("email") && PlayerPrefs.HasKey("password"))
        {
            if (PlayerPrefs.GetInt("rememberMe") == 1)
            {
                userOrEmail = PlayerPrefs.GetString("email");
                password = PlayerPrefs.GetString("password");
                login();
            }
        }
        Debug.Log("Start"); 
        CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        yield return new WaitForSeconds(0.5f);

        if (Common.getUserInfo(out string id, out string path))
        {
            print(id);
            SceneManager.LoadScene(1);
        }


    }

    public void toggleAuth()
    {
        GetComponent<Animator>().Play(isRegister ? "login" : "register");
        isRegister = !isRegister;
    }


    public void resetPassword()
    {
        string json = $"^'email' : '{UserOrEmail}'&".ToJSONForMail();
        print(json);
        this.fetchText(Common.Baseurl + "api/users/forgotPassword/", json, response =>
          {
              print(response);

              Common.alert("Veuillez vérifier votre email pour modifier votre mot de passe", "Please check your email to change your password");

          }
          , error =>
          {
              Common.alert("Aucun compte trouvé avec cet email", "No account found with this email");
          }
          );
    }


    public void login()
    {
        StartCoroutine(loginEnum());
        //Common.instance.load(1);
    }

    [System.Serializable]
    struct loginRequestData
    {
        public string email;
        public string password;

        public loginRequestData(string email, string password)
        {
            this.email = email;
            this.password = password;
        }
    }

    [System.Serializable]
    public struct responseData
    {
        
        public string status;
        public responseResult result;
    }

    [System.Serializable]
    public struct errorResponseData
    {

        public string status;
        public string message;
        public string stack;
    }

    [System.Serializable]
    public struct responseResult
    {
        public string _id;
        public string token;
        public string firstname, lastname, civility, origin, email;
    }

    IEnumerator loginEnum()
    {

        if (userOrEmail.Trim() == "")
        {
            Common.alert("Vous devez entrer votre email", "You must enter your email");
            yield break;
        }

        if (password == "")
        {
            Common.alert("Vous devez entrer votre mot de passe", "You must enter your password");
            yield break;
        }

        string jsonStr = JsonUtility.ToJson(new loginRequestData(userOrEmail, password));
        
        //print(jsonStr);

        //GameObject loading = Common.instance.startLoader("auth.login");

        GameObject wait = Common.alertAuth("Patientez...\nNous vous connectons.", "Please wait...\nWe are connecting you.");

        this.fetchText("https://mongence.herokuapp.com/" + loginPath, jsonStr,
            response =>
            {
                Destroy(wait);
                if (response.ToJSON().Contains("success"))
                {
                    res = JsonUtility.FromJson<responseData>(response);
                    if (res.status == "success" && res.result.origin == origine)
                    {

                        if (remeberMe)
                        {

                            PlayerPrefs.SetInt("rememberMe", 1);
                            PlayerPrefs.SetString("token", res.result.token);
                            PlayerPrefs.SetString("email", res.result.email);
                            PlayerPrefs.SetString("password", password);
                        }
                        PlayerPrefs.SetString("email", res.result.email);

                        SceneManager.LoadScene(1);
                        Common.alert("Connecté", "Connected");


                    }
                else
                {
                   
                        //Destroy(loading);
                        Common.alert("Aucun utilisateur n'a été trouvé avec ce nom d'utilisateur ou cette adresse e-mail", "No user was found with this username or email address");
                    
                }
                }
                print(response);
            },
            error =>
            {
                errorResponseData errorRes;
                Destroy(wait);
                //Destroy(loading);
                errorRes = JsonUtility.FromJson<errorResponseData>(error);
                if (errorRes.message.ToUpper().Contains("ACCOUNT"))
                {
                    //Destroy(loading);
                    Common.alert("Veuillez vérifier votre courrier avant de continuer.", "Please verify your mail before continuing.");
                }
                else if (errorRes.message.ToUpper().Contains("USER"))
                {
                    //Destroy(loading);
                    Common.alert("Aucun utilisateur n'a été trouvé avec ce nom d'utilisateur ou cette adresse e-mail", "No user was found with this username or email address");
                }
                else
                {
                    //Destroy(loading);
                    Common.alert("Votre mot de passe est incorrect", "Your password is incorrect");
                }
                //Common.alert("Assurez-vous que vous êtes connecté à Internet ... ou nos serveurs sont peut-être en maintenance ... essayez plus tard", "Make sure you are connected to the Internet ... or our servers may be under maintenance ... try later");
            }
        );

    }


    [System.Serializable]
    struct registerRequestData
    {
        public string firstname, lastname, email, civility, password, confirmPassword, birthday, origin;

        public registerRequestData(string firstName, string lastName,  string email, string civility, string password, string confirmPassowrd,  string birthday, string origin)
        {
            this.firstname = firstName;
            this.lastname = lastName;
            this.email = email;
            this.civility = civility;
            this.password = password;
            this.confirmPassword = confirmPassowrd;
            this.birthday = birthday;
            this.origin = origin;
        }
    }

    public void register()
    {
        StartCoroutine(registerEnum());
    }

    IEnumerator registerEnum()
    {
        print(civility);

        if (registerPassword != confirmRegisterPassword)
        {
            Common.alert("Votre mot de passe et confirmer le mot de passe de ne correspondent pas", "Your password and confirm password do not match");
            yield break;
        }

        if (/*!Regex.IsMatch(registerPassword, @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,32}$")*/ registerPassword.Length < 8)
        {
            Common.alert("Votre mot de passe doit contenir au moins 8 caractères", "Your password must contain at least 8 characters"/*caractères et au plus 32.il doit avoir au moins 1 chiffre et 1 lettre majuscule et minuscule*/);
            yield break;
        }

        if ((DateTime.Now - selectedDate).Days < (365 * 12.5f))
        {
            Common.alert("Vous devez avoir plus de 13 ans", "You must be older than 13 years old"/*caractères et au plus 32.il doit avoir au moins 1 chiffre et 1 lettre majuscule et minuscule*/);
            yield break;
        }

        if (!Regex.IsMatch(firstName, @"^[a-zA-Z ]{3,16}$"))
        {
            Common.alert("Votre prénom ne doit contenir que des lettres et au moins 3 caractères", "Your first name must contain only letters and at least 3 characters");
            yield break;
        }

        if (!Regex.IsMatch(lastName, @"^[a-zA-Z ]{3,16}$"))
        {
            Common.alert("Votre nom ne doit contenir que des lettres et au moins 3 caractères", "Your last name must contain only letters and at least 3 characters");
            yield break;
        }


        if (!Regex.IsMatch(email, @"[\w-\.]+@([\w-]+\.)+[\w-]{2,4}"))
        {
            Common.alert("Votre email n'est pas valide", "Your email is invalid");
            yield break;
        }

        if (!termsAndConditions)
        {
            Common.alert("Veuillez accepter les termes et conditions", "Please accept the terms and conditions");
            yield break;
        }

        string jsonStr = "";

        if (Application.identifier == "com.kaviar.Gate")
        {
            //Common.alert("gaaaaate");
            jsonStr = JsonUtility.ToJson(new registerRequestData(firstName, lastName, email, civility ? "M" : "Mme", registerPassword, confirmRegisterPassword , selectedDate.ToShortDateString(), "Gate"));
            Debug.Log(jsonStr);
        } 
        else if (Application.identifier == "com.kaviar.Monagence")
        {
            jsonStr = JsonUtility.ToJson(new registerRequestData(firstName, lastName, email, civility ? "M" : "Mme", registerPassword, confirmRegisterPassword, selectedDate.ToShortDateString(), "monagence"));
            Debug.Log(jsonStr);

        }
        else if (Application.identifier == "com.kaviar.Monagencetunisie")
        {
            jsonStr = JsonUtility.ToJson(new registerRequestData(firstName, lastName, email, civility ? "M" : "Mme", registerPassword, confirmRegisterPassword, selectedDate.ToShortDateString(), "monagencetunis"));
            Debug.Log(jsonStr);

        }
        else
        {
            jsonStr = JsonUtility.ToJson(new registerRequestData(firstName, lastName, email, civility ? "M" : "Mme", registerPassword, confirmRegisterPassword, selectedDate.ToShortDateString(), "Gate"));
            Debug.Log(jsonStr);

        }



        //print(jsonStr);

        //GameObject loading = Common.instance.startLoader("auth.signup");

        GameObject wait = Common.alert("Veuillez patienter", "Please wait");

        //this.fetchText(Common.Baseurl + registerPath, jsonStr,
        this.fetchText("https://mongence.herokuapp.com/" + registerPath, jsonStr,
            response =>
            {
                Destroy(wait);
                print(response.ToJSON());
                responseData res;
                //print(request.downloadHandler.text);
                if (response.ToJSON().Contains("success"))
                {
                    res = JsonUtility.FromJson<responseData>(response);
                    if (res.status == "success")
                    {
                        loginInput.text = email;

                        //Destroy(loading);

                        toggleAuth();
                        //print(res.token);

                        //Common.instance.load(1);

                        Common.alert("Compte créé, veuillez vérifier votre boîte de réception e-mail", "Account created, please check your email inbox");

                    }
                }
                else {
                   
                }
            },
            error =>
            {
                errorResponseData errorRes;
                print(error.ToJSON());
                Destroy(wait);
                errorRes = JsonUtility.FromJson<errorResponseData>(error);

                if (errorRes.message.ToUpper().Contains("ALREADY"))
                {
                    //Destroy(loading);
                    Common.alert("Le nom d'utilisateur ou l'adresse e-mail existent déjà, essayez de les changer", "The username or email address already exists, try to change them");
                }
                else if (errorRes.message.ToUpper().Contains("MATCH"))
                {
                    //Destroy(loading);
                    Common.alert("Votre mot de passe et confirmer le mot de passe de ne correspondent pas", "Your password and confirm password do not match");
                }
                else
                {
                    //Destroy(loading);
                    Common.alert("Assurez-vous que vous êtes connecté à Internet ... ou nos serveurs sont peut-être en maintenance ... essayez plus tard", "Make sure you are connected to the internet ... or our servers may be under maintenance ... try later");
                }
            });


    }


}
