﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clickTag : MonoBehaviour
{
    public void clickPhoto()
    {
        Button temp = errorHandler.showHelp();
        if (temp)
            temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.parent.gameObject, 1); });

    }

    public void click3D()
    {
        clickPhoto();
    }

    public void clickVideo()
    {
        clickPhoto();
    }
}
