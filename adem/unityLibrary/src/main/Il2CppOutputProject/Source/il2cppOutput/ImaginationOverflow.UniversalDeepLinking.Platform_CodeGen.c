﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::GetProvider(System.Boolean)
extern void LinkProviderFactory_GetProvider_mB4992EC88B46E9E394637FDD8043451C0F046D80 (void);
// 0x00000002 System.Void ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::.ctor()
extern void LinkProviderFactory__ctor_mA25C2DA9A5547F28574D29E54E49BACD334CE444 (void);
// 0x00000003 System.Boolean ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::Initialize()
extern void AndroidLinkProvider_Initialize_mE347CE03FDFF77D3F8EED9F1B1E1F139C4A1E70B (void);
// 0x00000004 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::VerifyIfHasOpenByLink()
extern void AndroidLinkProvider_VerifyIfHasOpenByLink_m26474818C784E3232EA131EF1E67E6BD3678C644 (void);
// 0x00000005 System.String ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::safeCallStringMethod(UnityEngine.AndroidJavaObject,System.String,System.Object[])
extern void AndroidLinkProvider_safeCallStringMethod_mA5C49D91356FEFA8F8E50227AADF5894EC71BEE3 (void);
// 0x00000006 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::add__linkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_add__linkReceived_mDB2248244B9EBA4441E4491B65615CF59AFAE621 (void);
// 0x00000007 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::remove__linkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_remove__linkReceived_m787947E6D0E7D2F38DB8A66EFE308C4EDE7CEFF7 (void);
// 0x00000008 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::add_LinkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_add_LinkReceived_m17BEE8D5B667F5CE7AA807E6B68AEACD26FE13EA (void);
// 0x00000009 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::remove_LinkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_remove_LinkReceived_m0B0D1E747EC4022764B3D4A393C2EB6C6E58C147 (void);
// 0x0000000A System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::PollInfoAfterPause()
extern void AndroidLinkProvider_PollInfoAfterPause_mB71DB3B9328B9D51B8EDFB35A8971A5B73C3B110 (void);
// 0x0000000B System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::OnLinkReceived(System.String)
extern void AndroidLinkProvider_OnLinkReceived_mCA2F19CC45F218A5EF81FD4FE20CD708D60052E4 (void);
// 0x0000000C System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::.ctor()
extern void AndroidLinkProvider__ctor_m063EBDB4B3689A66CF8E80100A5AFBDEF457100F (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	LinkProviderFactory_GetProvider_mB4992EC88B46E9E394637FDD8043451C0F046D80,
	LinkProviderFactory__ctor_mA25C2DA9A5547F28574D29E54E49BACD334CE444,
	AndroidLinkProvider_Initialize_mE347CE03FDFF77D3F8EED9F1B1E1F139C4A1E70B,
	AndroidLinkProvider_VerifyIfHasOpenByLink_m26474818C784E3232EA131EF1E67E6BD3678C644,
	AndroidLinkProvider_safeCallStringMethod_mA5C49D91356FEFA8F8E50227AADF5894EC71BEE3,
	AndroidLinkProvider_add__linkReceived_mDB2248244B9EBA4441E4491B65615CF59AFAE621,
	AndroidLinkProvider_remove__linkReceived_m787947E6D0E7D2F38DB8A66EFE308C4EDE7CEFF7,
	AndroidLinkProvider_add_LinkReceived_m17BEE8D5B667F5CE7AA807E6B68AEACD26FE13EA,
	AndroidLinkProvider_remove_LinkReceived_m0B0D1E747EC4022764B3D4A393C2EB6C6E58C147,
	AndroidLinkProvider_PollInfoAfterPause_mB71DB3B9328B9D51B8EDFB35A8971A5B73C3B110,
	AndroidLinkProvider_OnLinkReceived_mCA2F19CC45F218A5EF81FD4FE20CD708D60052E4,
	AndroidLinkProvider__ctor_m063EBDB4B3689A66CF8E80100A5AFBDEF457100F,
};
static const int32_t s_InvokerIndices[12] = 
{
	2125,
	3397,
	3368,
	3397,
	4191,
	2797,
	2797,
	2797,
	2797,
	3397,
	2797,
	3397,
};
extern const CustomAttributesCacheGenerator g_ImaginationOverflow_UniversalDeepLinking_Platform_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_ImaginationOverflow_UniversalDeepLinking_Platform_CodeGenModule;
const Il2CppCodeGenModule g_ImaginationOverflow_UniversalDeepLinking_Platform_CodeGenModule = 
{
	"ImaginationOverflow.UniversalDeepLinking.Platform.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_ImaginationOverflow_UniversalDeepLinking_Platform_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
