﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


public class portalManager : MonoBehaviour
{
    public GameObject cam;

    public Animator door;
    taskbar customButton;
    public GameObject scene;
    public static bool isInside;
    public GameObject gameTp;

    public List<Material> mats;
    
    private Material portalPlaneMat;
    public TextMeshProUGUI ntRState;
    public GameObject ntRStateGO;

    // Start is called before the first frame update
    void Start()
    {
        customButton = GameObject.FindGameObjectWithTag("customButton").transform.GetComponent<taskbar>();
        //foreach (GameObject go in GameObject.FindGameObjectsWithTag("customButton"))
        //    customButton = go.GetComponent<Animator>();
        isInside = false;
        cam = Camera.main.gameObject;

        //mats = scene.GetComponent<Renderer>().sharedMaterials;

        Renderer[] rends = scene.GetComponentsInChildren<Renderer>();

        mats = new List<Material>();

        foreach(Renderer r in rends)
        {
            mats.AddRange(r.sharedMaterials);
        }

        if (gameTp)
        {
            mats.Add(gameTp.GetComponentInChildren<Renderer>().sharedMaterial);
        }


        Image[] imgs = scene.GetComponentsInChildren<Image>();

        foreach(var img in imgs)
        {
            mats.Add(img.material);
        }

        TextMeshProUGUI[] txts = scene.GetComponentsInChildren<TextMeshProUGUI>();

        foreach (var txt in txts)
        {
            mats.Add(txt.fontMaterial);
        }


        portalPlaneMat = GetComponent<Renderer>().sharedMaterial;

        //enable stencil test
        foreach (var mat in mats)
        {
            mat.SetInt("_stencilComp", (int)CompareFunction.Equal);
        }

        portalPlaneMat.SetInt("_CullMode", (int)CullMode.Back);

        debugger.updateStencil("equal");
        debugger.updateCullMask("back");
    }

    

    private void OnTriggerStay(Collider other)
    {
        Vector3 camPosition = transform.InverseTransformPoint(cam.transform.position);
        //print(camPosition.z);

        /*if (camPosition.z >= 0.0f && camPosition.z <= 0.1f)
        {
            Vector3 dirFromAtoB = (cam.transform.position - transform.position).normalized;
            dirFromAtoB.y = 0;
            Vector3 camForward = cam.transform.forward;
            camForward.y = 0;
            float dotProd = Vector3.Dot(dirFromAtoB, camForward);

            print("when inside" + dotProd);

            if (dotProd <= -0.0)
            {
                print("looking at portal");

                foreach (var mat in mats)
                {
                    mat.SetInt("_stencilComp", (int)CompareFunction.Never);
                }
            }
        }
         else */
        if (camPosition.z >= 0.0f)
        {
            isInside = true;

            foreach (var mat in mats)
            {

                mat.SetInt("_stencilComp", (int)CompareFunction.NotEqual);

            }

            portalPlaneMat.SetInt("_CullMode", (int)CullMode.Front);

            debugger.updateStencil("notEqual");
            debugger.updateCullMask("front");

        }
        else if (camPosition.z >= -0.1f)
        {
            isInside = false;

            /* Vector3 dirFromAtoB = (cam.transform.position - transform.position).normalized;
             dirFromAtoB.y = 0;
             Vector3 camForward = cam.transform.forward;
             camForward.y = 0;
             float dotProd = Vector3.Dot(dirFromAtoB,camForward );

             print("when outside" + dotProd);

             if (dotProd >= 0.0)
             {
                 print("looking at portal");

                 foreach (var mat in mats)
                 {
                     mat.SetInt("_stencilComp", (int)CompareFunction.Never);
                 }
             }
             else
             {
                 //disable stencil test
                 foreach (var mat in mats)
                 {
                     mat.SetInt("_stencilComp", (int)CompareFunction.Always);
                 }

                 portalPlaneMat.SetInt("_CullMode", (int)CullMode.Off);
             }*/

            //disable stencil test
            foreach (var mat in mats)
            {
                mat.SetInt("_stencilComp", (int)CompareFunction.Always);
            }

            portalPlaneMat.SetInt("_CullMode", (int)CullMode.Off);

            debugger.updateStencil("always");
            debugger.updateCullMask("off");
            //foreach (GameObject go in GameObject.FindGameObjectsWithTag("Respawn"))
            //    go.GetComponent<MeshRenderer>().enabled = isInside;
            //foreach (GameObject go in GameObject.FindGameObjectsWithTag("canvaresp"))
            //    go.GetComponent<Canvas>().enabled = isInside;

        }

        else
        {
            //enable stencil test
            foreach (var mat in mats)
            {
                mat.SetInt("_stencilComp", (int)CompareFunction.Equal);
            }

            portalPlaneMat.SetInt("_CullMode", (int)CullMode.Back);

            debugger.updateStencil("equal");
            debugger.updateCullMask("back");

        }

        
    }
    private void OnTriggerEnter(Collider other)
    {
        door.Play("openold");
        customButton.pop();
        GetComponent<AudioSource>().Play();
    }

    bool isAlert = false;
    private NotTrackingReason[] ntR =  new NotTrackingReason[2];
    
    void Update()
    {
        ntR[0] = ARSession.notTrackingReason;
        if (ARSession.notTrackingReason != NotTrackingReason.None && ntR[0] != ntR[1])
        {
            isAlert = true;
            ntRStateGO.SetActive(isAlert);

        }
        else if(ARSession.notTrackingReason == NotTrackingReason.None)
        {
            ntRStateGO.SetActive(isAlert);
            ntR[1] = ARSession.notTrackingReason;
        }
        if (isAlert)
        {
            if (ARSession.notTrackingReason == NotTrackingReason.ExcessiveMotion)
            {
               ntRState.text = Lang.choose("Veuillez ralentir votre mouvement", "Please slow your mouvement");
            }
            else if (ARSession.notTrackingReason == NotTrackingReason.InsufficientFeatures)
            {
                ntRState.text = Lang.choose("Veuillez diriger votre telephone vers une surface ayant plus de details", "Please point your phone toward a surface with more details");
            }
            else if (ARSession.notTrackingReason == NotTrackingReason.InsufficientLight)
            {
                ntRState.text = Lang.choose("Veuillez diriger votre telephone vers une surface moins sombre", "Please point your phone toward a surface with more light");
            }
            else if (ARSession.notTrackingReason == NotTrackingReason.Relocalizing)
            {
                ntRState.text = Lang.choose("Veuillez diriger votre telephone vers le sol", "Please point your phone toward the ground");
            }
            ntR[1] = ARSession.notTrackingReason;
            isAlert = false;
        }
     }

   
}
