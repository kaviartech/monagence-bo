﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using System.Text;
using System;


public class cashing : MonoBehaviour
{
    public static string baseUrl = "https://mongence.herokuapp.com";

    //public static string projectsUrl = "https://monagence-kaviar.herokuapp.com/api/user/countries";
    public static string projectsUrl = "https://mongence.herokuapp.com/api/portals";
    //public static string projectsUrl = "https://gate.kaviar.app/projects/getByCountryWithAb.php";

    public static string dynamicProjectsUrl = "https://gate.kaviar.app/index.php/api/objects/getgallery";

    public static string testProjectsUrl = "https://gate.kaviar.app/index.php/api/objects/getgallerytest";

    public static string getprojectUrl = "https://mongence.herokuapp.com/api/portals";
    //public static string getprojectUrl = "https://gate.kaviar.app/projects/getByIdWithAb.php";

    public static string dynamicGetprojectUrl = "https://mongence.herokuapp.com/api/portals";
    //public static string dynamicGetprojectUrl = "https://gate.kaviar.app/index.php/api/objects/getproject";

    public static string gameModeUrl = "https://gate.kaviar.app/projects/gameMode.php";

    public static string gallerieJson = Application.persistentDataPath + "/portalsJson.json";

    public static string gameModeJson = Application.persistentDataPath + "/gameModeJson.json";

    public static string mainJson = Application.persistentDataPath + "/";

    static Texture2D LoadJPG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(10, 10);
            //tex.LoadRawTextureData(fileData);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }


    //public static IEnumerator LoadAssetBundle(string url, int id, int versionNumber, Slider sd)
    //{
    //    url = baseUrl + url;

    //    if (true)//(File.Exists(Application.persistentDataPath + "/uploads/bundles/" + id + "v" + versionNumber + ".unity3d"))
    //    {
    //        url = Application.persistentDataPath + "/uploads/bundles/" + id + "v" + versionNumber + ".unity3d";

    //        AssetBundleCreateRequest bundleCre = AssetBundle.LoadFromFileAsync(url);

    //        AssetBundle bundle = bundleCre.assetBundle;

    //        AssetBundleRequest request = bundle.LoadAssetAsync<GameObject>("test1");

    //        yield return request;

    //        GameObject obj = request.asset as GameObject;

    //        textureHolder.AB = obj;

    //        //Instantiate(obj);

    //        bundle.Unload(false);

    //        yield return null;

    //        sd.value = 1;

    //        sd.maxValue = 1;

    //        yield break;
    //    }


    //    //UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(url);

    //    UnityWebRequest www = UnityWebRequest.Get(url);

    //    www.SendWebRequest();

    //    while (!www.isDone)
    //    {
    //        //print(www.downloadProgress);
    //        sd.value = 1 + www.downloadProgress;
    //        yield return null;
    //    }

    //    if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
    //    {
    //        Debug.Log(www.error);
    //    }
    //    else
    //    {
    //        //print("here");
    //        byte[] bytes = www.downloadHandler.data;

    //        File.WriteAllBytes(Application.persistentDataPath + "/uploads/bundles/" + id + "v" + versionNumber + ".unity3d", bytes);

    //        AssetBundleCreateRequest bundleCre;

    //        yield return bundleCre = AssetBundle.LoadFromFileAsync(Application.persistentDataPath + "/uploads/bundles/" + id + "v" + versionNumber + ".unity3d");

    //        AssetBundle bundle = bundleCre.assetBundle;

    //        //AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);

    //        AssetBundleRequest request = bundle.LoadAssetAsync<GameObject>("test1");

    //        // Wait for completion
    //        yield return request;

    //        /*while (!request.isDone)
    //        {
    //            print(request.progress);
    //            sd.value = request.progress;
    //            yield return null;
    //        }*/

    //        // I don't want to instantiate like this right now

    //        GameObject obj = request.asset as GameObject;

    //        textureHolder.AB = obj;

    //        //Instantiate(obj);

    //        bundle.Unload(false);

    //        www.Dispose();

    //        yield return null;

    //        sd.maxValue = 1;
    //    }
    //}


    //public static IEnumerator load360InGameMode(int id, int versionNumber)
    //{
    //    using (var uwr = new UnityWebRequest("file:///"  + Application.persistentDataPath + "/uploads/images360/" + id + "v" + versionNumber + ".png", UnityWebRequest.kHttpVerbGET))
    //    {

    //        uwr.downloadHandler = new DownloadHandlerTexture();
    //        uwr.SendWebRequest();

    //        while (!uwr.isDone)
    //        {
    //            yield return uwr;
    //        }

    //        Texture2D temp = DownloadHandlerTexture.GetContent(uwr);

    //        byte[] bytes = temp.EncodeToJPG(100);

    //        string fileSavePath = Application.persistentDataPath + "/uploads/images360/" + id + "v" + versionNumber + ".png";

    //        BigFileTransfer.WriteAllBytesAsync(bytes, fileSavePath, success =>
    //        {
    //            if (success)
    //            {
    //                Debug.LogWarning("file has been written successfully in: " + fileSavePath);
    //            }
    //            else
    //            {
    //                Debug.LogWarning("could not write file in: " + fileSavePath);
    //            }
    //        });

    //        //File.WriteAllBytes(Application.persistentDataPath + "/uploads/images360/" + id + "v" + versionNumber + ".png", bytes);
    //    }
    //}

    //public static gMJson loadGameModeJson()
    //{
    //    if (File.Exists(gameModeJson))
    //    {
    //        string t = LoadData(gameModeJson);
    //        return JsonUtility.FromJson<gMJson>(t);
    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}


    /// <summary>
    /// manages the downloading of a 360 image and checks if it is already downloaded and loads it locally
    /// </summary>
    //public static IEnumerator getAllTps(List<mainJson> tps)
    //{
    //    textureHolder.tps = new Dictionary<int, Texture2D>();
    //    foreach (var tp in tps)
    //    {
    //        string filePath = Application.persistentDataPath + "/uploads/tps/" + tp.id + "v" + tp.versionNumber + ".png";

    //        print(filePath);

    //        /*Texture2D temp = new Texture2D(5000,2500,TextureFormat.ARGB32,false);

    //        temp.LoadRawTextureData(File.ReadAllBytes(filePath));

    //        textureHolder.images360.Clear();

    //        textureHolder.images360.Add(id, temp);*/

    //        using (var uwr = new UnityWebRequest("file:///" + filePath))
    //        {

    //            uwr.downloadHandler = new DownloadHandlerTexture();
    //            uwr.SendWebRequest();

    //            while (!uwr.isDone)
    //            {
    //                yield return uwr;
    //            }

    //            Texture2D temp = DownloadHandlerTexture.GetContent(uwr);

    //            textureHolder.tps.Add(tp.id, temp);
    //        }
    //    }

    //}



    /// <summary>
    /// manages the downloading of a 360 image and checks if it is already downloaded and loads it locally
    /// </summary>
    public static IEnumerator download360(string url, string id, Slider sd)
    {
        //print(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg");
        Debug.Log(textureHolder.images360.ContainsKey(id));
        if (!textureHolder.images360.ContainsKey(id))
        {
            Debug.Log(File.Exists(Application.persistentDataPath + "/uploads/images360/" + id + ".png"));
            if (!File.Exists(Application.persistentDataPath + "/uploads/images360/" + id +".png"))
            {

                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    if (PlayerPrefs.GetInt("isFR") == 1)
                    {
                        Button temp = errorHandler.showError(Fr.assetProblem, Fr.useOfflineOnce);
                        if (temp)
                            temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });
                        yield break;
                    }
                    else
                    {
                        Button temp = errorHandler.showError(En.assetProblem, En.useOfflineOnce);
                        if (temp)
                            temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });
                        yield break;
                    }

                    
                }


                using (var uwr = new UnityWebRequest(url, UnityWebRequest.kHttpVerbGET))
                {

                    uwr.downloadHandler = new DownloadHandlerTexture();
                    uwr.SendWebRequest();

                    while (!uwr.isDone)
                    {
                        if (sd)
                            sd.value = uwr.downloadProgress;
                        yield return uwr;
                    }

                    if (uwr.result == UnityWebRequest.Result.ProtocolError || uwr.result == UnityWebRequest.Result.ProtocolError)
                    {
                        print(uwr.error);
                        Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                        if (btnErr)
                            btnErr.onClick.AddListener(delegate { Application.Quit(); });
                    }

                    Texture2D temp = DownloadHandlerTexture.GetContent(uwr);

                    print("this is the url  :  " + url);

                    foreach (KeyValuePair<string, Texture2D> item in textureHolder.images360)
                    {
                        //Resources.UnloadAsset(item.Value);
                        Destroy(item.Value);
                    }

                    textureHolder.images360.Clear();
                    textureHolder.images360.Add(id, temp);

                    print("before encoding");

                    byte[] bytes = temp.EncodeToJPG(100);



                    print("after encoding");

                    string fileSavePath = Application.persistentDataPath + "/uploads/images360/" + id + ".png";

                    BigFileTransfer.WriteAllBytesAsync(bytes, fileSavePath, success =>
                    {
                        if (success)
                        {
                            Debug.LogWarning("file has been written successfully in: " + fileSavePath);
                        }
                        else
                        {
                            Debug.LogWarning("could not write file in: " + fileSavePath);
                        }
                    });

                    //File.WriteAllBytes(Application.persistentDataPath + "/uploads/images360/" + id + "v" + versionNumber + ".png", bytes);
                }
                //foreach (KeyValuePair<string, Texture2D> item in textureHolder.images360)
                //{
                //    //Resources.UnloadAsset(item.Value);
                //    Destroy(item.Value);
                //}

                


                //print("after encoding");

                //string fileSavePath = Application.persistentDataPath + "/uploads/images360/" + id + ".png";
                //byte[] uncodedTex = Convert.FromBase64String(url.Remove(0, 23));



                //Debug.Log(Convert.ToBase64String(Convert.FromBase64String(url.Remove(0, 23))));
                //if (uncodedTex == null)
                //{
                //    Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                //    if (btnErr)
                //        btnErr.onClick.AddListener(delegate { Application.Quit(); });
                //}
                //Texture2D tempTex = new Texture2D(2, 2);
                //tempTex.LoadImage(uncodedTex);
                //tempTex.Apply();
                //textureHolder.images360.Clear();
                //textureHolder.images360.Add(id, tempTex);

                //print("before encoding");

                //byte[] bytes = tempTex.EncodeToJPG(100);
                //BigFileTransfer.WriteAllBytesAsync(bytes, fileSavePath, success =>
                //{
                //    if (success)
                //    {
                //        Debug.LogWarning("file has been written successfully in: " + fileSavePath);
                //    }
                //    else
                //    {
                //        Debug.LogWarning("could not write file in: " + fileSavePath);
                //    }
                //});
            }
            else
            {
                //textureHolder.images360.Add(id, LoadJPG(Application.persistentDataPath + "/uploads/images360/" + id + "v" + versionNumber + ".png"));

                //yield return StartCoroutine(asyncLoad(Application.persistentDataPath + "/uploads/images360/" + id + "v" + versionNumber + ".png",id,sd));

                string filePath = Application.persistentDataPath + "/uploads/images360/" + id +".png";

                print(filePath);

                /*Texture2D temp = new Texture2D(5000,2500,TextureFormat.ARGB32,false);

                temp.LoadRawTextureData(File.ReadAllBytes(filePath));
               
                textureHolder.images360.Clear();
               
                textureHolder.images360.Add(id, temp);*/

                using (var uwr = new UnityWebRequest("file:///" + filePath))
                {

                    uwr.downloadHandler = new DownloadHandlerTexture();
                    uwr.SendWebRequest();

                    while (!uwr.isDone)
                    {
                        if (sd)
                            sd.value = uwr.downloadProgress;
                        yield return uwr;
                    }

                    Texture2D temp = DownloadHandlerTexture.GetContent(uwr);

                    

                    //print("this is the url  :  " + baseUrl + url);

                    foreach (KeyValuePair<string, Texture2D> item in textureHolder.images360)
                    {
                        //Resources.UnloadAsset(item.Value);
                        Destroy(item.Value);
                    }

                    textureHolder.images360.Clear();
                    textureHolder.images360.Add(id, temp);
                }

            }
        }
    }


    /// <summary>
    /// manages the downloading of a thumbnail of a project and checks if it is already downloaded and loads it locally
    /// </summary>
    public static IEnumerator download(string url, string id, bool isBlurred = false)
    {
        //print(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg");
        if((!textureHolder.thumbnailes.ContainsKey(id) && !isBlurred) || (!textureHolder.blurredThumbnailes.ContainsKey(id) && isBlurred))
        {
            if ((!File.Exists(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg") && !isBlurred) || (!File.Exists(Application.persistentDataPath + "/uploads/blurredThumbnail/" + id + ".jpg") && isBlurred))
            {
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    if(PlayerPrefs.GetInt("isFR") == 1)
                    {
                        Button temp = errorHandler.showError(Fr.error, Fr.openOnceOnline);
                        if (temp)
                            temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });
                        yield break;
                    }
                    else
                    {
                        Button temp = errorHandler.showError(En.error, En.openOnceOnline);
                        if (temp)
                            temp.onClick.AddListener
                                (delegate
                                        {
                                            Destroy(temp.transform.parent.parent.gameObject, 1);
                                            galleryImporter.instance.loading.SetActive(false);
                                        }
                                );
                        yield break;
                    }

                }

                using (var uwr = new UnityWebRequest(url, UnityWebRequest.kHttpVerbGET))
                {
                    uwr.downloadHandler = new DownloadHandlerTexture();
                    yield return uwr.SendWebRequest();

                    while (!uwr.isDone)
                    {
                        yield return uwr;
                    }
                    if (uwr.result == UnityWebRequest.Result.ProtocolError || uwr.result == UnityWebRequest.Result.ProtocolError)
                    {
                        print(uwr.error);
                        Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                        if (btnErr)
                            btnErr.onClick.AddListener(delegate { Application.Quit(); });
                    }
                    Debug.Log(uwr);
                    Texture2D temp = DownloadHandlerTexture.GetContent(uwr);
                    uwr.Dispose();

                    print("this is the url  :  " + url);
                    
                    if (isBlurred)
                    {

                        textureHolder.blurredThumbnailes.Add(id, temp);

                        byte[] bytes = temp.EncodeToJPG();

                        File.WriteAllBytes(Application.persistentDataPath + "/uploads/blurredThumbnail/" + id + ".jpg", bytes);

                    }
                    else
                    {
                        textureHolder.thumbnailes.Add(id, temp);

                        byte[] bytes = temp.EncodeToJPG();

                        File.WriteAllBytes(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg", bytes);
                    }
                }
                //byte[] uncodedText = Convert.FromBase64String(url.Remove(0, 23));
                //Debug.Log(Convert.ToBase64String(Convert.FromBase64String(url.Remove(0,23))));
                //if (uncodedText == null)
                //{
                //    Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                //    if (btnErr)
                //        btnErr.onClick.AddListener(delegate { Application.Quit(); });
                //}
                //Texture2D tempTex = new Texture2D(2,2);
                //tempTex.LoadImage(uncodedText);
                //tempTex.Apply();
                //print("this is the url  :  ");

                //if (isBlurred)
                //{

                //    textureHolder.blurredThumbnailes.Add(id, tempTex);

                //    byte[] bytes = tempTex.EncodeToJPG();

                //    File.WriteAllBytes(Application.persistentDataPath + "/uploads/blurredThumbnail/" + id + ".jpg", bytes);

                //}
                //else
                //{
                //    textureHolder.thumbnailes.Add(id, tempTex);

                //    byte[] bytes = tempTex.EncodeToJPG();

                //    File.WriteAllBytes(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg", bytes);
                //}
            }
            else
            {
                if (!isBlurred)
                {
                    textureHolder.thumbnailes.Add(id, LoadJPG(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg"));
                }
                else
                {
                    textureHolder.blurredThumbnailes.Add(id, LoadJPG(Application.persistentDataPath + "/uploads/blurredThumbnail/" + id + ".jpg"));
                }
            }
        }
    }


    public static IEnumerator getPortalsJson()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            if (File.Exists(gallerieJson))
            {
                string t = LoadData(gallerieJson);
                galleryImporter.instance.totalPj = JsonUtility.FromJson<portalsJson>(t);

            }
            else
            {
                if(PlayerPrefs.GetInt("isFR") == 1)
                {
                    Button temp = errorHandler.showError(Fr.checkInternet, Fr.openOnceOnline);
                    if (temp)
                        temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

                }
                else
                {
                    Button temp = errorHandler.showError(En.checkInternet, En.openOnceOnline);
                    if (temp)
                        temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

                }
            }
        }
        else
        {
            using (UnityWebRequest www = UnityWebRequest.Get(projectsUrl))
            {
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    print(www.downloadHandler.text);
                    string t = www.downloadHandler.text;
                    //string t;
                    //#if UNITY_EDITOR
                    //            t = LoadData("Assets/freeformatter-out.json");
                    //#endif
                    //            t = LoadData(Application.persistentDataPath+"/freeformatter-out.json");
                    SaveData(gallerieJson, t);
                    Debug.Log(gallerieJson);
                    print("here");
                    galleryImporter.instance.totalPj = JsonUtility.FromJson<portalsJson>(t);
                    foreach (portal p in galleryImporter.instance.totalPj.result)
                    {
                        print(p.origin);
                        print(p._id);
                        if (!textureHolder.portalJsons.TryGetValue(p._id, out string pJs))
                        {
                            Debug.Log(p._id);
                            textureHolder.portalJsons.Add(p._id, JsonUtility.ToJson(p));
                            File.WriteAllText(cashing.mainJson + p._id + ".json", JsonUtility.ToJson(p));
                        }
                        foreach (panorama pano in p.panoramas)
                        {
                            if (!textureHolder.mainJsons.ContainsKey(pano._id))
                            {

                                textureHolder.mainJsons.Add(pano._id, JsonUtility.ToJson(pano));
                                File.WriteAllText(cashing.mainJson + pano._id.ToString() + ".json", JsonUtility.ToJson(pano));
                            }
                            print(JsonUtility.ToJson(pano));
                        }
                    }
                    yield break;
                    //}
                    //}

                    //using (UnityWebRequest www = UnityWebRequest.Post(dynamicProjectsUrl, ""))
                    //{
                    //    yield return www.SendWebRequest();

                    //    if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
                    //    {
                    //        print(www.error);
                    //        Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                    //        if (btnErr)
                    //            btnErr.onClick.AddListener(delegate { Application.Quit(); });
                    //    }
                    //    else
                    //    {
                    //        //print(www.downloadHandler.text);
                    //        string t = www.downloadHandler.text;
                    //        galleryImporter.instance.totalPj.append(JsonUtility.FromJson<portalsJson>(t));
                    //        SaveData(gallerieJson, JsonUtility.ToJson(galleryImporter.instance.totalPj));
                    //    }
                    //}

                    //using (UnityWebRequest www = UnityWebRequest.Post(testProjectsUrl, ""))
                    //{
                    //    yield return www.SendWebRequest();

                    //    if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
                    //    {
                    //        print(www.error);
                    //        Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                    //        if (btnErr)
                    //            btnErr.onClick.AddListener(delegate { Application.Quit(); });
                    //    }
                    //    else
                    //    {
                    //        //print(www.downloadHandler.text);
                    //        string t = www.downloadHandler.text;
                    //        galleryImporter.instance.totalPj.append(JsonUtility.FromJson<portalsJson>(t));
                    //        SaveData(gallerieJson, JsonUtility.ToJson(galleryImporter.instance.totalPj));
                    //    }
                }

            }
        }
    }

    public static IEnumerator getPortal(string id)
    {
        if (textureHolder.mainJsons.ContainsKey(id))
        {
            yield break;
        }

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {


            if (File.Exists(mainJson+id.ToString()+".json"))
            {
                string t = LoadData(mainJson + id.ToString() + ".json");
                //galleryImporter.instance.totalPj = JsonUtility.FromJson<portalsJson>(t);

                mainJson mj = JsonUtility.FromJson<mainJson>(t);


                //if (mj.background.type == "Image")
                //{
                    textureHolder.mainJsons.Add(id, t);
                //}
                //else
                //{
                //    if(PlayerPrefs.GetInt("isFR") == 1)
                //    {
                //        Button temp = errorHandler.showError(Fr.assetProblem, Fr.only360);
                //        if (temp)
                //            temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

                //    }
                //    else
                //    {
                //        Button temp = errorHandler.showError(En.assetProblem, En.only360);
                //        if (temp)
                //            temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

                //    }

                //}
                
            }
            else
            {
                if(PlayerPrefs.GetInt("isFR") == 1)
                {
                    Button temp = errorHandler.showError(Fr.assetProblem, Fr.useOfflineOnce);
                    if (temp)
                        temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

                }
                else
                {
                    Button temp = errorHandler.showError(En.assetProblem, En.useOfflineOnce);
                    if (temp)
                        temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

                }

            }


        }
        else
        {
            //WWWForm form = new WWWForm();
            
            //form.AddField("projectId", id);

            //print(id);

            //if(id >= 30 &&  id != 99)
            //{
            //    getprojectUrl = dynamicGetprojectUrl;
            //}
            //else
            //{
            //    getprojectUrl = "https://mongence.herokuapp.com/api/portals";
            //    //getprojectUrl = "https://gate.kaviar.app/projects/getByIdWithAb.php";
            //}

            print(getprojectUrl);

            using (UnityWebRequest www = UnityWebRequest.Get(getprojectUrl))
            {
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
                {
                    print(www.error);
                    Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
                    if (btnErr)
                        btnErr.onClick.AddListener(delegate { Application.Quit(); });
                }
                else
                {
                    print(www.downloadHandler.text);
                    string t = www.downloadHandler.text;
                    SaveData(mainJson + id.ToString() + ".json", t);
                    //galleryImporter.instance.totalPj = JsonUtility.FromJson<portalsJson>(t);
                    textureHolder.mainJsons.Add(id, t);
                }
            }
        }
    }

    //public static IEnumerator getGameModeJson()
    //{
    //    if (Application.internetReachability == NetworkReachability.NotReachable)
    //    {
    //        if (File.Exists(gameModeJson))
    //        {
    //            string t = LoadData(gameModeJson);
    //            gameMode.instance.gameModeJson = JsonUtility.FromJson<gMJson>(t);
    //        }
    //        else
    //        {

    //            if(PlayerPrefs.GetInt("isFR") == 1)
    //            {
    //                Button temp = errorHandler.showError(Fr.checkInternet, Fr.openOnceOnline);
    //                if (temp)
    //                    temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

    //            }
    //            else
    //            {
    //                Button temp = errorHandler.showError(En.checkInternet, En.openOnceOnline);
    //                if (temp)
    //                    temp.onClick.AddListener(delegate { Destroy(temp.transform.parent.parent.gameObject, 1); });

    //            }

    //        }
    //    }
    //    else
    //    {
    //        using (UnityWebRequest www = UnityWebRequest.Get(gameModeUrl))
    //        {
    //            yield return www.SendWebRequest();

    //            if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
    //            {
    //                print(www.error);
    //                Button btnErr = errorHandler.showError(Lang.choose(En.networkTitle, Fr.networkTitle), Lang.choose(En.networkD, Fr.networkD));
    //                if (btnErr)
    //                    btnErr.onClick.AddListener(delegate { Application.Quit(); });
    //            }
    //            else
    //            {
    //                //print(www.downloadHandler.text);
    //                string t = www.downloadHandler.text;
    //                SaveData( gameModeJson , t);
    //                gameMode.instance.gameModeJson = JsonUtility.FromJson<gMJson>(t);
    //            }
    //        }
    //    }
    //}


    private static void SaveData(string path,string json)
    {
        File.WriteAllText(path, json);
    }

    public static string LoadData(string path)
    {
        return File.ReadAllText(path);
    }

}
