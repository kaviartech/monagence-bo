﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class Fetch
{
    //public static void Text(string url, string json, Action<string> callback)
    //{
    //    instance.StartCoroutine(downloadText(url, json, callback));
    //}


    public static IEnumerator downloadText(string url, string json, Action<string> callback)
    {


        using (UnityWebRequest request = UnityWebRequest.Put(url, json))
        {
            request.method = UnityWebRequest.kHttpVerbPOST;

            request.SetRequestHeader("Content-Type", "Application/json");
            request.SetRequestHeader("Accept", "Application/json");
            yield return request.SendWebRequest();

            if (request.result!= UnityWebRequest.Result.ConnectionError && request.result  != UnityWebRequest.Result.ProtocolError)
            {

                callback(request.downloadHandler.text);

            }
            else
            {
                Debug.Log(request.error);
            }

        }
    }

    public static IEnumerator downloadGetText(string url, Action<string> callback)
    {

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {

            yield return request.SendWebRequest();

            if (request.result!= UnityWebRequest.Result.ConnectionError && request.result  != UnityWebRequest.Result.ProtocolError)
            {

                callback(request.downloadHandler.text);

            }
            else
            {
                Debug.Log(request.error);
            }

        }

    }

    public static IEnumerator downloadImage(string name, string url, Action<Texture2D> callback, Action<string> error = null)
    {
        //Debug.LogError(checkCashe(url));

        using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(checkCashe(url)))
        {

            yield return request.SendWebRequest();

            if (request.result!= UnityWebRequest.Result.ConnectionError && request.result  != UnityWebRequest.Result.ProtocolError)
            {

                Texture2D t2d = DownloadHandlerTexture.GetContent(request);

                saveCashe(t2d,url);

                callback(t2d);

            }
            else
            {
                Debug.LogError(name + ":::" + url + ":::" + request.error);
                if (error != null)
                {
                    error(request.error);
                }
            }

        }
    }

    private static void saveCashe(Texture2D t2d,string url)
    {
        var filename = url.Split('/').Last();

        Debug.Log(Path.Combine("file://", Application.persistentDataPath, filename));

        string destination = Path.Combine("file://" , Application.persistentDataPath , filename);

        if (url.StartsWith("file:/"))
        {
            return;
        }

        byte[] bs = t2d.EncodeToPNG();

        File.WriteAllBytes(destination, bs);
    }

    private static string checkCashe(string url)
    {
        var filename = url.Split('/').Last();

        Debug.Log(Path.Combine("file://", Application.persistentDataPath, filename));

        if(File.Exists(Path.Combine("file://", Application.persistentDataPath, filename)))
        {
            Debug.Log("returning local file found");
            return "file://" + Application.persistentDataPath + "/" + filename;
        }
        else
        {
            Debug.Log("returning web file not found");
            return url;
        }

    }

    [System.Serializable]
    public struct pathResponse
    {
        public bool success;
        public string path;
    }

    

    //public static void Text(string url, string json, Action<string> callback, Action<string> error)
    //{
    //    instance.StartCoroutine(downloadText(url, json, callback, error));
    //}


    public static IEnumerator downloadText( string url, string json, Action<string> callback, Action<string> errorCallback)
    {


        using (UnityWebRequest request = UnityWebRequest.Put(url, json))
        {
            request.method = UnityWebRequest.kHttpVerbPOST;

            request.SetRequestHeader("Content-Type", "Application/json");
            request.SetRequestHeader("Accept", "Application/json");
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.ConnectionError && request.result != UnityWebRequest.Result.ProtocolError)
            {

                callback(request.downloadHandler.text);

            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                errorCallback(request.downloadHandler.text);
            }

        }
    }
}
