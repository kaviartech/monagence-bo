﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class taskbar : MonoBehaviour
{

    public TextMeshProUGUI gallery, share;

    public TextMeshProUGUI moveDevice, tapToPlace;

    public GameObject warningPref;


    public Transform originalParent;

    public GameObject UIHelper;

    public GameObject XImage;

    public Slider sd;

    public TMP_InputField inp;

    [Tooltip("the loading gameobject activated when isWaiting == true")]
    public GameObject loading;

    [Tooltip("this is the custom button that comes with every portal usially reservation")]
    public Button customBtn;
    public Button cta;
    public GameObject removeDoorButton;

    public void search(string input)
    {
        sceneHolder.instance.searchedValue = input;
        galleryImporter.instance.search(input);
    }

    private void Awake()
    {
        if(sceneHolder.instance)
        if (sceneHolder.instance.loadWithId)
        {
            //print("got here");
            originalParent.GetComponentInParent<Animator>().enabled = false;
            originalParent.GetComponentInParent<CanvasGroup>().alpha = 0;
            originalParent.GetComponentInParent<CanvasGroup>().interactable = false;
            originalParent.GetComponentInParent<CanvasGroup>().blocksRaycasts = false;
        }

    }

    private void Start()
    {
        galleryImporter x = galleryImporter.instance;

        x.transform.parent = originalParent;

        originalParent.GetComponent<ScrollRect>().content = x.GetComponent<RectTransform>();

        x.sd = sd;

        x.customBtn = customBtn;

        x.UIHelper = UIHelper;

        x.XImage = XImage;

        x.loading = loading;

        x.tapToPlace = tapToPlace;

        x.cta = cta;

        x.customBtnText = cta.GetComponentInChildren<TextMeshProUGUI>();
        if(sceneHolder.instance.searchedValue != "")
        {
            inp.text = sceneHolder.instance.searchedValue;
        }
        else
        {
            if(x.transform.childCount < x.totalPj.count())
            {
                search("");
            }
        }



        if (sceneHolder.instance.loadWithId)
        {
            sceneHolder.instance.loadWithId = false;
            galleryImporter.instance.deepLinkPortal(sceneHolder.instance.id);
        }



        if (PlayerPrefs.GetInt("isFR") == 1)
        {
            gallery.text = "Gallerie";
            share.text = "Partager";

            moveDevice.text = Fr.moveDevice;
            tapToPlace.text = Fr.tapToPlace;
            warningPref.GetComponentInChildren<TextMeshProUGUI>().text = Fr.reloadingAssets;
        }
        else
        {
            gallery.text = "Gallery";
            share.text = "Share";
            moveDevice.text = En.moveDevice;
            tapToPlace.text = En.tapToPlace;
            warningPref.GetComponentInChildren<TextMeshProUGUI>().text = En.reloadingAssets;
        }
    }
    
    public  void pop()
    {
        if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("disappear"))
        {
            GetComponent<Animator>().Play("appear");
            cta.GetComponent<Animator>().Play("custombutton");

        }
        else { 
            GetComponent<Animator>().Play("disappear");
            cta.GetComponent<Animator>().Play("custombutton 0");
        }
    }

    private void OnApplicationPause(bool pause)
    {
        //print("déplacer doucement l'appareil");
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            Destroy(Instantiate(warningPref),2);
        }
    }

    private void Update()
    {
        
            removeDoorButton.SetActive(portalManager.isInside);
    }

    public void removePortal()
    {

        if (removeDoorButton.transform.GetChild(0).gameObject.activeInHierarchy)
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("portal"))
            {
                go.GetComponent<Renderer>().enabled = false;
                go.TryGetComponent<BoxCollider>(out BoxCollider boxCollider);
                if (boxCollider)
                    boxCollider.enabled = false;
            }
            removeDoorButton.transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("portal"))
            {
                go.GetComponent<Renderer>().enabled = true;
                go.TryGetComponent<BoxCollider>(out BoxCollider boxCollider);
                if (boxCollider)
                    boxCollider.enabled = true;
            }
            removeDoorButton.transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
