using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hotTextRotation : MonoBehaviour
{
    public Transform bannerLookTarget;

    void Start()
    {
        bannerLookTarget = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 lookDir = transform.position - bannerLookTarget.position * 2;
        lookDir.y = 0;
        transform.rotation = Quaternion.LookRotation(lookDir);

    }
}
