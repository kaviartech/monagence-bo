﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using TMPro;
//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.XR.ARFoundation;

//[Serializable]
//public class config
//{
//    public string image1, image2, image3, tapToPlace, moveDevice;
//}

//public class nativeGalleryImporter : Singleton<nativeGalleryImporter>
//{
//    public RawImage r1, r2, r3;

//    //public int index = 0;

//    public static int currentid;

//    public bool isFloorReady = false;

//    public AudioSource ad;

//    public Color green;
//    public Color violet;

//    public Material mat;

//    public GameObject UIHelper;

//    public GameObject currentArSession;

//    public GameObject arSessionPref;

//    public GameObject XImage;

//    public Slider sd;

//    public TextMeshProUGUI tapToPlace;

//    public TextMeshProUGUI tmp;

//    [Tooltip("this is the custom button that comes with every portal usially reservation")]
//    public Button customBtn;


//    public GameObject warningPref;

//    public TextMeshProUGUI moveDevice;

//    #region intent_java
//    public override void Awake()
//    {
//        base.Awake();

//        getIntentData();
//    }

//    private bool getIntentData()
//    {
//#if (!UNITY_EDITOR && UNITY_ANDROid)
//    return CreatePushClass (new AndroidJavaClass ("com.unity3d.player.UnityPlayer"));
//#endif
//        return false;
//    }

//    public bool CreatePushClass(AndroidJavaClass UnityPlayer)
//    {
//#if UNITY_ANDROid
//        AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
//        AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
//        AndroidJavaObject extras = GetExtras(intent);

//        if (extras != null)
//        {
//            string ex = GetProperty(extras, "config");
//            tmp.text = ex;
//            customizeScene(ex);
//            return true;
//        }
//#endif
//        return false;
//    }



//    private AndroidJavaObject GetExtras(AndroidJavaObject intent)
//    {
//        AndroidJavaObject extras = null;

//        try
//        {
//            extras = intent.Call<AndroidJavaObject>("getExtras");
//        }
//        catch (Exception e)
//        {
//            Debug.Log(e.Message);
//        }

//        return extras;
//    }

//    private string GetProperty(AndroidJavaObject extras, string name)
//    {
//        string s = string.Empty;

//        try
//        {
//            s = extras.Call<string>("getString", name);
//        }
//        catch (Exception e)
//        {
//            Debug.Log(e.Message);
//        }

//        return s;
//    }


//    public void pop()
//    {
//        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("disappear"))
//        {
//            GetComponent<Animator>().Play("appear");
//        }
//        else
//        {
//            GetComponent<Animator>().Play("disappear");
//        }
//    }
//    #endregion


//    private void Start()
//    {
//        if (Application.systemLanguage == SystemLanguage.French)
//        {

//            moveDevice.text = "déplacer doucement l'appareil";
//            tapToPlace.text = "appuyez pour positionner votre porte";
//            warningPref.GetComponentInChildren<TextMeshProUGUI>().text = "Chargement de la Magie";
//        }
//        else
//        {

//            moveDevice.text = "slowly move camera to the ground";
//            tapToPlace.text = "press to position your door";
//            warningPref.GetComponentInChildren<TextMeshProUGUI>().text = "Loading the Magic";
//        }

//        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/thumbnail");
//        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/blurredThumbnail");
//        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/images360");
//        Directory.CreateDirectory(Application.persistentDataPath + "/uploads/bundles");

//        getImage(3);
//    }


//    public void customizeScene(string json)
//    {

//        config cfg = JsonUtility.FromJson<config>(json);

//        this.fetchImage(cfg.image1, texture =>
//        {
//            r1.texture = texture;
//        });

//        this.fetchImage(cfg.image2, texture =>
//        {
//            r2.texture = texture;
//        });

//        this.fetchImage(cfg.image3, texture =>
//        {
//            r3.texture = texture;
//        });
//    }

//    public void getImage(int index)
//    {
//        if (index == 0)
//        {

//            PickImage(1024, r1);
//            //index++;
//        }
//        else if (index == 1)
//        {

//            PickImage(1024, r2);
//            //index++;
//        }
//        else if (index == 2)
//        {

//            PickImage(1024, r3);
//            //index++;
//        }
//        else
//        {
//            placeObjs();
//        }
//    }

//    private void placeObjs()
//    {
//        StartCoroutine(setPortal(99));
//    }


//    //void PickImage(int maxSize, RawImage ri)
//    //{
//    //    if (NativeGallery.IsMediaPickerBusy())
//    //        return;

//    //    NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
//    //    {
//    //        Debug.Log("Image path: " + path);
//    //        if (path != null)
//    //        {
//    //            // Create Texture from selected image
//    //            Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
//    //            if (texture == null)
//    //            {
//    //                Debug.Log("Couldn't load texture from " + path);
//    //                return;
//    //            }

//    //            ri.texture = texture;

//    //            //ri.transform.localScale = new Vector3(ri.transform.localScale.x, ri.transform.localScale.y, ri.transform.localScale.z * texture.height / (float)texture.width);

//    //            ri.GetComponent<RectTransform>().sizeDelta = new Vector2(600, 600 * texture.height / (float)texture.width);

//    //            // Assign texture to a temporary quad and destroy it after 5 seconds
//    //            /*GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
//				//quad.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
//				//quad.transform.forward = Camera.main.transform.forward;
//				//quad.transform.localScale = new Vector3(1f, texture.height / (float)texture.width, 1f);*/


//    //            //material.mainTexture = texture;

//    //            //Destroy(quad, 5f);

//    //            // If a procedural texture is not destroyed manually, 
//    //            // it will only be freed after a scene change
//    //            //Destroy(texture, 5f);
//    //        }
//    //    }, "Select any image", "image/png");

//    //    Debug.Log("Permission result: " + permission);
//    //}



//    public IEnumerator setPortal(int id)
//    {

//        isFloorReady = false;

//        sceneHolder.instance.id = id;

//        mat.SetColor("_TexTintColor", violet);

//        currentid = id;
//        XImage.SetActive(true);
//        sd.gameObject.SetActive(true);

//        if (Application.systemLanguage == SystemLanguage.French)
//        {
//            tapToPlace.text = Fr.waitForSound;
//        }
//        else
//        {
//            tapToPlace.text = En.waitForSound;
//        }

//        //print("Veuillez attendre le chargement et le signal sonore.");



//        if (currentArSession)
//            DestroyImmediate(currentArSession);


//        //loading.SetActive(true);

//        mainJson mj = new mainJson();

//        yield return StartCoroutine(cashing.getPortal(id));

//        string pJs;

//        if (textureHolder.mainJsons.TryGetValue(id, out pJs))
//        {
//            mj = JsonUtility.FromJson<mainJson>(pJs);
//        }
//        else
//        {
//            sd.gameObject.SetActive(false);
//            yield break;
//        }

//        //mj = JsonUtility.FromJson<mainJson>(testJson);

//        //customBtn.GetComponentInChildren<TextMeshProUGUI>(true).text = mj.customBtn.name;

//        //customBtn.onClick.RemoveAllListeners();

//        //customBtn.onClick.AddListener(delegate { Application.OpenURL(mj.customBtn.url); });

//        //customBtn.gameObject.SetActive(true);

//        if (r1.transform.parent.parent.parent.GetComponent<Animator>().enabled)
//            r1.transform.parent.parent.parent.GetComponent<Animator>().Play("galleryDisappear");

//        UIHelper.SetActive(true);

//        //reset arSession
//        currentArSession = Instantiate(arSessionPref);


//        //transform.parent = sceneHolder.instance.transform;


//        FindObjectOfType<ARPlaneManager>().enabled = true;

//        PlaceMultipleObjectsOnPlane ppp = FindObjectOfType<PlaceMultipleObjectsOnPlane>();

//        GameObject temp = ppp.placedPrefab;

//        ppp.type = mj.background.type;

//        ppp.ableToPlace = false;


//        sd.maxValue = 2;

//#if UNITY_ANDROid

//        yield return StartCoroutine(cashing.download360(mj.background.url, id, mj.versionNumber, sd));


//        yield return StartCoroutine(cashing.LoadAssetBundle(mj.aB.androidLink, id, mj.versionNumber, sd));

//#elif UNITY_IOS

//            yield return StartCoroutine(cashing.download360(mj.background.url, id, mj.versionNumber, sd));


//            yield return StartCoroutine(cashing.LoadAssetBundle(mj.aB.iosLink, id, mj.versionNumber, sd));

//#endif


//        textureHolder.images360.TryGetValue(id, out Texture2D t);

//        if (!t)
//        {
//            sceneManager.errorReset();
//        }

//        //just a test

//        if (debugger.instance.isDebug)
//        {
//            GameObject obj = Instantiate(textureHolder.AB);
//        }


//        ppp.texture = t;

//        ppp.ableToPlace = true;
//        sd.value = 1;
//        sd.gameObject.SetActive(false);

//        ARPlaneManager ap = FindObjectOfType<ARPlaneManager>();

//        mat.SetColor("_TexTintColor", green);

//        foreach (var plane in ap.trackables)
//            plane.GetComponent<Renderer>().material.SetColor("_TexTintColor", green);


//        while (!isFloorReady)
//        {
//            yield return null;
//        }

//        ad.Play();

//        isFloorReady = false;


//        if (Application.systemLanguage == SystemLanguage.French)
//        {
//            tapToPlace.text = Fr.tapToPlace;
//        }
//        else
//        {
//            tapToPlace.text = En.tapToPlace;
//        }

//        //print("Appuyez pour positionner votre porte");

//        XImage.SetActive(false);

//    }

//    private void OnApplicationFocus(bool focus)
//    {
//        if (focus)
//        {
//            Destroy(Instantiate(warningPref), 2);
//        }
//    }

//}
