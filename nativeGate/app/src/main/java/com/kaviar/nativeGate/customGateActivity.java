package com.kaviar.nativeGate;
import com.kaviar.player.KaviarPlayerActivity;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class CustomGateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_custom_gate);*/

        Intent intent = new Intent(this, KaviarPlayerActivity.class);
        startActivity(intent);

    }
}
