using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using TMPro;

public class ChangePano : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    public  string id;
    

    public GameObject hot;
   
   
    public  void activateChange()
    {
        print(id);
       StartCoroutine(changePanonc(id));
    }

    
    public IEnumerator changePanonc(string ids)
    {
        Slider sd = galleryImporter.instance.sd;
        sd.gameObject.SetActive(true);
        GetComponent<Button>().onClick.RemoveAllListeners();
        PlaceMultipleObjectsOnPlane ppp = FindObjectOfType<PlaceMultipleObjectsOnPlane>();
        var spawnedObjectSetScale = ppp.spawnedObject.transform.localScale;
        ppp.spawnedObject.transform.localScale = new Vector3(1f, 1f, 1f);
        panorama mj = new panorama();
        location loc = new location();

        string pJs;
        //foreach (string s in textureHolder.mainJsons.Keys)
        //    print(s);
        if (textureHolder.mainJsons.TryGetValue(ids, out pJs))
        {
            mj = JsonUtility.FromJson<panorama>(pJs);
        }
        else
        {
            sd.gameObject.SetActive(false);
        }

        print("_____________________________________________");
        GameObject port = new GameObject(ids);
        //ChangePano cp = new ChangePano();
        Debug.Log(port.name);
        Debug.Log(mj.defaultPanorama);
        Debug.Log(sd.name);
        yield  return StartCoroutine(cashing.download360(mj.defaultPanorama, ids, sd));
        textureHolder.images360.TryGetValue(ids, out Texture2D t);

        if (!t)
        {
            sceneManager.errorReset();
        }

        ppp.texture = t;
        if (ppp.spawnedObject.GetComponentInChildren<VideoPlayer>() != null)
            ppp.spawnedObject.GetComponentInChildren<VideoPlayer>().GetComponent<Renderer>().material.mainTexture = t;
        sd.value = 1;
        sd.gameObject.SetActive(false);
        foreach (location l in mj.locations)
        {
            yield return StartCoroutine(cashing.download(textureHolder.panoramas[l.url], l.url));
            hot = Instantiate(Resources.Load<GameObject>("hotSpot"));
            Debug.Log(ppp.spawnedObject.transform.gameObject.name);
            ppp.spawnedObject.transform.GetChild(2).transform.position = Vector3.zero;
            ppp.spawnedObject.transform.GetChild(2).transform.rotation = new Quaternion(180f, 0f, 0f, 0f);
            port.transform.SetParent(ppp.spawnedObject.transform.GetChild(2));
            port.transform.localPosition = Vector3.zero;
            port.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            hot.transform.SetParent(port.transform);
            hot.transform.localPosition = new Vector3(-1 * l.x, l.y,  l.z);
            hot.GetComponent<ChangePano>().id = l.url;
            hot.GetComponent<Button>().onClick.AddListener(delegate { });
            hot.GetComponentInChildren<TextMeshProUGUI>().text = l.name;
            //hot.transform.GetComponentInChildren<Canvas>().enabled = false;
            hot.transform.GetChild(0).LookAt(Camera.main.transform);
            //hot.transform.SetParent(port.transform);
            textureHolder.thumbnailes.TryGetValue(l.url, out Texture2D thb);
            Debug.Log(thb);
            hot.GetComponent<MeshRenderer>().material.mainTexture = thb;
            //hot.GetComponent<MeshRenderer>().enabled = false;

            //hot.GetComponentInChildren<MeshRenderer>().enabled = false;

            print(JsonUtility.ToJson(l));
           

        }
        port.transform.localPosition = Vector3.zero;
        port.transform.localRotation = new Quaternion(180f, 0f, 0f, 0f);
        port.transform.localScale = new Vector3(port.transform.localScale.x, port.transform.localScale.y,-1* port.transform.localScale.z);
        ppp.spawnedObject.transform.localScale = spawnedObjectSetScale;
        debugger.updateSpawned(port.transform.localPosition);
        debugger.updateSpawnedRot(port.transform.localRotation);
        galleryImporter.instance.sd.gameObject.SetActive(false);
        Destroy(transform.parent.gameObject);
        
        //gameObject.SetActive(false);
        }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        //Output to console the clicked GameObject's name and the following message. You can replace this with your own actions for when clicking the GameObject.

        Debug.Log("raycasted");
        Debug.Log(pointerEventData.pointerClick.transform.GetComponent<ChangePano>().id);
        pointerEventData.pointerClick.transform.GetComponent<ChangePano>().activateChange();
        pointerEventData.pointerClick.GetComponent<SphereCollider>().enabled = false;
        Debug.Log(pointerEventData.pointerClick.name + " Game Object Clicked!");

    }

    
}
