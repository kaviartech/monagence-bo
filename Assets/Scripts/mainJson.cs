﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public static class IListExtensions
{
    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}

[System.Serializable]
public class leaderB
{
    public List<leader> leaders = new List<leader>();
}

[System.Serializable]
public class leader
{
    public int id;
    public string name;
    public string score;
}



[System.Serializable]
public class portalsJson
{
    public string  status;
    public List<portal> result = new List<portal>();

    public int count()
    {
        int sum = 0;
        foreach (portal p in result)
            sum++;
        return sum;
    }

    public void append(portalsJson pj)
    {
        foreach(portal p in pj.result)
        {
            portal temp;
            if (p.exists(result) == null)
            {
                result.Add(p);
            }
            else
            {
                temp = p.exists(result);

                foreach (panorama pano in p.panoramas)
                {
                    temp.panoramas.Add(pano);
                }
            }

            
        }
    }

}

[System.Serializable]
public class gMJson
{
    public int versionNumber;

    public List<mainJson> scenes = new List<mainJson>();

    public List<teleportBtn> teleports = new List<teleportBtn>();

}

[System.Serializable]
public class teleportBtn
{
    public int id;
    public int targetId;
    public string url;
}

[System.Serializable]
public class portal : IEnumerator
{
    public string _id;
    public string name;
    public string country;
    public string defaultPanorama;
    public List<panorama> panoramas = new List<panorama>();
    public string cta;
    public string buttonLabel;
    public string origin;

    public portal(string name)
    {
        this.name = name;
        this.origin = AuthManager.origine;
    }

    public object Current => throw new NotImplementedException();

    public portal exists(List<portal> c)
    {
        foreach(portal temp in c)
        {
            if(name == temp.name)
            {
                return temp;
            }
        }
        return null;
    }

    public bool MoveNext()
    {
        throw new NotImplementedException();
    }

    public void Reset()
    {
        throw new NotImplementedException();
    }

    public IEnumerator GetEnumerator()
    {
        yield return this;
    }
}

[System.Serializable]
public class panorama
{
    public string _id;
    //public int versionNumber;
    public string name;
    public string defaultPanorama;
    public string defaultPanoramaThumb;
    public string description;
    public List<location> locations = new List<location>();
    //public List<tag> tags= new List<tag>();

    //public bool tagContains(string v)
    //{
    //    foreach(tag t in tags)
    //    {
    //        if (t.name.ToUpper().Contains(v))
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //}
}

[System.Serializable]
public class location
{
    public string name;
    public float x;
    public float y;
    public float z;
    public string url;
}


[System.Serializable]
public class mainJson
{
    public int _id;
    public string defaultPanorama;
    public string description;
    public string name;
    public List<location> locations = new List<location>();

}
//public class mainJson
//{
//    public int id;
//    public int versionNumber;

//    public List<sceneObject> objects = new List<sceneObject>();

//    public Background background;

//    public assetBundle aB;

//    public CustomBtn customBtn;

//}

[System.Serializable]
public class assetBundle
{
    public int id;
    public string androidLink;
    public string iosLink;
}

[System.Serializable]
public class Background
{
    public int id;
    public string type;
    public string url;
}

[System.Serializable]
public class CustomBtn
{
    public string name;
    public string url;
}

[System.Serializable]
public class sceneObject
{
    public string name;
    public int id;
    public bool isPrefab;
    public v3 pos, scale;
    public v4 rot;
}

[System.Serializable]
public class v3
{
    public float x, y, z;

    public v3(Vector3 p)
    {
        x = p.x;
        y = p.y;
        z = p.z;
    }

    public Vector3 toVector3()
    {
        return new Vector3(x, y, z);
    }
}
[System.Serializable]
public class v4
{
    public float x, y, z, w;
    public v4(Quaternion p)
    {
        x = p.x;
        y = p.y;
        z = p.z;
        w = p.w;
    }

    public Quaternion toQuaternion()
    {
        return new Quaternion(x, y, z, w);
    }
}