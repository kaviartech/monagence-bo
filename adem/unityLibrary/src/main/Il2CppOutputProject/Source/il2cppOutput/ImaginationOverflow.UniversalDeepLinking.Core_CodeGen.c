﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::.ctor(System.Object,System.IntPtr)
extern void LinkActivationHandler__ctor_m52C87AC14F6D513DF44FB0C9510059C9A656ABE8 (void);
// 0x00000002 System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::Invoke(ImaginationOverflow.UniversalDeepLinking.LinkActivation)
extern void LinkActivationHandler_Invoke_mF17709851FDC3C8AF8E16285C4146E9EDE0D4524 (void);
// 0x00000003 System.IAsyncResult ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::BeginInvoke(ImaginationOverflow.UniversalDeepLinking.LinkActivation,System.AsyncCallback,System.Object)
extern void LinkActivationHandler_BeginInvoke_mABAC954E7D69A16088A27590CA32893466C9BA5F (void);
// 0x00000004 System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::EndInvoke(System.IAsyncResult)
extern void LinkActivationHandler_EndInvoke_mD431F1D6DAC455AAF9A5922185EFAEC07AF32199 (void);
// 0x00000005 System.Boolean ImaginationOverflow.UniversalDeepLinking.ILinkProvider::Initialize()
// 0x00000006 System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::add_LinkReceived(System.Action`1<System.String>)
// 0x00000007 System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::remove_LinkReceived(System.Action`1<System.String>)
// 0x00000008 System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::PollInfoAfterPause()
// 0x00000009 System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::get_Uri()
extern void LinkActivation_get_Uri_m755666E7FE053EE9016CF76C0B4A4C24A89A9249 (void);
// 0x0000000A System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_Uri(System.String)
extern void LinkActivation_set_Uri_m4ECB77BEBF218637DC1D536C7BC73536463DADB0 (void);
// 0x0000000B System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_RawQueryString(System.String)
extern void LinkActivation_set_RawQueryString_mC1643F7CF6995AB2F008C7D58002B18107DFAD53 (void);
// 0x0000000C System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.LinkActivation::get_QueryString()
extern void LinkActivation_get_QueryString_mB31DA4CC0E23C71EC6658F0E2C7B385B6FBFFC43 (void);
// 0x0000000D System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_QueryString(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void LinkActivation_set_QueryString_m259E8B6F4E85B9FCB3CD1F6DCD72ECE170482E75 (void);
// 0x0000000E System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::.ctor(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void LinkActivation__ctor_m8C9F2B2C1647E2E38C0B1EA8DC74D3FC09C057F2 (void);
// 0x0000000F System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::ToString()
extern void LinkActivation_ToString_m36BA6DD7740AF1F4474E4E9786532330313C6828 (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	LinkActivationHandler__ctor_m52C87AC14F6D513DF44FB0C9510059C9A656ABE8,
	LinkActivationHandler_Invoke_mF17709851FDC3C8AF8E16285C4146E9EDE0D4524,
	LinkActivationHandler_BeginInvoke_mABAC954E7D69A16088A27590CA32893466C9BA5F,
	LinkActivationHandler_EndInvoke_mD431F1D6DAC455AAF9A5922185EFAEC07AF32199,
	NULL,
	NULL,
	NULL,
	NULL,
	LinkActivation_get_Uri_m755666E7FE053EE9016CF76C0B4A4C24A89A9249,
	LinkActivation_set_Uri_m4ECB77BEBF218637DC1D536C7BC73536463DADB0,
	LinkActivation_set_RawQueryString_mC1643F7CF6995AB2F008C7D58002B18107DFAD53,
	LinkActivation_get_QueryString_mB31DA4CC0E23C71EC6658F0E2C7B385B6FBFFC43,
	LinkActivation_set_QueryString_m259E8B6F4E85B9FCB3CD1F6DCD72ECE170482E75,
	LinkActivation__ctor_m8C9F2B2C1647E2E38C0B1EA8DC74D3FC09C057F2,
	LinkActivation_ToString_m36BA6DD7740AF1F4474E4E9786532330313C6828,
};
static const int32_t s_InvokerIndices[15] = 
{
	1649,
	2797,
	861,
	2797,
	3368,
	2797,
	2797,
	3397,
	3345,
	2797,
	2797,
	3345,
	2797,
	1048,
	3345,
};
extern const CustomAttributesCacheGenerator g_ImaginationOverflow_UniversalDeepLinking_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_ImaginationOverflow_UniversalDeepLinking_Core_CodeGenModule;
const Il2CppCodeGenModule g_ImaginationOverflow_UniversalDeepLinking_Core_CodeGenModule = 
{
	"ImaginationOverflow.UniversalDeepLinking.Core.dll",
	15,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_ImaginationOverflow_UniversalDeepLinking_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
