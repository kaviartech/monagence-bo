﻿

public class Fr
{
    public static string tapToPlace = "touchez un endroit sur les points au sol pour placer votre porte";
    public static string moveDevice = "Diriger le téléphone vers le sol et laisser l’app scanner votre environnement";
    public static string waitForSound = "Veuillez attendre le chargement et le signal sonore.";
    public static string reloadingAssets = "Chargement de la Magie";
    public static string downloadingGameMode = "Télechargement de la Magie : ";

    //info
    public static string help = "aide";
    public static string image360 = "image 360";
    public static string video360 = "video 360";
    public static string assetBundle = "scène 3d";
    public static string image360D = "c'est une image 360 que vous pouvez regarder à l'intérieur";
    public static string video360D = "c'est une vidéo 360 que vous pouvez regarder à l'intérieur pendant qu'elle joue";
    public static string assetBundleD = "c'est une scène 3d que vous pouvez regarder autour et vous déplacer à l'intérieur";

    //gameMode
    public static string gameMode = "comment jouer";
    public static string gameModeD = "Trouvez les intrus dans les décors en un temps record";

    //done
    public static string done = "Bravo !";
    public static string doneD = "Défi terminé : \n";
    public static string textProb = "problème de texte";
    public static string FiveChar = "assurez-vous de saisir plus de 5 caractères";
    public static string EmailNes = "assurez-vous que vous entrez un e-mail correct";

    //errors
    public static string assetProblem = "problème d'asset";
    public static string useOfflineOnce = "assurez-vous d'avoir utilisé cet asset en ligne une fois";
    public static string error = "erreur";
    public static string checkInternet = "vérifier internet";
    public static string openOnceOnline = "assurez-vous d'ouvrir l'application au moins une fois en ligne";
    public static string only360 = "seules les images 360 peuvent être utilisées hors ligne. pour les vidéos dont vous avez besoin d'être en ligne";

    //permission gallery
    public static string galleryPermission = "Nécessaire pour les afficher en Realité Augmentée";

    //placeHolder
    public static string placeHolderDone = "Entrez votre email pour entrer dans le classement";

    //mainMenu
    public static string exploaration = "voyage";
    public static string gamer = "jeu";
    public static string custom = "musée";

    //errors network
    public static string networkTitle = "network error";
    public static string networkD = "the server may be down for maintenance or make sure you are connected to the internet and try again later ...";
}
