using UnityEngine;
using System.Collections;
using UnityEditor;
#if UNITY_EDITOR
[CustomEditor(typeof(ChangePano))]
public class SomeScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ChangePano myScript = (ChangePano)target;
        if (GUILayout.Button("Build Object"))
        {
            myScript.activateChange();
        }
    }
}
#endif