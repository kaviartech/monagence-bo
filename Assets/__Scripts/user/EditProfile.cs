using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class EditProfile : MonoBehaviour
{
    
    public void DeleteAccount()
    {
        Common.alertYN(LanguageManager.isFR ? "Voulez-vous vraiment supprimer votre compte?" : "Are you sure you want to delete your account?", () =>
        {
            StartCoroutine(SendDeleteAccountRequest());
        }, () => { });
    }

    IEnumerator SendDeleteAccountRequest()
    {
        using (UnityWebRequest request = UnityWebRequest.Delete(Common.Baseurl + "api/users/deleteUser/" + AuthManager.res.result._id))
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.ConnectionError && request.result != UnityWebRequest.Result.ProtocolError)
            {
                PlayerPrefs.DeleteAll();
                SceneManager.LoadScene(0);
            }
            else
            {
                Debug.Log(Common.Baseurl + "api/users/deleteUser/" + AuthManager.res.result._id);
                Debug.Log(request.error);
            }
        };
    }
}
