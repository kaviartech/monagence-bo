﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Common : Singleton<Common>
{

    public static string Baseurl = "https://mongence.herokuapp.com/";
    //private static string baseurlHosted = "https://mongence.herokuapp.com/";


    private static bool isFirstTime;

    public static Sprite SpriteFromTexture2D(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }


    public void load(int index)
    {
        StartCoroutine(LoadYourAsyncScene(index));
    }

    public GameObject loading;


    IEnumerator LoadYourAsyncScene(int index)
    {
        loading.SetActive(true);

        yield return new WaitForSeconds(.5f);

        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);

        //Slider sd = loading.GetComponentInChildren<Slider>(true);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            //sd.value = asyncLoad.progress;

            yield return null;
        }
    }

    public static bool IsPointerOverUIObject(Vector2 position)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = position;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        return results.Count > 0;
    }



    [System.Serializable]
    public struct UserInfo
    {
        public string id;
        public int iat, exp;

    }

    public static string getUserInfo()
    {
        string token = PlayerPrefs.GetString("token", "");

        if (!getUserInfo(out string id, out string path))
        {
            instance.load(0);
        }

        return token;


    }

    public static bool getUserInfo(out string id, out string path)
    {
        string token = PlayerPrefs.GetString("token", "");
        id = "";
        path = PlayerPrefs.GetString("path", "");
        if (token == "") return false;

        var parts = token.Split('.');
        if (parts.Length > 2)
        {
            var decode = parts[1];
            var padLength = 4 - decode.Length % 4;
            if (padLength < 4)
            {
                decode += new string('=', padLength);
            }
            var bytes = System.Convert.FromBase64String(decode);
            var userInfo = System.Text.ASCIIEncoding.ASCII.GetString(bytes);

            UserInfo i = JsonUtility.FromJson<UserInfo>(userInfo);

            id = i.id;
            int now = (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
            //print(-now + i.exp);
            return i.exp > now;

        }

        return false;
    }

    public void instanceAlert(string v)
    {
        alert(v);
    }


    public static void alert(string v)
    {
        GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;
        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
    }

    public static GameObject alert(string frensh, string english)
    {
        GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;
        temp.GetComponentInChildren<TextMeshProUGUI>().text = Lang.choose(english, frensh);
        return temp;
    }

    public static GameObject alertAuth(string frensh, string english)
    {
        GameObject temp = Instantiate(Resources.Load("alertAuth", typeof(GameObject))) as GameObject;
        temp.GetComponentInChildren<TextMeshProUGUI>().text = Lang.choose(english, frensh);
        return temp;
    }

    //internal static void alert(string v, Action p)
    //{
    //    GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;

    //    temp.GetComponentInChildren<TextMeshProUGUI>().text = v;

    //    p();
    //}

    internal static void alert(string v, Action p)
    {
        GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;

        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
        Button[] bts = temp.GetComponentsInChildren<Button>();

        //bts[0].onClick.AddListener(delegate { p(); });
        bts[1].onClick.AddListener(delegate { p(); });
    }

    internal static void alertYN(string v, Action yesCallback, Action noCallback)
    {
        GameObject temp = Instantiate(Resources.Load("alert-YN", typeof(GameObject))) as GameObject;

        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
        Button[] bts = temp.GetComponentsInChildren<Button>();

        //bts[0].onClick.AddListener(delegate { p(); });
        bts[1].onClick.AddListener(delegate { yesCallback(); });
        bts[2].onClick.AddListener(delegate { noCallback(); });
        bts[0].onClick.AddListener(delegate { noCallback(); });
    }



}
