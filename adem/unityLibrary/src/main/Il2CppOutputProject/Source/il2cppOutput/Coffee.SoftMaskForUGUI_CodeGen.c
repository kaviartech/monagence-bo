﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 Coffee.UIExtensions.SoftMask/DesamplingRate Coffee.UIExtensions.SoftMask::get_desamplingRate()
extern void SoftMask_get_desamplingRate_m4B57A259903EB7733B3810540C44DC7C9E84375F (void);
// 0x00000002 System.Void Coffee.UIExtensions.SoftMask::set_desamplingRate(Coffee.UIExtensions.SoftMask/DesamplingRate)
extern void SoftMask_set_desamplingRate_m6CB26BBA2CF285381C19FBD96B08160A9A3E46C9 (void);
// 0x00000003 System.Single Coffee.UIExtensions.SoftMask::get_softness()
extern void SoftMask_get_softness_m210D23C6F4AB64B167A4680EBA5CE19849F29ADB (void);
// 0x00000004 System.Void Coffee.UIExtensions.SoftMask::set_softness(System.Single)
extern void SoftMask_set_softness_m9793094421FD86BF17E5E1499B8A7A3B755C646C (void);
// 0x00000005 System.Single Coffee.UIExtensions.SoftMask::get_alpha()
extern void SoftMask_get_alpha_mD28DBCC679B8D1243E0781284634F4B72C631B31 (void);
// 0x00000006 System.Void Coffee.UIExtensions.SoftMask::set_alpha(System.Single)
extern void SoftMask_set_alpha_mAEC5FB0D063EC06A775A1BC47D88FEE1B1930CCF (void);
// 0x00000007 System.Boolean Coffee.UIExtensions.SoftMask::get_ignoreParent()
extern void SoftMask_get_ignoreParent_mAC48EF132DD659F30CE9671C8814A67ADB6415FA (void);
// 0x00000008 System.Void Coffee.UIExtensions.SoftMask::set_ignoreParent(System.Boolean)
extern void SoftMask_set_ignoreParent_m914FE8700055B90B68A490C6DF8D251371C089A5 (void);
// 0x00000009 System.Boolean Coffee.UIExtensions.SoftMask::get_partOfParent()
extern void SoftMask_get_partOfParent_mE7ACEBF409D6FAF5E378E4DD76E5DF3659283868 (void);
// 0x0000000A System.Void Coffee.UIExtensions.SoftMask::set_partOfParent(System.Boolean)
extern void SoftMask_set_partOfParent_m1C1ECB57B9E5D98129CF2F2FB011EE9002976998 (void);
// 0x0000000B UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::get_softMaskBuffer()
extern void SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981 (void);
// 0x0000000C System.Boolean Coffee.UIExtensions.SoftMask::get_hasChanged()
extern void SoftMask_get_hasChanged_m60D0CBDA2A52E323C5FA17433EDAA7A14A86E361 (void);
// 0x0000000D System.Void Coffee.UIExtensions.SoftMask::set_hasChanged(System.Boolean)
extern void SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA (void);
// 0x0000000E Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::get_parent()
extern void SoftMask_get_parent_mB1469E842BC5ADF3FE5DAAFA819D3FA77FCCDBF1 (void);
// 0x0000000F UnityEngine.Material Coffee.UIExtensions.SoftMask::GetModifiedMaterial(UnityEngine.Material)
extern void SoftMask_GetModifiedMaterial_mAFC7B1C862B7DA813DB072B249D830A4F8572738 (void);
// 0x00000010 System.Void Coffee.UIExtensions.SoftMask::UnityEngine.UI.IMeshModifier.ModifyMesh(UnityEngine.Mesh)
extern void SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_mCF2644320EF30231E21BD4D4932EFB20E0DAB553 (void);
// 0x00000011 System.Void Coffee.UIExtensions.SoftMask::UnityEngine.UI.IMeshModifier.ModifyMesh(UnityEngine.UI.VertexHelper)
extern void SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_m379C23D0EE5F6DEE8DD02342443AF0F0F7D21CE6 (void);
// 0x00000012 System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.UI.Graphic,System.Int32[])
extern void SoftMask_IsRaycastLocationValid_m419A604EFD1A8EA76EC040ECBC4337011E84BAC1 (void);
// 0x00000013 System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void SoftMask_IsRaycastLocationValid_m0F669181B54F89BB67875E8A8F7687468E57FC38 (void);
// 0x00000014 System.Void Coffee.UIExtensions.SoftMask::OnEnable()
extern void SoftMask_OnEnable_m853EE9B681CA115D833B9586A4104AE65201CD11 (void);
// 0x00000015 System.Void Coffee.UIExtensions.SoftMask::OnDisable()
extern void SoftMask_OnDisable_mCFEF0F10F085D4722208571EE27F6519FB93AA1A (void);
// 0x00000016 System.Void Coffee.UIExtensions.SoftMask::OnTransformParentChanged()
extern void SoftMask_OnTransformParentChanged_mE96013BC30FD7AC2CCAA07538A3A77E9B7B8200F (void);
// 0x00000017 System.Void Coffee.UIExtensions.SoftMask::OnRectTransformDimensionsChange()
extern void SoftMask_OnRectTransformDimensionsChange_m8CF9BE917A141A9ACD0BCA909AADC5959CCE8CE5 (void);
// 0x00000018 UnityEngine.Material Coffee.UIExtensions.SoftMask::get_material()
extern void SoftMask_get_material_mC2B5EECA152A1D35860B2C07A38A8839E0DFAB16 (void);
// 0x00000019 UnityEngine.Mesh Coffee.UIExtensions.SoftMask::get_mesh()
extern void SoftMask_get_mesh_mD8B874B7C69EA61B11F1B1CC3ED78EA11C5D75A1 (void);
// 0x0000001A System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTextures()
extern void SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF (void);
// 0x0000001B System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTexture()
extern void SoftMask_UpdateMaskTexture_m1A0B74909D06DDF4E98CCDE6645CE7BC7430F2E0 (void);
// 0x0000001C System.Void Coffee.UIExtensions.SoftMask::GetDesamplingSize(Coffee.UIExtensions.SoftMask/DesamplingRate,System.Int32&,System.Int32&)
extern void SoftMask_GetDesamplingSize_mADB1BE4385FF8E47F9E393C1CFB18DDF3A1240CE (void);
// 0x0000001D System.Void Coffee.UIExtensions.SoftMask::ReleaseRT(UnityEngine.RenderTexture&)
extern void SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE (void);
// 0x0000001E System.Void Coffee.UIExtensions.SoftMask::ReleaseObject(UnityEngine.Object)
extern void SoftMask_ReleaseObject_mB296E9939AB74191D556253DACA5E21CA29FD54D (void);
// 0x0000001F System.Void Coffee.UIExtensions.SoftMask::SetParent(Coffee.UIExtensions.SoftMask)
extern void SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F (void);
// 0x00000020 System.Single Coffee.UIExtensions.SoftMask::GetPixelValue(System.Int32,System.Int32,System.Int32[])
extern void SoftMask_GetPixelValue_m27E4D0600AE29C45EA0D02B77BE8BC00AAA3C56D (void);
// 0x00000021 System.Void Coffee.UIExtensions.SoftMask::.ctor()
extern void SoftMask__ctor_mAD022F6F9DBE190453F4D94259EB148FB630095D (void);
// 0x00000022 System.Void Coffee.UIExtensions.SoftMask::.cctor()
extern void SoftMask__cctor_mC69D648F38CCA8A6C097EF5AB9B716959F3F1679 (void);
// 0x00000023 System.Void Coffee.UIExtensions.SoftMask/<>c::.cctor()
extern void U3CU3Ec__cctor_mC715F90BD22A9D06AA40B858B6D8CC288622D3CA (void);
// 0x00000024 System.Void Coffee.UIExtensions.SoftMask/<>c::.ctor()
extern void U3CU3Ec__ctor_m4E1870D305C6424F6F7E64F02312D7DD3F993999 (void);
// 0x00000025 System.Boolean Coffee.UIExtensions.SoftMask/<>c::<SetParent>b__70_0(Coffee.UIExtensions.SoftMask)
extern void U3CU3Ec_U3CSetParentU3Eb__70_0_mC32C9A814621E217595F893608F9952930B5630F (void);
// 0x00000026 UnityEngine.Material Coffee.UIExtensions.SoftMaskable::GetModifiedMaterial(UnityEngine.Material)
extern void SoftMaskable_GetModifiedMaterial_m071CDF00E45C51885DD1FD89A5B69629580F3978 (void);
// 0x00000027 System.Boolean Coffee.UIExtensions.SoftMaskable::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void SoftMaskable_IsRaycastLocationValid_mE605A0336D4EBEFC1BAA512F9CC4E9C394650787 (void);
// 0x00000028 System.Boolean Coffee.UIExtensions.SoftMaskable::get_inverse()
extern void SoftMaskable_get_inverse_m1048E5B624D02DEF9662F2A2F3E8532636C1381F (void);
// 0x00000029 System.Void Coffee.UIExtensions.SoftMaskable::set_inverse(System.Boolean)
extern void SoftMaskable_set_inverse_mEBDF2BFBF3A1FDD72DEED8305DB7D1BE555AB46F (void);
// 0x0000002A UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::get_graphic()
extern void SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9 (void);
// 0x0000002B System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction)
extern void SoftMaskable_SetMaskInteraction_mE902481BF70D77339DF466721D5A3779C06B665E (void);
// 0x0000002C System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction)
extern void SoftMaskable_SetMaskInteraction_m9C75BABE68C3E28C63F5016F9CBF247961642C3C (void);
// 0x0000002D System.Void Coffee.UIExtensions.SoftMaskable::OnEnable()
extern void SoftMaskable_OnEnable_m60B257F638DABEFCD28D014B1BE9AAAA8A048835 (void);
// 0x0000002E System.Void Coffee.UIExtensions.SoftMaskable::OnDisable()
extern void SoftMaskable_OnDisable_m04F3EF3F80AFD448128EC6C4A8222075297FAB1D (void);
// 0x0000002F System.Void Coffee.UIExtensions.SoftMaskable::ReleaseMaterial(UnityEngine.Material&)
extern void SoftMaskable_ReleaseMaterial_mE62C3AF98B25EBD4244B4508975E39D26D8F5238 (void);
// 0x00000030 System.Void Coffee.UIExtensions.SoftMaskable::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3422EE65AF518B8F5BD238702E5A15E96A5CE351 (void);
// 0x00000031 System.Void Coffee.UIExtensions.SoftMaskable::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m0EAA4A639A80D109C4FE9FEEB7410EF681F01C8D (void);
// 0x00000032 System.Void Coffee.UIExtensions.SoftMaskable::.ctor()
extern void SoftMaskable__ctor_m7798272190D02CFAF51DFB6D81E7C4561A9BCA90 (void);
// 0x00000033 System.Void Coffee.UIExtensions.SoftMaskable::.cctor()
extern void SoftMaskable__cctor_mC2CF7B99315A1BB8D643BB96EBE0BA20775256BA (void);
static Il2CppMethodPointer s_methodPointers[51] = 
{
	SoftMask_get_desamplingRate_m4B57A259903EB7733B3810540C44DC7C9E84375F,
	SoftMask_set_desamplingRate_m6CB26BBA2CF285381C19FBD96B08160A9A3E46C9,
	SoftMask_get_softness_m210D23C6F4AB64B167A4680EBA5CE19849F29ADB,
	SoftMask_set_softness_m9793094421FD86BF17E5E1499B8A7A3B755C646C,
	SoftMask_get_alpha_mD28DBCC679B8D1243E0781284634F4B72C631B31,
	SoftMask_set_alpha_mAEC5FB0D063EC06A775A1BC47D88FEE1B1930CCF,
	SoftMask_get_ignoreParent_mAC48EF132DD659F30CE9671C8814A67ADB6415FA,
	SoftMask_set_ignoreParent_m914FE8700055B90B68A490C6DF8D251371C089A5,
	SoftMask_get_partOfParent_mE7ACEBF409D6FAF5E378E4DD76E5DF3659283868,
	SoftMask_set_partOfParent_m1C1ECB57B9E5D98129CF2F2FB011EE9002976998,
	SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981,
	SoftMask_get_hasChanged_m60D0CBDA2A52E323C5FA17433EDAA7A14A86E361,
	SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA,
	SoftMask_get_parent_mB1469E842BC5ADF3FE5DAAFA819D3FA77FCCDBF1,
	SoftMask_GetModifiedMaterial_mAFC7B1C862B7DA813DB072B249D830A4F8572738,
	SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_mCF2644320EF30231E21BD4D4932EFB20E0DAB553,
	SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_m379C23D0EE5F6DEE8DD02342443AF0F0F7D21CE6,
	SoftMask_IsRaycastLocationValid_m419A604EFD1A8EA76EC040ECBC4337011E84BAC1,
	SoftMask_IsRaycastLocationValid_m0F669181B54F89BB67875E8A8F7687468E57FC38,
	SoftMask_OnEnable_m853EE9B681CA115D833B9586A4104AE65201CD11,
	SoftMask_OnDisable_mCFEF0F10F085D4722208571EE27F6519FB93AA1A,
	SoftMask_OnTransformParentChanged_mE96013BC30FD7AC2CCAA07538A3A77E9B7B8200F,
	SoftMask_OnRectTransformDimensionsChange_m8CF9BE917A141A9ACD0BCA909AADC5959CCE8CE5,
	SoftMask_get_material_mC2B5EECA152A1D35860B2C07A38A8839E0DFAB16,
	SoftMask_get_mesh_mD8B874B7C69EA61B11F1B1CC3ED78EA11C5D75A1,
	SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF,
	SoftMask_UpdateMaskTexture_m1A0B74909D06DDF4E98CCDE6645CE7BC7430F2E0,
	SoftMask_GetDesamplingSize_mADB1BE4385FF8E47F9E393C1CFB18DDF3A1240CE,
	SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE,
	SoftMask_ReleaseObject_mB296E9939AB74191D556253DACA5E21CA29FD54D,
	SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F,
	SoftMask_GetPixelValue_m27E4D0600AE29C45EA0D02B77BE8BC00AAA3C56D,
	SoftMask__ctor_mAD022F6F9DBE190453F4D94259EB148FB630095D,
	SoftMask__cctor_mC69D648F38CCA8A6C097EF5AB9B716959F3F1679,
	U3CU3Ec__cctor_mC715F90BD22A9D06AA40B858B6D8CC288622D3CA,
	U3CU3Ec__ctor_m4E1870D305C6424F6F7E64F02312D7DD3F993999,
	U3CU3Ec_U3CSetParentU3Eb__70_0_mC32C9A814621E217595F893608F9952930B5630F,
	SoftMaskable_GetModifiedMaterial_m071CDF00E45C51885DD1FD89A5B69629580F3978,
	SoftMaskable_IsRaycastLocationValid_mE605A0336D4EBEFC1BAA512F9CC4E9C394650787,
	SoftMaskable_get_inverse_m1048E5B624D02DEF9662F2A2F3E8532636C1381F,
	SoftMaskable_set_inverse_mEBDF2BFBF3A1FDD72DEED8305DB7D1BE555AB46F,
	SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9,
	SoftMaskable_SetMaskInteraction_mE902481BF70D77339DF466721D5A3779C06B665E,
	SoftMaskable_SetMaskInteraction_m9C75BABE68C3E28C63F5016F9CBF247961642C3C,
	SoftMaskable_OnEnable_m60B257F638DABEFCD28D014B1BE9AAAA8A048835,
	SoftMaskable_OnDisable_m04F3EF3F80AFD448128EC6C4A8222075297FAB1D,
	SoftMaskable_ReleaseMaterial_mE62C3AF98B25EBD4244B4508975E39D26D8F5238,
	SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3422EE65AF518B8F5BD238702E5A15E96A5CE351,
	SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m0EAA4A639A80D109C4FE9FEEB7410EF681F01C8D,
	SoftMaskable__ctor_m7798272190D02CFAF51DFB6D81E7C4561A9BCA90,
	SoftMaskable__cctor_mC2CF7B99315A1BB8D643BB96EBE0BA20775256BA,
};
static const int32_t s_InvokerIndices[51] = 
{
	3329,
	2781,
	3371,
	2820,
	3371,
	2820,
	3368,
	2817,
	3368,
	2817,
	3345,
	3368,
	2817,
	3345,
	2123,
	2797,
	2797,
	632,
	1337,
	3397,
	3397,
	3397,
	3397,
	3345,
	3345,
	5179,
	3397,
	993,
	2707,
	2797,
	2797,
	964,
	3397,
	5179,
	5179,
	3397,
	2413,
	2123,
	1337,
	3368,
	2817,
	3345,
	2781,
	658,
	3397,
	3397,
	2707,
	3397,
	3397,
	3397,
	5179,
};
extern const CustomAttributesCacheGenerator g_Coffee_SoftMaskForUGUI_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Coffee_SoftMaskForUGUI_CodeGenModule;
const Il2CppCodeGenModule g_Coffee_SoftMaskForUGUI_CodeGenModule = 
{
	"Coffee.SoftMaskForUGUI.dll",
	51,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Coffee_SoftMaskForUGUI_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
