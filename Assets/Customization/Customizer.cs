﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public struct request
{
    public string packageName;

    public request(string packageName)
    {
        this.packageName = packageName;
    }
}

public class Customizer : MonoBehaviour
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public string json;

    public Sprite defaultSprite;

    public CustomizationResponse cr;

    public List<GameObject> shapes;

    public Image bg;

    public Animator GlobalLoader;

    public int imageCount;

    public int ImageCount
    {
        get => imageCount; set
        {
            imageCount = value;
            Debug.LogWarning(value);
            if (imageCount == 0)
            {
                //Invoke("showContent", 1.5f);

                GlobalLoader.Play("hide");
            }
        }
    }

    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/



    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        request req = new request(Application.identifier);//"com.kaviar.app.nader.x8iPVPSh8");

        StartCoroutine(Fetch.downloadText("https://dev-appmaker.herokuapp.com/api/project/read/by/package/name", JsonUtility.ToJson(req), response =>
        {
            json = response;
            treat();
        }));

        imageCount--;

    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/

    void Update()
    {

    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void treat()
    {
        try
        {
            cr = JsonUtility.FromJson<CustomizationResponse>(json);

            int sceneIndex = SceneManager.GetActiveScene().buildIndex;

            Page currentPage = cr.project.pages[sceneIndex];

            ImageCount++;

            StartCoroutine(Fetch.downloadImage("image src", currentPage.background.url, texture =>
            {

                var sprite = SpriteFromTexture2D(texture);
                bg.GetComponent<Image>().sprite = sprite;
                bg.GetComponent<Image>().preserveAspect = false;
                bg.GetComponent<Image>().type = Image.Type.Simple;
                bg.GetComponent<Image>().pixelsPerUnitMultiplier = 1f;

                imageCount--;

            }));



            for (int i = 0; i < shapes.Count; i++)
            {
                GameObject g = shapes[i];
                if (!g)
                {
                    continue;
                }
                Shape s = currentPage.shapes[i];

                Vector2 referenceSize = FindObjectOfType<Canvas>().GetComponent<RectTransform>().sizeDelta;

                if (s.type == "image")
                {

                    ImageCount++;
                    //print("_______________________________________ downloading img");
                    StartCoroutine(Fetch.downloadImage("image src", s.src.url, texture =>
                    {

                        var sprite = SpriteFromTexture2D(texture);
                        g.GetComponent<Image>().sprite = sprite;
                        g.GetComponent<Image>().preserveAspect = true;
                        g.GetComponent<Image>().type = Image.Type.Simple;
                        g.GetComponent<Image>().pixelsPerUnitMultiplier = 1f;

                        ImageCount--;

                    }));
                }
                else
                {
                    g.GetComponent<Image>().sprite = defaultSprite;
                    g.GetComponent<Image>().preserveAspect = false;
                    g.GetComponent<Image>().type = Image.Type.Tiled;
                    g.GetComponent<Image>().pixelsPerUnitMultiplier = 18.1f - s.cornerRadius;
                }

                if (s.fill != "")
                {
                    g.GetComponent<Image>().color = toColor(s.fill, s.opacity);
                }

                print("_____________________________________________________"+s.content);

                if (s.content != "")
                {
                    g.GetComponentInChildren<TextMeshProUGUI>().text = s.content;
                    g.GetComponentInChildren<TextMeshProUGUI>().fontSize = s.fontSize * referenceSize.x / 234;
                    g.GetComponentInChildren<TextMeshProUGUI>().color = toColor(s.color, s.opacity);
                }



                float width = s.width * referenceSize.x / 234;
                float height = s.height * referenceSize.y / 488;

                g.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);

                g.GetComponent<RectTransform>().anchoredPosition = convertPosition(s.x, s.y, width, height, referenceSize);

            }
        }
        catch (System.Exception)
        {
            GlobalLoader.Play("hide");
        }
    }


    public Vector2 convertPosition(float x, float y, float width, float height, Vector2 referenceSize)
    {

        var tempX = referenceSize.x * (x / 234 - .5f) + width / 2;
        var tempY = referenceSize.y * (-y / 488 + .5f) - height / 2;

        return new Vector2(tempX, tempY);
    }

    public Color toColor(string hex, float opacity = 1)
    {
        Color color;



        if (ColorUtility.TryParseHtmlString(hex, out color))
        {

            color.a = opacity;

            return color;
        }
        else
        {
            return new Color(1, 1, 1, 0);
        }
    }

    public Sprite SpriteFromTexture2D(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    public void showContent()
    {
        GlobalLoader.Play("hide");
    }

}
