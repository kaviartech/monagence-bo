﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class placeholderText : MonoBehaviour
{
    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = Lang.choose(En.placeHolderDone,Fr.placeHolderDone);
    }

}
