﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class errorHandler : Singleton<errorHandler>
{

    public GameObject error;
    public GameObject done;
    public GameObject info;
    public GameObject help;

    public TextMeshProUGUI text,textD,textI;

    public TextMeshProUGUI titleText,titleTextD,titleTextI,titleTextHelp;


    private void Start()
    {
        text = error.GetComponentInChildren<TextMeshProUGUI>();

        if (done)
            textD = done.GetComponentInChildren<TextMeshProUGUI>();

        if (info)
            textI = info.GetComponentInChildren<TextMeshProUGUI>();


        titleText = error.GetComponentsInChildren<TextMeshProUGUI>()[1];

        if (done)
            titleTextD = done.GetComponentsInChildren<TextMeshProUGUI>()[1];

        if (info)
            titleTextI = info.GetComponentsInChildren<TextMeshProUGUI>()[1];
    }


    public static Button showError(string title, string content)
    {
        GameObject error = GameObject.Find(title);

        if (!error)
        {
            error = new GameObject(title);
        }
         
        instance.text.text = content;

        instance.titleText.text = title;

        /*foreach(Transform child in error.transform)
        {
            DestroyImmediate(child.gameObject);
        }*/

        if(error.transform.childCount >= 1)
        {
            return null;
        }
        else
        {
            GameObject temp = Instantiate(instance.error, error.transform);
            return temp.GetComponentInChildren<Button>();
        }
        
    }

    public static Button showHelp()
    {
        string title = Lang.choose(En.help, Fr.help);
        GameObject help = GameObject.Find(title);

        if (!help)
        {
            help = new GameObject(title);
        }

        instance.titleText.text = title;

        /*foreach(Transform child in error.transform)
        {
            DestroyImmediate(child.gameObject);
        }*/

        if (help.transform.childCount >= 1)
        {
            return null;
        }
        else
        {
            GameObject temp = Instantiate(instance.help, help.transform);
            return temp.GetComponentInChildren<Button>();
        }

    }

    public static Button showInfo(string title, string content)
    {
        GameObject info = GameObject.Find(title);

        if (!info)
        {
            info = new GameObject(title);
        }

        instance.textI.text = content;

        instance.titleTextI.text = title;

        /*foreach(Transform child in error.transform)
        {
            DestroyImmediate(child.gameObject);
        }*/

        if (info.transform.childCount >= 1)
        {
            return null;
        }
        else
        {
            GameObject temp = Instantiate(instance.info, info.transform);
            return temp.GetComponentInChildren<Button>();
        }

    }


    public static Button showDone(string title, string content)
    {
        GameObject done = GameObject.Find(title);

        if (!done)
        {
            done = new GameObject(title);
        }

        instance.textD.text = content;

        instance.titleTextD.text = title;

        /*foreach(Transform child in error.transform)
        {
            DestroyImmediate(child.gameObject);
        }*/

        if (done.transform.childCount >= 1)
        {
            return null;
        }
        else
        {
            GameObject temp = Instantiate(instance.done, done.transform);
            return temp.GetComponentInChildren<Button>(true);
        }

    }

}
