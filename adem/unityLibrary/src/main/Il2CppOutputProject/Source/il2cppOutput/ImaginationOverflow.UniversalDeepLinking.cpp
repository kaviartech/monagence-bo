﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_tCA4820F8266AF4059CC5A14888D8195F0D797499;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389;
// System.Exception
struct Exception_t;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// ImaginationOverflow.UniversalDeepLinking.ILinkProvider
struct ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0;
// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A;
// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6;
// ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory
struct LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// System.Type
struct Type_t;
// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
struct UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser
struct UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86;
// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4
struct U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1802072B6C85CB9E034162DE2A5ECF0CFC99AB85;
IL2CPP_EXTERN_C String_t* _stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E;
IL2CPP_EXTERN_C String_t* _stringLiteral4697D4C3588C40B4B90B49284BEEC64F643F83E0;
IL2CPP_EXTERN_C String_t* _stringLiteral51D9E2BE3865F31E8579BB2E215D2CBA557E7777;
IL2CPP_EXTERN_C String_t* _stringLiteral5A1FA7C21B5462E60F25F1F1448FB5B3CB05FCFD;
IL2CPP_EXTERN_C String_t* _stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900;
IL2CPP_EXTERN_C String_t* _stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E;
IL2CPP_EXTERN_C String_t* _stringLiteralED267C8DCB7043D2BDE06822955C482B2637216C;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeepLinkManager_OnActivated_m2E2B64FA03163C54A9E9A352B223533428C48D6C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeepLinkManager_RegisterIfNecessary_m2E20577596BF4EFAC4AC9E0E6C3C85002945BD1C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeepLinkManager__currProvider_LinkReceived_m356EB5F16FD7891D0627620AD7A29EA2AC22C08E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Clear_mB853C65F23D3966B3D3026F3541601042CF7DF8A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m5BB06692D9A48A3FEEB102881A86417DE6DA5027_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Keys_m69FA0927978855661A3EA0DC3B9AE1D00FCE4825_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mFB991F7C2A32C8DA7C4D2F522B9C69C36F7DF904_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m5357493D8DA32113DC35EE95205D4E3D607C9F33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m7FA8CBC3A46C7349CA380C3BA7B3294C6C0FC9D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_mB8E76B61549B2812C7D41E09B82D023542710F35_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_m4512CC598556AD77369212209F79C8F290C43F80_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyCollection_GetEnumerator_mBEA854721D5E7717F856476B1E4563A59661CBF7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_mA68C06E0EDF2690E5364A0EA10E47E69E6B57378_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t7E0CA9272B9C3020D673BA8160B4EC99D397F974 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___entries_1)); }
	inline EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t52A654EA9927D1B5F56CA05CF209F2E4393C4510* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___keys_7)); }
	inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ___values_8)); }
	inline ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t9161A5C97376D261665798FA27DAFD5177305C81 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::dictionary
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D, ___dictionary_0)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF  : public RuntimeObject
{
public:
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::<IsSteamBuild>k__BackingField
	bool ___U3CIsSteamBuildU3Ek__BackingField_1;
	// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_activated
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ____activated_2;
	// ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider
	RuntimeObject* ____currProvider_3;
	// UnityEngine.GameObject ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_go
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____go_4;
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_storedActivation
	String_t* ____storedActivation_5;

public:
	inline static int32_t get_offset_of_U3CIsSteamBuildU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF, ___U3CIsSteamBuildU3Ek__BackingField_1)); }
	inline bool get_U3CIsSteamBuildU3Ek__BackingField_1() const { return ___U3CIsSteamBuildU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsSteamBuildU3Ek__BackingField_1() { return &___U3CIsSteamBuildU3Ek__BackingField_1; }
	inline void set_U3CIsSteamBuildU3Ek__BackingField_1(bool value)
	{
		___U3CIsSteamBuildU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__activated_2() { return static_cast<int32_t>(offsetof(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF, ____activated_2)); }
	inline LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * get__activated_2() const { return ____activated_2; }
	inline LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 ** get_address_of__activated_2() { return &____activated_2; }
	inline void set__activated_2(LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * value)
	{
		____activated_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activated_2), (void*)value);
	}

	inline static int32_t get_offset_of__currProvider_3() { return static_cast<int32_t>(offsetof(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF, ____currProvider_3)); }
	inline RuntimeObject* get__currProvider_3() const { return ____currProvider_3; }
	inline RuntimeObject** get_address_of__currProvider_3() { return &____currProvider_3; }
	inline void set__currProvider_3(RuntimeObject* value)
	{
		____currProvider_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currProvider_3), (void*)value);
	}

	inline static int32_t get_offset_of__go_4() { return static_cast<int32_t>(offsetof(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF, ____go_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__go_4() const { return ____go_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__go_4() { return &____go_4; }
	inline void set__go_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____go_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____go_4), (void*)value);
	}

	inline static int32_t get_offset_of__storedActivation_5() { return static_cast<int32_t>(offsetof(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF, ____storedActivation_5)); }
	inline String_t* get__storedActivation_5() const { return ____storedActivation_5; }
	inline String_t** get_address_of__storedActivation_5() { return &____storedActivation_5; }
	inline void set__storedActivation_5(String_t* value)
	{
		____storedActivation_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____storedActivation_5), (void*)value);
	}
};

struct DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_StaticFields
{
public:
	// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::<Instance>k__BackingField
	DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_StaticFields, ___U3CInstanceU3Ek__BackingField_0)); }
	inline DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF ** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_0), (void*)value);
	}
};


// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389  : public RuntimeObject
{
public:
	// System.Action`1<System.String> ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::LinkReceived
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___LinkReceived_1;

public:
	inline static int32_t get_offset_of_LinkReceived_1() { return static_cast<int32_t>(offsetof(EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389, ___LinkReceived_1)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_LinkReceived_1() const { return ___LinkReceived_1; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_LinkReceived_1() { return &___LinkReceived_1; }
	inline void set_LinkReceived_1(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___LinkReceived_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LinkReceived_1), (void*)value);
	}
};

struct EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_StaticFields
{
public:
	// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::_instance
	EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_StaticFields, ____instance_0)); }
	inline EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * get__instance_0() const { return ____instance_0; }
	inline EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_0), (void*)value);
	}
};


// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A  : public RuntimeObject
{
public:
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<Uri>k__BackingField
	String_t* ___U3CUriU3Ek__BackingField_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<RawQueryString>k__BackingField
	String_t* ___U3CRawQueryStringU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.LinkActivation::<QueryString>k__BackingField
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___U3CQueryStringU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A, ___U3CUriU3Ek__BackingField_0)); }
	inline String_t* get_U3CUriU3Ek__BackingField_0() const { return ___U3CUriU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUriU3Ek__BackingField_0() { return &___U3CUriU3Ek__BackingField_0; }
	inline void set_U3CUriU3Ek__BackingField_0(String_t* value)
	{
		___U3CUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUriU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRawQueryStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A, ___U3CRawQueryStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CRawQueryStringU3Ek__BackingField_1() const { return ___U3CRawQueryStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRawQueryStringU3Ek__BackingField_1() { return &___U3CRawQueryStringU3Ek__BackingField_1; }
	inline void set_U3CRawQueryStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CRawQueryStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRawQueryStringU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CQueryStringU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A, ___U3CQueryStringU3Ek__BackingField_2)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_U3CQueryStringU3Ek__BackingField_2() const { return ___U3CQueryStringU3Ek__BackingField_2; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_U3CQueryStringU3Ek__BackingField_2() { return &___U3CQueryStringU3Ek__BackingField_2; }
	inline void set_U3CQueryStringU3Ek__BackingField_2(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___U3CQueryStringU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CQueryStringU3Ek__BackingField_2), (void*)value);
	}
};


// ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory
struct LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4
struct U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5  : public RuntimeObject
{
public:
	// System.Int32 ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>4__this
	UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5, ___U3CU3E4__this_2)); }
	inline UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::dictionary
	Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::version
	int32_t ___version_2;
	// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::currentKey
	RuntimeObject * ___currentKey_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031, ___dictionary_0)); }
	inline Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentKey_3() { return static_cast<int32_t>(offsetof(Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031, ___currentKey_3)); }
	inline RuntimeObject * get_currentKey_3() const { return ___currentKey_3; }
	inline RuntimeObject ** get_address_of_currentKey_3() { return &___currentKey_3; }
	inline void set_currentKey_3(RuntimeObject * value)
	{
		___currentKey_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentKey_3), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>
struct Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::dictionary
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::version
	int32_t ___version_2;
	// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::currentKey
	String_t* ___currentKey_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547, ___dictionary_0)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentKey_3() { return static_cast<int32_t>(offsetof(Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547, ___currentKey_3)); }
	inline String_t* get_currentKey_3() const { return ___currentKey_3; }
	inline String_t** get_address_of_currentKey_3() { return &___currentKey_3; }
	inline void set_currentKey_3(String_t* value)
	{
		___currentKey_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentKey_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser
struct UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86  : public Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5
{
public:
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_14;
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86, ___U3CUrlU3Ek__BackingField_14)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_14() const { return ___U3CUrlU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_14() { return &___U3CUrlU3Ek__BackingField_14; }
	inline void set_U3CUrlU3Ek__BackingField_14(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUrlU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86, ___U3CQueryU3Ek__BackingField_15)); }
	inline String_t* get_U3CQueryU3Ek__BackingField_15() const { return ___U3CQueryU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CQueryU3Ek__BackingField_15() { return &___U3CQueryU3Ek__BackingField_15; }
	inline void set_U3CQueryU3Ek__BackingField_15(String_t* value)
	{
		___U3CQueryU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CQueryU3Ek__BackingField_15), (void*)value);
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RuntimePlatform
struct RuntimePlatform_tB8798C800FD9810C0FE2B7D2F2A0A3979D239065 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tB8798C800FD9810C0FE2B7D2F2A0A3979D239065, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.UriKind
struct UriKind_tFC16ACC1842283AAE2C7F50C9C70EFBF6550B3FC 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_tFC16ACC1842283AAE2C7F50C9C70EFBF6550B3FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6  : public MulticastDelegate_t
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
struct UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::_onJob
	bool ____onJob_4;

public:
	inline static int32_t get_offset_of__onJob_4() { return static_cast<int32_t>(offsetof(UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF, ____onJob_4)); }
	inline bool get__onJob_4() const { return ____onJob_4; }
	inline bool* get_address_of__onJob_4() { return &____onJob_4; }
	inline void set__onJob_4(bool value)
	{
		____onJob_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m883E91BB19072DD91E8FA3BEDA31D0FA095667EA_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyCollection_tCA4820F8266AF4059CC5A14888D8195F0D797499 * Dictionary_2_get_Keys_m771BB389A4CB93B7EDBDC71F503B50049B8999C7_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031  KeyCollection_GetEnumerator_m6B4DC134BB6777F8C99D26315F359E069004CF13_gshared (KeyCollection_tCA4820F8266AF4059CC5A14888D8195F0D797499 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mF254C3BDEA9F0829958522BF88C75DE8BEC4961F_gshared_inline (Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m3714ECE30727E77F475635710D707743B6D930E6_gshared (Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m5440E65428351CA6F19B1804D172DCB5E9C59C98_gshared (Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031 * __this, const RuntimeMethod* method);

// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__ctor_mDEBA3F5D307C2A41E768C0C0BB5D31249EA58EF9 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::set_Instance(ImaginationOverflow.UniversalDeepLinking.DeepLinkManager)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_set_Instance_m6FDEFEDD41CF5A8A1DEF1343A288ED70B87075C8_inline (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::add__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_add__activated_mF9C8FA360175BC62D71F7CBE61138203D96D4057 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ___value0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::RegisterIfNecessary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_RegisterIfNecessary_m2E20577596BF4EFAC4AC9E0E6C3C85002945BD1C (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::remove__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_remove__activated_m224808B5FFD30330813D11E90390F40DDDB35DB2 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ___value0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4 (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider_LinkReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__currProvider_LinkReceived_m356EB5F16FD7891D0627620AD7A29EA2AC22C08E (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_mB22F7F39CDD46667C3EF64507E55BB7DA18F66C4 (const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider__ctor_mA4413A126A46DB73C74152831ABF4DF1C6596491 (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * __this, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkProviderFactory__ctor_mA25C2DA9A5547F28574D29E54E49BACD334CE444 (LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB * __this, const RuntimeMethod* method);
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_IsSteamBuild()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DeepLinkManager_get_IsSteamBuild_m5403EA906417467D1076C11010A7B94696128AE3_inline (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method);
// ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::GetProvider(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LinkProviderFactory_GetProvider_mB4992EC88B46E9E394637FDD8043451C0F046D80 (LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB * __this, bool ___isSteamBuild0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreatePauseGameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_CreatePauseGameObject_m59B5EC71D3CF69745AECD27D29102E2A498BB531 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161 (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared)(__this, ___object0, ___method1, method);
}
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript>()
inline UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_m4512CC598556AD77369212209F79C8F290C43F80 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mACDBD7A1F25B33D006A60F67EF901B33DD3D52E9 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript>()
inline UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_mB8E76B61549B2812C7D41E09B82D023542710F35 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::OnActivated(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeepLinkManager_OnActivated_m2E2B64FA03163C54A9E9A352B223533428C48D6C (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::StoreActivation(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_StoreActivation_m9479B517341C6A2949972FCC975D1E2FDFE974DE_inline (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method);
// ImaginationOverflow.UniversalDeepLinking.LinkActivation ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreateLinkActivation(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * DeepLinkManager_CreateLinkActivation_mEA21A19C3C5DEFA54134ED845AA198C9362E799D (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::Invoke(ImaginationOverflow.UniversalDeepLinking.LinkActivation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivationHandler_Invoke_mF17709851FDC3C8AF8E16285C4146E9EDE0D4524 (LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * __this, LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * ___s0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA (Exception_t * ___exception0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Debug_get_isDebugBuild_mD0384FE4EA71AA14019FF171ADCE2F637C6E541C (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
inline void Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser__ctor_mF7F36EF49BC323178086264EEBFB0CC12E49611E (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___queryStringOrUrl0, const RuntimeMethod* method);
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Query()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Query_m3AD6CC626663C50666AB304238BE344126108646_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::.ctor(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivation__ctor_m8C9F2B2C1647E2E38C0B1EA8DC74D3FC09C057F2 (LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * __this, String_t* ___uri0, String_t* ___rawQueryString1, Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___queryStringParams2, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___target0, const RuntimeMethod* method);
// System.Collections.IEnumerator ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::CallDeepLinkManagerAfterDelay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_mFF1AD6F07D1CCE14F95C375F0646FD998BED332F (UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4__ctor_m3A0D7189FD384A2291756D99C8E2441152D24B4D (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Url(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Url_m1A693E00E113D3451ED4EE021A63B82E025A464B_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * UrlEncodingParser_Parse_mC585A371964ED1CBF30216D7109D71A4D73E8DD5 (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___query0, const RuntimeMethod* method);
// System.Boolean System.Uri::IsWellFormedUriString(System.String,System.UriKind)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Uri_IsWellFormedUriString_mF90D5434E6C63D063E21C97064A098F6DB3C5CBA (String_t* ___uriString0, int32_t ___uriKind1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear()
inline void Dictionary_2_Clear_mB853C65F23D3966B3D3026F3541601042CF7DF8A (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, const RuntimeMethod*))Dictionary_2_Clear_m883E91BB19072DD91E8FA3BEDA31D0FA095667EA_gshared)(__this, method);
}
// System.Int32 System.String::IndexOf(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_IndexOf_mEE2D2F738175E3FF05580366D6226DBD673CA2BE (String_t* __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190 (String_t* __this, int32_t ___startIndex0, const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Query(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Query_m308D23C620696589665DA5BDE202F464E32C8312_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___separator0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4D0DDA7FEDB75304E5FDAF8489A0478EE58A45F2 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m5BB06692D9A48A3FEEB102881A86417DE6DA5027 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared)(__this, ___key0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, String_t* ___key0, String_t* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared)(__this, ___key0, ___value1, method);
}
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Keys()
inline KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * Dictionary_2_get_Keys_m69FA0927978855661A3EA0DC3B9AE1D00FCE4825 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, const RuntimeMethod* method)
{
	return ((  KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, const RuntimeMethod*))Dictionary_2_get_Keys_m771BB389A4CB93B7EDBDC71F503B50049B8999C7_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>::GetEnumerator()
inline Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547  KeyCollection_GetEnumerator_mBEA854721D5E7717F856476B1E4563A59661CBF7 (KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547  (*) (KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D *, const RuntimeMethod*))KeyCollection_GetEnumerator_m6B4DC134BB6777F8C99D26315F359E069004CF13_gshared)(__this, method);
}
// !0 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>::get_Current()
inline String_t* Enumerator_get_Current_m7FA8CBC3A46C7349CA380C3BA7B3294C6C0FC9D8_inline (Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 *, const RuntimeMethod*))Enumerator_get_Current_mF254C3BDEA9F0829958522BF88C75DE8BEC4961F_gshared_inline)(__this, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0)
inline String_t* Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141 (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared)(__this, ___key0, method);
}
// System.String System.Uri::EscapeUriString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Uri_EscapeUriString_m381555089C7EE743700F115FE56F52D5757F0028 (String_t* ___stringToEscape0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___values0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>::MoveNext()
inline bool Enumerator_MoveNext_m5357493D8DA32113DC35EE95205D4E3D607C9F33 (Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 *, const RuntimeMethod*))Enumerator_MoveNext_m3714ECE30727E77F475635710D707743B6D930E6_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>::Dispose()
inline void Enumerator_Dispose_mFB991F7C2A32C8DA7C4D2F522B9C69C36F7DF904 (Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 *, const RuntimeMethod*))Enumerator_Dispose_m5440E65428351CA6F19B1804D172DCB5E9C59C98_gshared)(__this, method);
}
// System.String System.String::Trim(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_m10D967E03EDCB170227406426558B7FEA27CD6CC (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___trimChars0, const RuntimeMethod* method);
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Url()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * DeepLinkManager_get_Instance_mDEF4E7332FABA5E12FC7838E80D8DB8DA34BABC2_inline (const RuntimeMethod* method);
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::GameCameFromPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_GameCameFromPause_mD73F9C7B9381B98C97E1789F86D7B093BF1AC608 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199 (RuntimeObject * ___message0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___context1, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * DeepLinkManager_get_Instance_mDEF4E7332FABA5E12FC7838E80D8DB8DA34BABC2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * L_0 = ((DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::set_Instance(ImaginationOverflow.UniversalDeepLinking.DeepLinkManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_set_Instance_m6FDEFEDD41CF5A8A1DEF1343A288ED70B87075C8 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		((DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_IsSteamBuild()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeepLinkManager_get_IsSteamBuild_m5403EA906417467D1076C11010A7B94696128AE3 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CIsSteamBuildU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__cctor_m1CCF9A25424F835DC63C1999A10F627F76396402 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * L_0 = (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF *)il2cpp_codegen_object_new(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		DeepLinkManager__ctor_mDEBA3F5D307C2A41E768C0C0BB5D31249EA58EF9(L_0, /*hidden argument*/NULL);
		DeepLinkManager_set_Instance_m6FDEFEDD41CF5A8A1DEF1343A288ED70B87075C8_inline(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__ctor_mDEBA3F5D307C2A41E768C0C0BB5D31249EA58EF9 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::add_LinkActivated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_add_LinkActivated_m539EEACBB674F5872776A795B178F8BB9BD6A8EC (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ___value0, const RuntimeMethod* method)
{
	{
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_0 = ___value0;
		DeepLinkManager_add__activated_mF9C8FA360175BC62D71F7CBE61138203D96D4057(__this, L_0, /*hidden argument*/NULL);
		DeepLinkManager_RegisterIfNecessary_m2E20577596BF4EFAC4AC9E0E6C3C85002945BD1C(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::remove_LinkActivated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_remove_LinkActivated_mC4900D6ADEE048398DA74554D261A74541EB541D (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ___value0, const RuntimeMethod* method)
{
	{
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_0 = ___value0;
		DeepLinkManager_remove__activated_m224808B5FFD30330813D11E90390F40DDDB35DB2(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::add__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_add__activated_mF9C8FA360175BC62D71F7CBE61138203D96D4057 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_0 = NULL;
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_1 = NULL;
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_2 = NULL;
	{
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_0 = __this->get__activated_2();
		V_0 = L_0;
	}

IL_0007:
	{
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_1 = V_0;
		V_1 = L_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_2 = V_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *)CastclassSealed((RuntimeObject*)L_4, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6_il2cpp_TypeInfo_var));
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 ** L_5 = __this->get_address_of__activated_2();
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_6 = V_2;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_7 = V_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_8;
		L_8 = InterlockedCompareExchangeImpl<LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *>((LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 **)L_5, L_6, L_7);
		V_0 = L_8;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_9 = V_0;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *)L_9) == ((RuntimeObject*)(LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::remove__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_remove__activated_m224808B5FFD30330813D11E90390F40DDDB35DB2 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_0 = NULL;
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_1 = NULL;
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_2 = NULL;
	{
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_0 = __this->get__activated_2();
		V_0 = L_0;
	}

IL_0007:
	{
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_1 = V_0;
		V_1 = L_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_2 = V_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *)CastclassSealed((RuntimeObject*)L_4, LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6_il2cpp_TypeInfo_var));
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 ** L_5 = __this->get_address_of__activated_2();
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_6 = V_2;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_7 = V_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_8;
		L_8 = InterlockedCompareExchangeImpl<LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *>((LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 **)L_5, L_6, L_7);
		V_0 = L_8;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_9 = V_0;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *)L_9) == ((RuntimeObject*)(LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::RegisterIfNecessary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_RegisterIfNecessary_m2E20577596BF4EFAC4AC9E0E6C3C85002945BD1C (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager__currProvider_LinkReceived_m356EB5F16FD7891D0627620AD7A29EA2AC22C08E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		RuntimeObject* L_0 = __this->get__currProvider_3();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = __this->get__storedActivation_5();
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_3 = __this->get__storedActivation_5();
		V_0 = L_3;
		__this->set__storedActivation_5((String_t*)NULL);
		String_t* L_4 = V_0;
		DeepLinkManager__currProvider_LinkReceived_m356EB5F16FD7891D0627620AD7A29EA2AC22C08E(__this, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}

IL_002b:
	{
		int32_t L_5;
		L_5 = Application_get_platform_mB22F7F39CDD46667C3EF64507E55BB7DA18F66C4(/*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)7)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_004a;
		}
	}

IL_003d:
	{
		EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * L_9 = (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 *)il2cpp_codegen_object_new(EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_il2cpp_TypeInfo_var);
		EditorLinkProvider__ctor_mA4413A126A46DB73C74152831ABF4DF1C6596491(L_9, /*hidden argument*/NULL);
		__this->set__currProvider_3(L_9);
		goto IL_0060;
	}

IL_004a:
	{
		LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB * L_10 = (LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB *)il2cpp_codegen_object_new(LinkProviderFactory_t828F56CBF94F18DB5AB05BB60801F84B26135EBB_il2cpp_TypeInfo_var);
		LinkProviderFactory__ctor_mA25C2DA9A5547F28574D29E54E49BACD334CE444(L_10, /*hidden argument*/NULL);
		bool L_11;
		L_11 = DeepLinkManager_get_IsSteamBuild_m5403EA906417467D1076C11010A7B94696128AE3_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		RuntimeObject* L_12;
		L_12 = LinkProviderFactory_GetProvider_mB4992EC88B46E9E394637FDD8043451C0F046D80(L_10, L_11, /*hidden argument*/NULL);
		__this->set__currProvider_3(L_12);
	}

IL_0060:
	{
		DeepLinkManager_CreatePauseGameObject_m59B5EC71D3CF69745AECD27D29102E2A498BB531(__this, /*hidden argument*/NULL);
	}

IL_0066:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject* L_13 = __this->get__currProvider_3();
			NullCheck(L_13);
			bool L_14;
			L_14 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean ImaginationOverflow.UniversalDeepLinking.ILinkProvider::Initialize() */, ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_007e;
			}
		}

IL_0073:
		{
			Exception_t * L_15 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
			Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_15, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral1802072B6C85CB9E034162DE2A5ECF0CFC99AB85)), /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeepLinkManager_RegisterIfNecessary_m2E20577596BF4EFAC4AC9E0E6C3C85002945BD1C_RuntimeMethod_var)));
		}

IL_007e:
		{
			RuntimeObject* L_16 = __this->get__currProvider_3();
			Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_17 = (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)il2cpp_codegen_object_new(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
			Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161(L_17, __this, (intptr_t)((intptr_t)DeepLinkManager__currProvider_LinkReceived_m356EB5F16FD7891D0627620AD7A29EA2AC22C08E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var);
			NullCheck(L_16);
			InterfaceActionInvoker1< Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * >::Invoke(1 /* System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::add_LinkReceived(System.Action`1<System.String>) */, ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0_il2cpp_TypeInfo_var, L_16, L_17);
			goto IL_00c4;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0097;
		}
		throw e;
	}

CATCH_0097:
	{ // begin catch(System.Exception)
		RuntimeObject* L_18 = __this->get__currProvider_3();
		NullCheck(L_18);
		Type_t * L_19;
		L_19 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20;
		L_20 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		String_t* L_21;
		L_21 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralED267C8DCB7043D2BDE06822955C482B2637216C)), L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var)));
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_21, /*hidden argument*/NULL);
		__this->set__currProvider_3((RuntimeObject*)NULL);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *)), /*hidden argument*/NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_00c4;
	} // end catch (depth: 1)

IL_00c4:
	{
		DeepLinkManager_CreatePauseGameObject_m59B5EC71D3CF69745AECD27D29102E2A498BB531(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreatePauseGameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_CreatePauseGameObject_m59B5EC71D3CF69745AECD27D29102E2A498BB531 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_mB8E76B61549B2812C7D41E09B82D023542710F35_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_m4512CC598556AD77369212209F79C8F290C43F80_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51D9E2BE3865F31E8579BB2E215D2CBA557E7777);
		s_Il2CppMethodInitialized = true;
	}
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get__go_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get__go_4();
		NullCheck(L_2);
		UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * L_3;
		L_3 = GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_m4512CC598556AD77369212209F79C8F290C43F80(L_2, /*hidden argument*/GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_m4512CC598556AD77369212209F79C8F290C43F80_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get__go_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		__this->set__go_4((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_0034:
	{
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)il2cpp_codegen_object_new(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		GameObject__ctor_mACDBD7A1F25B33D006A60F67EF901B33DD3D52E9(L_6, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = L_6;
		NullCheck(L_7);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_7, _stringLiteral51D9E2BE3865F31E8579BB2E215D2CBA557E7777, /*hidden argument*/NULL);
		__this->set__go_4(L_7);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get__go_4();
		NullCheck(L_8);
		UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * L_9;
		L_9 = GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_mB8E76B61549B2812C7D41E09B82D023542710F35(L_8, /*hidden argument*/GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF_mB8E76B61549B2812C7D41E09B82D023542710F35_RuntimeMethod_var);
		goto IL_0060;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0059;
		}
		throw e;
	}

CATCH_0059:
	{ // begin catch(System.Exception)
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var)));
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *)), /*hidden argument*/NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0060;
	} // end catch (depth: 1)

IL_0060:
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider_LinkReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__currProvider_LinkReceived_m356EB5F16FD7891D0627620AD7A29EA2AC22C08E (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		bool L_1;
		L_1 = DeepLinkManager_OnActivated_m2E2B64FA03163C54A9E9A352B223533428C48D6C(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		String_t* L_2 = ___s0;
		DeepLinkManager_StoreActivation_m9479B517341C6A2949972FCC975D1E2FDFE974DE_inline(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::StoreActivation(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_StoreActivation_m9479B517341C6A2949972FCC975D1E2FDFE974DE (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		__this->set__storedActivation_5(L_0);
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::OnActivated(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeepLinkManager_OnActivated_m2E2B64FA03163C54A9E9A352B223533428C48D6C (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method)
{
	LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * V_0 = NULL;
	LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		String_t* L_0 = ___s0;
		LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * L_1;
		L_1 = DeepLinkManager_CreateLinkActivation_mEA21A19C3C5DEFA54134ED845AA198C9362E799D(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_2 = __this->get__activated_2();
		V_1 = L_2;
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0037;
		}
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		LinkActivationHandler_tD42357446D097EB158294D5D2AFBAD80641D80D6 * L_4 = V_1;
		LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * L_5 = V_0;
		NullCheck(L_4);
		LinkActivationHandler_Invoke_mF17709851FDC3C8AF8E16285C4146E9EDE0D4524(L_4, L_5, /*hidden argument*/NULL);
		goto IL_0035;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001b;
		}
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		{
			IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var)));
			Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral5A1FA7C21B5462E60F25F1F1448FB5B3CB05FCFD)), /*hidden argument*/NULL);
			Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA(((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *)), /*hidden argument*/NULL);
			bool L_6;
			L_6 = Debug_get_isDebugBuild_mD0384FE4EA71AA14019FF171ADCE2F637C6E541C(/*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0033;
			}
		}

IL_0031:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *), ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeepLinkManager_OnActivated_m2E2B64FA03163C54A9E9A352B223533428C48D6C_RuntimeMethod_var)));
		}

IL_0033:
		{
			IL2CPP_POP_ACTIVE_EXCEPTION();
			goto IL_0035;
		}
	} // end catch (depth: 1)

IL_0035:
	{
		return (bool)1;
	}

IL_0037:
	{
		return (bool)0;
	}
}
// ImaginationOverflow.UniversalDeepLinking.LinkActivation ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreateLinkActivation(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * DeepLinkManager_CreateLinkActivation_mEA21A19C3C5DEFA54134ED845AA198C9362E799D (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_0;
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_1 = (Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 *)il2cpp_codegen_object_new(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666(L_1, /*hidden argument*/Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		String_t* L_2 = ___s0;
		UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * L_3 = (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 *)il2cpp_codegen_object_new(UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86_il2cpp_TypeInfo_var);
		UrlEncodingParser__ctor_mF7F36EF49BC323178086264EEBFB0CC12E49611E(L_3, L_2, /*hidden argument*/NULL);
		UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * L_4 = L_3;
		V_1 = L_4;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = UrlEncodingParser_get_Query_m3AD6CC626663C50666AB304238BE344126108646_inline(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001c;
		}
		throw e;
	}

CATCH_001c:
	{ // begin catch(System.Exception)
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		String_t* L_6 = ___s0;
		String_t* L_7 = V_0;
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_8 = V_1;
		LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A * L_9 = (LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A *)il2cpp_codegen_object_new(LinkActivation_tD820AFA6C098284CBCA2BA033609C3381D82878A_il2cpp_TypeInfo_var);
		LinkActivation__ctor_m8C9F2B2C1647E2E38C0B1EA8DC74D3FC09C057F2(L_9, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::GameCameFromPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_GameCameFromPause_mD73F9C7B9381B98C97E1789F86D7B093BF1AC608 (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get__currProvider_3();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = __this->get__currProvider_3();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::PollInfoAfterPause() */, ILinkProvider_tE9E4320A02A5A11BF773681077CCB2182453F9E0_il2cpp_TypeInfo_var, L_1);
	}

IL_0013:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider__ctor_mA4413A126A46DB73C74152831ABF4DF1C6596491 (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		((EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_StaticFields*)il2cpp_codegen_static_fields_for(EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389_il2cpp_TypeInfo_var))->set__instance_0(__this);
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EditorLinkProvider_Initialize_m6C9E372A4385A201ED7B6B27B264338DC8F95B16 (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::add_LinkReceived(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_add_LinkReceived_mAC798266FD5E021A310F25E68008A7D1274DCA09 (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * __this, Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * V_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * V_1 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * V_2 = NULL;
	{
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_LinkReceived_1();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = V_0;
		V_1 = L_1;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_2 = V_1;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)CastclassSealed((RuntimeObject*)L_4, Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var));
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** L_5 = __this->get_address_of_LinkReceived_1();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_6 = V_2;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_7 = V_1;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *>((Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_9 = V_0;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)L_9) == ((RuntimeObject*)(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::remove_LinkReceived(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_remove_LinkReceived_m4F959A083F9D5C7B28D744AF620C2FF3D84BA757 (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * __this, Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * V_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * V_1 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * V_2 = NULL;
	{
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_LinkReceived_1();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = V_0;
		V_1 = L_1;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_2 = V_1;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)CastclassSealed((RuntimeObject*)L_4, Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var));
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** L_5 = __this->get_address_of_LinkReceived_1();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_6 = V_2;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_7 = V_1;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *>((Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_9 = V_0;
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)L_9) == ((RuntimeObject*)(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::PollInfoAfterPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_PollInfoAfterPause_m0772522E1760A592D75299D9440C0617B4C30068 (EditorLinkProvider_t0B781BAAD65212A29A46991F94DFEE6715AB4389 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript_Start_mEEDACD4DC1BA40A21AD5C47359FFB36D3903958C (UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::OnApplicationPause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript_OnApplicationPause_mB3D0CABC8375C5929B81E5E4C4B1C1C8A2967C53 (UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * __this, bool ___pauseStatus0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___pauseStatus0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		RuntimeObject* L_1;
		L_1 = UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_mFF1AD6F07D1CCE14F95C375F0646FD998BED332F(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_2;
		L_2 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_1, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::OnApplicationFocus(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript_OnApplicationFocus_m8669F7F29C567A6868C5433237ACD23B4186E1B2 (UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * __this, bool ___focus0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___focus0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		RuntimeObject* L_1;
		L_1 = UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_mFF1AD6F07D1CCE14F95C375F0646FD998BED332F(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_2;
		L_2 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_1, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Collections.IEnumerator ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::CallDeepLinkManagerAfterDelay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_mFF1AD6F07D1CCE14F95C375F0646FD998BED332F (UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * L_0 = (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 *)il2cpp_codegen_object_new(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5_il2cpp_TypeInfo_var);
		U3CCallDeepLinkManagerAfterDelayU3Ed__4__ctor_m3A0D7189FD384A2291756D99C8E2441152D24B4D(L_0, 0, /*hidden argument*/NULL);
		U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript__ctor_m77EC4F6FCD293FF6EFAD0CEB4F50ACCEBFC16A12 (UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Url()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUrlU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Url(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Url_m1A693E00E113D3451ED4EE021A63B82E025A464B (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUrlU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Query()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Query_m3AD6CC626663C50666AB304238BE344126108646 (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CQueryU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Query(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Query_m308D23C620696589665DA5BDE202F464E32C8312 (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CQueryU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser__ctor_mF7F36EF49BC323178086264EEBFB0CC12E49611E (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___queryStringOrUrl0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666(__this, /*hidden argument*/Dictionary_2__ctor_mA6747E78BD4DF1D09D9091C1B3EBAE0FDB200666_RuntimeMethod_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		UrlEncodingParser_set_Url_m1A693E00E113D3451ED4EE021A63B82E025A464B_inline(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___queryStringOrUrl0;
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___queryStringOrUrl0;
		Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * L_4;
		L_4 = UrlEncodingParser_Parse_mC585A371964ED1CBF30216D7109D71A4D73E8DD5(__this, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * UrlEncodingParser_Parse_mC585A371964ED1CBF30216D7109D71A4D73E8DD5 (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___query0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Clear_mB853C65F23D3966B3D3026F3541601042CF7DF8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m5BB06692D9A48A3FEEB102881A86417DE6DA5027_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___query0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Uri_IsWellFormedUriString_mF90D5434E6C63D063E21C97064A098F6DB3C5CBA(L_0, 1, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_2 = ___query0;
		UrlEncodingParser_set_Url_m1A693E00E113D3451ED4EE021A63B82E025A464B_inline(__this, L_2, /*hidden argument*/NULL);
	}

IL_0010:
	{
		String_t* L_3 = ___query0;
		bool L_4;
		L_4 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_Clear_mB853C65F23D3966B3D3026F3541601042CF7DF8A(__this, /*hidden argument*/Dictionary_2_Clear_mB853C65F23D3966B3D3026F3541601042CF7DF8A_RuntimeMethod_var);
		goto IL_00c9;
	}

IL_0023:
	{
		String_t* L_5 = ___query0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = String_IndexOf_mEE2D2F738175E3FF05580366D6226DBD673CA2BE(L_5, ((int32_t)63), /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) <= ((int32_t)(-1))))
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_8 = ___query0;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)))))
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_11 = ___query0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13;
		L_13 = String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190(L_11, ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)), /*hidden argument*/NULL);
		___query0 = L_13;
	}

IL_0046:
	{
		String_t* L_14 = ___query0;
		UrlEncodingParser_set_Query_m308D23C620696589665DA5BDE202F464E32C8312_inline(__this, L_14, /*hidden argument*/NULL);
		String_t* L_15 = ___query0;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_16 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_17 = L_16;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		NullCheck(L_15);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18;
		L_18 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_15, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		V_2 = 0;
		goto IL_00c3;
	}

IL_0063:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_3 = L_22;
		String_t* L_23 = V_3;
		NullCheck(L_23);
		int32_t L_24;
		L_24 = String_IndexOf_mEE2D2F738175E3FF05580366D6226DBD673CA2BE(L_23, ((int32_t)61), /*hidden argument*/NULL);
		V_4 = L_24;
		int32_t L_25 = V_4;
		if ((((int32_t)L_25) <= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		String_t* L_26 = V_3;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		String_t* L_28;
		L_28 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_26, 0, L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		String_t* L_29 = V_3;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		String_t* L_31;
		L_31 = String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190(L_29, ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1)), /*hidden argument*/NULL);
		V_6 = L_31;
		String_t* L_32 = V_5;
		V_7 = L_32;
		V_8 = 2;
		goto IL_00ab;
	}

IL_0096:
	{
		String_t* L_33 = V_7;
		int32_t L_34 = V_8;
		int32_t L_35 = L_34;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
		int32_t L_36 = L_35;
		RuntimeObject * L_37 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_36);
		String_t* L_38;
		L_38 = String_Concat_m4D0DDA7FEDB75304E5FDAF8489A0478EE58A45F2(L_33, L_37, /*hidden argument*/NULL);
		V_5 = L_38;
	}

IL_00ab:
	{
		String_t* L_39 = V_5;
		bool L_40;
		L_40 = Dictionary_2_ContainsKey_m5BB06692D9A48A3FEEB102881A86417DE6DA5027(__this, L_39, /*hidden argument*/Dictionary_2_ContainsKey_m5BB06692D9A48A3FEEB102881A86417DE6DA5027_RuntimeMethod_var);
		if (L_40)
		{
			goto IL_0096;
		}
	}
	{
		String_t* L_41 = V_5;
		String_t* L_42 = V_6;
		Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92(__this, L_41, L_42, /*hidden argument*/Dictionary_2_Add_mE0EF428186E444BFEAD18AC6810D423EEABB3F92_RuntimeMethod_var);
	}

IL_00bf:
	{
		int32_t L_43 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
	}

IL_00c3:
	{
		int32_t L_44 = V_2;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_45 = V_1;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_45)->max_length))))))
		{
			goto IL_0063;
		}
	}

IL_00c9:
	{
		return __this;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_ToString_mF4D8D91A3F028084F8111F750B67090E6D073BF5 (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Keys_m69FA0927978855661A3EA0DC3B9AE1D00FCE4825_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mFB991F7C2A32C8DA7C4D2F522B9C69C36F7DF904_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m5357493D8DA32113DC35EE95205D4E3D607C9F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m7FA8CBC3A46C7349CA380C3BA7B3294C6C0FC9D8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyCollection_GetEnumerator_mBEA854721D5E7717F856476B1E4563A59661CBF7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547  V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_0;
		KeyCollection_t52C81163A051BCD87A36FEF95F736DD600E2305D * L_1;
		L_1 = Dictionary_2_get_Keys_m69FA0927978855661A3EA0DC3B9AE1D00FCE4825(__this, /*hidden argument*/Dictionary_2_get_Keys_m69FA0927978855661A3EA0DC3B9AE1D00FCE4825_RuntimeMethod_var);
		NullCheck(L_1);
		Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547  L_2;
		L_2 = KeyCollection_GetEnumerator_mBEA854721D5E7717F856476B1E4563A59661CBF7(L_1, /*hidden argument*/KeyCollection_GetEnumerator_mBEA854721D5E7717F856476B1E4563A59661CBF7_RuntimeMethod_var);
		V_1 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_0014:
		{
			String_t* L_3;
			L_3 = Enumerator_get_Current_m7FA8CBC3A46C7349CA380C3BA7B3294C6C0FC9D8_inline((Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m7FA8CBC3A46C7349CA380C3BA7B3294C6C0FC9D8_RuntimeMethod_var);
			V_2 = L_3;
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_4;
			String_t* L_6 = V_0;
			NullCheck(L_5);
			ArrayElementTypeCheck (L_5, L_6);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_6);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_5;
			String_t* L_8 = V_2;
			NullCheck(L_7);
			ArrayElementTypeCheck (L_7, L_8);
			(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_7;
			NullCheck(L_9);
			ArrayElementTypeCheck (L_9, _stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
			String_t* L_11 = V_2;
			String_t* L_12;
			L_12 = Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141(__this, L_11, /*hidden argument*/Dictionary_2_get_Item_mFCD5E71429358EE225039B602674518740D30141_RuntimeMethod_var);
			IL2CPP_RUNTIME_CLASS_INIT(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
			String_t* L_13;
			L_13 = Uri_EscapeUriString_m381555089C7EE743700F115FE56F52D5757F0028(L_12, /*hidden argument*/NULL);
			NullCheck(L_10);
			ArrayElementTypeCheck (L_10, L_13);
			(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_13);
			StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_10;
			NullCheck(L_14);
			ArrayElementTypeCheck (L_14, _stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E);
			String_t* L_15;
			L_15 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_14, /*hidden argument*/NULL);
			V_0 = L_15;
		}

IL_004f:
		{
			bool L_16;
			L_16 = Enumerator_MoveNext_m5357493D8DA32113DC35EE95205D4E3D607C9F33((Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m5357493D8DA32113DC35EE95205D4E3D607C9F33_RuntimeMethod_var);
			if (L_16)
			{
				goto IL_0014;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005a;
	}

FINALLY_005a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mFB991F7C2A32C8DA7C4D2F522B9C69C36F7DF904((Enumerator_t02FBEF5309FE3C4F8A7A1140B9590258FF5FA547 *)(&V_1), /*hidden argument*/Enumerator_Dispose_mFB991F7C2A32C8DA7C4D2F522B9C69C36F7DF904_RuntimeMethod_var);
		IL2CPP_END_FINALLY(90)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(90)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x68, IL_0068)
	}

IL_0068:
	{
		String_t* L_17 = V_0;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_18 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_19 = L_18;
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		NullCheck(L_17);
		String_t* L_20;
		L_20 = String_Trim_m10D967E03EDCB170227406426558B7FEA27CD6CC(L_17, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		String_t* L_21;
		L_21 = UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline(__this, /*hidden argument*/NULL);
		bool L_22;
		L_22 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00cf;
		}
	}
	{
		String_t* L_23;
		L_23 = UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24;
		L_24 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_23, _stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00bd;
		}
	}
	{
		String_t* L_25;
		L_25 = UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline(__this, /*hidden argument*/NULL);
		String_t* L_26;
		L_26 = UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27;
		L_27 = String_IndexOf_mEE2D2F738175E3FF05580366D6226DBD673CA2BE(L_26, ((int32_t)63), /*hidden argument*/NULL);
		NullCheck(L_25);
		String_t* L_28;
		L_28 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_25, 0, ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1)), /*hidden argument*/NULL);
		String_t* L_29 = V_0;
		String_t* L_30;
		L_30 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_28, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		goto IL_00cf;
	}

IL_00bd:
	{
		String_t* L_31;
		L_31 = UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline(__this, /*hidden argument*/NULL);
		String_t* L_32 = V_0;
		String_t* L_33;
		L_33 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_31, _stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
	}

IL_00cf:
	{
		String_t* L_34 = V_0;
		return L_34;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4__ctor_m3A0D7189FD384A2291756D99C8E2441152D24B4D (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_IDisposable_Dispose_m05AC12C3B06AA9DD5790BD567F4DDE826CF54B64 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCallDeepLinkManagerAfterDelayU3Ed__4_MoveNext_m72122D595463D6493315294A7D69163843046BB4 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_004b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		bool L_4 = L_3->get__onJob_4();
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		return (bool)0;
	}

IL_0026:
	{
		UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		L_5->set__onJob_4((bool)1);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_6 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_6, (0.200000003f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_004b:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
			DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * L_7;
			L_7 = DeepLinkManager_get_Instance_mDEF4E7332FABA5E12FC7838E80D8DB8DA34BABC2_inline(/*hidden argument*/NULL);
			NullCheck(L_7);
			DeepLinkManager_GameCameFromPause_mD73F9C7B9381B98C97E1789F86D7B093BF1AC608(L_7, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x89, FINALLY_007c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			{
				IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
				goto CATCH_005e;
			}
			throw e;
		}

CATCH_005e:
		{ // begin catch(System.Exception)
			V_1 = ((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *));
			Exception_t * L_8 = V_1;
			String_t* L_9;
			L_9 = String_Concat_m4D0DDA7FEDB75304E5FDAF8489A0478EE58A45F2(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4697D4C3588C40B4B90B49284BEEC64F643F83E0)), L_8, /*hidden argument*/NULL);
			UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * L_10 = __this->get_U3CU3E4__this_2();
			NullCheck(L_10);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
			L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_10, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var)));
			Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199(L_9, L_11, /*hidden argument*/NULL);
			IL2CPP_POP_ACTIVE_EXCEPTION();
			IL2CPP_LEAVE(0x89, FINALLY_007c);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		UniversalDeeplinkingRuntimeScript_t463716621E70CB88FCFD59284591F9D8DB4EBEBF * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_12);
		L_12->set__onJob_4((bool)0);
		IL2CPP_END_FINALLY(124)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x89, IL_0089)
	}

IL_0089:
	{
		return (bool)0;
	}
}
// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41B52D480503AB1FD2A8F581DB163F2EACEA5C38 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_mA68C06E0EDF2690E5364A0EA10E47E69E6B57378 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_mA68C06E0EDF2690E5364A0EA10E47E69E6B57378_RuntimeMethod_var)));
	}
}
// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_get_Current_mF6388ADC1A1C1E6A04D50F58FBFFA2A2A6B4B2DF (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t06251D3C7C22A848760EADD42846BEF6743FC5D5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_set_Instance_m6FDEFEDD41CF5A8A1DEF1343A288ED70B87075C8_inline (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		((DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DeepLinkManager_get_IsSteamBuild_m5403EA906417467D1076C11010A7B94696128AE3_inline (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CIsSteamBuildU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_StoreActivation_m9479B517341C6A2949972FCC975D1E2FDFE974DE_inline (DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		__this->set__storedActivation_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Query_m3AD6CC626663C50666AB304238BE344126108646_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CQueryU3Ek__BackingField_15();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Url_m1A693E00E113D3451ED4EE021A63B82E025A464B_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUrlU3Ek__BackingField_14(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Query_m308D23C620696589665DA5BDE202F464E32C8312_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CQueryU3Ek__BackingField_15(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Url_m4AB99A9DF4ADC6ECE71132090A4546FBE4D0E54C_inline (UrlEncodingParser_t627F2DB7888F70FE972BAF263E00B72A884A5B86 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUrlU3Ek__BackingField_14();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * DeepLinkManager_get_Instance_mDEF4E7332FABA5E12FC7838E80D8DB8DA34BABC2_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var);
		DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF * L_0 = ((DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tBFACEAFCEBC20377D71191084401C89A5F35A1CF_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mF254C3BDEA9F0829958522BF88C75DE8BEC4961F_gshared_inline (Enumerator_t4F7151B1D8B03D97F931400ABBC97A08CE419031 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_currentKey_3();
		return (RuntimeObject *)L_0;
	}
}
