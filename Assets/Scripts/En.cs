﻿

public class En 
{
    public static string tapToPlace = "touch a spot on the ground points to place your door";
    public static string moveDevice = "Point the phone at the ground and let the app scan your environment";
    public static string waitForSound = "Please wait for charging and the signal tone.";
    public static string reloadingAssets = "Loading the Magic";
    public static string downloadingGameMode = "Downloading the Magic : ";
    
    //infos
    public static string help = "help";
    public static string image360 = "360 image";
    public static string video360 = "360 video ";
    public static string assetBundle = "3d scene";
    public static string image360D = "this is a 360 image you can look around inside of it";
    public static string video360D = "this is a 360 video you can look around inside of it while it's playing";
    public static string assetBundleD = "this is a 3d scene you can look around and move around inside of it";

    //gameMode
    public static string gameMode = "how to play";
    public static string gameModeD = "try to find the odd hidden objects in the shortest time possible";

    //done
    public static string done = "Well done !";
    public static string doneD = "Challenge complete : \n";
    public static string textProb = "text problem";
    public static string FiveChar = "make sure you enter more than 5 characters";
    public static string EmailNes = "make sure you are entering a correct Email";


    //errors
    public static string assetProblem = "asset problem";
    public static string useOfflineOnce = "make sure you have used this asset online once";
    public static string error = "error";
    public static string checkInternet = "check internet";
    public static string openOnceOnline = "make sure you open the app at least once online";
    public static string only360 = "only 360 images can be used offline for videos you need to be online";


    //permission gallery
    public static string galleryPermission = "Required to display in augmented reality";


    //placeHolder
    public static string placeHolderDone = "Enter your email to enter the ranking";

    //mainMenu
    public static string exploaration = "travel";
    public static string gamer = "game";
    public static string custom = "museum";

    //errors network
    public static string networkTitle = "network error";
    public static string networkD = "the server may be down for maintenance or make sure you are connected to the internet and try again later ...";


}
