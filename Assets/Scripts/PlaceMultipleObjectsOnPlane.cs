﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(ARRaycastManager))]
public class PlaceMultipleObjectsOnPlane : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    public Animator customButton;
    public GameObject ttt;
    public GameObject uiHelper;

    public Material mt;

    public bool ableToPlace = false;

    public string type;

    public AudioClip clip;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    public Texture2D texture;

    public Texture2D tp;

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; private set; }

    /// <summary>
    /// Invoked whenever an object is placed in on a plane.
    /// </summary>
    public static event Action onPlacedObject;
    public List<location> locations;
    ARRaycastManager m_RaycastManager;
    RaycastHit HitInfo;
    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
    RaycastHit hit;

    void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
    }


    void Update()
    {
       
        debugger.updateCamera(Camera.main.transform.position);
        if (ableToPlace)
        {
            if (onPlacedObject != null)
            {
                onPlacedObject();
            }
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out HitInfo))
            {
                if (HitInfo.transform.name.ToLower().Contains("arplane") && ableToPlace)
                {
                    uiHelper.SetActive(false);
                    if (placeHolder.activeInHierarchy == false)
                        placeHolder.SetActive(true);
                    placeHolder.transform.position = HitInfo.point;
                }
            }
        }
        
    }

    public Slider scaleObject;
    public void adjustDoorRot(Vector2 v2)
    {
        placeHolder.transform.localEulerAngles = new Vector3(placeHolder.transform.localEulerAngles.x, v2.x * -180f, placeHolder.transform.localEulerAngles.z);
    }
    public void adjustDoorScale()
    {

        if (placeHolder.GetComponent<Animator>().enabled)
            placeHolder.GetComponent<Animator>().enabled = false;
        placeHolder.transform.localScale = new Vector3(scaleObject.value, scaleObject.value, scaleObject.value);

    }
    public GameObject placeHolder;

    public void placeDoor()
    {
            spawnedObject = Instantiate(m_PlacedPrefab, placeHolder.transform.position, placeHolder.transform.rotation);
            if (type.ToUpper() == "IMAGE")
            {
                spawnedObject.GetComponentInChildren<VideoPlayer>().enabled = false;
                spawnedObject.GetComponentInChildren<VideoPlayer>().GetComponent<Renderer>().material.mainTexture = texture;

            }
            else if (type.ToUpper() == "VIDEO")
            {
                spawnedObject.GetComponentInChildren<VideoPlayer>().enabled = true;
                spawnedObject.GetComponentInChildren<VideoPlayer>().Play();

            }
            else if (type.ToUpper() == "ASSETBUNDLE")
            {
                spawnedObject.GetComponentInChildren<VideoPlayer>().enabled = false;
                spawnedObject.GetComponentInChildren<VideoPlayer>().GetComponent<Renderer>().material.mainTexture = texture;

                GameObject temp = Instantiate(textureHolder.AB, spawnedObject.transform.GetChild(1));

                textureHolder.AB = new GameObject();

            }

            //if (nativeGalleryImporter.instance)
            //{

            //    spawnedObject = spawnedObject.GetComponentInChildren<VideoPlayer>().transform.GetChild(0).gameObject;

            //    spawnedObject.transform.GetChild(0).localScale = new Vector3(spawnedObject.transform.GetChild(0).localScale.x, spawnedObject.transform.GetChild(0).localScale.y, spawnedObject.transform.GetChild(0).localScale.z * (int)nativeGalleryImporter.instance.r1.texture.height / nativeGalleryImporter.instance.r1.texture.width);
            //    spawnedObject.transform.GetChild(1).localScale = new Vector3(spawnedObject.transform.GetChild(1).localScale.x, spawnedObject.transform.GetChild(1).localScale.y, spawnedObject.transform.GetChild(1).localScale.z * (int)nativeGalleryImporter.instance.r2.texture.height / nativeGalleryImporter.instance.r2.texture.width);
            //    spawnedObject.transform.GetChild(2).localScale = new Vector3(spawnedObject.transform.GetChild(2).localScale.x, spawnedObject.transform.GetChild(2).localScale.y, spawnedObject.transform.GetChild(2).localScale.z * (int)nativeGalleryImporter.instance.r3.texture.height / nativeGalleryImporter.instance.r3.texture.width);


            //    foreach (var mat in spawnedObject.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().materials)
            //    {
            //        mat.mainTexture = nativeGalleryImporter.instance.r1.texture;
            //    }

            //    foreach (var mat in spawnedObject.transform.GetChild(1).GetChild(0).GetComponent<Renderer>().materials)
            //    {
            //        mat.mainTexture = nativeGalleryImporter.instance.r2.texture;
            //    }

            //    foreach (var mat in spawnedObject.transform.GetChild(2).GetChild(0).GetComponent<Renderer>().materials)
            //    {
            //        mat.mainTexture = nativeGalleryImporter.instance.r3.texture;
            //    }
            //    if (clip != null)
            //    {
            //        GetComponent<AudioSource>().PlayOneShot(clip);
            //    }


            //}


            //if (scoreManager.instance)
            //{
            //    scoreManager.instance.IsCounting = true;

            //    spawnedObject.GetComponentInChildren<scoreManager>().GetComponent<Renderer>().material.mainTexture = tp;
            //}

            ARPlaneManager ap = GetComponent<ARPlaneManager>();

            ap.enabled = false;
            foreach (var plane in ap.trackables)
                plane.gameObject.SetActive(false);


            //mt.SetColor("_PlaneColor", new Color(1, 1, 1, 0));
            //mt.SetColor("_TexTintColor", new Color(1, 1, 1, 0));

            ableToPlace = false;
        placeHolder.SetActive(false);
            if (onPlacedObject != null)
            {
                onPlacedObject();
            }
            GameObject port = new GameObject();

            foreach (location l in locations)
            {

                Debug.Log(l.name);
                Debug.Log("pano location url :" + textureHolder.panoramas[l.url]);
                GameObject hot = Instantiate(Resources.Load<GameObject>("hotSpot"));
                port.transform.SetParent(spawnedObject.transform.GetChild(2));
                port.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
                hot.transform.SetParent(port.transform);
                hot.transform.localPosition = new Vector3(-1 * l.x, l.y, l.z);
                hot.GetComponent<ChangePano>().id = l.url;
                hot.GetComponent<Button>().onClick.AddListener(delegate { });
                hot.GetComponentInChildren<TextMeshProUGUI>().text = l.name;
                hot.transform.GetComponentInChildren<Canvas>().enabled = false;
                hot.transform.GetChild(0).LookAt(Camera.main.transform);
                textureHolder.thumbnailes.TryGetValue(l.url, out Texture2D thb);
                Debug.Log(thb);
                hot.GetComponent<MeshRenderer>().material.mainTexture = thb;
                hot.GetComponent<MeshRenderer>().enabled = false;
                debugger.updateSpawned(hot.transform.position);
                debugger.updateSpawnedRot(hot.transform.rotation);
                //hot.GetComponent<MeshRenderer>().enabled = false;
                //hot.GetComponentInChildren<MeshRenderer>().enabled = false;

                print(JsonUtility.ToJson(l));
            }
            port.transform.localPosition = Vector3.zero;
            port.transform.localRotation = new Quaternion(-180f, 0f, 0f, 0f);
        port.transform.localScale = new Vector3(port.transform.localScale.x, port.transform.localScale.y, -1 * port.transform.localScale.z);

        spawnedObject.transform.localScale = placeHolder.transform.localScale;
            debugger.updateSpawned(port.transform.localPosition);
            debugger.updateSpawnedRot(port.transform.localRotation);

        

    }





}
