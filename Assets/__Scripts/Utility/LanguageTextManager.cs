﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LanguageTextManager : MonoBehaviour
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public string fr, en;

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = LanguageManager.isFR ? fr : en;

        LanguageManager.changed += this.change;

    }

    private void OnDestroy()
    {
        LanguageManager.changed -= this.change;
    }


    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void change(bool isFR)
    {
        GetComponent<TextMeshProUGUI>().text = isFR ? fr : en;
    }

}
