﻿using DanielLochner.Assets.SimpleScrollSnap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class firstLast : MonoBehaviour
{
    public GameObject next, back, title;

    public int versionNumber;

    public void ff(Vector2 v)
    {
        if(v.x <= 0.1f)
        {
            back.SetActive(false);
        }else if(v.x >= 0.9f)
        {
            next.SetActive(false);
            //title.SetActive(true);
        }
        else
        {
            back.SetActive(true);
            next.SetActive(true);
            //title.SetActive(false);
        }
    }

    private void Start()
    {
        //title.GetComponent<TextMeshProUGUI>().text = Lang.choose("Ranking", "Classement");

        if(PlayerPrefs.GetInt("v" + versionNumber) ==  versionNumber)
        {
            GetComponent<SimpleScrollSnap>().startingPanel = 5;
            if(PlayerPrefs.GetInt("showScore", 0) == 1)
            {
                GetComponent<SimpleScrollSnap>().startingPanel = 6;
                next.SetActive(false);
                PlayerPrefs.SetInt("showScore", 0);
            }
            back.SetActive(true);
        }
        else
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("v" + versionNumber, versionNumber);
        }

        int v = GetComponent<SimpleScrollSnap>().CurrentPanel;

        if (v == 1)
        {
            back.SetActive(false);
        }
        else if (v == 6)
        {
            next.SetActive(false);
            //title.SetActive(true);
        }
        else
        {
            back.SetActive(true);
            next.SetActive(true);
            //title.SetActive(false);
        }
    }
}
