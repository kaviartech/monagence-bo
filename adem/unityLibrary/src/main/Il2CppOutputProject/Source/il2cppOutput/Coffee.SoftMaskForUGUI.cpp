﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>
struct Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t62010156673DE1460AB1D1CEBE5DCD48665E1A38;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,UnityEngine.Matrix4x4>
struct KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>
struct List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>
struct List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<Coffee.UIExtensions.SoftMask>
struct Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.Matrix4x4>
struct ValueCollection_t98702D87132F9598283232D006EDE17A67B2D9EA;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,UnityEngine.Matrix4x4>[]
struct EntryU5BU5D_t6E8DCA3DE19E971B62F0D95CF5AFFA03E1B408FB;
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[]
struct List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// Coffee.UIExtensions.SoftMask[]
struct SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6;
// Coffee.UIExtensions.SoftMaskable[]
struct SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.UI.Mask
struct Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849;
// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39;
// Coffee.UIExtensions.SoftMask
struct SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7;
// Coffee.UIExtensions.SoftMaskable
struct SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// Coffee.UIExtensions.SoftMask/<>c
struct U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B;

IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t97FAEBE964F3F622D4865E7EC62717FE94D1F56D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0CA801CE75C2F860477B707B285573DE1591BE00;
IL2CPP_EXTERN_C String_t* _stringLiteral0F52C788AC4796FE5841155F7DF3896E049C051E;
IL2CPP_EXTERN_C String_t* _stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21;
IL2CPP_EXTERN_C String_t* _stringLiteral9AD9AC2A179FDCC5DF15DA875A0DAF5F51C43BCA;
IL2CPP_EXTERN_C String_t* _stringLiteralCB14659CB03507C756FB26DFDC1D82D6AE87A527;
IL2CPP_EXTERN_C String_t* _stringLiteralD0A6E6DC25E45868734BB4AF5E23E886068187CE;
IL2CPP_EXTERN_C String_t* _stringLiteralDA40AA217B2961790D4C788E975FD50D97848330;
IL2CPP_EXTERN_C String_t* _stringLiteralEDACEEFD6A4FDC09F7F84820C5D5791EB514AF7D;
IL2CPP_EXTERN_C String_t* _stringLiteralF081554143A3CB72659C18709A22913DF21549E6;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mDB51DAE14977345C6094B986C51FF7DCC3A5DB66_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m2D42408638C489F4076E2D9AEACB059EBAF843BB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAll_mB4F9BDD13B74F228229F3EF77915CA1880585C36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m29CA3BE16DCB73AAAD0345E10DB6953D4D8626D2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8C6DA11294CCBAB33C07414BC54634526B1C2D0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Predicate_1__ctor_m39AAD12C99FF47DF1421C18605E877CD25AFFDA5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CSetParentU3Eb__70_0_mC32C9A814621E217595F893608F9952930B5630F_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106;
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBEC84EF8E67A7C2D3D0DD6CD1F736154D4FD6BDF 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>
struct Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t6E8DCA3DE19E971B62F0D95CF5AFFA03E1B408FB* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t98702D87132F9598283232D006EDE17A67B2D9EA * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___entries_1)); }
	inline EntryU5BU5D_t6E8DCA3DE19E971B62F0D95CF5AFFA03E1B408FB* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t6E8DCA3DE19E971B62F0D95CF5AFFA03E1B408FB** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t6E8DCA3DE19E971B62F0D95CF5AFFA03E1B408FB* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___keys_7)); }
	inline KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ___values_8)); }
	inline ValueCollection_t98702D87132F9598283232D006EDE17A67B2D9EA * get_values_8() const { return ___values_8; }
	inline ValueCollection_t98702D87132F9598283232D006EDE17A67B2D9EA ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t98702D87132F9598283232D006EDE17A67B2D9EA * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,UnityEngine.Matrix4x4>
struct KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::dictionary
	Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2, ___dictionary_0)); }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>
struct List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C, ____items_1)); }
	inline SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6* get__items_1() const { return ____items_1; }
	inline SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_StaticFields, ____emptyArray_5)); }
	inline SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SoftMaskU5BU5D_tBA3871433B639111062A74EFD3BBA27CB6C87CC6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>
struct List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2, ____items_1)); }
	inline SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D* get__items_1() const { return ____items_1; }
	inline SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2_StaticFields, ____emptyArray_5)); }
	inline SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SoftMaskableU5BU5D_t5D601AD75DD1BF068D85B8E8D9528124A47AF77D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Coffee.UIExtensions.SoftMask/<>c
struct U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields
{
public:
	// Coffee.UIExtensions.SoftMask/<>c Coffee.UIExtensions.SoftMask/<>c::<>9
	U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * ___U3CU3E9_0;
	// System.Predicate`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask/<>c::<>9__70_0
	Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * ___U3CU3E9__70_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__70_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields, ___U3CU3E9__70_0_1)); }
	inline Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * get_U3CU3E9__70_0_1() const { return ___U3CU3E9__70_0_1; }
	inline Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 ** get_address_of_U3CU3E9__70_0_1() { return &___U3CU3E9__70_0_1; }
	inline void set_U3CU3E9__70_0_1(Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * value)
	{
		___U3CU3E9__70_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__70_0_1), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>
struct Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2, ___list_0)); }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * get_list_0() const { return ___list_0; }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2, ___current_3)); }
	inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * get_current_3() const { return ___current_3; }
	inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>
struct Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::dictionary
	Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::version
	int32_t ___version_2;
	// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::currentKey
	int32_t ___currentKey_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9, ___dictionary_0)); }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentKey_3() { return static_cast<int32_t>(offsetof(Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9, ___currentKey_3)); }
	inline int32_t get_currentKey_3() const { return ___currentKey_3; }
	inline int32_t* get_address_of_currentKey_3() { return &___currentKey_3; }
	inline void set_currentKey_3(int32_t value)
	{
		___currentKey_3 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.Rendering.BuiltinRenderTextureType
struct BuiltinRenderTextureType_t89FFB8A7C9095150BCA40E573A73664CC37F023A 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t89FFB8A7C9095150BCA40E573A73664CC37F023A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.CubemapFace
struct CubemapFace_t74FBCA71A21252C2E10E256E61FE0B1E09D7B9E5 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CubemapFace_t74FBCA71A21252C2E10E256E61FE0B1E09D7B9E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.HideFlags
struct HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RenderMode
struct RenderMode_tFF8E9ABC771ACEBD5ACC2D9DFB02264E0EA6CDBF 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderMode_tFF8E9ABC771ACEBD5ACC2D9DFB02264E0EA6CDBF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RenderTextureFormat
struct RenderTextureFormat_t8371287102ED67772EF78229CF4AB9D38C2CD626 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t8371287102ED67772EF78229CF4AB9D38C2CD626, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RenderTextureReadWrite
struct RenderTextureReadWrite_t4F64C0CC7097707282602ADD52760C1A86552580 
{
public:
	// System.Int32 UnityEngine.RenderTextureReadWrite::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureReadWrite_t4F64C0CC7097707282602ADD52760C1A86552580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SpriteMaskInteraction
struct SpriteMaskInteraction_t35C03F1CE86D053B455FAB3EFFCB429CA9C6A74C 
{
public:
	// System.Int32 UnityEngine.SpriteMaskInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteMaskInteraction_t35C03F1CE86D053B455FAB3EFFCB429CA9C6A74C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Positions_0)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Colors_1)); }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv0S_2)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv0S_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv1S_3)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv1S_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv2S_4)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv2S_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv3S_5)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv3S_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Normals_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Tangents_7)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tangents_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Indices_8)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Indices_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___s_DefaultNormal_10 = value;
	}
};


// Coffee.UIExtensions.SoftMask/DesamplingRate
struct DesamplingRate_t05DCB4B43F9EB44B5E216F48DD6BD5E07064E99D 
{
public:
	// System.Int32 Coffee.UIExtensions.SoftMask/DesamplingRate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DesamplingRate_t05DCB4B43F9EB44B5E216F48DD6BD5E07064E99D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Rendering.RenderTargetIdentifier
struct RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};


// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.Predicate`1<Coffee.UIExtensions.SoftMask>
struct Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___willRenderCanvases_5;

public:
	inline static int32_t get_offset_of_preWillRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields, ___preWillRenderCanvases_4)); }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * get_preWillRenderCanvases_4() const { return ___preWillRenderCanvases_4; }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 ** get_address_of_preWillRenderCanvases_4() { return &___preWillRenderCanvases_4; }
	inline void set_preWillRenderCanvases_4(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * value)
	{
		___preWillRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___preWillRenderCanvases_4), (void*)value);
	}

	inline static int32_t get_offset_of_willRenderCanvases_5() { return static_cast<int32_t>(offsetof(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields, ___willRenderCanvases_5)); }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * get_willRenderCanvases_5() const { return ___willRenderCanvases_5; }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 ** get_address_of_willRenderCanvases_5() { return &___willRenderCanvases_5; }
	inline void set_willRenderCanvases_5(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * value)
	{
		___willRenderCanvases_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_5), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// Coffee.UIExtensions.SoftMaskable
struct SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Coffee.UIExtensions.SoftMaskable::m_Inverse
	bool ___m_Inverse_6;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::m_MaskInteraction
	int32_t ___m_MaskInteraction_7;
	// System.Boolean Coffee.UIExtensions.SoftMaskable::m_UseStencil
	bool ___m_UseStencil_8;
	// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::_graphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ____graphic_9;
	// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMaskable::_softMask
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ____softMask_10;
	// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::_maskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____maskMaterial_11;

public:
	inline static int32_t get_offset_of_m_Inverse_6() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC, ___m_Inverse_6)); }
	inline bool get_m_Inverse_6() const { return ___m_Inverse_6; }
	inline bool* get_address_of_m_Inverse_6() { return &___m_Inverse_6; }
	inline void set_m_Inverse_6(bool value)
	{
		___m_Inverse_6 = value;
	}

	inline static int32_t get_offset_of_m_MaskInteraction_7() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC, ___m_MaskInteraction_7)); }
	inline int32_t get_m_MaskInteraction_7() const { return ___m_MaskInteraction_7; }
	inline int32_t* get_address_of_m_MaskInteraction_7() { return &___m_MaskInteraction_7; }
	inline void set_m_MaskInteraction_7(int32_t value)
	{
		___m_MaskInteraction_7 = value;
	}

	inline static int32_t get_offset_of_m_UseStencil_8() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC, ___m_UseStencil_8)); }
	inline bool get_m_UseStencil_8() const { return ___m_UseStencil_8; }
	inline bool* get_address_of_m_UseStencil_8() { return &___m_UseStencil_8; }
	inline void set_m_UseStencil_8(bool value)
	{
		___m_UseStencil_8 = value;
	}

	inline static int32_t get_offset_of__graphic_9() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC, ____graphic_9)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get__graphic_9() const { return ____graphic_9; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of__graphic_9() { return &____graphic_9; }
	inline void set__graphic_9(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		____graphic_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____graphic_9), (void*)value);
	}

	inline static int32_t get_offset_of__softMask_10() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC, ____softMask_10)); }
	inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * get__softMask_10() const { return ____softMask_10; }
	inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 ** get_address_of__softMask_10() { return &____softMask_10; }
	inline void set__softMask_10(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * value)
	{
		____softMask_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____softMask_10), (void*)value);
	}

	inline static int32_t get_offset_of__maskMaterial_11() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC, ____maskMaterial_11)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__maskMaterial_11() const { return ____maskMaterial_11; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__maskMaterial_11() { return &____maskMaterial_11; }
	inline void set__maskMaterial_11(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____maskMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____maskMaterial_11), (void*)value);
	}
};

struct SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields
{
public:
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_SoftMaskTexId
	int32_t ___s_SoftMaskTexId_12;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_StencilCompId
	int32_t ___s_StencilCompId_13;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_MaskInteractionId
	int32_t ___s_MaskInteractionId_14;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_GameVPId
	int32_t ___s_GameVPId_15;
	// System.Int32 Coffee.UIExtensions.SoftMaskable::s_GameTVPId
	int32_t ___s_GameTVPId_16;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable> Coffee.UIExtensions.SoftMaskable::s_ActiveSoftMaskables
	List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * ___s_ActiveSoftMaskables_17;
	// System.Int32[] Coffee.UIExtensions.SoftMaskable::s_Interactions
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___s_Interactions_18;
	// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::s_DefaultMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultMaterial_19;

public:
	inline static int32_t get_offset_of_s_SoftMaskTexId_12() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_SoftMaskTexId_12)); }
	inline int32_t get_s_SoftMaskTexId_12() const { return ___s_SoftMaskTexId_12; }
	inline int32_t* get_address_of_s_SoftMaskTexId_12() { return &___s_SoftMaskTexId_12; }
	inline void set_s_SoftMaskTexId_12(int32_t value)
	{
		___s_SoftMaskTexId_12 = value;
	}

	inline static int32_t get_offset_of_s_StencilCompId_13() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_StencilCompId_13)); }
	inline int32_t get_s_StencilCompId_13() const { return ___s_StencilCompId_13; }
	inline int32_t* get_address_of_s_StencilCompId_13() { return &___s_StencilCompId_13; }
	inline void set_s_StencilCompId_13(int32_t value)
	{
		___s_StencilCompId_13 = value;
	}

	inline static int32_t get_offset_of_s_MaskInteractionId_14() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_MaskInteractionId_14)); }
	inline int32_t get_s_MaskInteractionId_14() const { return ___s_MaskInteractionId_14; }
	inline int32_t* get_address_of_s_MaskInteractionId_14() { return &___s_MaskInteractionId_14; }
	inline void set_s_MaskInteractionId_14(int32_t value)
	{
		___s_MaskInteractionId_14 = value;
	}

	inline static int32_t get_offset_of_s_GameVPId_15() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_GameVPId_15)); }
	inline int32_t get_s_GameVPId_15() const { return ___s_GameVPId_15; }
	inline int32_t* get_address_of_s_GameVPId_15() { return &___s_GameVPId_15; }
	inline void set_s_GameVPId_15(int32_t value)
	{
		___s_GameVPId_15 = value;
	}

	inline static int32_t get_offset_of_s_GameTVPId_16() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_GameTVPId_16)); }
	inline int32_t get_s_GameTVPId_16() const { return ___s_GameTVPId_16; }
	inline int32_t* get_address_of_s_GameTVPId_16() { return &___s_GameTVPId_16; }
	inline void set_s_GameTVPId_16(int32_t value)
	{
		___s_GameTVPId_16 = value;
	}

	inline static int32_t get_offset_of_s_ActiveSoftMaskables_17() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_ActiveSoftMaskables_17)); }
	inline List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * get_s_ActiveSoftMaskables_17() const { return ___s_ActiveSoftMaskables_17; }
	inline List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 ** get_address_of_s_ActiveSoftMaskables_17() { return &___s_ActiveSoftMaskables_17; }
	inline void set_s_ActiveSoftMaskables_17(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * value)
	{
		___s_ActiveSoftMaskables_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActiveSoftMaskables_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_Interactions_18() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_Interactions_18)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_s_Interactions_18() const { return ___s_Interactions_18; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_s_Interactions_18() { return &___s_Interactions_18; }
	inline void set_s_Interactions_18(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___s_Interactions_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Interactions_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultMaterial_19() { return static_cast<int32_t>(offsetof(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields, ___s_DefaultMaterial_19)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultMaterial_19() const { return ___s_DefaultMaterial_19; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultMaterial_19() { return &___s_DefaultMaterial_19; }
	inline void set_s_DefaultMaterial_19(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultMaterial_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultMaterial_19), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Mask
struct Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Mask::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_4;
	// System.Boolean UnityEngine.UI.Mask::m_ShowMaskGraphic
	bool ___m_ShowMaskGraphic_5;
	// UnityEngine.UI.Graphic UnityEngine.UI.Mask::m_Graphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_Graphic_6;
	// UnityEngine.Material UnityEngine.UI.Mask::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_7;
	// UnityEngine.Material UnityEngine.UI.Mask::m_UnmaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_UnmaskMaterial_8;

public:
	inline static int32_t get_offset_of_m_RectTransform_4() { return static_cast<int32_t>(offsetof(Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D, ___m_RectTransform_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_4() const { return ___m_RectTransform_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_4() { return &___m_RectTransform_4; }
	inline void set_m_RectTransform_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShowMaskGraphic_5() { return static_cast<int32_t>(offsetof(Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D, ___m_ShowMaskGraphic_5)); }
	inline bool get_m_ShowMaskGraphic_5() const { return ___m_ShowMaskGraphic_5; }
	inline bool* get_address_of_m_ShowMaskGraphic_5() { return &___m_ShowMaskGraphic_5; }
	inline void set_m_ShowMaskGraphic_5(bool value)
	{
		___m_ShowMaskGraphic_5 = value;
	}

	inline static int32_t get_offset_of_m_Graphic_6() { return static_cast<int32_t>(offsetof(Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D, ___m_Graphic_6)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_Graphic_6() const { return ___m_Graphic_6; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_Graphic_6() { return &___m_Graphic_6; }
	inline void set_m_Graphic_6(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_Graphic_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Graphic_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_MaskMaterial_7() { return static_cast<int32_t>(offsetof(Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D, ___m_MaskMaterial_7)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_7() const { return ___m_MaskMaterial_7; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_7() { return &___m_MaskMaterial_7; }
	inline void set_m_MaskMaterial_7(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_UnmaskMaterial_8() { return static_cast<int32_t>(offsetof(Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D, ___m_UnmaskMaterial_8)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_UnmaskMaterial_8() const { return ___m_UnmaskMaterial_8; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_UnmaskMaterial_8() { return &___m_UnmaskMaterial_8; }
	inline void set_m_UnmaskMaterial_8(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_UnmaskMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UnmaskMaterial_8), (void*)value);
	}
};


// Coffee.UIExtensions.SoftMask
struct SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7  : public Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D
{
public:
	// Coffee.UIExtensions.SoftMask/DesamplingRate Coffee.UIExtensions.SoftMask::m_DesamplingRate
	int32_t ___m_DesamplingRate_12;
	// System.Single Coffee.UIExtensions.SoftMask::m_Softness
	float ___m_Softness_13;
	// System.Single Coffee.UIExtensions.SoftMask::m_Alpha
	float ___m_Alpha_14;
	// System.Boolean Coffee.UIExtensions.SoftMask::m_IgnoreParent
	bool ___m_IgnoreParent_15;
	// System.Boolean Coffee.UIExtensions.SoftMask::m_PartOfParent
	bool ___m_PartOfParent_16;
	// UnityEngine.MaterialPropertyBlock Coffee.UIExtensions.SoftMask::_mpb
	MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * ____mpb_26;
	// UnityEngine.Rendering.CommandBuffer Coffee.UIExtensions.SoftMask::_cb
	CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * ____cb_27;
	// UnityEngine.Material Coffee.UIExtensions.SoftMask::_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____material_28;
	// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::_softMaskBuffer
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ____softMaskBuffer_29;
	// System.Int32 Coffee.UIExtensions.SoftMask::_stencilDepth
	int32_t ____stencilDepth_30;
	// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::_mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ____mesh_31;
	// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::_parent
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ____parent_32;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::_children
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * ____children_33;
	// System.Boolean Coffee.UIExtensions.SoftMask::_hasChanged
	bool ____hasChanged_34;
	// System.Boolean Coffee.UIExtensions.SoftMask::_hasStencilStateChanged
	bool ____hasStencilStateChanged_35;

public:
	inline static int32_t get_offset_of_m_DesamplingRate_12() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ___m_DesamplingRate_12)); }
	inline int32_t get_m_DesamplingRate_12() const { return ___m_DesamplingRate_12; }
	inline int32_t* get_address_of_m_DesamplingRate_12() { return &___m_DesamplingRate_12; }
	inline void set_m_DesamplingRate_12(int32_t value)
	{
		___m_DesamplingRate_12 = value;
	}

	inline static int32_t get_offset_of_m_Softness_13() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ___m_Softness_13)); }
	inline float get_m_Softness_13() const { return ___m_Softness_13; }
	inline float* get_address_of_m_Softness_13() { return &___m_Softness_13; }
	inline void set_m_Softness_13(float value)
	{
		___m_Softness_13 = value;
	}

	inline static int32_t get_offset_of_m_Alpha_14() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ___m_Alpha_14)); }
	inline float get_m_Alpha_14() const { return ___m_Alpha_14; }
	inline float* get_address_of_m_Alpha_14() { return &___m_Alpha_14; }
	inline void set_m_Alpha_14(float value)
	{
		___m_Alpha_14 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreParent_15() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ___m_IgnoreParent_15)); }
	inline bool get_m_IgnoreParent_15() const { return ___m_IgnoreParent_15; }
	inline bool* get_address_of_m_IgnoreParent_15() { return &___m_IgnoreParent_15; }
	inline void set_m_IgnoreParent_15(bool value)
	{
		___m_IgnoreParent_15 = value;
	}

	inline static int32_t get_offset_of_m_PartOfParent_16() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ___m_PartOfParent_16)); }
	inline bool get_m_PartOfParent_16() const { return ___m_PartOfParent_16; }
	inline bool* get_address_of_m_PartOfParent_16() { return &___m_PartOfParent_16; }
	inline void set_m_PartOfParent_16(bool value)
	{
		___m_PartOfParent_16 = value;
	}

	inline static int32_t get_offset_of__mpb_26() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____mpb_26)); }
	inline MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * get__mpb_26() const { return ____mpb_26; }
	inline MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 ** get_address_of__mpb_26() { return &____mpb_26; }
	inline void set__mpb_26(MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * value)
	{
		____mpb_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mpb_26), (void*)value);
	}

	inline static int32_t get_offset_of__cb_27() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____cb_27)); }
	inline CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * get__cb_27() const { return ____cb_27; }
	inline CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 ** get_address_of__cb_27() { return &____cb_27; }
	inline void set__cb_27(CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * value)
	{
		____cb_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cb_27), (void*)value);
	}

	inline static int32_t get_offset_of__material_28() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____material_28)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__material_28() const { return ____material_28; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__material_28() { return &____material_28; }
	inline void set__material_28(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____material_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____material_28), (void*)value);
	}

	inline static int32_t get_offset_of__softMaskBuffer_29() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____softMaskBuffer_29)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get__softMaskBuffer_29() const { return ____softMaskBuffer_29; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of__softMaskBuffer_29() { return &____softMaskBuffer_29; }
	inline void set__softMaskBuffer_29(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		____softMaskBuffer_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____softMaskBuffer_29), (void*)value);
	}

	inline static int32_t get_offset_of__stencilDepth_30() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____stencilDepth_30)); }
	inline int32_t get__stencilDepth_30() const { return ____stencilDepth_30; }
	inline int32_t* get_address_of__stencilDepth_30() { return &____stencilDepth_30; }
	inline void set__stencilDepth_30(int32_t value)
	{
		____stencilDepth_30 = value;
	}

	inline static int32_t get_offset_of__mesh_31() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____mesh_31)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get__mesh_31() const { return ____mesh_31; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of__mesh_31() { return &____mesh_31; }
	inline void set__mesh_31(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		____mesh_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mesh_31), (void*)value);
	}

	inline static int32_t get_offset_of__parent_32() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____parent_32)); }
	inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * get__parent_32() const { return ____parent_32; }
	inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 ** get_address_of__parent_32() { return &____parent_32; }
	inline void set__parent_32(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * value)
	{
		____parent_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parent_32), (void*)value);
	}

	inline static int32_t get_offset_of__children_33() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____children_33)); }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * get__children_33() const { return ____children_33; }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C ** get_address_of__children_33() { return &____children_33; }
	inline void set__children_33(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * value)
	{
		____children_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____children_33), (void*)value);
	}

	inline static int32_t get_offset_of__hasChanged_34() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____hasChanged_34)); }
	inline bool get__hasChanged_34() const { return ____hasChanged_34; }
	inline bool* get_address_of__hasChanged_34() { return &____hasChanged_34; }
	inline void set__hasChanged_34(bool value)
	{
		____hasChanged_34 = value;
	}

	inline static int32_t get_offset_of__hasStencilStateChanged_35() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7, ____hasStencilStateChanged_35)); }
	inline bool get__hasStencilStateChanged_35() const { return ____hasStencilStateChanged_35; }
	inline bool* get_address_of__hasStencilStateChanged_35() { return &____hasStencilStateChanged_35; }
	inline void set__hasStencilStateChanged_35(bool value)
	{
		____hasStencilStateChanged_35 = value;
	}
};

struct SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields
{
public:
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[] Coffee.UIExtensions.SoftMask::s_TmpSoftMasks
	List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* ___s_TmpSoftMasks_9;
	// UnityEngine.Color[] Coffee.UIExtensions.SoftMask::s_ClearColors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___s_ClearColors_10;
	// System.Boolean Coffee.UIExtensions.SoftMask::s_UVStartsAtTop
	bool ___s_UVStartsAtTop_11;
	// UnityEngine.Shader Coffee.UIExtensions.SoftMask::s_SoftMaskShader
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ___s_SoftMaskShader_17;
	// UnityEngine.Texture2D Coffee.UIExtensions.SoftMask::s_ReadTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_ReadTexture_18;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::s_ActiveSoftMasks
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * ___s_ActiveSoftMasks_19;
	// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask> Coffee.UIExtensions.SoftMask::s_TempRelatables
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * ___s_TempRelatables_20;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_StencilCompId
	int32_t ___s_StencilCompId_21;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_ColorMaskId
	int32_t ___s_ColorMaskId_22;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_MainTexId
	int32_t ___s_MainTexId_23;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_SoftnessId
	int32_t ___s_SoftnessId_24;
	// System.Int32 Coffee.UIExtensions.SoftMask::s_Alpha
	int32_t ___s_Alpha_25;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4> Coffee.UIExtensions.SoftMask::s_previousViewProjectionMatrices
	Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * ___s_previousViewProjectionMatrices_36;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4> Coffee.UIExtensions.SoftMask::s_nowViewProjectionMatrices
	Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * ___s_nowViewProjectionMatrices_37;

public:
	inline static int32_t get_offset_of_s_TmpSoftMasks_9() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_TmpSoftMasks_9)); }
	inline List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* get_s_TmpSoftMasks_9() const { return ___s_TmpSoftMasks_9; }
	inline List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106** get_address_of_s_TmpSoftMasks_9() { return &___s_TmpSoftMasks_9; }
	inline void set_s_TmpSoftMasks_9(List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* value)
	{
		___s_TmpSoftMasks_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_TmpSoftMasks_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_ClearColors_10() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_ClearColors_10)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_s_ClearColors_10() const { return ___s_ClearColors_10; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_s_ClearColors_10() { return &___s_ClearColors_10; }
	inline void set_s_ClearColors_10(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___s_ClearColors_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ClearColors_10), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVStartsAtTop_11() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_UVStartsAtTop_11)); }
	inline bool get_s_UVStartsAtTop_11() const { return ___s_UVStartsAtTop_11; }
	inline bool* get_address_of_s_UVStartsAtTop_11() { return &___s_UVStartsAtTop_11; }
	inline void set_s_UVStartsAtTop_11(bool value)
	{
		___s_UVStartsAtTop_11 = value;
	}

	inline static int32_t get_offset_of_s_SoftMaskShader_17() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_SoftMaskShader_17)); }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * get_s_SoftMaskShader_17() const { return ___s_SoftMaskShader_17; }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 ** get_address_of_s_SoftMaskShader_17() { return &___s_SoftMaskShader_17; }
	inline void set_s_SoftMaskShader_17(Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * value)
	{
		___s_SoftMaskShader_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SoftMaskShader_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_ReadTexture_18() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_ReadTexture_18)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_ReadTexture_18() const { return ___s_ReadTexture_18; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_ReadTexture_18() { return &___s_ReadTexture_18; }
	inline void set_s_ReadTexture_18(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_ReadTexture_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ReadTexture_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_ActiveSoftMasks_19() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_ActiveSoftMasks_19)); }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * get_s_ActiveSoftMasks_19() const { return ___s_ActiveSoftMasks_19; }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C ** get_address_of_s_ActiveSoftMasks_19() { return &___s_ActiveSoftMasks_19; }
	inline void set_s_ActiveSoftMasks_19(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * value)
	{
		___s_ActiveSoftMasks_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActiveSoftMasks_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_TempRelatables_20() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_TempRelatables_20)); }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * get_s_TempRelatables_20() const { return ___s_TempRelatables_20; }
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C ** get_address_of_s_TempRelatables_20() { return &___s_TempRelatables_20; }
	inline void set_s_TempRelatables_20(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * value)
	{
		___s_TempRelatables_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_TempRelatables_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_StencilCompId_21() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_StencilCompId_21)); }
	inline int32_t get_s_StencilCompId_21() const { return ___s_StencilCompId_21; }
	inline int32_t* get_address_of_s_StencilCompId_21() { return &___s_StencilCompId_21; }
	inline void set_s_StencilCompId_21(int32_t value)
	{
		___s_StencilCompId_21 = value;
	}

	inline static int32_t get_offset_of_s_ColorMaskId_22() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_ColorMaskId_22)); }
	inline int32_t get_s_ColorMaskId_22() const { return ___s_ColorMaskId_22; }
	inline int32_t* get_address_of_s_ColorMaskId_22() { return &___s_ColorMaskId_22; }
	inline void set_s_ColorMaskId_22(int32_t value)
	{
		___s_ColorMaskId_22 = value;
	}

	inline static int32_t get_offset_of_s_MainTexId_23() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_MainTexId_23)); }
	inline int32_t get_s_MainTexId_23() const { return ___s_MainTexId_23; }
	inline int32_t* get_address_of_s_MainTexId_23() { return &___s_MainTexId_23; }
	inline void set_s_MainTexId_23(int32_t value)
	{
		___s_MainTexId_23 = value;
	}

	inline static int32_t get_offset_of_s_SoftnessId_24() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_SoftnessId_24)); }
	inline int32_t get_s_SoftnessId_24() const { return ___s_SoftnessId_24; }
	inline int32_t* get_address_of_s_SoftnessId_24() { return &___s_SoftnessId_24; }
	inline void set_s_SoftnessId_24(int32_t value)
	{
		___s_SoftnessId_24 = value;
	}

	inline static int32_t get_offset_of_s_Alpha_25() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_Alpha_25)); }
	inline int32_t get_s_Alpha_25() const { return ___s_Alpha_25; }
	inline int32_t* get_address_of_s_Alpha_25() { return &___s_Alpha_25; }
	inline void set_s_Alpha_25(int32_t value)
	{
		___s_Alpha_25 = value;
	}

	inline static int32_t get_offset_of_s_previousViewProjectionMatrices_36() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_previousViewProjectionMatrices_36)); }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * get_s_previousViewProjectionMatrices_36() const { return ___s_previousViewProjectionMatrices_36; }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D ** get_address_of_s_previousViewProjectionMatrices_36() { return &___s_previousViewProjectionMatrices_36; }
	inline void set_s_previousViewProjectionMatrices_36(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * value)
	{
		___s_previousViewProjectionMatrices_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_previousViewProjectionMatrices_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_nowViewProjectionMatrices_37() { return static_cast<int32_t>(offsetof(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields, ___s_nowViewProjectionMatrices_37)); }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * get_s_nowViewProjectionMatrices_37() const { return ___s_nowViewProjectionMatrices_37; }
	inline Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D ** get_address_of_s_nowViewProjectionMatrices_37() { return &___s_nowViewProjectionMatrices_37; }
	inline void set_s_nowViewProjectionMatrices_37(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * value)
	{
		___s_nowViewProjectionMatrices_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_nowViewProjectionMatrices_37), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>[]
struct List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * m_Items[1];

public:
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  m_Items[1];

public:
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean,System.Collections.Generic.List`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component_GetComponentsInChildren_TisRuntimeObject_mCFD9EE28706E43CCE81CF186BF0E5EED482B56D9_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, bool ___includeInactive0, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___result1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m39B6A35CFE684CD1FFF77873E20D7297B36A55E8_gshared (String_t* ___path0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396_gshared (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, int32_t ___key0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF_gshared (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, int32_t ___key0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1_gshared (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::get_Keys()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6_gshared (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,UnityEngine.Matrix4x4>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9  KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379_gshared (KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_gshared_inline (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E_gshared (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8_gshared (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27_gshared (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared (Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m3CA48D06F70CC498ADA7410EB930969256F43675_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___match0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A_gshared (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, const RuntimeMethod* method);

// System.Void Coffee.UIExtensions.SoftMask::set_hasChanged(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, bool ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::ReleaseRT(UnityEngine.RenderTexture&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** ___tmpRT0, const RuntimeMethod* method);
// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::get_softMaskBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::GetDesamplingSize(Coffee.UIExtensions.SoftMask/DesamplingRate,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_GetDesamplingSize_mADB1BE4385FF8E47F9E393C1CFB18DDF3A1240CE (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, int32_t ___rate0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * RenderTexture_GetTemporary_mFDF23E91A85221C7EF61B0A5D46AAC858816E5F1 (int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, const RuntimeMethod* method);
// System.Boolean Coffee.UIExtensions.SoftMask::get_hasChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_get_hasChanged_m60D0CBDA2A52E323C5FA17433EDAA7A14A86E361 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Mask_GetModifiedMaterial_mB0819C9D0018D8DC89510BBD5BD8DB35EF50837D (Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::get_mesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * SoftMask_get_mesh_mD8B874B7C69EA61B11F1B1CC3ED78EA11C5D75A1 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::FillMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_FillMesh_m69ADAB814A243F7F5578BC07086F373B85A34269 (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, const RuntimeMethod* method);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4 (Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE (const RuntimeMethod* method);
// System.Single Coffee.UIExtensions.SoftMask::GetPixelValue(System.Int32,System.Int32,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftMask_GetPixelValue_m27E4D0600AE29C45EA0D02B77BE8BC00AAA3C56D (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, int32_t ___x0, int32_t ___y1, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___interactions2, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::get_Count()
inline int32_t List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Boolean UnityEngine.SystemInfo::get_graphicsUVStartsAtTop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SystemInfo_get_graphicsUVStartsAtTop_m8A76908C20DE5B8BEE70D7E21C16EE8CC7927501 (const RuntimeMethod* method);
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WillRenderCanvases__ctor_m8A46E9A5DED6B54DC2A8A3137AE3637081EADFB6 (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Canvas_add_willRenderCanvases_m00E391FCCE9839EEB6D7A729DCBF6B841FDF02B7 (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F (String_t* ___name0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Add(!0)
inline void List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419 (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.Component::GetComponentsInChildren<Coffee.UIExtensions.SoftMask>(System.Boolean,System.Collections.Generic.List`1<!!0>)
inline void Component_GetComponentsInChildren_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mDB51DAE14977345C6094B986C51FF7DCC3A5DB66 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, bool ___includeInactive0, List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * ___result1, const RuntimeMethod* method)
{
	((  void (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, bool, List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_mCFD9EE28706E43CCE81CF186BF0E5EED482B56D9_gshared)(__this, ___includeInactive0, ___result1, method);
}
// !0 System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::get_Item(System.Int32)
inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_inline (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Clear()
inline void List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock__ctor_m8EB29E415C68427B841A0C68A902A8368B9228E8 (MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer__ctor_mB7E1174EB3B4E2E53BCC6532840AB55ECE6D06CF (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Mask::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mask_OnEnable_mD321373E240B3013212217903A2902D12C904A21 (Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Remove(!0)
inline bool List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Canvas_remove_willRenderCanvases_m4A631D84D6DBB6035620ED9496542E43F19D0EF9 (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___value0, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::SetParent(Coffee.UIExtensions.SoftMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___newParent0, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_Clear_mAB598168AB3F657597804EE57D62E67443794813 (MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_Release_m34AA2C11825D26FBD327987EA995E5235D522DAF (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask::ReleaseObject(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_ReleaseObject_mB296E9939AB74191D556253DACA5E21CA29FD54D (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Mask::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mask_OnDisable_m24823C343812F3A0CE28B2B0347139B61B07B289 (Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Coffee.UIExtensions.SoftMask>()
inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.Shader>(System.String)
inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001 (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m39B6A35CFE684CD1FFF77873E20D7297B36A55E8_gshared)(___path0, method);
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ___shader0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::GetEnumerator()
inline Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2  List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2  (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>::get_Current()
inline SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_inline (Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 * __this, const RuntimeMethod* method)
{
	return ((  SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * (*) (Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// UnityEngine.Canvas UnityEngine.UI.Graphic::get_canvas()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * Graphic_get_canvas_mDB17EC66AF3FD40E8D368FC11C8F07319BB9D1B0 (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * __this, const RuntimeMethod* method);
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Canvas_get_renderMode_mAEC8A341577CC74EC89D5890E6D6E4A82B03574D (Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Canvas_get_worldCamera_mFE4C9FDA7996FE20AC5CA3CB45B4190C40122D51 (Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Camera_get_worldToCameraMatrix_m7E2B63F64437E2C91C07F7FC819C79BE2152C5F6 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_op_Multiply_mC2B30D333D4399C1693414F1A73D87FB3450F39F (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___lhs0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___rhs1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Object::GetInstanceID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396 (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, int32_t ___key0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *, int32_t, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *, const RuntimeMethod*))Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, int32_t ___key0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *, int32_t, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 , const RuntimeMethod*))Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean UnityEngine.Matrix4x4::op_Inequality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Matrix4x4_op_Inequality_m6E62E1FDEDC9AF783BE1041726A18BCA5E6CFC6D (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___lhs0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___rhs1, const RuntimeMethod* method);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * Mask_get_rectTransform_mF1081D590012320A4D694F33A7965B7A8DFE4A28 (Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Transform::get_hasChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Transform_get_hasChanged_m59490E3CAC42DF8CB2BCDFC0ED75DB6F89432F06 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_hasChanged_mD1CDCAE366DB514FBECD9DAAED0F7834029E1304 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>::MoveNext()
inline bool Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41 (Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Coffee.UIExtensions.SoftMask>::Dispose()
inline void Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED (Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_UpdateMaskTexture_m1A0B74909D06DDF4E98CCDE6645CE7BC7430F2E0 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.MaskUtilities::NotifyStencilStateChanged(UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaskUtilities_NotifyStencilStateChanged_m9ABCE3B9E8E67931DED17B1F856C4A71600E28C6 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * ___mask0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::Clear()
inline void Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1 (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *, const RuntimeMethod*))Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::get_Keys()
inline KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6 (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, const RuntimeMethod* method)
{
	return ((  KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * (*) (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *, const RuntimeMethod*))Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,UnityEngine.Matrix4x4>::GetEnumerator()
inline Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9  KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379 (KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9  (*) (KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 *, const RuntimeMethod*))KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379_gshared)(__this, method);
}
// !0 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>::get_Current()
inline int32_t Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_inline (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 *, const RuntimeMethod*))Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_gshared_inline)(__this, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::get_Item(!0)
inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  (*) (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>::MoveNext()
inline bool Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8 (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 *, const RuntimeMethod*))Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.Matrix4x4>::Dispose()
inline void Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27 (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 *, const RuntimeMethod*))Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.UI.MaskUtilities::FindRootSortOverrideCanvas(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * MaskUtilities_FindRootSortOverrideCanvas_m4760B83EDA0BF632346FDE90302AB43EAC0524E0 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___start0, const RuntimeMethod* method);
// System.Int32 UnityEngine.UI.MaskUtilities::GetStencilDepth(UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MaskUtilities_GetStencilDepth_m102B187BAF39A903EA0BEC302B9DFC2993BC1E30 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___stopAfter1, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_Clear_mDE798ACE9294B43B9387A6B06E43B3D4A30A7092 (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, const RuntimeMethod* method);
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13  RenderTargetIdentifier_op_Implicit_mFF9B98B136B3AB4E9413777F931392D02AA7A8FA (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___tex0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget(UnityEngine.Rendering.RenderTargetIdentifier)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_SetRenderTarget_mCDFEB57580AF2EC11E02BA87EFB6CF18AD3AC840 (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13  ___rt0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::ClearRenderTarget(System.Boolean,System.Boolean,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_ClearRenderTarget_mD911CD9DACD86BB1A9734235E6D55AF9BE844CA0 (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, bool ___clearDepth0, bool ___clearColor1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___backgroundColor2, const RuntimeMethod* method);
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * Canvas_get_rootCanvas_mB1C93410A4AA793D88130FD08C05D71327641DC5 (Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.GL::GetGPUProjectionMatrix(UnityEngine.Matrix4x4,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  GL_GetGPUProjectionMatrix_m8DAC433EACD75ECD86B3148EA7658F6604378457 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___proj0, bool ___renderIntoTexture1, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::SetViewProjectionMatrices(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_SetViewProjectionMatrices_m3530A45C10FAD54367B483A1A1B03641ACF05187 (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___view0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___proj1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___q1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s2, const RuntimeMethod* method);
// UnityEngine.Material Coffee.UIExtensions.SoftMask::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * SoftMask_get_material_mC2B5EECA152A1D35860B2C07A38A8839E0DFAB16 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::SetTexture(System.Int32,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetTexture_m8F81CA94E5261618BEDD93EC02DAA2412E732C04 (MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * __this, int32_t ___nameID0, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::SetFloat(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetFloat_mD2F1653CEDB22B1A3EE844E353CF125A607AEDEC (MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * __this, int32_t ___nameID0, float ___value1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_DrawMesh_m8AE72FB8773642F0293697038A3262E84E269AB0 (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___matrix1, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material2, int32_t ___submeshIndex3, int32_t ___shaderPass4, MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * ___properties5, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_ExecuteCommandBuffer_mCE45F0B19843D5B2803B456A5F037857D20FCC51 (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * ___buffer0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::ClosestPowerOfTwo(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_ClosestPowerOfTwo_mF46C05D09E914DE9DC7C42EEE7FA69FC087D2832 (int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_CeilToInt_m3A3E7C0F6A3CF731411BB90F264F989D8311CC6F (float ___f0, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_Release_m533506E903688E798921C0D35F1B082522D88986 (RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_m2BF2BDDC359A491C05C401B977878DAE1D0850D4 (RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___temp0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::Contains(!0)
inline bool List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31 (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *, const RuntimeMethod*))List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared)(__this, ___item0, method);
}
// System.Void System.Predicate`1<Coffee.UIExtensions.SoftMask>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m39AAD12C99FF47DF1421C18605E877CD25AFFDA5 (Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared)(__this, ___object0, ___method1, method);
}
// System.Int32 System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_mB4F9BDD13B74F228229F3EF77915CA1880585C36 (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * ___match0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 *, const RuntimeMethod*))List_1_RemoveAll_m3CA48D06F70CC498ADA7410EB930969256F43675_gshared)(__this, ___match0, method);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * RenderTexture_get_active_mB73718A56673D36F74B5338B310ED7FDFEB34AB7 (const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_set_active_mA70AFD6D3CB54E9AEDDD45E48B8B6979FDB75ED9 (RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m4C6FE8C2798C39C20A14DAFC963C720D17F4F987 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___source0, int32_t ___destX1, int32_t ___destY2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method);
// System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Texture2D_GetRawTextureData_m60C0B5EF034F31FE1824B31AC1DE71042E2ACB55 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMask>::.ctor()
inline void List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7 (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Mask::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mask__ctor_mC3A6C226EDD04AC7058424F80D413DB9A85570AE (Mask_t8DE5E31E7C928D3B32AA60E36E49B4DCFED4417D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>::.ctor()
inline void Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *, const RuntimeMethod*))Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A_gshared)(__this, method);
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___source0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, int32_t ___nameID0, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2 (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetVector_m47F7F5B5B21FA28885C4E747AF1C32F40C1022CB (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, int32_t ___nameID0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StencilMaterial_Remove_m8C971D3D0DDDD92710C011FD7B630E6C853B744D (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___customMat0, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMaskable::ReleaseMaterial(UnityEngine.Material&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_ReleaseMaterial_mE62C3AF98B25EBD4244B4508975E39D26D8F5238 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE ** ___mat0, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * Texture2D_get_whiteTexture_m4ED96995BA1D42F7D2823BD9D18023CFE3C680A0 (const RuntimeMethod* method);
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RectTransformUtility_RectangleContainsScreenPoint_m7D92A04D6DA6F4C7CC72439221C2EE46034A0595 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rect0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPoint1, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cam2, const RuntimeMethod* method);
// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::get_parent()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * SoftMask_get_parent_mB1469E842BC5ADF3FE5DAAFA819D3FA77FCCDBF1_inline (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method);
// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::get_graphic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method);
// System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.UI.Graphic,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_IsRaycastLocationValid_m419A604EFD1A8EA76EC040ECBC4337011E84BAC1 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___sp0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___eventCamera1, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___g2, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___interactions3, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_SetMaskInteraction_m9C75BABE68C3E28C63F5016F9CBF247961642C3C (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, int32_t ___layer00, int32_t ___layer11, int32_t ___layer22, int32_t ___layer33, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>::.ctor()
inline void List_1__ctor_m8C6DA11294CCBAB33C07414BC54634526B1C2D0A (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>::Add(!0)
inline void List_1_Add_m2D42408638C489F4076E2D9AEACB059EBAF843BB (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * __this, SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 *, SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// UnityEngine.Material UnityEngine.UI.Graphic::get_defaultGraphicMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Graphic_get_defaultGraphicMaterial_mAED427ABBA1C93E2AB3794D4FE43F4B8F6D93198 (const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Coffee.UIExtensions.SoftMaskable>::Remove(!0)
inline bool List_1_Remove_m29CA3BE16DCB73AAAD0345E10DB6953D4D8626D2 (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * __this, SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 *, SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void Coffee.UIExtensions.SoftMask/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4E1870D305C6424F6F7E64F02312D7DD3F993999 (U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Coffee.UIExtensions.SoftMask/DesamplingRate Coffee.UIExtensions.SoftMask::get_desamplingRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SoftMask_get_desamplingRate_m4B57A259903EB7733B3810540C44DC7C9E84375F (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_DesamplingRate; }
		int32_t L_0 = __this->get_m_DesamplingRate_12();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_desamplingRate(Coffee.UIExtensions.SoftMask/DesamplingRate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_desamplingRate_m6CB26BBA2CF285381C19FBD96B08160A9A3E46C9 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_DesamplingRate != value)
		int32_t L_0 = __this->get_m_DesamplingRate_12();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		// m_DesamplingRate = value;
		int32_t L_2 = ___value0;
		__this->set_m_DesamplingRate_12(L_2);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Single Coffee.UIExtensions.SoftMask::get_softness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftMask_get_softness_m210D23C6F4AB64B167A4680EBA5CE19849F29ADB (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Softness; }
		float L_0 = __this->get_m_Softness_13();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_softness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_softness_m9793094421FD86BF17E5E1499B8A7A3B755C646C (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp01(value);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___value0 = L_1;
		// if (m_Softness != value)
		float L_2 = __this->get_m_Softness_13();
		float L_3 = ___value0;
		if ((((float)L_2) == ((float)L_3)))
		{
			goto IL_001f;
		}
	}
	{
		// m_Softness = value;
		float L_4 = ___value0;
		__this->set_m_Softness_13(L_4);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Single Coffee.UIExtensions.SoftMask::get_alpha()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftMask_get_alpha_mD28DBCC679B8D1243E0781284634F4B72C631B31 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Alpha; }
		float L_0 = __this->get_m_Alpha_14();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_alpha(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_alpha_mAEC5FB0D063EC06A775A1BC47D88FEE1B1930CCF (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp01(value);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___value0 = L_1;
		// if (m_Alpha != value)
		float L_2 = __this->get_m_Alpha_14();
		float L_3 = ___value0;
		if ((((float)L_2) == ((float)L_3)))
		{
			goto IL_001f;
		}
	}
	{
		// m_Alpha = value;
		float L_4 = ___value0;
		__this->set_m_Alpha_14(L_4);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::get_ignoreParent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_get_ignoreParent_mAC48EF132DD659F30CE9671C8814A67ADB6415FA (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_IgnoreParent; }
		bool L_0 = __this->get_m_IgnoreParent_15();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_ignoreParent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_ignoreParent_m914FE8700055B90B68A490C6DF8D251371C089A5 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_IgnoreParent != value)
		bool L_0 = __this->get_m_IgnoreParent_15();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		// m_IgnoreParent = value;
		bool L_2 = ___value0;
		__this->set_m_IgnoreParent_15(L_2);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// OnTransformParentChanged();
		VirtActionInvoker0::Invoke(12 /* System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged() */, __this);
	}

IL_001d:
	{
		// }
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::get_partOfParent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_get_partOfParent_mE7ACEBF409D6FAF5E378E4DD76E5DF3659283868 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_PartOfParent; }
		bool L_0 = __this->get_m_PartOfParent_16();
		return L_0;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_partOfParent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_partOfParent_m1C1ECB57B9E5D98129CF2F2FB011EE9002976998 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_PartOfParent != value)
		bool L_0 = __this->get_m_PartOfParent_16();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		// m_PartOfParent = value;
		bool L_2 = ___value0;
		__this->set_m_PartOfParent_16(L_2);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// OnTransformParentChanged();
		VirtActionInvoker0::Invoke(12 /* System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged() */, __this);
	}

IL_001d:
	{
		// }
		return;
	}
}
// UnityEngine.RenderTexture Coffee.UIExtensions.SoftMask::get_softMaskBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (_parent)
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = __this->get__parent_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		// ReleaseRT(ref _softMaskBuffer);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_2 = __this->get_address_of__softMaskBuffer_29();
		SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE(__this, (RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 **)L_2, /*hidden argument*/NULL);
		// return _parent.softMaskBuffer;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_3 = __this->get__parent_32();
		NullCheck(L_3);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_4;
		L_4 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0025:
	{
		// GetDesamplingSize(m_DesamplingRate, out w, out h);
		int32_t L_5 = __this->get_m_DesamplingRate_12();
		SoftMask_GetDesamplingSize_mADB1BE4385FF8E47F9E393C1CFB18DDF3A1240CE(__this, L_5, (int32_t*)(&V_0), (int32_t*)(&V_1), /*hidden argument*/NULL);
		// if (_softMaskBuffer && (_softMaskBuffer.width != w || _softMaskBuffer.height != h))
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_6 = __this->get__softMaskBuffer_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_8 = __this->get__softMaskBuffer_29();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_005e;
		}
	}
	{
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_11 = __this->get__softMaskBuffer_29();
		NullCheck(L_11);
		int32_t L_12;
		L_12 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_11);
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) == ((int32_t)L_13)))
		{
			goto IL_006a;
		}
	}

IL_005e:
	{
		// ReleaseRT(ref _softMaskBuffer);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_14 = __this->get_address_of__softMaskBuffer_29();
		SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE(__this, (RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 **)L_14, /*hidden argument*/NULL);
	}

IL_006a:
	{
		// if (!_softMaskBuffer)
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_15 = __this->get__softMaskBuffer_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0095;
		}
	}
	{
		// _softMaskBuffer = RenderTexture.GetTemporary(w, h, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
		int32_t L_17 = V_0;
		int32_t L_18 = V_1;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_19;
		L_19 = RenderTexture_GetTemporary_mFDF23E91A85221C7EF61B0A5D46AAC858816E5F1(L_17, L_18, 0, 0, 0, /*hidden argument*/NULL);
		__this->set__softMaskBuffer_29(L_19);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// _hasStencilStateChanged = true;
		__this->set__hasStencilStateChanged_35((bool)1);
	}

IL_0095:
	{
		// return _softMaskBuffer;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_20 = __this->get__softMaskBuffer_29();
		return L_20;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::get_hasChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_get_hasChanged_m60D0CBDA2A52E323C5FA17433EDAA7A14A86E361 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return _parent ? _parent.hasChanged : _hasChanged;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = __this->get__parent_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		bool L_2 = __this->get__hasChanged_34();
		return L_2;
	}

IL_0014:
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_3 = __this->get__parent_32();
		NullCheck(L_3);
		bool L_4;
		L_4 = SoftMask_get_hasChanged_m60D0CBDA2A52E323C5FA17433EDAA7A14A86E361(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::set_hasChanged(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_parent)
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = __this->get__parent_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// _parent.hasChanged = value;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_2 = __this->get__parent_32();
		bool L_3 = ___value0;
		NullCheck(L_2);
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// _hasChanged = value;
		bool L_4 = ___value0;
		__this->set__hasChanged_34(L_4);
		// }
		return;
	}
}
// Coffee.UIExtensions.SoftMask Coffee.UIExtensions.SoftMask::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * SoftMask_get_parent_mB1469E842BC5ADF3FE5DAAFA819D3FA77FCCDBF1 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// return _parent;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = __this->get__parent_32();
		return L_0;
	}
}
// UnityEngine.Material Coffee.UIExtensions.SoftMask::GetModifiedMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * SoftMask_GetModifiedMaterial_mAFC7B1C862B7DA813DB072B249D830A4F8572738 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * V_0 = NULL;
	{
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// var result = base.GetModifiedMaterial(baseMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___baseMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1;
		L_1 = Mask_GetModifiedMaterial_mB0819C9D0018D8DC89510BBD5BD8DB35EF50837D(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (m_IgnoreParent && result != baseMaterial)
		bool L_2 = __this->get_m_IgnoreParent_15();
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = V_0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_4 = ___baseMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		// result.SetInt(s_StencilCompId, (int)CompareFunction.Always);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		int32_t L_7 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_StencilCompId_21();
		NullCheck(L_6);
		Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C(L_6, L_7, 8, /*hidden argument*/NULL);
	}

IL_002c:
	{
		// return result;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = V_0;
		return L_8;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UnityEngine.UI.IMeshModifier.ModifyMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_mCF2644320EF30231E21BD4D4932EFB20E0DAB553 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, const RuntimeMethod* method)
{
	{
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// _mesh = mesh;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = ___mesh0;
		__this->set__mesh_31(L_0);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UnityEngine.UI.IMeshModifier.ModifyMesh(UnityEngine.UI.VertexHelper)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_UnityEngine_UI_IMeshModifier_ModifyMesh_m379C23D0EE5F6DEE8DD02342443AF0F0F7D21CE6 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___verts0, const RuntimeMethod* method)
{
	{
		// if (isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// verts.FillMesh(mesh);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_1 = ___verts0;
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2;
		L_2 = SoftMask_get_mesh_mD8B874B7C69EA61B11F1B1CC3ED78EA11C5D75A1(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VertexHelper_FillMesh_m69ADAB814A243F7F5578BC07086F373B85A34269(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.UI.Graphic,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_IsRaycastLocationValid_m419A604EFD1A8EA76EC040ECBC4337011E84BAC1 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___sp0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___eventCamera1, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___g2, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___interactions3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B7_0 = 0;
	{
		// if (!isActiveAndEnabled || (g == graphic && !g.raycastTarget))
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1 = ___g2;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2;
		L_2 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0020;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_4 = ___g2;
		NullCheck(L_4);
		bool L_5;
		L_5 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean UnityEngine.UI.Graphic::get_raycastTarget() */, L_4);
		if (L_5)
		{
			goto IL_0020;
		}
	}

IL_001e:
	{
		// return true;
		return (bool)1;
	}

IL_0020:
	{
		// int x = (int)((softMaskBuffer.width - 1) * Mathf.Clamp01(sp.x / Screen.width));
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_6;
		L_6 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7;
		L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = ___sp0;
		float L_9 = L_8.get_x_0();
		int32_t L_10;
		L_10 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		float L_11;
		L_11 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)L_9/(float)((float)((float)L_10)))), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1)))), (float)L_11))));
		// int y = s_UVStartsAtTop
		//     ? (int)((softMaskBuffer.height - 1) * Mathf.Clamp01(sp.y / Screen.height))
		//     : (int)((softMaskBuffer.height - 1) * (1 - Mathf.Clamp01(sp.y / Screen.height)));
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		bool L_12 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_UVStartsAtTop_11();
		if (L_12)
		{
			goto IL_0074;
		}
	}
	{
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_13;
		L_13 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14;
		L_14 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_13);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_15 = ___sp0;
		float L_16 = L_15.get_y_1();
		int32_t L_17;
		L_17 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		float L_18;
		L_18 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)L_16/(float)((float)((float)L_17)))), /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1)))), (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_18))))));
		goto IL_0096;
	}

IL_0074:
	{
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_19;
		L_19 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20;
		L_20 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_19);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21 = ___sp0;
		float L_22 = L_21.get_y_1();
		int32_t L_23;
		L_23 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		float L_24;
		L_24 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)L_22/(float)((float)((float)L_23)))), /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1)))), (float)L_24))));
	}

IL_0096:
	{
		V_1 = G_B7_0;
		// return 0.5f < GetPixelValue(x, y, interactions);
		int32_t L_25 = V_0;
		int32_t L_26 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_27 = ___interactions3;
		float L_28;
		L_28 = SoftMask_GetPixelValue_m27E4D0600AE29C45EA0D02B77BE8BC00AAA3C56D(__this, L_25, L_26, L_27, /*hidden argument*/NULL);
		return (bool)((((float)(0.5f)) < ((float)L_28))? 1 : 0);
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMask_IsRaycastLocationValid_m0F669181B54F89BB67875E8A8F7687468E57FC38 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___sp0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___eventCamera1, const RuntimeMethod* method)
{
	{
		// return true;
		return (bool)1;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_OnEnable_m853EE9B681CA115D833B9586A4104AE65201CD11 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mDB51DAE14977345C6094B986C51FF7DCC3A5DB66_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0F52C788AC4796FE5841155F7DF3896E049C051E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9AD9AC2A179FDCC5DF15DA875A0DAF5F51C43BCA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB14659CB03507C756FB26DFDC1D82D6AE87A527);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0A6E6DC25E45868734BB4AF5E23E886068187CE);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// if (s_ActiveSoftMasks.Count == 0)
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_0 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_19();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_0, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0080;
		}
	}
	{
		// s_UVStartsAtTop = SystemInfo.graphicsUVStartsAtTop;
		bool L_2;
		L_2 = SystemInfo_get_graphicsUVStartsAtTop_m8A76908C20DE5B8BEE70D7E21C16EE8CC7927501(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_UVStartsAtTop_11(L_2);
		// Canvas.willRenderCanvases += UpdateMaskTextures;
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_3 = (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 *)il2cpp_codegen_object_new(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		WillRenderCanvases__ctor_m8A46E9A5DED6B54DC2A8A3137AE3637081EADFB6(L_3, NULL, (intptr_t)((intptr_t)SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF_RuntimeMethod_var), /*hidden argument*/NULL);
		Canvas_add_willRenderCanvases_m00E391FCCE9839EEB6D7A729DCBF6B841FDF02B7(L_3, /*hidden argument*/NULL);
		// if (s_StencilCompId == 0)
		int32_t L_4 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_StencilCompId_21();
		if (L_4)
		{
			goto IL_0080;
		}
	}
	{
		// s_StencilCompId = Shader.PropertyToID("_StencilComp");
		int32_t L_5;
		L_5 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral0F52C788AC4796FE5841155F7DF3896E049C051E, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_StencilCompId_21(L_5);
		// s_ColorMaskId = Shader.PropertyToID("_ColorMask");
		int32_t L_6;
		L_6 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteralD0A6E6DC25E45868734BB4AF5E23E886068187CE, /*hidden argument*/NULL);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_ColorMaskId_22(L_6);
		// s_MainTexId = Shader.PropertyToID("_MainTex");
		int32_t L_7;
		L_7 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral4B8146FB95E4F51B29DA41EB5F6D60F8FD0ECF21, /*hidden argument*/NULL);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_MainTexId_23(L_7);
		// s_SoftnessId = Shader.PropertyToID("_Softness");
		int32_t L_8;
		L_8 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral9AD9AC2A179FDCC5DF15DA875A0DAF5F51C43BCA, /*hidden argument*/NULL);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_SoftnessId_24(L_8);
		// s_Alpha = Shader.PropertyToID("_Alpha");
		int32_t L_9;
		L_9 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteralCB14659CB03507C756FB26DFDC1D82D6AE87A527, /*hidden argument*/NULL);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_Alpha_25(L_9);
	}

IL_0080:
	{
		// s_ActiveSoftMasks.Add(this);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_10 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_19();
		NullCheck(L_10);
		List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419(L_10, __this, /*hidden argument*/List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
		// GetComponentsInChildren<SoftMask>(false, s_TempRelatables);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_11 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TempRelatables_20();
		Component_GetComponentsInChildren_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mDB51DAE14977345C6094B986C51FF7DCC3A5DB66(__this, (bool)0, L_11, /*hidden argument*/Component_GetComponentsInChildren_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mDB51DAE14977345C6094B986C51FF7DCC3A5DB66_RuntimeMethod_var);
		// for (int i = s_TempRelatables.Count - 1; 0 <= i; i--)
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_12 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TempRelatables_20();
		NullCheck(L_12);
		int32_t L_13;
		L_13 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_12, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
		goto IL_00ba;
	}

IL_00a6:
	{
		// s_TempRelatables[i].OnTransformParentChanged();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_14 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TempRelatables_20();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_16;
		L_16 = List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_inline(L_14, L_15, /*hidden argument*/List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(12 /* System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged() */, L_16);
		// for (int i = s_TempRelatables.Count - 1; 0 <= i; i--)
		int32_t L_17 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1));
	}

IL_00ba:
	{
		// for (int i = s_TempRelatables.Count - 1; 0 <= i; i--)
		int32_t L_18 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_18)))
		{
			goto IL_00a6;
		}
	}
	{
		// s_TempRelatables.Clear();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_19 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TempRelatables_20();
		NullCheck(L_19);
		List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A(L_19, /*hidden argument*/List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var);
		// _mpb = new MaterialPropertyBlock();
		MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * L_20 = (MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 *)il2cpp_codegen_object_new(MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0_il2cpp_TypeInfo_var);
		MaterialPropertyBlock__ctor_m8EB29E415C68427B841A0C68A902A8368B9228E8(L_20, /*hidden argument*/NULL);
		__this->set__mpb_26(L_20);
		// _cb = new CommandBuffer();
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_21 = (CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 *)il2cpp_codegen_object_new(CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_mB7E1174EB3B4E2E53BCC6532840AB55ECE6D06CF(L_21, /*hidden argument*/NULL);
		__this->set__cb_27(L_21);
		// graphic.SetVerticesDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_22;
		L_22 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_22);
		// base.OnEnable();
		Mask_OnEnable_mD321373E240B3013212217903A2902D12C904A21(__this, /*hidden argument*/NULL);
		// _hasStencilStateChanged = false;
		__this->set__hasStencilStateChanged_35((bool)0);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_OnDisable_mCFEF0F10F085D4722208571EE27F6519FB93AA1A (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// s_ActiveSoftMasks.Remove(this);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_0 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_19();
		NullCheck(L_0);
		bool L_1;
		L_1 = List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE(L_0, __this, /*hidden argument*/List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE_RuntimeMethod_var);
		// if (s_ActiveSoftMasks.Count == 0)
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_2 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_19();
		NullCheck(L_2);
		int32_t L_3;
		L_3 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_2, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		// Canvas.willRenderCanvases -= UpdateMaskTextures;
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_4 = (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 *)il2cpp_codegen_object_new(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		WillRenderCanvases__ctor_m8A46E9A5DED6B54DC2A8A3137AE3637081EADFB6(L_4, NULL, (intptr_t)((intptr_t)SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF_RuntimeMethod_var), /*hidden argument*/NULL);
		Canvas_remove_willRenderCanvases_m4A631D84D6DBB6035620ED9496542E43F19D0EF9(L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		// for (int i = _children.Count - 1; 0 <= i; i--)
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_5 = __this->get__children_33();
		NullCheck(L_5);
		int32_t L_6;
		L_6 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_5, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1));
		goto IL_0054;
	}

IL_0039:
	{
		// _children[i].SetParent(_parent);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_7 = __this->get__children_33();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_9;
		L_9 = List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_10 = __this->get__parent_32();
		NullCheck(L_9);
		SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F(L_9, L_10, /*hidden argument*/NULL);
		// for (int i = _children.Count - 1; 0 <= i; i--)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1));
	}

IL_0054:
	{
		// for (int i = _children.Count - 1; 0 <= i; i--)
		int32_t L_12 = V_0;
		if ((((int32_t)0) <= ((int32_t)L_12)))
		{
			goto IL_0039;
		}
	}
	{
		// _children.Clear();
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_13 = __this->get__children_33();
		NullCheck(L_13);
		List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A(L_13, /*hidden argument*/List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var);
		// SetParent(null);
		SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F(__this, (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *)NULL, /*hidden argument*/NULL);
		// _mpb.Clear();
		MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * L_14 = __this->get__mpb_26();
		NullCheck(L_14);
		MaterialPropertyBlock_Clear_mAB598168AB3F657597804EE57D62E67443794813(L_14, /*hidden argument*/NULL);
		// _mpb = null;
		__this->set__mpb_26((MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 *)NULL);
		// _cb.Release();
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_15 = __this->get__cb_27();
		NullCheck(L_15);
		CommandBuffer_Release_m34AA2C11825D26FBD327987EA995E5235D522DAF(L_15, /*hidden argument*/NULL);
		// _cb = null;
		__this->set__cb_27((CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 *)NULL);
		// ReleaseObject(_mesh);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_16 = __this->get__mesh_31();
		SoftMask_ReleaseObject_mB296E9939AB74191D556253DACA5E21CA29FD54D(__this, L_16, /*hidden argument*/NULL);
		// _mesh = null;
		__this->set__mesh_31((Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *)NULL);
		// ReleaseObject(_material);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_17 = __this->get__material_28();
		SoftMask_ReleaseObject_mB296E9939AB74191D556253DACA5E21CA29FD54D(__this, L_17, /*hidden argument*/NULL);
		// _material = null;
		__this->set__material_28((Material_t8927C00353A72755313F046D0CE85178AE8218EE *)NULL);
		// ReleaseRT(ref _softMaskBuffer);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_18 = __this->get_address_of__softMaskBuffer_29();
		SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE(__this, (RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 **)L_18, /*hidden argument*/NULL);
		// base.OnDisable();
		Mask_OnDisable_m24823C343812F3A0CE28B2B0347139B61B07B289(__this, /*hidden argument*/NULL);
		// _hasStencilStateChanged = false;
		__this->set__hasStencilStateChanged_35((bool)0);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnTransformParentChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_OnTransformParentChanged_mE96013BC30FD7AC2CCAA07538A3A77E9B7B8200F (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_0 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	{
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// SoftMask newParent = null;
		V_0 = (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *)NULL;
		// if (isActiveAndEnabled && !m_IgnoreParent)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		bool L_1 = __this->get_m_IgnoreParent_15();
		if (L_1)
		{
			goto IL_004d;
		}
	}
	{
		// var parentTransform = transform.parent;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0035;
	}

IL_0027:
	{
		// newParent = parentTransform.GetComponent<SoftMask>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = V_1;
		NullCheck(L_4);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_5;
		L_5 = Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D(L_4, /*hidden argument*/Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D_RuntimeMethod_var);
		V_0 = L_5;
		// parentTransform = parentTransform.parent;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = V_1;
		NullCheck(L_6);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_0035:
	{
		// while (parentTransform && (!newParent || !newParent.enabled))
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004d;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0027;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_12 = V_0;
		NullCheck(L_12);
		bool L_13;
		L_13 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0027;
		}
	}

IL_004d:
	{
		// SetParent(newParent);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_14 = V_0;
		SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F(__this, L_14, /*hidden argument*/NULL);
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::OnRectTransformDimensionsChange()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_OnRectTransformDimensionsChange_m8CF9BE917A141A9ACD0BCA909AADC5959CCE8CE5 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// hasChanged = true;
		SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Material Coffee.UIExtensions.SoftMask::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * SoftMask_get_material_mC2B5EECA152A1D35860B2C07A38A8839E0DFAB16 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA40AA217B2961790D4C788E975FD50D97848330);
		s_Il2CppMethodInitialized = true;
	}
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * V_0 = NULL;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * G_B3_0 = NULL;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * G_B2_0 = NULL;
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * G_B4_0 = NULL;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * G_B4_1 = NULL;
	{
		// Material material { get { return _material ? _material : _material = new Material(s_SoftMaskShader ? s_SoftMaskShader : s_SoftMaskShader = Resources.Load<Shader>("SoftMask")){ hideFlags = HideFlags.HideAndDontSave }; } }
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = __this->get__material_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_2 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_SoftMaskShader_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if (L_3)
		{
			G_B3_0 = __this;
			goto IL_002c;
		}
	}
	{
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001(_stringLiteralDA40AA217B2961790D4C788E975FD50D97848330, /*hidden argument*/Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001_RuntimeMethod_var);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_5 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_SoftMaskShader_17(L_5);
		G_B4_0 = L_5;
		G_B4_1 = G_B2_0;
		goto IL_0031;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_6 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_SoftMaskShader_17();
		G_B4_0 = L_6;
		G_B4_1 = G_B3_0;
	}

IL_0031:
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)il2cpp_codegen_object_new(Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E(L_7, G_B4_0, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = L_7;
		NullCheck(L_8);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_8, ((int32_t)61), /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_9 = L_8;
		V_0 = L_9;
		NullCheck(G_B4_1);
		G_B4_1->set__material_28(L_9);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_10 = V_0;
		return L_10;
	}

IL_0047:
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_11 = __this->get__material_28();
		return L_11;
	}
}
// UnityEngine.Mesh Coffee.UIExtensions.SoftMask::get_mesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * SoftMask_get_mesh_mD8B874B7C69EA61B11F1B1CC3ED78EA11C5D75A1 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * V_0 = NULL;
	{
		// Mesh mesh { get { return _mesh ? _mesh : _mesh = new Mesh(){ hideFlags = HideFlags.HideAndDontSave }; } }
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get__mesh_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0024;
		}
	}
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2 = (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *)il2cpp_codegen_object_new(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6_il2cpp_TypeInfo_var);
		Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6(L_2, /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_3 = L_2;
		NullCheck(L_3);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_3, ((int32_t)61), /*hidden argument*/NULL);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_4 = L_3;
		V_0 = L_4;
		__this->set__mesh_31(L_4);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_5 = V_0;
		return L_5;
	}

IL_0024:
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_6 = __this->get__mesh_31();
		return L_6;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTextures()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_UpdateMaskTextures_m7DE8F0C3BF83A88EA569131ADDA304B2AC0249EF (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_1 = NULL;
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * V_2 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * V_3 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_4 = NULL;
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_6;
	memset((&V_6), 0, sizeof(V_6));
	int32_t V_7 = 0;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_8 = NULL;
	Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9  V_9;
	memset((&V_9), 0, sizeof(V_9));
	int32_t V_10 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 3> __leave_targets;
	{
		// foreach (var sm in s_ActiveSoftMasks)
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_0 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_19();
		NullCheck(L_0);
		Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2  L_1;
		L_1 = List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D(L_0, /*hidden argument*/List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00d1;
		}

IL_0010:
		{
			// foreach (var sm in s_ActiveSoftMasks)
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_2;
			L_2 = Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_inline((Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_RuntimeMethod_var);
			V_1 = L_2;
			// if (!sm || sm._hasChanged)
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_3 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_4;
			L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_00d1;
			}
		}

IL_0023:
		{
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_5 = V_1;
			NullCheck(L_5);
			bool L_6 = L_5->get__hasChanged_34();
			if (L_6)
			{
				goto IL_00d1;
			}
		}

IL_002e:
		{
			// var canvas = sm.graphic.canvas;
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_7 = V_1;
			NullCheck(L_7);
			Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_8;
			L_8 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(L_7, /*hidden argument*/NULL);
			NullCheck(L_8);
			Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_9;
			L_9 = Graphic_get_canvas_mDB17EC66AF3FD40E8D368FC11C8F07319BB9D1B0(L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			// if(!canvas)
			Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_10 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_11;
			L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_00d1;
			}
		}

IL_0045:
		{
			// if (canvas.renderMode == RenderMode.WorldSpace)
			Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_12 = V_2;
			NullCheck(L_12);
			int32_t L_13;
			L_13 = Canvas_get_renderMode_mAEC8A341577CC74EC89D5890E6D6E4A82B03574D(L_12, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_13) == ((uint32_t)2))))
			{
				goto IL_00b4;
			}
		}

IL_004e:
		{
			// var cam = canvas.worldCamera;
			Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_14 = V_2;
			NullCheck(L_14);
			Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_15;
			L_15 = Canvas_get_worldCamera_mFE4C9FDA7996FE20AC5CA3CB45B4190C40122D51(L_14, /*hidden argument*/NULL);
			V_4 = L_15;
			// if(!cam)
			Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_16 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_17;
			L_17 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_16, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_00d1;
			}
		}

IL_005f:
		{
			// Matrix4x4 nowVP = cam.projectionMatrix * cam.worldToCameraMatrix;
			Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_18 = V_4;
			NullCheck(L_18);
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_19;
			L_19 = Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756(L_18, /*hidden argument*/NULL);
			Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_20 = V_4;
			NullCheck(L_20);
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_21;
			L_21 = Camera_get_worldToCameraMatrix_m7E2B63F64437E2C91C07F7FC819C79BE2152C5F6(L_20, /*hidden argument*/NULL);
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_22;
			L_22 = Matrix4x4_op_Multiply_mC2B30D333D4399C1693414F1A73D87FB3450F39F(L_19, L_21, /*hidden argument*/NULL);
			V_5 = L_22;
			// Matrix4x4 previousVP = default(Matrix4x4);
			il2cpp_codegen_initobj((&V_6), sizeof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 ));
			// int id = cam.GetInstanceID ();
			Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_23 = V_4;
			NullCheck(L_23);
			int32_t L_24;
			L_24 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_23, /*hidden argument*/NULL);
			V_7 = L_24;
			// s_previousViewProjectionMatrices.TryGetValue (id, out previousVP);
			IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
			Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_25 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_previousViewProjectionMatrices_36();
			int32_t L_26 = V_7;
			NullCheck(L_25);
			bool L_27;
			L_27 = Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396(L_25, L_26, (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&V_6), /*hidden argument*/Dictionary_2_TryGetValue_m23DF2FA2BA6EC13E80DF3DCCEDDF9DB1737D4396_RuntimeMethod_var);
			// s_nowViewProjectionMatrices[id] = nowVP;
			Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_28 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_nowViewProjectionMatrices_37();
			int32_t L_29 = V_7;
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_30 = V_5;
			NullCheck(L_28);
			Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF(L_28, L_29, L_30, /*hidden argument*/Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF_RuntimeMethod_var);
			// if (previousVP != nowVP)
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_31 = V_6;
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_32 = V_5;
			bool L_33;
			L_33 = Matrix4x4_op_Inequality_m6E62E1FDEDC9AF783BE1041726A18BCA5E6CFC6D(L_31, L_32, /*hidden argument*/NULL);
			if (!L_33)
			{
				goto IL_00b4;
			}
		}

IL_00ad:
		{
			// sm.hasChanged = true;
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_34 = V_1;
			NullCheck(L_34);
			SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(L_34, (bool)1, /*hidden argument*/NULL);
		}

IL_00b4:
		{
			// var rt = sm.rectTransform;
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_35 = V_1;
			NullCheck(L_35);
			RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_36;
			L_36 = Mask_get_rectTransform_mF1081D590012320A4D694F33A7965B7A8DFE4A28(L_35, /*hidden argument*/NULL);
			V_3 = L_36;
			// if (rt.hasChanged)
			RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_37 = V_3;
			NullCheck(L_37);
			bool L_38;
			L_38 = Transform_get_hasChanged_m59490E3CAC42DF8CB2BCDFC0ED75DB6F89432F06(L_37, /*hidden argument*/NULL);
			if (!L_38)
			{
				goto IL_00d1;
			}
		}

IL_00c3:
		{
			// rt.hasChanged = false;
			RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_39 = V_3;
			NullCheck(L_39);
			Transform_set_hasChanged_mD1CDCAE366DB514FBECD9DAAED0F7834029E1304(L_39, (bool)0, /*hidden argument*/NULL);
			// sm.hasChanged = true;
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_40 = V_1;
			NullCheck(L_40);
			SoftMask_set_hasChanged_m69C080CA8B29EC26B904EDF77EBC1DD4753EC1AA(L_40, (bool)1, /*hidden argument*/NULL);
		}

IL_00d1:
		{
			// foreach (var sm in s_ActiveSoftMasks)
			bool L_41;
			L_41 = Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41((Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41_RuntimeMethod_var);
			if (L_41)
			{
				goto IL_0010;
			}
		}

IL_00dd:
		{
			IL2CPP_LEAVE(0xED, FINALLY_00df);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00df;
	}

FINALLY_00df:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED((Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED_RuntimeMethod_var);
		IL2CPP_END_FINALLY(223)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(223)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xED, IL_00ed)
	}

IL_00ed:
	{
		// foreach (var sm in s_ActiveSoftMasks)
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_42 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ActiveSoftMasks_19();
		NullCheck(L_42);
		Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2  L_43;
		L_43 = List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D(L_42, /*hidden argument*/List_1_GetEnumerator_m761CB1378CD1447057FCD675D7F51772E6EC502D_RuntimeMethod_var);
		V_0 = L_43;
	}

IL_00f8:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014a;
		}

IL_00fa:
		{
			// foreach (var sm in s_ActiveSoftMasks)
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_44;
			L_44 = Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_inline((Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mF23150BD37588F2130524B3380671C81C3908B2F_RuntimeMethod_var);
			V_8 = L_44;
			// if (!sm || !sm._hasChanged)
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_45 = V_8;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_46;
			L_46 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_45, /*hidden argument*/NULL);
			if (!L_46)
			{
				goto IL_014a;
			}
		}

IL_010c:
		{
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_47 = V_8;
			NullCheck(L_47);
			bool L_48 = L_47->get__hasChanged_34();
			if (!L_48)
			{
				goto IL_014a;
			}
		}

IL_0115:
		{
			// sm._hasChanged = false;
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_49 = V_8;
			NullCheck(L_49);
			L_49->set__hasChanged_34((bool)0);
			// if (!sm._parent)
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_50 = V_8;
			NullCheck(L_50);
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_51 = L_50->get__parent_32();
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_52;
			L_52 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_51, /*hidden argument*/NULL);
			if (L_52)
			{
				goto IL_014a;
			}
		}

IL_012b:
		{
			// sm.UpdateMaskTexture();
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_53 = V_8;
			NullCheck(L_53);
			SoftMask_UpdateMaskTexture_m1A0B74909D06DDF4E98CCDE6645CE7BC7430F2E0(L_53, /*hidden argument*/NULL);
			// if (sm._hasStencilStateChanged)
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_54 = V_8;
			NullCheck(L_54);
			bool L_55 = L_54->get__hasStencilStateChanged_35();
			if (!L_55)
			{
				goto IL_014a;
			}
		}

IL_013b:
		{
			// sm._hasStencilStateChanged = false;
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_56 = V_8;
			NullCheck(L_56);
			L_56->set__hasStencilStateChanged_35((bool)0);
			// MaskUtilities.NotifyStencilStateChanged (sm);
			SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_57 = V_8;
			MaskUtilities_NotifyStencilStateChanged_m9ABCE3B9E8E67931DED17B1F856C4A71600E28C6(L_57, /*hidden argument*/NULL);
		}

IL_014a:
		{
			// foreach (var sm in s_ActiveSoftMasks)
			bool L_58;
			L_58 = Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41((Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mFB2F84D8C22A5331F555CF301136B431D95B8D41_RuntimeMethod_var);
			if (L_58)
			{
				goto IL_00fa;
			}
		}

IL_0153:
		{
			IL2CPP_LEAVE(0x163, FINALLY_0155);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0155;
	}

FINALLY_0155:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED((Enumerator_t1CC9DFE3A584BF3F01D0F487DE93C5C9F0469AF2 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mC8CCA6DFD94ECC947504F4C0CBE2B8D08F845CED_RuntimeMethod_var);
		IL2CPP_END_FINALLY(341)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(341)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x163, IL_0163)
	}

IL_0163:
	{
		// s_previousViewProjectionMatrices.Clear ();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_59 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_previousViewProjectionMatrices_36();
		NullCheck(L_59);
		Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1(L_59, /*hidden argument*/Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1_RuntimeMethod_var);
		// foreach (int id in s_nowViewProjectionMatrices.Keys)
		Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_60 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_nowViewProjectionMatrices_37();
		NullCheck(L_60);
		KeyCollection_tB7CA44BEF97879E102304212CDF03F341972F0A2 * L_61;
		L_61 = Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6(L_60, /*hidden argument*/Dictionary_2_get_Keys_mA06D4BBEE670034AEEF17025D1FC016374EAB2F6_RuntimeMethod_var);
		NullCheck(L_61);
		Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9  L_62;
		L_62 = KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379(L_61, /*hidden argument*/KeyCollection_GetEnumerator_m9E7051EA90397442D432DAFD71C5C3C29ABD9379_RuntimeMethod_var);
		V_9 = L_62;
	}

IL_017e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01a1;
		}

IL_0180:
		{
			// foreach (int id in s_nowViewProjectionMatrices.Keys)
			int32_t L_63;
			L_63 = Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_inline((Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 *)(&V_9), /*hidden argument*/Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_RuntimeMethod_var);
			V_10 = L_63;
			// s_previousViewProjectionMatrices [id] = s_nowViewProjectionMatrices [id];
			IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
			Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_64 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_previousViewProjectionMatrices_36();
			int32_t L_65 = V_10;
			Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_66 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_nowViewProjectionMatrices_37();
			int32_t L_67 = V_10;
			NullCheck(L_66);
			Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_68;
			L_68 = Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E(L_66, L_67, /*hidden argument*/Dictionary_2_get_Item_mA65B2A08FED05AA3C578BF580ED529AB76BB524E_RuntimeMethod_var);
			NullCheck(L_64);
			Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF(L_64, L_65, L_68, /*hidden argument*/Dictionary_2_set_Item_mFC3AEAF99060FA986C4DE0AAAA331CACB62B99DF_RuntimeMethod_var);
		}

IL_01a1:
		{
			// foreach (int id in s_nowViewProjectionMatrices.Keys)
			bool L_69;
			L_69 = Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8((Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 *)(&V_9), /*hidden argument*/Enumerator_MoveNext_m7D79D48EC4593E600F6293287F2ECD1FF9F0F0C8_RuntimeMethod_var);
			if (L_69)
			{
				goto IL_0180;
			}
		}

IL_01aa:
		{
			IL2CPP_LEAVE(0x1BA, FINALLY_01ac);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01ac;
	}

FINALLY_01ac:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27((Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 *)(&V_9), /*hidden argument*/Enumerator_Dispose_m0F38ED80D4701E42E3958E155C8BF7368B427D27_RuntimeMethod_var);
		IL2CPP_END_FINALLY(428)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(428)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1BA, IL_01ba)
	}

IL_01ba:
	{
		// s_nowViewProjectionMatrices.Clear ();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_70 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_nowViewProjectionMatrices_37();
		NullCheck(L_70);
		Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1(L_70, /*hidden argument*/Dictionary_2_Clear_m4C5E8E35C22D23E26945014711EB6421DB0EB9D1_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::UpdateMaskTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_UpdateMaskTexture_m1A0B74909D06DDF4E98CCDE6645CE7BC7430F2E0 (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t97FAEBE964F3F622D4865E7EC62717FE94D1F56D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * V_1 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_8 = NULL;
	int32_t V_9 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_11;
	memset((&V_11), 0, sizeof(V_11));
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_12;
	memset((&V_12), 0, sizeof(V_12));
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_16 = NULL;
	int32_t G_B9_0 = 0;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B17_0 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * G_B16_0 = NULL;
	{
		// if (!graphic || !graphic.canvas)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0;
		L_0 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2;
		L_2 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_3;
		L_3 = Graphic_get_canvas_mDB17EC66AF3FD40E8D368FC11C8F07319BB9D1B0(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0020;
		}
	}

IL_001f:
	{
		// return;
		return;
	}

IL_0020:
	{
		// _stencilDepth = MaskUtilities.GetStencilDepth(transform, MaskUtilities.FindRootSortOverrideCanvas(transform));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = MaskUtilities_FindRootSortOverrideCanvas_m4760B83EDA0BF632346FDE90302AB43EAC0524E0(L_6, /*hidden argument*/NULL);
		int32_t L_8;
		L_8 = MaskUtilities_GetStencilDepth_m102B187BAF39A903EA0BEC302B9DFC2993BC1E30(L_5, L_7, /*hidden argument*/NULL);
		__this->set__stencilDepth_30(L_8);
		// int depth = 0;
		V_0 = 0;
		// s_TmpSoftMasks[0].Add(this);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_9 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		NullCheck(L_9);
		int32_t L_10 = 0;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_11 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419(L_11, __this, /*hidden argument*/List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
		goto IL_00c8;
	}

IL_004d:
	{
		// int count = s_TmpSoftMasks[depth].Count;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_12 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_15 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		int32_t L_16;
		L_16 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_15, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		V_3 = L_16;
		// for (int i = 0; i < count; i++)
		V_4 = 0;
		goto IL_00bf;
	}

IL_005f:
	{
		// List<SoftMask> children = s_TmpSoftMasks[depth][i]._children;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_17 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_20 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = V_4;
		NullCheck(L_20);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_22;
		L_22 = List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_inline(L_20, L_21, /*hidden argument*/List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		NullCheck(L_22);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_23 = L_22->get__children_33();
		V_5 = L_23;
		// int childCount = children.Count;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_24 = V_5;
		NullCheck(L_24);
		int32_t L_25;
		L_25 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_24, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		V_6 = L_25;
		// for (int j = 0; j < childCount; j++)
		V_7 = 0;
		goto IL_00b3;
	}

IL_0082:
	{
		// var child = children[j];
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_26 = V_5;
		int32_t L_27 = V_7;
		NullCheck(L_26);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_28;
		L_28 = List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_inline(L_26, L_27, /*hidden argument*/List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		V_8 = L_28;
		// var childDepth = child.m_PartOfParent ? depth : depth + 1;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_29 = V_8;
		NullCheck(L_29);
		bool L_30 = L_29->get_m_PartOfParent_16();
		if (L_30)
		{
			goto IL_009b;
		}
	}
	{
		int32_t L_31 = V_0;
		G_B9_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
		goto IL_009c;
	}

IL_009b:
	{
		int32_t L_32 = V_0;
		G_B9_0 = L_32;
	}

IL_009c:
	{
		V_9 = G_B9_0;
		// s_TmpSoftMasks[childDepth].Add(child);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_33 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_34 = V_9;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_36 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_37 = V_8;
		NullCheck(L_36);
		List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419(L_36, L_37, /*hidden argument*/List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
		// for (int j = 0; j < childCount; j++)
		int32_t L_38 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00b3:
	{
		// for (int j = 0; j < childCount; j++)
		int32_t L_39 = V_7;
		int32_t L_40 = V_6;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_0082;
		}
	}
	{
		// for (int i = 0; i < count; i++)
		int32_t L_41 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00bf:
	{
		// for (int i = 0; i < count; i++)
		int32_t L_42 = V_4;
		int32_t L_43 = V_3;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_005f;
		}
	}
	{
		// depth++;
		int32_t L_44 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_00c8:
	{
		// while (_stencilDepth + depth < 3)
		int32_t L_45 = __this->get__stencilDepth_30();
		int32_t L_46 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)L_46))) < ((int32_t)3)))
		{
			goto IL_004d;
		}
	}
	{
		// _cb.Clear();
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_47 = __this->get__cb_27();
		NullCheck(L_47);
		CommandBuffer_Clear_mDE798ACE9294B43B9387A6B06E43B3D4A30A7092(L_47, /*hidden argument*/NULL);
		// _cb.SetRenderTarget(softMaskBuffer);
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_48 = __this->get__cb_27();
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_49;
		L_49 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(__this, /*hidden argument*/NULL);
		RenderTargetIdentifier_t70F41F3016FFCC4AAF4D7C57F280818114534C13  L_50;
		L_50 = RenderTargetIdentifier_op_Implicit_mFF9B98B136B3AB4E9413777F931392D02AA7A8FA(L_49, /*hidden argument*/NULL);
		NullCheck(L_48);
		CommandBuffer_SetRenderTarget_mCDFEB57580AF2EC11E02BA87EFB6CF18AD3AC840(L_48, L_50, /*hidden argument*/NULL);
		// _cb.ClearRenderTarget(false, true, s_ClearColors[_stencilDepth]);
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_51 = __this->get__cb_27();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_52 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ClearColors_10();
		int32_t L_53 = __this->get__stencilDepth_30();
		NullCheck(L_52);
		int32_t L_54 = L_53;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_51);
		CommandBuffer_ClearRenderTarget_mD911CD9DACD86BB1A9734235E6D55AF9BE844CA0(L_51, (bool)0, (bool)1, L_55, /*hidden argument*/NULL);
		// var c = graphic.canvas.rootCanvas;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_56;
		L_56 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_57;
		L_57 = Graphic_get_canvas_mDB17EC66AF3FD40E8D368FC11C8F07319BB9D1B0(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_58;
		L_58 = Canvas_get_rootCanvas_mB1C93410A4AA793D88130FD08C05D71327641DC5(L_57, /*hidden argument*/NULL);
		V_1 = L_58;
		// var cam = c.worldCamera ?? Camera.main;
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_59 = V_1;
		NullCheck(L_59);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_60;
		L_60 = Canvas_get_worldCamera_mFE4C9FDA7996FE20AC5CA3CB45B4190C40122D51(L_59, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_61 = L_60;
		G_B16_0 = L_61;
		if (L_61)
		{
			G_B17_0 = L_61;
			goto IL_0134;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_62;
		L_62 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		G_B17_0 = L_62;
	}

IL_0134:
	{
		V_2 = G_B17_0;
		// if (c && c.renderMode != RenderMode.ScreenSpaceOverlay && cam)
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_63 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_64;
		L_64 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_016f;
		}
	}
	{
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_65 = V_1;
		NullCheck(L_65);
		int32_t L_66;
		L_66 = Canvas_get_renderMode_mAEC8A341577CC74EC89D5890E6D6E4A82B03574D(L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_016f;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_67 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_68;
		L_68 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_016f;
		}
	}
	{
		// _cb.SetViewProjectionMatrices(cam.worldToCameraMatrix, GL.GetGPUProjectionMatrix(cam.projectionMatrix, false));
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_69 = __this->get__cb_27();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_70 = V_2;
		NullCheck(L_70);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_71;
		L_71 = Camera_get_worldToCameraMatrix_m7E2B63F64437E2C91C07F7FC819C79BE2152C5F6(L_70, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_72 = V_2;
		NullCheck(L_72);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_73;
		L_73 = Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756(L_72, /*hidden argument*/NULL);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_74;
		L_74 = GL_GetGPUProjectionMatrix_m8DAC433EACD75ECD86B3148EA7658F6604378457(L_73, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_69);
		CommandBuffer_SetViewProjectionMatrices_m3530A45C10FAD54367B483A1A1B03641ACF05187(L_69, L_71, L_74, /*hidden argument*/NULL);
		// }
		goto IL_0209;
	}

IL_016f:
	{
		// var pos = c.transform.position;
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_75 = V_1;
		NullCheck(L_75);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_76;
		L_76 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_77;
		L_77 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_76, /*hidden argument*/NULL);
		V_10 = L_77;
		// var vm = Matrix4x4.TRS(new Vector3(-pos.x, -pos.y, -1000), Quaternion.identity, new Vector3(1, 1, -1f));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_78 = V_10;
		float L_79 = L_78.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_80 = V_10;
		float L_81 = L_80.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_82;
		memset((&L_82), 0, sizeof(L_82));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_82), ((-L_79)), ((-L_81)), (-1000.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_83;
		L_83 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_84;
		memset((&L_84), 0, sizeof(L_84));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_84), (1.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_85;
		L_85 = Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5(L_82, L_83, L_84, /*hidden argument*/NULL);
		V_11 = L_85;
		// var pm = Matrix4x4.TRS(new Vector3(0, 0, -1), Quaternion.identity, new Vector3(1 / pos.x, 1 / pos.y, -2 / 10000f));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_86;
		memset((&L_86), 0, sizeof(L_86));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_86), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_87;
		L_87 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_88 = V_10;
		float L_89 = L_88.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_90 = V_10;
		float L_91 = L_90.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_92;
		memset((&L_92), 0, sizeof(L_92));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_92), ((float)((float)(1.0f)/(float)L_89)), ((float)((float)(1.0f)/(float)L_91)), (-0.000199999995f), /*hidden argument*/NULL);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_93;
		L_93 = Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5(L_86, L_87, L_92, /*hidden argument*/NULL);
		V_12 = L_93;
		// _cb.SetViewProjectionMatrices(vm, pm);
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_94 = __this->get__cb_27();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_95 = V_11;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_96 = V_12;
		NullCheck(L_94);
		CommandBuffer_SetViewProjectionMatrices_m3530A45C10FAD54367B483A1A1B03641ACF05187(L_94, L_95, L_96, /*hidden argument*/NULL);
	}

IL_0209:
	{
		// for (int i = 0; i < s_TmpSoftMasks.Length; i++)
		V_13 = 0;
		goto IL_031a;
	}

IL_0211:
	{
		// int count = s_TmpSoftMasks[i].Count;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_97 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_98 = V_13;
		NullCheck(L_97);
		int32_t L_99 = L_98;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_100 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		NullCheck(L_100);
		int32_t L_101;
		L_101 = List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_inline(L_100, /*hidden argument*/List_1_get_Count_m82D9583355BD9C3A08D3E5C873CDC007EB4BC0BC_RuntimeMethod_var);
		V_14 = L_101;
		// for (int j = 0; j < count; j++)
		V_15 = 0;
		goto IL_02fe;
	}

IL_0228:
	{
		// var sm = s_TmpSoftMasks[i][j];
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_102 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_103 = V_13;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_105 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		int32_t L_106 = V_15;
		NullCheck(L_105);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_107;
		L_107 = List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_inline(L_105, L_106, /*hidden argument*/List_1_get_Item_mF7FDEEB4D4EDAC429496DB4192576A7B720882A7_RuntimeMethod_var);
		V_16 = L_107;
		// if (i != 0)
		int32_t L_108 = V_13;
		if (!L_108)
		{
			goto IL_025c;
		}
	}
	{
		// sm._stencilDepth = MaskUtilities.GetStencilDepth(sm.transform, MaskUtilities.FindRootSortOverrideCanvas(sm.transform));
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_109 = V_16;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_110 = V_16;
		NullCheck(L_110);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_111;
		L_111 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_110, /*hidden argument*/NULL);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_112 = V_16;
		NullCheck(L_112);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_113;
		L_113 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_112, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_114;
		L_114 = MaskUtilities_FindRootSortOverrideCanvas_m4760B83EDA0BF632346FDE90302AB43EAC0524E0(L_113, /*hidden argument*/NULL);
		int32_t L_115;
		L_115 = MaskUtilities_GetStencilDepth_m102B187BAF39A903EA0BEC302B9DFC2993BC1E30(L_111, L_114, /*hidden argument*/NULL);
		NullCheck(L_109);
		L_109->set__stencilDepth_30(L_115);
	}

IL_025c:
	{
		// sm.material.SetInt(s_ColorMaskId, (int)1 << (3 - _stencilDepth - i));
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_116 = V_16;
		NullCheck(L_116);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_117;
		L_117 = SoftMask_get_material_mC2B5EECA152A1D35860B2C07A38A8839E0DFAB16(L_116, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		int32_t L_118 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ColorMaskId_22();
		int32_t L_119 = __this->get__stencilDepth_30();
		int32_t L_120 = V_13;
		NullCheck(L_117);
		Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C(L_117, L_118, ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)3, (int32_t)L_119)), (int32_t)L_120))&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		// sm._mpb.SetTexture(s_MainTexId, sm.graphic.mainTexture);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_121 = V_16;
		NullCheck(L_121);
		MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * L_122 = L_121->get__mpb_26();
		int32_t L_123 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_MainTexId_23();
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_124 = V_16;
		NullCheck(L_124);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_125;
		L_125 = Mask_get_graphic_m3C7C03706C188F60DB2068C62459E1BF0277FED4(L_124, /*hidden argument*/NULL);
		NullCheck(L_125);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_126;
		L_126 = VirtFuncInvoker0< Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * >::Invoke(35 /* UnityEngine.Texture UnityEngine.UI.Graphic::get_mainTexture() */, L_125);
		NullCheck(L_122);
		MaterialPropertyBlock_SetTexture_m8F81CA94E5261618BEDD93EC02DAA2412E732C04(L_122, L_123, L_126, /*hidden argument*/NULL);
		// sm._mpb.SetFloat(s_SoftnessId, sm.m_Softness);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_127 = V_16;
		NullCheck(L_127);
		MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * L_128 = L_127->get__mpb_26();
		int32_t L_129 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_SoftnessId_24();
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_130 = V_16;
		NullCheck(L_130);
		float L_131 = L_130->get_m_Softness_13();
		NullCheck(L_128);
		MaterialPropertyBlock_SetFloat_mD2F1653CEDB22B1A3EE844E353CF125A607AEDEC(L_128, L_129, L_131, /*hidden argument*/NULL);
		// sm._mpb.SetFloat(s_Alpha, sm.m_Alpha);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_132 = V_16;
		NullCheck(L_132);
		MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * L_133 = L_132->get__mpb_26();
		int32_t L_134 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_Alpha_25();
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_135 = V_16;
		NullCheck(L_135);
		float L_136 = L_135->get_m_Alpha_14();
		NullCheck(L_133);
		MaterialPropertyBlock_SetFloat_mD2F1653CEDB22B1A3EE844E353CF125A607AEDEC(L_133, L_134, L_136, /*hidden argument*/NULL);
		// _cb.DrawMesh(sm.mesh, sm.transform.localToWorldMatrix, sm.material, 0, 0, sm._mpb);
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_137 = __this->get__cb_27();
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_138 = V_16;
		NullCheck(L_138);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_139;
		L_139 = SoftMask_get_mesh_mD8B874B7C69EA61B11F1B1CC3ED78EA11C5D75A1(L_138, /*hidden argument*/NULL);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_140 = V_16;
		NullCheck(L_140);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_141;
		L_141 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_140, /*hidden argument*/NULL);
		NullCheck(L_141);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_142;
		L_142 = Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC(L_141, /*hidden argument*/NULL);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_143 = V_16;
		NullCheck(L_143);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_144;
		L_144 = SoftMask_get_material_mC2B5EECA152A1D35860B2C07A38A8839E0DFAB16(L_143, /*hidden argument*/NULL);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_145 = V_16;
		NullCheck(L_145);
		MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * L_146 = L_145->get__mpb_26();
		NullCheck(L_137);
		CommandBuffer_DrawMesh_m8AE72FB8773642F0293697038A3262E84E269AB0(L_137, L_139, L_142, L_144, 0, 0, L_146, /*hidden argument*/NULL);
		// for (int j = 0; j < count; j++)
		int32_t L_147 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_147, (int32_t)1));
	}

IL_02fe:
	{
		// for (int j = 0; j < count; j++)
		int32_t L_148 = V_15;
		int32_t L_149 = V_14;
		if ((((int32_t)L_148) < ((int32_t)L_149)))
		{
			goto IL_0228;
		}
	}
	{
		// s_TmpSoftMasks[i].Clear();
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_150 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		int32_t L_151 = V_13;
		NullCheck(L_150);
		int32_t L_152 = L_151;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_153 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)(L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_152));
		NullCheck(L_153);
		List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A(L_153, /*hidden argument*/List_1_Clear_m99E6E18663747A9A190492BF150910D3224FD02A_RuntimeMethod_var);
		// for (int i = 0; i < s_TmpSoftMasks.Length; i++)
		int32_t L_154 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_154, (int32_t)1));
	}

IL_031a:
	{
		// for (int i = 0; i < s_TmpSoftMasks.Length; i++)
		int32_t L_155 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_156 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_TmpSoftMasks_9();
		NullCheck(L_156);
		if ((((int32_t)L_155) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_156)->max_length))))))
		{
			goto IL_0211;
		}
	}
	{
		// Graphics.ExecuteCommandBuffer(_cb);
		CommandBuffer_t25CD231BD3E822660339DB7D0E8F8ED6B7DBEA29 * L_157 = __this->get__cb_27();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t97FAEBE964F3F622D4865E7EC62717FE94D1F56D_il2cpp_TypeInfo_var);
		Graphics_ExecuteCommandBuffer_mCE45F0B19843D5B2803B456A5F037857D20FCC51(L_157, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::GetDesamplingSize(Coffee.UIExtensions.SoftMask/DesamplingRate,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_GetDesamplingSize_mADB1BE4385FF8E47F9E393C1CFB18DDF3A1240CE (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, int32_t ___rate0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// w = Screen.width;
		int32_t* L_0 = ___w1;
		int32_t L_1;
		L_1 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		*((int32_t*)L_0) = (int32_t)L_1;
		// h = Screen.height;
		int32_t* L_2 = ___h2;
		int32_t L_3;
		L_3 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		*((int32_t*)L_2) = (int32_t)L_3;
		// if (rate == DesamplingRate.None)
		int32_t L_4 = ___rate0;
		if (L_4)
		{
			goto IL_0012;
		}
	}
	{
		// return;
		return;
	}

IL_0012:
	{
		// float aspect = (float)w / h;
		int32_t* L_5 = ___w1;
		int32_t L_6 = *((int32_t*)L_5);
		int32_t* L_7 = ___h2;
		int32_t L_8 = *((int32_t*)L_7);
		V_0 = ((float)((float)((float)((float)L_6))/(float)((float)((float)L_8))));
		// if (w < h)
		int32_t* L_9 = ___w1;
		int32_t L_10 = *((int32_t*)L_9);
		int32_t* L_11 = ___h2;
		int32_t L_12 = *((int32_t*)L_11);
		if ((((int32_t)L_10) >= ((int32_t)L_12)))
		{
			goto IL_0038;
		}
	}
	{
		// h = Mathf.ClosestPowerOfTwo(h / (int)rate);
		int32_t* L_13 = ___h2;
		int32_t* L_14 = ___h2;
		int32_t L_15 = *((int32_t*)L_14);
		int32_t L_16 = ___rate0;
		int32_t L_17;
		L_17 = Mathf_ClosestPowerOfTwo_mF46C05D09E914DE9DC7C42EEE7FA69FC087D2832(((int32_t)((int32_t)L_15/(int32_t)L_16)), /*hidden argument*/NULL);
		*((int32_t*)L_13) = (int32_t)L_17;
		// w = Mathf.CeilToInt(h * aspect);
		int32_t* L_18 = ___w1;
		int32_t* L_19 = ___h2;
		int32_t L_20 = *((int32_t*)L_19);
		float L_21 = V_0;
		int32_t L_22;
		L_22 = Mathf_CeilToInt_m3A3E7C0F6A3CF731411BB90F264F989D8311CC6F(((float)il2cpp_codegen_multiply((float)((float)((float)L_20)), (float)L_21)), /*hidden argument*/NULL);
		*((int32_t*)L_18) = (int32_t)L_22;
		// }
		return;
	}

IL_0038:
	{
		// w = Mathf.ClosestPowerOfTwo(w / (int)rate);
		int32_t* L_23 = ___w1;
		int32_t* L_24 = ___w1;
		int32_t L_25 = *((int32_t*)L_24);
		int32_t L_26 = ___rate0;
		int32_t L_27;
		L_27 = Mathf_ClosestPowerOfTwo_mF46C05D09E914DE9DC7C42EEE7FA69FC087D2832(((int32_t)((int32_t)L_25/(int32_t)L_26)), /*hidden argument*/NULL);
		*((int32_t*)L_23) = (int32_t)L_27;
		// h = Mathf.CeilToInt(w / aspect);
		int32_t* L_28 = ___h2;
		int32_t* L_29 = ___w1;
		int32_t L_30 = *((int32_t*)L_29);
		float L_31 = V_0;
		int32_t L_32;
		L_32 = Mathf_CeilToInt_m3A3E7C0F6A3CF731411BB90F264F989D8311CC6F(((float)((float)((float)((float)L_30))/(float)L_31)), /*hidden argument*/NULL);
		*((int32_t*)L_28) = (int32_t)L_32;
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::ReleaseRT(UnityEngine.RenderTexture&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_ReleaseRT_mF135F577E65CCBB8746B1FF3920BC49CB867AACE (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** ___tmpRT0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (tmpRT)
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_0 = ___tmpRT0;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_1 = *((RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 **)L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		// tmpRT.Release();
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_3 = ___tmpRT0;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_4 = *((RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 **)L_3);
		NullCheck(L_4);
		RenderTexture_Release_m533506E903688E798921C0D35F1B082522D88986(L_4, /*hidden argument*/NULL);
		// RenderTexture.ReleaseTemporary(tmpRT);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_5 = ___tmpRT0;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_6 = *((RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 **)L_5);
		RenderTexture_ReleaseTemporary_m2BF2BDDC359A491C05C401B977878DAE1D0850D4(L_6, /*hidden argument*/NULL);
		// tmpRT = null;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** L_7 = ___tmpRT0;
		*((RuntimeObject **)L_7) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_7, (void*)(RuntimeObject *)NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::ReleaseObject(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_ReleaseObject_mB296E9939AB74191D556253DACA5E21CA29FD54D (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (obj)
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		// Destroy(obj);
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_2 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_2, /*hidden argument*/NULL);
		// obj = null;
		___obj0 = (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL;
	}

IL_0011:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::SetParent(Coffee.UIExtensions.SoftMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask_SetParent_m53D75AB0B91F5C078E7F117660B5B233E5320D9F (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___newParent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAll_mB4F9BDD13B74F228229F3EF77915CA1880585C36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m39AAD12C99FF47DF1421C18605E877CD25AFFDA5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CSetParentU3Eb__70_0_mC32C9A814621E217595F893608F9952930B5630F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * G_B6_0 = NULL;
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * G_B6_1 = NULL;
	Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * G_B5_0 = NULL;
	List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * G_B5_1 = NULL;
	{
		// if (_parent != newParent && this != newParent)
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = __this->get__parent_32();
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_1 = ___newParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0080;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_3 = ___newParent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(__this, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0080;
		}
	}
	{
		// if (_parent && _parent._children.Contains(this))
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_5 = __this->get__parent_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0079;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_7 = __this->get__parent_32();
		NullCheck(L_7);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_8 = L_7->get__children_33();
		NullCheck(L_8);
		bool L_9;
		L_9 = List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31(L_8, __this, /*hidden argument*/List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_0079;
		}
	}
	{
		// _parent._children.Remove(this);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_10 = __this->get__parent_32();
		NullCheck(L_10);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_11 = L_10->get__children_33();
		NullCheck(L_11);
		bool L_12;
		L_12 = List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE(L_11, __this, /*hidden argument*/List_1_Remove_m5F63E8344778F03026E11D3657261895F8572EDE_RuntimeMethod_var);
		// _parent._children.RemoveAll(x => x == null);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_13 = __this->get__parent_32();
		NullCheck(L_13);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_14 = L_13->get__children_33();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var);
		Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * L_15 = ((U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var))->get_U3CU3E9__70_0_1();
		Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * L_16 = L_15;
		G_B5_0 = L_16;
		G_B5_1 = L_14;
		if (L_16)
		{
			G_B6_0 = L_16;
			G_B6_1 = L_14;
			goto IL_0073;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var);
		U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * L_17 = ((U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * L_18 = (Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 *)il2cpp_codegen_object_new(Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m39AAD12C99FF47DF1421C18605E877CD25AFFDA5(L_18, L_17, (intptr_t)((intptr_t)U3CU3Ec_U3CSetParentU3Eb__70_0_mC32C9A814621E217595F893608F9952930B5630F_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m39AAD12C99FF47DF1421C18605E877CD25AFFDA5_RuntimeMethod_var);
		Predicate_1_tCA9669AA1034427E07DAECA143A12A8FC55BEB43 * L_19 = L_18;
		((U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var))->set_U3CU3E9__70_0_1(L_19);
		G_B6_0 = L_19;
		G_B6_1 = G_B5_1;
	}

IL_0073:
	{
		NullCheck(G_B6_1);
		int32_t L_20;
		L_20 = List_1_RemoveAll_mB4F9BDD13B74F228229F3EF77915CA1880585C36(G_B6_1, G_B6_0, /*hidden argument*/List_1_RemoveAll_mB4F9BDD13B74F228229F3EF77915CA1880585C36_RuntimeMethod_var);
	}

IL_0079:
	{
		// _parent = newParent;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_21 = ___newParent0;
		__this->set__parent_32(L_21);
	}

IL_0080:
	{
		// if (_parent && !_parent._children.Contains(this))
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_22 = __this->get__parent_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_23;
		L_23 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b1;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_24 = __this->get__parent_32();
		NullCheck(L_24);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_25 = L_24->get__children_33();
		NullCheck(L_25);
		bool L_26;
		L_26 = List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31(L_25, __this, /*hidden argument*/List_1_Contains_m3A45694EC22C8CF72FFF94B325B8B9A8D5804A31_RuntimeMethod_var);
		if (L_26)
		{
			goto IL_00b1;
		}
	}
	{
		// _parent._children.Add(this);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_27 = __this->get__parent_32();
		NullCheck(L_27);
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_28 = L_27->get__children_33();
		NullCheck(L_28);
		List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419(L_28, __this, /*hidden argument*/List_1_Add_m99C99DADF4F0E63BCE9C53BB048524B7E3F83419_RuntimeMethod_var);
	}

IL_00b1:
	{
		// }
		return;
	}
}
// System.Single Coffee.UIExtensions.SoftMask::GetPixelValue(System.Int32,System.Int32,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftMask_GetPixelValue_m27E4D0600AE29C45EA0D02B77BE8BC00AAA3C56D (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, int32_t ___x0, int32_t ___y1, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___interactions2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// if (!s_ReadTexture)
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ReadTexture_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		// s_ReadTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092(L_2, 1, 1, 5, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_ReadTexture_18(L_2);
	}

IL_001a:
	{
		// var currentRT = RenderTexture.active;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_3;
		L_3 = RenderTexture_get_active_mB73718A56673D36F74B5338B310ED7FDFEB34AB7(/*hidden argument*/NULL);
		// RenderTexture.active = softMaskBuffer;
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_4;
		L_4 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(__this, /*hidden argument*/NULL);
		RenderTexture_set_active_mA70AFD6D3CB54E9AEDDD45E48B8B6979FDB75ED9(L_4, /*hidden argument*/NULL);
		// s_ReadTexture.ReadPixels(new Rect(x, y, 1, 1), 0, 0);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_5 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ReadTexture_18();
		int32_t L_6 = ___x0;
		int32_t L_7 = ___y1;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70((&L_8), ((float)((float)L_6)), ((float)((float)L_7)), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Texture2D_ReadPixels_m4C6FE8C2798C39C20A14DAFC963C720D17F4F987(L_5, L_8, 0, 0, /*hidden argument*/NULL);
		// s_ReadTexture.Apply(false, false);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_9 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ReadTexture_18();
		NullCheck(L_9);
		Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660(L_9, (bool)0, (bool)0, /*hidden argument*/NULL);
		// RenderTexture.active = currentRT;
		RenderTexture_set_active_mA70AFD6D3CB54E9AEDDD45E48B8B6979FDB75ED9(L_3, /*hidden argument*/NULL);
		// var colors = s_ReadTexture.GetRawTextureData();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_10 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->get_s_ReadTexture_18();
		NullCheck(L_10);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11;
		L_11 = Texture2D_GetRawTextureData_m60C0B5EF034F31FE1824B31AC1DE71042E2ACB55(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		// for (int i = 0; i < 4; i++)
		V_1 = 0;
		goto IL_0095;
	}

IL_0069:
	{
		// switch (interactions[(i + 3)%4])
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_12 = ___interactions2;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)3))%(int32_t)4));
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_2 = L_15;
		int32_t L_16 = V_2;
		if (!L_16)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_17 = V_2;
		if ((((int32_t)L_17) == ((int32_t)2)))
		{
			goto IL_0084;
		}
	}
	{
		goto IL_0091;
	}

IL_007a:
	{
		// case 0: colors[i] = 255; break;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = V_0;
		int32_t L_19 = V_1;
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (uint8_t)((int32_t)255));
		// case 0: colors[i] = 255; break;
		goto IL_0091;
	}

IL_0084:
	{
		// case 2: colors[i] = (byte)(255 - colors[i]); break;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = V_0;
		int32_t L_21 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = V_0;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		uint8_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_21), (uint8_t)((int32_t)((uint8_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)255), (int32_t)L_25)))));
	}

IL_0091:
	{
		// for (int i = 0; i < 4; i++)
		int32_t L_26 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_0095:
	{
		// for (int i = 0; i < 4; i++)
		int32_t L_27 = V_1;
		if ((((int32_t)L_27) < ((int32_t)4)))
		{
			goto IL_0069;
		}
	}
	{
		// switch (_stencilDepth)
		int32_t L_28 = __this->get__stencilDepth_30();
		V_2 = L_28;
		int32_t L_29 = V_2;
		switch (L_29)
		{
			case 0:
			{
				goto IL_00b8;
			}
			case 1:
			{
				goto IL_00c3;
			}
			case 2:
			{
				goto IL_00d9;
			}
			case 3:
			{
				goto IL_00fa;
			}
		}
	}
	{
		goto IL_0126;
	}

IL_00b8:
	{
		// case 0: return (colors[1] / 255f);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_30 = V_0;
		NullCheck(L_30);
		int32_t L_31 = 1;
		uint8_t L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		return ((float)((float)((float)((float)L_32))/(float)(255.0f)));
	}

IL_00c3:
	{
		// case 1: return (colors[1] / 255f) * (colors[2] / 255f);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = 1;
		uint8_t L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_36 = V_0;
		NullCheck(L_36);
		int32_t L_37 = 2;
		uint8_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		return ((float)il2cpp_codegen_multiply((float)((float)((float)((float)((float)L_35))/(float)(255.0f))), (float)((float)((float)((float)((float)L_38))/(float)(255.0f)))));
	}

IL_00d9:
	{
		// case 2: return (colors[1] / 255f) * (colors[2] / 255f) * (colors[3] / 255f);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_39 = V_0;
		NullCheck(L_39);
		int32_t L_40 = 1;
		uint8_t L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_42 = V_0;
		NullCheck(L_42);
		int32_t L_43 = 2;
		uint8_t L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_45 = V_0;
		NullCheck(L_45);
		int32_t L_46 = 3;
		uint8_t L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		return ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)((float)((float)((float)L_41))/(float)(255.0f))), (float)((float)((float)((float)((float)L_44))/(float)(255.0f))))), (float)((float)((float)((float)((float)L_47))/(float)(255.0f)))));
	}

IL_00fa:
	{
		// case 3: return (colors[1] / 255f) * (colors[2] / 255f) * (colors[3] / 255f) * (colors[0] / 255f);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_48 = V_0;
		NullCheck(L_48);
		int32_t L_49 = 1;
		uint8_t L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_51 = V_0;
		NullCheck(L_51);
		int32_t L_52 = 2;
		uint8_t L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_54 = V_0;
		NullCheck(L_54);
		int32_t L_55 = 3;
		uint8_t L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_57 = V_0;
		NullCheck(L_57);
		int32_t L_58 = 0;
		uint8_t L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		return ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)((float)((float)((float)L_50))/(float)(255.0f))), (float)((float)((float)((float)((float)L_53))/(float)(255.0f))))), (float)((float)((float)((float)((float)L_56))/(float)(255.0f))))), (float)((float)((float)((float)((float)L_59))/(float)(255.0f)))));
	}

IL_0126:
	{
		// default: return 0;
		return (0.0f);
	}
}
// System.Void Coffee.UIExtensions.SoftMask::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask__ctor_mAD022F6F9DBE190453F4D94259EB148FB630095D (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField][Range(0.01f, 1)] float m_Softness = 1;
		__this->set_m_Softness_13((1.0f));
		// [SerializeField][Range(0f, 1f)] float m_Alpha = 1;
		__this->set_m_Alpha_14((1.0f));
		// List<SoftMask> _children = new List<SoftMask>();
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_0 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_0, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		__this->set__children_33(L_0);
		Mask__ctor_mC3A6C226EDD04AC7058424F80D413DB9A85570AE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMask__cctor_mC69D648F38CCA8A6C097EF5AB9B716959F3F1679 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static readonly List<SoftMask>[] s_TmpSoftMasks = new List<SoftMask>[]
		// {
		//     new List<SoftMask>(),
		//     new List<SoftMask>(),
		//     new List<SoftMask>(),
		//     new List<SoftMask>(),
		// };
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_0 = (List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106*)(List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106*)SZArrayNew(List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106_il2cpp_TypeInfo_var, (uint32_t)4);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_1 = L_0;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_2 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_2, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)L_2);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_3 = L_1;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_4 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_4, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)L_4);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_5 = L_3;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_6 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_6, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)L_6);
		List_1U5BU5D_tAA1D689D2E82B36A92DD866F09122DFFDD919106* L_7 = L_5;
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_8 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_8, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)L_8);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_TmpSoftMasks_9(L_7);
		// static readonly Color[] s_ClearColors = new Color[]
		// {
		//     new Color(0, 0, 0, 0),
		//     new Color(1, 0, 0, 0),
		//     new Color(1, 1, 0, 0),
		//     new Color(1, 1, 1, 0),
		// };
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_9 = (ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)SZArrayNew(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var, (uint32_t)4);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_10 = L_9;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_11), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_11);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_12 = L_10;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_13), (1.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_13);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_14 = L_12;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_15), (1.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_15);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_16 = L_14;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_17;
		memset((&L_17), 0, sizeof(L_17));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_17), (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_17);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_ClearColors_10(L_16);
		// static List<SoftMask> s_ActiveSoftMasks = new List<SoftMask>();
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_18 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_18, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_ActiveSoftMasks_19(L_18);
		// static List<SoftMask> s_TempRelatables = new List<SoftMask>();
		List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C * L_19 = (List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C *)il2cpp_codegen_object_new(List_1_t224A1E861F9DF480A931CB5C1C109FA97DC1095C_il2cpp_TypeInfo_var);
		List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7(L_19, /*hidden argument*/List_1__ctor_m8224F6AAEBAD2E6B93AC71B78DA18E8A4AB372C7_RuntimeMethod_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_TempRelatables_20(L_19);
		// static readonly Dictionary<int, Matrix4x4> s_previousViewProjectionMatrices = new Dictionary<int, Matrix4x4> ();
		Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_20 = (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *)il2cpp_codegen_object_new(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A(L_20, /*hidden argument*/Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A_RuntimeMethod_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_previousViewProjectionMatrices_36(L_20);
		// static readonly Dictionary<int, Matrix4x4> s_nowViewProjectionMatrices = new Dictionary<int, Matrix4x4> ();
		Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D * L_21 = (Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D *)il2cpp_codegen_object_new(Dictionary_2_tEEFB5132159AF00ECFFDD2DA6DE843F175EFD72D_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A(L_21, /*hidden argument*/Dictionary_2__ctor_m1066B3FF584C094A8979D5C7BFEB83B5B7C8D59A_RuntimeMethod_var);
		((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_StaticFields*)il2cpp_codegen_static_fields_for(SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_il2cpp_TypeInfo_var))->set_s_nowViewProjectionMatrices_37(L_21);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Material Coffee.UIExtensions.SoftMaskable::GetModifiedMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * SoftMaskable_GetModifiedMaterial_m071CDF00E45C51885DD1FD89A5B69629580F3978 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_0 = NULL;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * V_1 = NULL;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_2 = NULL;
	int32_t G_B11_0 = 0;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B11_1 = NULL;
	int32_t G_B10_0 = 0;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B10_1 = NULL;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B12_2 = NULL;
	{
		// _softMask = null;
		__this->set__softMask_10((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *)NULL);
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// return baseMaterial;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___baseMaterial0;
		return L_1;
	}

IL_0011:
	{
		// var parentTransform = transform.parent;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0046;
	}

IL_001f:
	{
		// var sm = parentTransform.GetComponent<SoftMask>();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = V_0;
		NullCheck(L_4);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_5;
		L_5 = Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D(L_4, /*hidden argument*/Component_GetComponent_TisSoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7_mE04C2E647E0F7B8B94DB80E8189FE1CCBBA7C32D_RuntimeMethod_var);
		V_2 = L_5;
		// if (sm && sm.enabled)
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_8 = V_2;
		NullCheck(L_8);
		bool L_9;
		L_9 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		// _softMask = sm;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_10 = V_2;
		__this->set__softMask_10(L_10);
		// break;
		goto IL_004e;
	}

IL_003f:
	{
		// parentTransform = parentTransform.parent;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11 = V_0;
		NullCheck(L_11);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0046:
	{
		// while (parentTransform)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_001f;
		}
	}

IL_004e:
	{
		// Material result = baseMaterial;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_15 = ___baseMaterial0;
		V_1 = L_15;
		// if (_softMask)
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_16 = __this->get__softMask_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17;
		L_17 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00f1;
		}
	}
	{
		// result = new Material(baseMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = ___baseMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_19 = (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)il2cpp_codegen_object_new(Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245(L_19, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		// result.hideFlags = HideFlags.HideAndDontSave;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_20 = V_1;
		NullCheck(L_20);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_20, ((int32_t)61), /*hidden argument*/NULL);
		// result.SetTexture(s_SoftMaskTexId, _softMask.softMaskBuffer);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		int32_t L_22 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_SoftMaskTexId_12();
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_23 = __this->get__softMask_10();
		NullCheck(L_23);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_24;
		L_24 = SoftMask_get_softMaskBuffer_mF6D80E9B145CBA6A8F511D25EF86850DF22E2981(L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60(L_21, L_22, L_24, /*hidden argument*/NULL);
		// result.SetInt(s_StencilCompId, m_UseStencil ? (int)CompareFunction.Equal : (int)CompareFunction.Always);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_25 = V_1;
		int32_t L_26 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_StencilCompId_13();
		bool L_27 = __this->get_m_UseStencil_8();
		G_B10_0 = L_26;
		G_B10_1 = L_25;
		if (L_27)
		{
			G_B11_0 = L_26;
			G_B11_1 = L_25;
			goto IL_0096;
		}
	}
	{
		G_B12_0 = 8;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_0097;
	}

IL_0096:
	{
		G_B12_0 = 3;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_0097:
	{
		NullCheck(G_B12_2);
		Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C(G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		// result.SetVector(s_MaskInteractionId, new Vector4(
		//         (m_MaskInteraction & 0x3),
		//         ((m_MaskInteraction >> 2) & 0x3),
		//         ((m_MaskInteraction >> 4) & 0x3),
		//         ((m_MaskInteraction >> 6) & 0x3)
		//     ));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		int32_t L_29 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_MaskInteractionId_14();
		int32_t L_30 = __this->get_m_MaskInteraction_7();
		int32_t L_31 = __this->get_m_MaskInteraction_7();
		int32_t L_32 = __this->get_m_MaskInteraction_7();
		int32_t L_33 = __this->get_m_MaskInteraction_7();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2((&L_34), ((float)((float)((int32_t)((int32_t)L_30&(int32_t)3)))), ((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_31>>(int32_t)2))&(int32_t)3)))), ((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_32>>(int32_t)4))&(int32_t)3)))), ((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_33>>(int32_t)6))&(int32_t)3)))), /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m47F7F5B5B21FA28885C4E747AF1C32F40C1022CB(L_28, L_29, L_34, /*hidden argument*/NULL);
		// StencilMaterial.Remove(baseMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_35 = ___baseMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_il2cpp_TypeInfo_var);
		StencilMaterial_Remove_m8C971D3D0DDDD92710C011FD7B630E6C853B744D(L_35, /*hidden argument*/NULL);
		// ReleaseMaterial(ref _maskMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE ** L_36 = __this->get_address_of__maskMaterial_11();
		SoftMaskable_ReleaseMaterial_mE62C3AF98B25EBD4244B4508975E39D26D8F5238(__this, (Material_t8927C00353A72755313F046D0CE85178AE8218EE **)L_36, /*hidden argument*/NULL);
		// _maskMaterial = result;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_37 = V_1;
		__this->set__maskMaterial_11(L_37);
		// }
		goto IL_0101;
	}

IL_00f1:
	{
		// baseMaterial.SetTexture(s_SoftMaskTexId, Texture2D.whiteTexture);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_38 = ___baseMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		int32_t L_39 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_SoftMaskTexId_12();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_40;
		L_40 = Texture2D_get_whiteTexture_m4ED96995BA1D42F7D2823BD9D18023CFE3C680A0(/*hidden argument*/NULL);
		NullCheck(L_38);
		Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60(L_38, L_39, L_40, /*hidden argument*/NULL);
	}

IL_0101:
	{
		// return result;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_41 = V_1;
		return L_41;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMaskable::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMaskable_IsRaycastLocationValid_mE605A0336D4EBEFC1BAA512F9CC4E9C394650787 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___sp0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___eventCamera1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B8_0 = 0;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B8_1 = NULL;
	int32_t G_B7_0 = 0;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B7_1 = NULL;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* G_B9_2 = NULL;
	SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * G_B12_0 = NULL;
	{
		// if (!isActiveAndEnabled || !_softMask)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_1 = __this->get__softMask_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		// return true;
		return (bool)1;
	}

IL_0017:
	{
		// if (!RectTransformUtility.RectangleContainsScreenPoint(transform as RectTransform, sp, eventCamera))
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___sp0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_5 = ___eventCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t829C94C0D38759683C2BED9FCE244D5EA9842396_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = RectTransformUtility_RectangleContainsScreenPoint_m7D92A04D6DA6F4C7CC72439221C2EE46034A0595(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)IsInstSealed((RuntimeObject*)L_3, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002d;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_002d:
	{
		// var sm = _softMask;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_7 = __this->get__softMask_10();
		V_0 = L_7;
		// for (int i = 0; i < 4; i++)
		V_1 = 0;
		goto IL_006f;
	}

IL_0038:
	{
		// s_Interactions[i] = sm ? ((m_MaskInteraction >> i * 2) & 0x3) : 0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_8 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_Interactions_18();
		int32_t L_9 = V_1;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		G_B7_0 = L_9;
		G_B7_1 = L_8;
		if (L_11)
		{
			G_B8_0 = L_9;
			G_B8_1 = L_8;
			goto IL_0049;
		}
	}
	{
		G_B9_0 = 0;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_0058;
	}

IL_0049:
	{
		int32_t L_12 = __this->get_m_MaskInteraction_7();
		int32_t L_13 = V_1;
		G_B9_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12>>(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)2))&(int32_t)((int32_t)31)))))&(int32_t)3));
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_0058:
	{
		NullCheck(G_B9_2);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (int32_t)G_B9_0);
		// sm = sm ? sm.parent : null;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0064;
		}
	}
	{
		G_B12_0 = ((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *)(NULL));
		goto IL_006a;
	}

IL_0064:
	{
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_16 = V_0;
		NullCheck(L_16);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_17;
		L_17 = SoftMask_get_parent_mB1469E842BC5ADF3FE5DAAFA819D3FA77FCCDBF1_inline(L_16, /*hidden argument*/NULL);
		G_B12_0 = L_17;
	}

IL_006a:
	{
		V_0 = G_B12_0;
		// for (int i = 0; i < 4; i++)
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_006f:
	{
		// for (int i = 0; i < 4; i++)
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) < ((int32_t)4)))
		{
			goto IL_0038;
		}
	}
	{
		// return _softMask.IsRaycastLocationValid(sp, eventCamera, graphic, s_Interactions);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_20 = __this->get__softMask_10();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21 = ___sp0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_22 = ___eventCamera1;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_23;
		L_23 = SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_24 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_Interactions_18();
		NullCheck(L_20);
		bool L_25;
		L_25 = SoftMask_IsRaycastLocationValid_m419A604EFD1A8EA76EC040ECBC4337011E84BAC1(L_20, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMaskable::get_inverse()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SoftMaskable_get_inverse_m1048E5B624D02DEF9662F2A2F3E8532636C1381F (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	{
		// get { return m_MaskInteraction == kVisibleOutside; }
		int32_t L_0 = __this->get_m_MaskInteraction_7();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)170)))? 1 : 0);
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::set_inverse(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_set_inverse_mEBDF2BFBF3A1FDD72DEED8305DB7D1BE555AB46F (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		// int intValue = value ? kVisibleOutside : kVisibleInside;
		bool L_0 = ___value0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = ((int32_t)85);
		goto IL_000c;
	}

IL_0007:
	{
		G_B3_0 = ((int32_t)170);
	}

IL_000c:
	{
		V_0 = G_B3_0;
		// if (m_MaskInteraction != intValue)
		int32_t L_1 = __this->get_m_MaskInteraction_7();
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		// m_MaskInteraction = intValue;
		int32_t L_3 = V_0;
		__this->set_m_MaskInteraction_7(L_3);
		// graphic.SetMaterialDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_4;
		L_4 = SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_4);
	}

IL_0028:
	{
		// }
		return;
	}
}
// UnityEngine.UI.Graphic Coffee.UIExtensions.SoftMaskable::get_graphic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * V_0 = NULL;
	{
		// public Graphic graphic{ get { return _graphic ? _graphic : _graphic = GetComponent<Graphic>(); } }
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = __this->get__graphic_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2;
		L_2 = Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1(__this, /*hidden argument*/Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1_RuntimeMethod_var);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_3 = L_2;
		V_0 = L_3;
		__this->set__graphic_9(L_3);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_4 = V_0;
		return L_4;
	}

IL_001d:
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_5 = __this->get__graphic_9();
		return L_5;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_SetMaskInteraction_mE902481BF70D77339DF466721D5A3779C06B665E (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, int32_t ___intr0, const RuntimeMethod* method)
{
	{
		// SetMaskInteraction(intr, intr, intr, intr);
		int32_t L_0 = ___intr0;
		int32_t L_1 = ___intr0;
		int32_t L_2 = ___intr0;
		int32_t L_3 = ___intr0;
		SoftMaskable_SetMaskInteraction_m9C75BABE68C3E28C63F5016F9CBF247961642C3C(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::SetMaskInteraction(UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction,UnityEngine.SpriteMaskInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_SetMaskInteraction_m9C75BABE68C3E28C63F5016F9CBF247961642C3C (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, int32_t ___layer00, int32_t ___layer11, int32_t ___layer22, int32_t ___layer33, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_MaskInteraction = (int)layer0 + ((int)layer1 << 2) + ((int)layer2 << 4) + ((int)layer3 << 6);
		int32_t L_0 = ___layer00;
		int32_t L_1 = ___layer11;
		int32_t L_2 = ___layer22;
		int32_t L_3 = ___layer33;
		__this->set_m_MaskInteraction_7(((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)((int32_t)((int32_t)L_1<<(int32_t)2)))), (int32_t)((int32_t)((int32_t)L_2<<(int32_t)4)))), (int32_t)((int32_t)((int32_t)L_3<<(int32_t)6)))));
		// if (graphic)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_4;
		L_4 = SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		// graphic.SetMaterialDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_6;
		L_6 = SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_6);
	}

IL_002c:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_OnEnable_m60B257F638DABEFCD28D014B1BE9AAAA8A048835 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m2D42408638C489F4076E2D9AEACB059EBAF843BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8C6DA11294CCBAB33C07414BC54634526B1C2D0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0CA801CE75C2F860477B707B285573DE1591BE00);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0F52C788AC4796FE5841155F7DF3896E049C051E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEDACEEFD6A4FDC09F7F84820C5D5791EB514AF7D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF081554143A3CB72659C18709A22913DF21549E6);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * V_0 = NULL;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B7_0 = NULL;
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * G_B7_1 = NULL;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B6_0 = NULL;
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * G_B6_1 = NULL;
	{
		// if (s_ActiveSoftMaskables == null)
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * L_0 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_ActiveSoftMaskables_17();
		if (L_0)
		{
			goto IL_003e;
		}
	}
	{
		// s_ActiveSoftMaskables = new List<SoftMaskable>();
		List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * L_1 = (List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 *)il2cpp_codegen_object_new(List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2_il2cpp_TypeInfo_var);
		List_1__ctor_m8C6DA11294CCBAB33C07414BC54634526B1C2D0A(L_1, /*hidden argument*/List_1__ctor_m8C6DA11294CCBAB33C07414BC54634526B1C2D0A_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->set_s_ActiveSoftMaskables_17(L_1);
		// s_SoftMaskTexId = Shader.PropertyToID("_SoftMaskTex");
		int32_t L_2;
		L_2 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral0CA801CE75C2F860477B707B285573DE1591BE00, /*hidden argument*/NULL);
		((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->set_s_SoftMaskTexId_12(L_2);
		// s_StencilCompId = Shader.PropertyToID("_StencilComp");
		int32_t L_3;
		L_3 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral0F52C788AC4796FE5841155F7DF3896E049C051E, /*hidden argument*/NULL);
		((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->set_s_StencilCompId_13(L_3);
		// s_MaskInteractionId = Shader.PropertyToID("_MaskInteraction");
		int32_t L_4;
		L_4 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteralF081554143A3CB72659C18709A22913DF21549E6, /*hidden argument*/NULL);
		((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->set_s_MaskInteractionId_14(L_4);
	}

IL_003e:
	{
		// s_ActiveSoftMaskables.Add(this);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * L_5 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_ActiveSoftMaskables_17();
		NullCheck(L_5);
		List_1_Add_m2D42408638C489F4076E2D9AEACB059EBAF843BB(L_5, __this, /*hidden argument*/List_1_Add_m2D42408638C489F4076E2D9AEACB059EBAF843BB_RuntimeMethod_var);
		// var g = graphic;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_6;
		L_6 = SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		// if (g)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a9;
		}
	}
	{
		// if (!g.material || g.material == Graphic.defaultGraphicMaterial)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_9 = V_0;
		NullCheck(L_9);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_10;
		L_10 = VirtFuncInvoker0< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0077;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_12 = V_0;
		NullCheck(L_12);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_13;
		L_13 = VirtFuncInvoker0< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_il2cpp_TypeInfo_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_14;
		L_14 = Graphic_get_defaultGraphicMaterial_mAED427ABBA1C93E2AB3794D4FE43F4B8F6D93198(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00a3;
		}
	}

IL_0077:
	{
		// g.material = s_DefaultMaterial ?? (s_DefaultMaterial = new Material(Resources.Load<Shader>("UI-Default-SoftMask")) { hideFlags = HideFlags.HideAndDontSave, });
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_17 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_DefaultMaterial_19();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = L_17;
		G_B6_0 = L_18;
		G_B6_1 = L_16;
		if (L_18)
		{
			G_B7_0 = L_18;
			G_B7_1 = L_16;
			goto IL_009e;
		}
	}
	{
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_19;
		L_19 = Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001(_stringLiteralEDACEEFD6A4FDC09F7F84820C5D5791EB514AF7D, /*hidden argument*/Resources_Load_TisShader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39_m957A0295FB707054CC7AFC54DA4657C145FC9001_RuntimeMethod_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_20 = (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)il2cpp_codegen_object_new(Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E(L_20, L_19, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_21 = L_20;
		NullCheck(L_21);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_21, ((int32_t)61), /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_22 = L_21;
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->set_s_DefaultMaterial_19(L_22);
		G_B7_0 = L_22;
		G_B7_1 = G_B6_1;
	}

IL_009e:
	{
		NullCheck(G_B7_1);
		VirtActionInvoker1< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, G_B7_1, G_B7_0);
	}

IL_00a3:
	{
		// g.SetMaterialDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_23 = V_0;
		NullCheck(L_23);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_23);
	}

IL_00a9:
	{
		// _softMask = null;
		__this->set__softMask_10((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *)NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_OnDisable_m04F3EF3F80AFD448128EC6C4A8222075297FAB1D (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m29CA3BE16DCB73AAAD0345E10DB6953D4D8626D2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * V_0 = NULL;
	{
		// s_ActiveSoftMaskables.Remove(this);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		List_1_tED87EC67A1E43C6BEC1A421F45B00F08DE41C6B2 * L_0 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_ActiveSoftMaskables_17();
		NullCheck(L_0);
		bool L_1;
		L_1 = List_1_Remove_m29CA3BE16DCB73AAAD0345E10DB6953D4D8626D2(L_0, __this, /*hidden argument*/List_1_Remove_m29CA3BE16DCB73AAAD0345E10DB6953D4D8626D2_RuntimeMethod_var);
		// var g = graphic;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2;
		L_2 = SoftMaskable_get_graphic_m3A95EA4CAE3FEF81A75A9D98B6A1A7EC383C1ED9(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (g)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		// if (g.material == s_DefaultMaterial)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_5 = V_0;
		NullCheck(L_5);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_6;
		L_6 = VirtFuncInvoker0< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = ((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->get_s_DefaultMaterial_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0034;
		}
	}
	{
		// g.material = null;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker1< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, L_9, (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)NULL);
	}

IL_0034:
	{
		// g.SetMaterialDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_10 = V_0;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_10);
	}

IL_003a:
	{
		// ReleaseMaterial(ref _maskMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE ** L_11 = __this->get_address_of__maskMaterial_11();
		SoftMaskable_ReleaseMaterial_mE62C3AF98B25EBD4244B4508975E39D26D8F5238(__this, (Material_t8927C00353A72755313F046D0CE85178AE8218EE **)L_11, /*hidden argument*/NULL);
		// _softMask = null;
		__this->set__softMask_10((SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 *)NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::ReleaseMaterial(UnityEngine.Material&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_ReleaseMaterial_mE62C3AF98B25EBD4244B4508975E39D26D8F5238 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE ** ___mat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (mat)
		Material_t8927C00353A72755313F046D0CE85178AE8218EE ** L_0 = ___mat0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = *((Material_t8927C00353A72755313F046D0CE85178AE8218EE **)L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		// StencilMaterial.Remove(mat);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE ** L_3 = ___mat0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_4 = *((Material_t8927C00353A72755313F046D0CE85178AE8218EE **)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t498DA9A7C15643B79E27575F27F1D2FC2FEA6AC5_il2cpp_TypeInfo_var);
		StencilMaterial_Remove_m8C971D3D0DDDD92710C011FD7B630E6C853B744D(L_4, /*hidden argument*/NULL);
		// Destroy(mat);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE ** L_5 = ___mat0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_6 = *((Material_t8927C00353A72755313F046D0CE85178AE8218EE **)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_6, /*hidden argument*/NULL);
		// mat = null;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE ** L_7 = ___mat0;
		*((RuntimeObject **)L_7) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_7, (void*)(RuntimeObject *)NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3422EE65AF518B8F5BD238702E5A15E96A5CE351 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m0EAA4A639A80D109C4FE9FEEB7410EF681F01C8D (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	{
		// if (m_Inverse)
		bool L_0 = __this->get_m_Inverse_6();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		// m_Inverse = false;
		__this->set_m_Inverse_6((bool)0);
		// m_MaskInteraction = (2 << 0) + (2 << 2) + (2 << 4) + (2 << 6);
		__this->set_m_MaskInteraction_7(((int32_t)170));
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable__ctor_m7798272190D02CFAF51DFB6D81E7C4561A9BCA90 (SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] int m_MaskInteraction = kVisibleInside;
		__this->set_m_MaskInteraction_7(((int32_t)85));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMaskable::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftMaskable__cctor_mC2CF7B99315A1BB8D643BB96EBE0BA20775256BA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static int[] s_Interactions = new int[4];
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)4);
		((SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_StaticFields*)il2cpp_codegen_static_fields_for(SoftMaskable_tBD9DFE8F3C8CFD6D0D9D155ACD0E26018BECEABC_il2cpp_TypeInfo_var))->set_s_Interactions_18(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIExtensions.SoftMask/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mC715F90BD22A9D06AA40B858B6D8CC288622D3CA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * L_0 = (U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B *)il2cpp_codegen_object_new(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m4E1870D305C6424F6F7E64F02312D7DD3F993999(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Coffee.UIExtensions.SoftMask/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4E1870D305C6424F6F7E64F02312D7DD3F993999 (U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Coffee.UIExtensions.SoftMask/<>c::<SetParent>b__70_0(Coffee.UIExtensions.SoftMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CSetParentU3Eb__70_0_mC32C9A814621E217595F893608F9952930B5630F (U3CU3Ec_t29B9624A3997478BDA37979C33F39C6960B99C6B * __this, SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _parent._children.RemoveAll(x => x == null);
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * SoftMask_get_parent_mB1469E842BC5ADF3FE5DAAFA819D3FA77FCCDBF1_inline (SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * __this, const RuntimeMethod* method)
{
	{
		// return _parent;
		SoftMask_t2B6CCD2233560E0B4E110C357DEF5AB5480D42C7 * L_0 = __this->get__parent_32();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_mECFAAB3BB9EC4314CC32DCB1F3E669E87860B208_gshared_inline (Enumerator_t0D61C3ABC2A3A7BFB4FA60D0B03C0731A9BFA3F9 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_currentKey_3();
		return (int32_t)L_0;
	}
}
