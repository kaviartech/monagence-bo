﻿//using System.Collections;
//using UnityEngine;
//using TMPro;
//using UnityEngine.UI;
//using UnityEngine.XR.ARFoundation;
//using UnityEngine.SceneManagement;
//using UnityEngine.Networking;

//public class gameManager : Singleton<gameManager>
//{

//    string addLeaderUrl = "https://gate.kaviar.app/index.php/api/leaderboard/newscore";
    

//    public static int currentId;

//    public static Renderer current;

//    public bool isFloorReady = false;

//    public AudioSource ad,doneGame,inGame;

//    public AudioClip[] clips;

//    public Color green;
//    public Color violet;

//    public Material mat;

//    public GameObject UIHelper,flash;

//    public GameObject arSessionPref;

//    public GameObject XImage;

//    public TextMeshProUGUI tapToPlace;

//    public TextMeshProUGUI timer;

//    int order = 0;

//    bool pressedOk = false;


//    [Tooltip("this is the custom button that comes with every portal usially reservation")]
//    public Button customBtn;

//    public gMJson gameModeJson;

//    public IEnumerator Start()
//    {
//        Button temp = errorHandler.showInfo(Lang.choose(En.gameMode,Fr.gameMode),Lang.choose(En.gameModeD,Fr.gameModeD));
//        if (temp)
//            temp.onClick.AddListener(delegate 
//            {

//                //access tapToPlace and put ableToPlace
//                pressedOk = true;
//                Destroy(temp.transform.parent.parent.gameObject, 1);

//            }
//            );



//        gameModeJson = cashing.loadGameModeJson();

//        gameModeJson.scenes.Shuffle();

//        gameModeJson.teleports.Shuffle();

//        //print("am here");

//        yield return StartCoroutine(cashing.getAllTps(gameModeJson.scenes));

//        yield return StartCoroutine(setPortal(gameModeJson.scenes[order]));
        
//        yield return null;

//    }


//    /// <summary>
//    /// downloads the portal content and make it ready to be useable
//    /// </summary>
//    public IEnumerator setPortal(mainJson mj)
//    {
        
//        isFloorReady = false;

//        mat.SetColor("_TexTintColor", violet);

//        currentId = mj.id;

//        XImage.SetActive(true);

//        if (Application.systemLanguage == SystemLanguage.French)
//        {
//            tapToPlace.text = Fr.waitForSound;
//        }
//        else
//        {
//            tapToPlace.text = En.waitForSound;
//        }

       
//        //mj = JsonUtility.FromJson<mainJson>(testJson);

//        customBtn.GetComponentInChildren<TextMeshProUGUI>(true).text = mj.customBtn.name;

//        customBtn.onClick.RemoveAllListeners();

//        customBtn.onClick.AddListener(delegate { Application.OpenURL(mj.customBtn.url); });

//        customBtn.gameObject.SetActive(true);


//        UIHelper.SetActive(true);

//        //reset arSession
//        Instantiate(arSessionPref);


//        FindObjectOfType<ARPlaneManager>().enabled = true;

//        PlaceMultipleObjectsOnPlane ppp = FindObjectOfType<PlaceMultipleObjectsOnPlane>();

//        //GameObject temp = ppp.placedPrefab;

//        //temp.GetComponentInChildren<portalManager>().gameTp.transform.localRotation = Quaternion.Euler(Random.Range(-40f, 40f), Random.Range(0f, 360f), Random.Range(0f, 360f));

//        print(textureHolder.tps.TryGetValue(mj.id, out Texture2D tp));

//        taskbarForGameMode.changeTo(tp);


//        //ppp.tp = tp;

//        //ppp.type = mj.background.type;

//        //ppp.ableToPlace = false;

//        yield return StartCoroutine(cashing.download360(mj.background.url, mj.id, mj.versionNumber, null));

//        textureHolder.images360.TryGetValue(mj.id, out Texture2D t);

//        if (!t)
//        {
//            sceneManager.errorReset();
//        }

//        //ppp.texture = t;

//        yield return new WaitUntil(() => pressedOk);

//        //ppp.ableToPlace = true;

//        ARPlaneManager ap = FindObjectOfType<ARPlaneManager>();

//        mat.SetColor("_TexTintColor", green);

//        foreach (var plane in ap.trackables)
//            plane.GetComponent<Renderer>().material.SetColor("_TexTintColor", green);


//        while (!isFloorReady)
//        {
//            yield return null;
//        }

//        ad.Play();

//        isFloorReady = false;


//        if (Application.systemLanguage == SystemLanguage.French)
//        {
//            tapToPlace.text = Fr.tapToPlace;
//        }
//        else
//        {
//            tapToPlace.text = En.tapToPlace;
//        }

//        //print("Appuyez pour positionner votre porte");

//        XImage.SetActive(false);

//    }


//    /// <summary>
//    /// downloads the portal content and make it ready to be useable
//    /// </summary>
//    public IEnumerator setPortalNoReload(mainJson mj)
//    {

//        //mj = JsonUtility.FromJson<mainJson>(testJson);

//        inGame.Stop();

//        current.transform.parent.GetComponentInChildren<scoreManager>().IsCounting = false;

//        customBtn.GetComponentInChildren<TextMeshProUGUI>(true).text = mj.customBtn.name;

//        customBtn.onClick.RemoveAllListeners();

//        customBtn.onClick.AddListener(delegate { Application.OpenURL(mj.customBtn.url); });

//        customBtn.gameObject.SetActive(true);

//        current.transform.parent.GetComponentInChildren<portalManager>().gameTp.transform.localRotation = Quaternion.Euler(Random.Range(-40f, 40f), Random.Range(0f, 360f), Random.Range(0f, 360f));

//        GameObject flashTemp = Instantiate(flash);

//        while (current.material.mainTextureScale.x >= 0.01f)
//        {
//            current.material.mainTextureScale = Vector2.Lerp(current.material.mainTextureScale, new Vector2(0, 0), 5 * Time.deltaTime);
//            yield return null;
//        }

//        current.material.mainTextureScale = new Vector2(0, 0);

//        yield return StartCoroutine(cashing.download360(mj.background.url, mj.id, mj.versionNumber, null));

//        textureHolder.images360.TryGetValue(mj.id, out Texture2D t);

//        if (!t)
//        {
//            sceneManager.errorReset();
//        }

//        current.material.mainTexture = t;

//        textureHolder.tps.TryGetValue(mj.id, out Texture2D tp);

//        current.transform.parent.GetComponentInChildren<scoreManager>().GetComponent<Renderer>().material.mainTexture = tp;
//        taskbarForGameMode.changeTo(tp);

//        while (current.material.mainTextureScale.x <= 0.99f)
//        {
//            current.material.mainTextureScale = Vector2.Lerp(current.material.mainTextureScale, new Vector2(1, 1), 5 * Time.deltaTime);
//            yield return null;
//        }

//        flashTemp.GetComponent<Animator>().Play("hide");

//        Destroy(flashTemp, 1);
//        current.material.mainTextureScale = new Vector2(1, 1);

//        current.transform.parent.GetComponentInChildren<scoreManager>().IsCounting = true;

//        inGame.clip = clips[Random.Range(0, 3)];
//        inGame.Play();

//        //ppp.texture = t;

//    }

//    public IEnumerator next()
//    {
//        order++;
//        StopAllCoroutines();
//        if(order >= gameModeJson.scenes.Count)
//        {
//            scoreManager.instance.IsCounting = false;

//            //disable timer
//            timer.gameObject.SetActive(false);
//            inGame.Stop();
//            doneGame.Play();
            
//            Button btn = errorHandler.showDone(Lang.choose( En.done,Fr.done),Lang.choose(En.doneD,Fr.doneD) + scoreManager.instance.time);

//            btn.onClick.AddListener(delegate { PlayerPrefs.SetInt("showScore", 1) ; SceneManager.LoadScene(1); });

//            btn.transform.parent.GetComponentInChildren<TMP_InputField>().onEndEdit.AddListener(delegate { StartCoroutine(sendLeaderBoard(btn, btn.transform.parent.GetComponentInChildren<TMP_InputField>().text, scoreManager.instance.time)); });


//            yield break;
//        }
//        yield return StartCoroutine(setPortalNoReload(gameModeJson.scenes[order]));
//    }


//    public IEnumerator sendLeaderBoard(Button btn , string name , string score)
//    {
//        if (name.Length < 5) 
//        {

//            if (Application.systemLanguage == SystemLanguage.French)
//            {
//                Button bttn = errorHandler.showError(Fr.textProb,Fr.FiveChar);
//                bttn.onClick.AddListener(delegate { Destroy(bttn.transform.parent.parent.parent.gameObject, 2); });

//            }
//            else
//            {
//                Button bttn = errorHandler.showError(En.textProb, En.FiveChar);
//                bttn.onClick.AddListener(delegate { Destroy(bttn.transform.parent.parent.parent.gameObject, 2); });

//            }

            
//        }else if (!name.Contains("@"))
//        {
//            if (Application.systemLanguage == SystemLanguage.French)
//            {
//                Button bttn = errorHandler.showError(Fr.textProb, Fr.EmailNes);
//                bttn.onClick.AddListener(delegate { Destroy(bttn.transform.parent.parent.parent.gameObject, 2); });

//            }
//            else
//            {
//                Button bttn = errorHandler.showError(En.textProb, En.EmailNes);
//                bttn.onClick.AddListener(delegate { Destroy(bttn.transform.parent.parent.parent.gameObject, 2); });

//            }
//        }
//        else
//        {
//            print(score.Replace(" ", ""));
//            WWWForm form = new WWWForm();
//            form.AddField("name", name);
//            form.AddField("score", score.Replace(" ",""));

//            using (UnityWebRequest www = UnityWebRequest.Post(addLeaderUrl, form))
//            {
//                yield return www.SendWebRequest();

//                if (www.isNetworkError || www.isHttpError)
//                {
//                    Debug.Log(www.error);
//                }
//                else
//                {
//                    string t = www.downloadHandler.text;
//                    btn.onClick.Invoke();
//                }
//            }
//        }
//    }
//}
