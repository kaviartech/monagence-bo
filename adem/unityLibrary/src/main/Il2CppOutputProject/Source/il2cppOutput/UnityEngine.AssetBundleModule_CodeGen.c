﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mCE6DB7758AAD0EDDB044FC67C5BC7EC987BF3F71 (void);
// 0x00000002 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFileAsync_Internal_m5946F1D5D2D456570FA97C194593B4BB7BCBD290 (void);
// 0x00000003 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String)
extern void AssetBundle_LoadFromFileAsync_m32FB2B292114D2ED2DCAF0170E9DE16B21E9DAD8 (void);
// 0x00000004 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String)
// 0x00000005 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_m63C7C5654FA8D0824DC920A1B1530C37CCB3DF6E (void);
// 0x00000006 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_Internal_m89D45EFB48D3C4CF7E481EF41F7ACF24500E7019 (void);
// 0x00000007 System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m0DEBACB284F6CECA8DF21486D1BBE1189F6A5D66 (void);
// 0x00000008 UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern void AssetBundleCreateRequest_get_assetBundle_m608C1516A7DC8E4B1F9D63EDCF6EE8D6C2CFF013 (void);
// 0x00000009 System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern void AssetBundleCreateRequest__ctor_m7B04FFC9566D1B0F820DDE9844BA2822A74881B6 (void);
// 0x0000000A UnityEngine.Object UnityEngine.AssetBundleRequest::GetResult()
extern void AssetBundleRequest_GetResult_m792F2C703230D18A0B6B18C86636961125B1B9A2 (void);
// 0x0000000B UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern void AssetBundleRequest_get_asset_mB0A96FBC026D143638E467DEB37228ACD55F1813 (void);
// 0x0000000C System.Void UnityEngine.AssetBundleRequest::.ctor()
extern void AssetBundleRequest__ctor_mD09AF030644EF7F3386ABB3B5C593F61ADE25017 (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	AssetBundle__ctor_mCE6DB7758AAD0EDDB044FC67C5BC7EC987BF3F71,
	AssetBundle_LoadFromFileAsync_Internal_m5946F1D5D2D456570FA97C194593B4BB7BCBD290,
	AssetBundle_LoadFromFileAsync_m32FB2B292114D2ED2DCAF0170E9DE16B21E9DAD8,
	NULL,
	AssetBundle_LoadAssetAsync_m63C7C5654FA8D0824DC920A1B1530C37CCB3DF6E,
	AssetBundle_LoadAssetAsync_Internal_m89D45EFB48D3C4CF7E481EF41F7ACF24500E7019,
	AssetBundle_Unload_m0DEBACB284F6CECA8DF21486D1BBE1189F6A5D66,
	AssetBundleCreateRequest_get_assetBundle_m608C1516A7DC8E4B1F9D63EDCF6EE8D6C2CFF013,
	AssetBundleCreateRequest__ctor_m7B04FFC9566D1B0F820DDE9844BA2822A74881B6,
	AssetBundleRequest_GetResult_m792F2C703230D18A0B6B18C86636961125B1B9A2,
	AssetBundleRequest_get_asset_mB0A96FBC026D143638E467DEB37228ACD55F1813,
	AssetBundleRequest__ctor_mD09AF030644EF7F3386ABB3B5C593F61ADE25017,
};
static const int32_t s_InvokerIndices[12] = 
{
	3397,
	4185,
	5012,
	-1,
	1247,
	1247,
	2817,
	3345,
	3397,
	3345,
	3345,
	3397,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000004, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)1, 71 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_AssetBundleModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AssetBundleModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModule_CodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_UnityEngine_AssetBundleModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
