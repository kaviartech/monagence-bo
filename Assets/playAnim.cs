﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playAnim : MonoBehaviour
{
    public Animator character;
    public void play(string animation)
    {
        character.Play(animation);
    }
}
