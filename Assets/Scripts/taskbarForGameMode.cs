﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class taskbarForGameMode : Singleton<taskbarForGameMode>
{

    public GameObject warningPref;

    public TextMeshProUGUI moveDevice, tapToPlace;

    public RawImage icon;


    public static void changeTo(Texture2D t)
    {
        instance.icon.gameObject.SetActive(true);
        instance.icon.texture = t;
    }

    private void Start()
    {
        if (PlayerPrefs.GetInt("isFR") == 1)
        {
            
            moveDevice.text = Fr.moveDevice;
            tapToPlace.text = Fr.tapToPlace;
            warningPref.GetComponentInChildren<TextMeshProUGUI>().text = Fr.reloadingAssets;
        }
        else
        {
            
            moveDevice.text = En.moveDevice;
            tapToPlace.text = En.tapToPlace;
            warningPref.GetComponentInChildren<TextMeshProUGUI>().text = En.reloadingAssets;
        }
    }

    public void pop()
    {
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("disappear"))
        {
            GetComponent<Animator>().Play("appear");
        }
        else
        {
            GetComponent<Animator>().Play("disappear");
        }
    }


    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            Destroy(Instantiate(warningPref), 2);
        }
    }
}
