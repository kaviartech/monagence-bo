﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, Type_t * ___requiredComponent32, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void Customizer_t900A35AA9BEDFCF1190CB9FB56693B1D76B71914_CustomAttributesCacheGenerator_Customizer_U3CStartU3Eb__10_0_mFC7C8E187BD7C36F36DEE117E2391927A0FE8B81(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Customizer_t900A35AA9BEDFCF1190CB9FB56693B1D76B71914_CustomAttributesCacheGenerator_Customizer_U3CtreatU3Eb__12_0_m735B07CBFF747B143A745027920E8072099590F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t75B37BD1CB0832F2ED8D3A2C43E68F219D6BCEE6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadText_mE26F63971A466CC2ACBA2F607A8CE2ABD78D7329(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_0_0_0_var), NULL);
	}
}
static void Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadGetText_m85B48FDD4F8BA4CFE5CB6D7B2EAC94D1081C9224(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_0_0_0_var), NULL);
	}
}
static void Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadImage_m292D9436BBD741ADD1159ACD372308448898E126(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_0_0_0_var), NULL);
	}
}
static void Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadText_m1D7AEDECA401D81E0FE0B80242A6F11B1244CBC6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_0_0_0_var), NULL);
	}
}
static void U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0__ctor_m8B1F338D399BBDFB1577FE3078058A0C0ECEB996(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_IDisposable_Dispose_m023B93775875DDA5FC56B136B53B39BE3476C3E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D70CD4D54E4A073567F9044DB5EBA58485BCE69(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_Reset_m5C6238E1F272A88B022EF1D5BD33E1A2F4E54E57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_get_Current_mF8C7FC36607E131889A9A9AED8331CE043370F57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1__ctor_m48C2E8D4A6E69416525D1D10ECA4357A1F709158(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_IDisposable_Dispose_mEAD4E07760EC46CD000717CF865BA36785AF7C4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4723D0018EAA7B6A932E0937FA85661288EA808F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_Reset_mBFBD2050CD5EBB6324D256BBDC9A43484B739600(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_get_Current_m6BC8285F42611EBF06A5F784D34952F7FF2CB13C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2__ctor_m46565F438AEA7CC91C53F58D968DA3C585F70003(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_IDisposable_Dispose_mE5405D9D37A7D2F1D164A3B8C86D3284E4E91B16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1E5DE562F1972533B57AD4C08E9003DF3FB8E44(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_Reset_m6AEBE2D189CBBD69C57BB6B363D4B1CBFE11EF06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_get_Current_m44621CCBB0A8DF7A54D323974919F4424397F182(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6__ctor_m736D4A1F1D30BB619CC8AF938713E9B93B0C75F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_IDisposable_Dispose_m0C927DFD368CFE1E6F31DDF44039D8BD71B8229F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m597DBB3BF363A420B77DB60D2B4D574445409836(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_Reset_m0751DCDC13928AA5D907AC7B36516C65D707DF14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_get_Current_m4DF213FF52A5A077F31FF7451F20153D024682C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6(tmp, il2cpp_codegen_type_get_object(ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var), il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), il2cpp_codegen_type_get_object(ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var), NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator_m_FeatheringWidth(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x77\x69\x64\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x65\x61\x74\x68\x65\x72\x69\x6E\x67\x20\x28\x69\x6E\x20\x77\x6F\x72\x6C\x64\x20\x75\x6E\x69\x74\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BigFileTransfer_t5B293FB7D88B7FFBFEA260C17A621E0635808A85_CustomAttributesCacheGenerator_BigFileTransfer_NowCheckForResourceUnload_m5EB71F677981F73F4F3775E68423D6059B0AF4FB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_0_0_0_var), NULL);
	}
}
static void U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5__ctor_m4810B14070C5216BCA0F1F217A7B3653BC66F5B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_IDisposable_Dispose_mDA95B5C816AEC937D165CF6D263C7085959E6A2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD8597AAA4D7CC5CCC1EA88DCA8417825EAE7C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_Reset_m61B8907EC6A8728FEB290A78E71268521E2E7EAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_get_Current_m47DEB1A7423358FEA2F80C95D805C376FB593CC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t547128B2223988233E2B28DA1FC6FAE2D91A9856_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_RemoveQuotes_m9A717CF268B56CC0E5DE3D67272B856909B8E166(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_ToJSON_mCA4FCBE50520BBEE0533C4BB26710BAC73021462(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_Invoke_m0A97AA69FF14FC349F826EA8CB5E22F7E958A50D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_InvokeRoutine_m68D13618A04F1101FC57EB9B7F6A6240EF47E027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_0_0_0_var), NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fetchText_m5BC095AD33A5648A3A1F5A8DCC2966C70EC96D45(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_getText_m1C9828B2FB479308B4245F59087EAF0C4232B7C2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fetchText_m41CC23C150225080757EB0305E55CC9F747D79C3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fetchImage_mFFAD95B615A556F8FAA456813FF7C0F4A4ECE4EC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fit_m6CE160463F7C2D2A0E3CE704DB5D2A45E48768F9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fitIn_m098C6C9472334448B87968472E68FC0E7EB7A7F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3__ctor_mDE74617B2BB991AC518479ADBD4FC449F2CF8713(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_IDisposable_Dispose_m6ADC93DE5909213D7180E3892F23EEF1391A085D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DB589A82DEB3980CD467F5210816729B6332E2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_Reset_m1054613E0F54ADCCA9F9B985EEF2D99D23816070(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_get_Current_mCD45E7F0EF779A679CC0267BB47747283483AD63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LeaderBoard_tD4CC602D4E66148EE1DCDE17472560F977122150_CustomAttributesCacheGenerator_LeaderBoard_Start_m662CE1902678089ED1725BD705C2D55569FA2717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t8A0A04CCD899D07994320C470D86EFA3548C530F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1__ctor_m3678519FEB2A830B52694CC1A8F920BC498ACD37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_IDisposable_Dispose_mFAE344D106FD947608664C6EAA75C92211E6D1F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B7D1CA1CFA697529F4FA22E71C7E4B74771A002(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m6C97C235FF601436BEEE231C487C46F7E51D8E1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mAA56FDC02462F6064F0966239B3399EC2E3FE7CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_m_PlacedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x73\x20\x74\x68\x69\x73\x20\x70\x72\x65\x66\x61\x62\x20\x6F\x6E\x20\x61\x20\x70\x6C\x61\x6E\x65\x20\x61\x74\x20\x74\x68\x65\x20\x74\x6F\x75\x63\x68\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_U3CspawnedObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_onPlacedObject(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_get_spawnedObject_m6E6DC83FA54E6A49680A69B3D087119A2F6010CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_set_spawnedObject_m101BBFC64960985CF1A5BC47C788741012A02728(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_add_onPlacedObject_m03FFE730BC4665310982D88817DD2F01CAFE4DB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_remove_onPlacedObject_m903CF85547E4B7FA691C36447C003F5FC41A54A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_m_PlacedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x73\x20\x74\x68\x69\x73\x20\x70\x72\x65\x66\x61\x62\x20\x6F\x6E\x20\x61\x20\x70\x6C\x61\x6E\x65\x20\x61\x74\x20\x74\x68\x65\x20\x74\x6F\x75\x63\x68\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_U3CspawnedObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_PlaceOnPlane_get_spawnedObject_m3EEB260F2583438D88633A58DB2A0E7C9FC9853E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_PlaceOnPlane_set_spawnedObject_m2FCF31C894AF24CB5E36FA21620911FC41270E7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaneDetectionController_tDBC42138D037A4A6349259E91DB2E80F97E930BA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var), NULL);
	}
}
static void PlaneDetectionController_tDBC42138D037A4A6349259E91DB2E80F97E930BA_CustomAttributesCacheGenerator_m_TogglePlaneDetectionText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x55\x49\x20\x54\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x70\x6C\x61\x6E\x65\x20\x64\x65\x74\x65\x63\x74\x69\x6F\x6E\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FadePlaneOnBoundaryChange_t505F6F32EC6CD33C0DAF386F378B0C0968D786CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_CameraManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x52\x43\x61\x6D\x65\x72\x61\x4D\x61\x6E\x61\x67\x65\x72\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x70\x72\x6F\x64\x75\x63\x65\x20\x66\x72\x61\x6D\x65\x20\x65\x76\x65\x6E\x74\x73\x2E"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_PlaneManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_MoveDeviceAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_TapToPlaceAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_LoadAssetBundle_m2A8F39B091A6E2678A513F029728116BAD6B321A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_load360InGameMode_mAC10C32153021F0DE28F29DAD37800D5DEC44469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getAllTps_m63B27A953D9BBF10760CE224ED695E490CFD1050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_download360_m0A0CEDC96DF65F9AC90CADE6C1BAC75D9D196E87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_downloadForGameMode_mA8EF232F20FF4C0B9DE8F25580DC2185B7F6494A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_download360ForGameMode_m2D430260CD0B1CE0E80849BDB2C623BDD57963F0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_download_mE46059585947898E3F841CBF2E20DCF0CBCB3F42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getPortalsJson_m36A6E9ECB959C23FE3F2AC3BDC4ED6738B842700(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getPortal_m44629A7892D19B1D30F933394D2E9C82A23211F9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_0_0_0_var), NULL);
	}
}
static void cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getGameModeJson_m6E1164B53A6B983E1CB7A91FB0254D141C878B68(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_0_0_0_var), NULL);
	}
}
static void U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11__ctor_m27DB2E4150ED40D54B6D7AC09A233248B2892300(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_IDisposable_Dispose_m757415C4F7CCEF82DC8DD2BB460FFD3483BCF4A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75089F44DE96500C0BF6967D14008FBF15EDDD24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mCC75770FBA53EB5CFA7D41DDD37C694BC01FBD84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m6C541003B1786239D2669D83C3568C39A97D13DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t84B991042A6DB2E4E14673B20459A5C553458271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12__ctor_mE83D806E0EE2815CAB01D8B62CF1E7FBB6A2BC36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_IDisposable_Dispose_mD73B9A03551E2E0B1DDE124768B4637E36901413(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFE70ED4FAF646F8C999092EBA78F42076C88CEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_Reset_m6108BAF5C6A492E6A5B33A9FE215256428726EFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_get_Current_mCFAF5DAA51544C6913BA7C2CBB0CB047B5635D45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14__ctor_m51508F758A4EE470759F88A90E33C10CA651C4CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_IDisposable_Dispose_m59A4443BEAFEC7E72871F04F73ABE41CBB20953D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3280A4DC4A2D656206EF0199368AA999FC3CA53D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_Reset_mC2E810E082B85571FA8F14A2EE0F9BEAF1020D10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_get_Current_m94A816C63B45183CBAE8C2BFCB9A4AB1F458BAF1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t599CC2C57287DFC074E39FF89F088A5505978DBF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_1_t023B67C40E047D22AEEA6867B4A5E3184A615327_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_2_t7287BACF19C861B2D54A5DBA32B8C41816A250DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tB015DCC41EC88A2FDCBC3F805FE24C4AE7BAD1D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15__ctor_mFB7F1D6BC6F63CC8129E1EDD9F042D86A2C247A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_IDisposable_Dispose_m33E0DEAF9648839BD56911E0484ACFB603D4B02E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m691B920495544D18CEEFCE8127C290181909C4AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_Collections_IEnumerator_Reset_m1B4C4BAB956CDCFB14DE8386FE2D17A088CF4463(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_Collections_IEnumerator_get_Current_mA5E9CEDC5B9EB29BB4F38156CA7D4B4B07B5022C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_tCD69C0A09EDA3620564B1101D30E5A35B66D9860_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_1_tE2475B212D6491C824BD27EA0B263A4A5721A70F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_2_t02DB53FC866EBB1103BC31882FDA6BF6E93D024D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16__ctor_m472E02B641AFDF72C75D3725D7126F85EFC9BD53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_IDisposable_Dispose_m0EA57873FAD8370AE52B273CA4EFE711BDF18933(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m358F85E1553EB93E39939F33714E1BBE1136BB70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_Reset_mA8F421E36E6A9C9EB392365B7788722EC3B6AA08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_get_Current_mADB9FC2B5CD54319C4C552D6E54FCB88F6E25DEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t943769AF13B2A99231F1621FF21169F918B8ACB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_1_t42959D7BEE9CA63A3AEEC276E0E21D11B1642846_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_2_tA8339C34E6123ADBE220A6920C6BEF8B64386866_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17__ctor_mE9557100C0BA840F6F03FE0EEEFDC692ED8A6011(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_IDisposable_Dispose_m5DE4CA4615F57DF289C548A1631ACEE10BB11894(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC17E8C51DAC6D1212F7BA907F67C8030B1E3CE36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_Reset_mBBDB9EC2A4D88EED8B85216A582E109A4BBD31C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_get_Current_m7FE0DBAF0EF8B6FD7A6D2D39C1BD3BAB76C6DF49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_tC53579BE647D467D9BA1D3B4C5F60B5928331119_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_1_tDD1F816D5F97582F7063102C30CBB91D913FE667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18__ctor_m43E1E27480854B1FE2DB6B6DEAE884B7FBEA7134(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_IDisposable_Dispose_mA29A63F245B20473233D4D16D370D84417CF9617(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB4E2FC6B956A6AF4568DE51640B3ACC4BABE482(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_Collections_IEnumerator_Reset_m946ACD78A5CA4F8B7E98BCE82ED7E3D9C97CAD3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_Collections_IEnumerator_get_Current_m81787FF7DF119D442C3C3291F65A91B2A1271E18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t0A6BFD01C69FBBB8128FBA48A058BAC0E1DFB3EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_1_t13D5BC8504CC8DB0F88FCE382672C1849328A1A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19__ctor_m0D8998FC0BB840571494BFCF5E5059EC16CB430B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_IDisposable_Dispose_m5C0DD30947E713AF6E1C124D915F9D9AF4D625CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AF10A11810A70141E8CE7B926EEB339D650B226(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_Reset_mE676442194F6699FC2B720F79D01A1F6EE85086A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_get_Current_m530301696820C06A9957D466F8AD0B3FC86A74A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_tBEA8813AF5442EC06B89ED6679D9F1AEE2D9C4A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_1_t55863385EA88ABF24B6EB688E6B08629F5D6F387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_2_t56D4E3837F36B756F9CB7AA6B0DF0C21AE84795E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_3_t58594B8307432013C7D47C312031796627F58AAD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20__ctor_m575CD131084A10603A0E610EF658BFB9091E420B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_IDisposable_Dispose_mA6058E0DB176DDBEF05040D7F4D9BA588BCB094E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E4907A063AF1F0C4A0A3F4DE4520126AB94AE79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_Collections_IEnumerator_Reset_m4095F38A748F61E9F7A2CAE7B8140FF0D95F408A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_Collections_IEnumerator_get_Current_mDD7D091DBC38241704DCE7A16B08A60F7ACF54F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t915C2A46A97F314BEB37DD828E47192C1CF335CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_1_t93B511A5DFEAF891F7AA9DA171FF33AB0A576528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21__ctor_m33BA62505B1495742109388FDD4AD47D59299713(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_IDisposable_Dispose_m2BA528F86EB7740587B9DE8992F9ABEA4AE6F1E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3682D310CCEBCC6227CEDEF4937C0FE58C4C0D15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_Reset_m5B4D96E2D62CA34C2DF12D1FB947F73C91C10D6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_get_Current_m6C4AECAF32F3DF93C95D46238555A5F7671913DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tED7640CA24D4EDA27563E599ADC26E580157F6B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void collapsibleList_t42A7A7596008BD989870E9A5720D7268093E0266_CustomAttributesCacheGenerator_collapsibleList_expandCollapse_mADFAD1573901F55E3251D47EA2ACAB29C6AC731E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_0_0_0_var), NULL);
	}
}
static void U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5__ctor_mBFBDFACBF40F3A6AC86C6232A962D8D5A460733F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_IDisposable_Dispose_m426B430EA932152626ED4F5A729047211B853E06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B073DE4ACAC5B582EE2DC2358E6453237C630F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_Reset_mF6D7DFDB099383114F4D246A1157C185A59D76CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_get_Current_m8D4E549664F10A3B58AFA4921925EB42C6F57E8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_panelPref(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x74\x68\x65\x20\x70\x72\x6F\x6A\x65\x63\x74\x73\x20\x74\x68\x75\x6D\x62\x6E\x61\x69\x6C\x20\x66\x6F\x72\x20\x32\x20\x70\x72\x6F\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_collapsiblePref(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6C\x6C\x61\x70\x73\x69\x62\x6C\x65"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_json(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x6A\x73\x6F\x6E\x20\x66\x6F\x72\x20\x64\x65\x62\x75\x67\x67\x69\x6E\x67"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_testJson(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x6A\x73\x6F\x6E\x20\x66\x6F\x72\x20\x64\x65\x62\x75\x67\x67\x69\x6E\x67"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_toLoad(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x70\x72\x6F\x6A\x65\x63\x74\x73\x20\x79\x65\x74\x20\x74\x6F\x20\x64\x6F\x77\x6E\x6C\x6F\x61\x64"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_isWaiting(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x72\x65\x20\x77\x65\x20\x64\x6F\x77\x6E\x6C\x6F\x61\x64\x69\x6E\x67\x20\x6E\x6F\x77\x20\x6F\x72\x20\x6E\x6F\x74"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_loading(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6C\x6F\x61\x64\x69\x6E\x67\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x61\x63\x74\x69\x76\x61\x74\x65\x64\x20\x77\x68\x65\x6E\x20\x69\x73\x57\x61\x69\x74\x69\x6E\x67\x20\x3D\x3D\x20\x74\x72\x75\x65"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_customBtn(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x62\x75\x74\x74\x6F\x6E\x20\x74\x68\x61\x74\x20\x63\x6F\x6D\x65\x73\x20\x77\x69\x74\x68\x20\x65\x76\x65\x72\x79\x20\x70\x6F\x72\x74\x61\x6C\x20\x75\x73\x69\x61\x6C\x6C\x79\x20\x72\x65\x73\x65\x72\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_galleryImporter_Start_m0C28E1792CF565FA829FFCDA4787A28F23C99D30(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_0_0_0_var), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_galleryImporter_spawnGallery_mB8E622DDCEBFAB6D3D1462282F3104D8D42EB698(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_0_0_0_var), NULL);
	}
}
static void galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_galleryImporter_setPortal_m03AA7011878B44152E20186188DD20F05379CF1B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27__ctor_m0B8811B30EB600364DF6C15D193DCAC339F6B626(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_IDisposable_Dispose_mE8D87EB2F4514AEDAB6555362B72FC96A2E0EDB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA2DBFD21ADBAF930E826FFD85CAB9195E8665E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_Collections_IEnumerator_Reset_m4029AE5DF84AD76E8097B4F4B258F88374A49793(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_Collections_IEnumerator_get_Current_m882972B1CA3F6ADD16C5533231020519B1F64E2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29__ctor_m0193ADC12AEB1D701A614D2B10A9F265EE4A9992(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_IDisposable_Dispose_m7FEBF588D867DC10A5585017993C635B8DAB45D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4871A28B2322CB8FAB15A72BD66A58EEDD6B9629(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_Reset_m32DC4030997459632CE3C216C330990BDF25503E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_get_Current_m96EFD1F62A0244F072445F09F66F821B22D8ED79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t72877768B1252DD4C7A03185DE7853DAB591BC5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_tC9C35FEF6B88244A988BDB0AA86E681A1EB876C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_tCD77A01F68E0B15ED76668889A68FD58A7407B99_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33__ctor_m58AD80E68A85B07032DC3D27D26BC188D3E43D5C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_IDisposable_Dispose_m21A4150C350D623D1C5F6F9DF5A90F622A82D797(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34134D144A2E3435E70851F744F08B6F705AC206(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_Collections_IEnumerator_Reset_mAA9424DFE9D8706A426B724705BC890DA5550A97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_Collections_IEnumerator_get_Current_mD5F908FBA910960F3A26A4BA1D9AC922314F8305(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_customBtn(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x62\x75\x74\x74\x6F\x6E\x20\x74\x68\x61\x74\x20\x63\x6F\x6D\x65\x73\x20\x77\x69\x74\x68\x20\x65\x76\x65\x72\x79\x20\x70\x6F\x72\x74\x61\x6C\x20\x75\x73\x69\x61\x6C\x6C\x79\x20\x72\x65\x73\x65\x72\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_Start_m169616D522718461F2522FC60796972765513C0B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_0_0_0_var), NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_setPortal_mA087AAC5911FA190935E3F15A8E68127F2563FDC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_0_0_0_var), NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_setPortalNoReload_mC693A3202E474BD7084B3925328C1A45C44597B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_0_0_0_var), NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_next_mADD0A4EA4D45B942DE04516A3311E13C6AC6BC14(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_0_0_0_var), NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_sendLeaderBoard_mA350466B6838C990593636057E0F360174E3A11C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t3D26617107211036C073D3A2592E214C7B16C9FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21__ctor_mEF2B87CF03BF250F61C1E22922C890B32917C866(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_IDisposable_Dispose_m3BC66AD8F8AE7802BCFD4D98362AA5305A338CC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5258F4F2C9E7DD1733DE60CE2DEC9597FF26A25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m145FD8392C86D200899A2C4599867A8D891F6DBC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_m5D6592FF1D9F9E1BC91BAF232D12E12097694C82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t428F076B73AD034EF38929031B9D69337E4572B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22__ctor_m14C3F1881692CB63AC92768EC611CDEC88C8E3D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_IDisposable_Dispose_m933F60E78613B8F77909F38934408D65DACC7212(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D2B7D2355AC39A635454CAAEA2C9F29E38D34D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_Collections_IEnumerator_Reset_m8194E63ADEDFADC0621E20A60D8968E09281954A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_Collections_IEnumerator_get_Current_mA9DE23C794BA2DEC3072A3E1B64A4624B705BB71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t9FAF1D4B27C787AB53C958BCA06AD8C503E1D79C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23__ctor_m1767EFC32956AAAF6F7D60E14EE15C19F411BF49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_IDisposable_Dispose_m48415DE3EB47BCF40947029C0FDA0AC0071B4C09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45321B203FA4AE4176FE9E7778990D99605467AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_Reset_m36BC0AA0642EFFC8B82559416D679FD080578627(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_get_Current_mED9BFBAF66CB09E82ECA7BBE6F9E3F531487D95D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tFA2C6A21BA99B860227E8F85F6C81767815B4FB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t87A840DF6184D9401201576438539B556CFA7334_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24__ctor_m17D0A0EC8F3115BFF62CB09AEB60A1FCB4EA556E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_IDisposable_Dispose_m4B20891703F191AB5B860B84215319B9F809E975(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m919363BEC99F1629E5E9B975AFC36E11299A47FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_Collections_IEnumerator_Reset_mD787BBEEDCF6BDA102082176386C6821C7BF4CA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_Collections_IEnumerator_get_Current_m3EBF0F869F7F8E21C2FD3227D29BFEFB85A013CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_t4B9A47B4C863CD4A4C880C33F2C1D4C7D8592D6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_1_t2983D80BFDD752478D99C1C2B3AF0D29978530DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_2_t65D46B1D3ED4D40184B91A598A71E100D1E05420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_3_t59774C8C860D1B14D26B7291E8A15A5F858BC666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25__ctor_m3135E302BFD2C0C3D21A122EA1D2C1D9901B629D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_IDisposable_Dispose_m24270614E4514553D51DC65691B789672AA2A984(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1064BB8870C65E12FCAF6E50F53AA409F9552585(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_Reset_m66A7DDDD5C34A611D7AAB4166A61B80028048A4E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_get_Current_m0A31FDCB2696350455C3F79BFDC9779F3B43F6DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void gameMode_t3482C86F16D1F4C44F0062A99B7A0FEEA60D9507_CustomAttributesCacheGenerator_gameMode_download_m3657B9D533F61FABB22FCD3EAAC82F1158E801E4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_0_0_0_var), NULL);
	}
}
static void gameMode_t3482C86F16D1F4C44F0062A99B7A0FEEA60D9507_CustomAttributesCacheGenerator_gameMode_downloadHidden_m68D4C21E769238439ACE96C78F8F9070758F471D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_0_0_0_var), NULL);
	}
}
static void U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5__ctor_m48EEE33F5E91228F9519B94A0F1A574DB8B87977(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_IDisposable_Dispose_m45936B0F78F3A28CFEC857966FA9B4C0B5C11B05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE5F967C2CF51C2616C0784F560DEC048EC9E4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_Collections_IEnumerator_Reset_m457EFAE4B023CED9721AEB88FB9E758FC46B8DC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_Collections_IEnumerator_get_Current_m397E0633C764549FDAB0F322F0B1ACECE9443A91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6__ctor_m2810B2657357FEF21FC927248373AE1845FB39D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_IDisposable_Dispose_mC3C65E624E782F46E034947EB42B4F362E764688(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E52325C5905BE8D6C05233270C82E5CB7805FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_Reset_m46369049429C9A0C7EF2FE276441BAE764F80E6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_get_Current_m6EF7A2146BA4DC6BC2877876AC97DE5FC20F5DFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void IListExtensions_t0E6B49245242D031CDD0CB4D73AAB9337BD3BBE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IListExtensions_t0E6B49245242D031CDD0CB4D73AAB9337BD3BBE4_CustomAttributesCacheGenerator_IListExtensions_Shuffle_m86A28B141E93AA1F2082B17F3FA18BB5078C927A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_customBtn(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x62\x75\x74\x74\x6F\x6E\x20\x74\x68\x61\x74\x20\x63\x6F\x6D\x65\x73\x20\x77\x69\x74\x68\x20\x65\x76\x65\x72\x79\x20\x70\x6F\x72\x74\x61\x6C\x20\x75\x73\x69\x61\x6C\x6C\x79\x20\x72\x65\x73\x65\x72\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_setPortal_m2380CD9BD029464024B834CD3C723FA8ABB123DE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_0_0_0_var), NULL);
	}
}
static void nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_0_m270B1D1CE3829824E010011625AF105A74A3AEFB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_1_mCD2790B37EE115F46C41F7D49FAA6C58710523C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_2_mA1A540D526EC3EACC52D115C32751D307497D7E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t9B5895BD8D60C639381CB8ABD17D24DE91E08190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30__ctor_m6C8E4E232B9E6DF5BF9CF28A383DF2B314A3142C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_IDisposable_Dispose_mB0CD9C5D764E231E71230E223E2D19C07EA09BDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1EDC811A1D4AFAFCA0CDC184534C02C70A2699C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_Collections_IEnumerator_Reset_mA2BB601112A640FE30489C0E4E93B407E5075C22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_Collections_IEnumerator_get_Current_m6D35ECB1F8B9011F1C5D29BE07833F759381CD0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void taskbar_t30C83E4B5BCB2B8786F98EDF8C8FACEEEF2F1372_CustomAttributesCacheGenerator_loading(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6C\x6F\x61\x64\x69\x6E\x67\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x61\x63\x74\x69\x76\x61\x74\x65\x64\x20\x77\x68\x65\x6E\x20\x69\x73\x57\x61\x69\x74\x69\x6E\x67\x20\x3D\x3D\x20\x74\x72\x75\x65"), NULL);
	}
}
static void taskbar_t30C83E4B5BCB2B8786F98EDF8C8FACEEEF2F1372_CustomAttributesCacheGenerator_customBtn(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x62\x75\x74\x74\x6F\x6E\x20\x74\x68\x61\x74\x20\x63\x6F\x6D\x65\x73\x20\x77\x69\x74\x68\x20\x65\x76\x65\x72\x79\x20\x70\x6F\x72\x74\x61\x6C\x20\x75\x73\x69\x61\x6C\x6C\x79\x20\x72\x65\x73\x65\x72\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void SimpleScrollSnap_tC27AA20908C5278006492835AD8B6226A1B00257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x2F\x53\x69\x6D\x70\x6C\x65\x20\x53\x63\x72\x6F\x6C\x6C\x2D\x53\x6E\x61\x70"), NULL);
	}
}
static void U3CU3Ec__DisplayClass77_0_tF8478878136F387D38516AA7CC53BCC3DCFA4F7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_minDisplacement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_maxDisplacement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_minValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_maxValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMinValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMaxValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMinDisplacement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMaxDisplacement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_showPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_showDisplacement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_showValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_label(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_function(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultFunction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_simpleScrollSnap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[311] = 
{
	U3CU3Ec__DisplayClass12_0_t75B37BD1CB0832F2ED8D3A2C43E68F219D6BCEE6_CustomAttributesCacheGenerator,
	U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator,
	U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator,
	U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator,
	U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator,
	ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator,
	U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t547128B2223988233E2B28DA1FC6FAE2D91A9856_CustomAttributesCacheGenerator,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator,
	U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t8A0A04CCD899D07994320C470D86EFA3548C530F_CustomAttributesCacheGenerator,
	U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator,
	PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator,
	PlaneDetectionController_tDBC42138D037A4A6349259E91DB2E80F97E930BA_CustomAttributesCacheGenerator,
	FadePlaneOnBoundaryChange_t505F6F32EC6CD33C0DAF386F378B0C0968D786CA_CustomAttributesCacheGenerator,
	U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t84B991042A6DB2E4E14673B20459A5C553458271_CustomAttributesCacheGenerator,
	U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator,
	U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t599CC2C57287DFC074E39FF89F088A5505978DBF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_1_t023B67C40E047D22AEEA6867B4A5E3184A615327_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_2_t7287BACF19C861B2D54A5DBA32B8C41816A250DA_CustomAttributesCacheGenerator,
	U3CU3Ec_tB015DCC41EC88A2FDCBC3F805FE24C4AE7BAD1D2_CustomAttributesCacheGenerator,
	U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_tCD69C0A09EDA3620564B1101D30E5A35B66D9860_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_1_tE2475B212D6491C824BD27EA0B263A4A5721A70F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_2_t02DB53FC866EBB1103BC31882FDA6BF6E93D024D_CustomAttributesCacheGenerator,
	U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t943769AF13B2A99231F1621FF21169F918B8ACB9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_1_t42959D7BEE9CA63A3AEEC276E0E21D11B1642846_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_2_tA8339C34E6123ADBE220A6920C6BEF8B64386866_CustomAttributesCacheGenerator,
	U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_tC53579BE647D467D9BA1D3B4C5F60B5928331119_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_1_tDD1F816D5F97582F7063102C30CBB91D913FE667_CustomAttributesCacheGenerator,
	U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t0A6BFD01C69FBBB8128FBA48A058BAC0E1DFB3EE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_1_t13D5BC8504CC8DB0F88FCE382672C1849328A1A1_CustomAttributesCacheGenerator,
	U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_tBEA8813AF5442EC06B89ED6679D9F1AEE2D9C4A7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_1_t55863385EA88ABF24B6EB688E6B08629F5D6F387_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_2_t56D4E3837F36B756F9CB7AA6B0DF0C21AE84795E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_3_t58594B8307432013C7D47C312031796627F58AAD_CustomAttributesCacheGenerator,
	U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t915C2A46A97F314BEB37DD828E47192C1CF335CE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_1_t93B511A5DFEAF891F7AA9DA171FF33AB0A576528_CustomAttributesCacheGenerator,
	U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tED7640CA24D4EDA27563E599ADC26E580157F6B1_CustomAttributesCacheGenerator,
	U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator,
	U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator,
	U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t72877768B1252DD4C7A03185DE7853DAB591BC5C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_tC9C35FEF6B88244A988BDB0AA86E681A1EB876C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_tCD77A01F68E0B15ED76668889A68FD58A7407B99_CustomAttributesCacheGenerator,
	U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t3D26617107211036C073D3A2592E214C7B16C9FB_CustomAttributesCacheGenerator,
	U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t428F076B73AD034EF38929031B9D69337E4572B2_CustomAttributesCacheGenerator,
	U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t9FAF1D4B27C787AB53C958BCA06AD8C503E1D79C_CustomAttributesCacheGenerator,
	U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tFA2C6A21BA99B860227E8F85F6C81767815B4FB6_CustomAttributesCacheGenerator,
	U3CU3Ec_t87A840DF6184D9401201576438539B556CFA7334_CustomAttributesCacheGenerator,
	U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_t4B9A47B4C863CD4A4C880C33F2C1D4C7D8592D6E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_1_t2983D80BFDD752478D99C1C2B3AF0D29978530DE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_2_t65D46B1D3ED4D40184B91A598A71E100D1E05420_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_3_t59774C8C860D1B14D26B7291E8A15A5F858BC666_CustomAttributesCacheGenerator,
	U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator,
	U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator,
	U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator,
	IListExtensions_t0E6B49245242D031CDD0CB4D73AAB9337BD3BBE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t9B5895BD8D60C639381CB8ABD17D24DE91E08190_CustomAttributesCacheGenerator,
	U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator,
	SimpleScrollSnap_tC27AA20908C5278006492835AD8B6226A1B00257_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass77_0_tF8478878136F387D38516AA7CC53BCC3DCFA4F7E_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator_m_FeatheringWidth,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_m_PlacedPrefab,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_U3CspawnedObjectU3Ek__BackingField,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_onPlacedObject,
	PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_m_PlacedPrefab,
	PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_U3CspawnedObjectU3Ek__BackingField,
	PlaneDetectionController_tDBC42138D037A4A6349259E91DB2E80F97E930BA_CustomAttributesCacheGenerator_m_TogglePlaneDetectionText,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_CameraManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_PlaneManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_MoveDeviceAnimation,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_TapToPlaceAnimation,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_panelPref,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_collapsiblePref,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_json,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_testJson,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_toLoad,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_isWaiting,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_loading,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_customBtn,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_customBtn,
	nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_customBtn,
	taskbar_t30C83E4B5BCB2B8786F98EDF8C8FACEEEF2F1372_CustomAttributesCacheGenerator_loading,
	taskbar_t30C83E4B5BCB2B8786F98EDF8C8FACEEEF2F1372_CustomAttributesCacheGenerator_customBtn,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_minDisplacement,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_maxDisplacement,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_minValue,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_maxValue,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMinValue,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMaxValue,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMinDisplacement,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultMaxDisplacement,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_showPanel,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_showDisplacement,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_showValue,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_label,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_function,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_defaultFunction,
	TransitionEffect_tABAF81FA628F708310F9A626D195E024387DF5AB_CustomAttributesCacheGenerator_simpleScrollSnap,
	Customizer_t900A35AA9BEDFCF1190CB9FB56693B1D76B71914_CustomAttributesCacheGenerator_Customizer_U3CStartU3Eb__10_0_mFC7C8E187BD7C36F36DEE117E2391927A0FE8B81,
	Customizer_t900A35AA9BEDFCF1190CB9FB56693B1D76B71914_CustomAttributesCacheGenerator_Customizer_U3CtreatU3Eb__12_0_m735B07CBFF747B143A745027920E8072099590F5,
	Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadText_mE26F63971A466CC2ACBA2F607A8CE2ABD78D7329,
	Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadGetText_m85B48FDD4F8BA4CFE5CB6D7B2EAC94D1081C9224,
	Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadImage_m292D9436BBD741ADD1159ACD372308448898E126,
	Fetch_t8F8FC9BBDBFFE8B58DA9AFAB39BF8209EB8BE0B1_CustomAttributesCacheGenerator_Fetch_downloadText_m1D7AEDECA401D81E0FE0B80242A6F11B1244CBC6,
	U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0__ctor_m8B1F338D399BBDFB1577FE3078058A0C0ECEB996,
	U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_IDisposable_Dispose_m023B93775875DDA5FC56B136B53B39BE3476C3E2,
	U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D70CD4D54E4A073567F9044DB5EBA58485BCE69,
	U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_Reset_m5C6238E1F272A88B022EF1D5BD33E1A2F4E54E57,
	U3CdownloadTextU3Ed__0_t79C9BC9F50EE99420F4A2C2C535FB5012491E7F6_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__0_System_Collections_IEnumerator_get_Current_mF8C7FC36607E131889A9A9AED8331CE043370F57,
	U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1__ctor_m48C2E8D4A6E69416525D1D10ECA4357A1F709158,
	U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_IDisposable_Dispose_mEAD4E07760EC46CD000717CF865BA36785AF7C4B,
	U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4723D0018EAA7B6A932E0937FA85661288EA808F,
	U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_Reset_mBFBD2050CD5EBB6324D256BBDC9A43484B739600,
	U3CdownloadGetTextU3Ed__1_t37274433237C9E4EAC3C8FE56DCBBF186FDC3330_CustomAttributesCacheGenerator_U3CdownloadGetTextU3Ed__1_System_Collections_IEnumerator_get_Current_m6BC8285F42611EBF06A5F784D34952F7FF2CB13C,
	U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2__ctor_m46565F438AEA7CC91C53F58D968DA3C585F70003,
	U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_IDisposable_Dispose_mE5405D9D37A7D2F1D164A3B8C86D3284E4E91B16,
	U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1E5DE562F1972533B57AD4C08E9003DF3FB8E44,
	U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_Reset_m6AEBE2D189CBBD69C57BB6B363D4B1CBFE11EF06,
	U3CdownloadImageU3Ed__2_t69AF9051DA471CD3540F09150173D6692C7519CE_CustomAttributesCacheGenerator_U3CdownloadImageU3Ed__2_System_Collections_IEnumerator_get_Current_m44621CCBB0A8DF7A54D323974919F4424397F182,
	U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6__ctor_m736D4A1F1D30BB619CC8AF938713E9B93B0C75F8,
	U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_IDisposable_Dispose_m0C927DFD368CFE1E6F31DDF44039D8BD71B8229F,
	U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m597DBB3BF363A420B77DB60D2B4D574445409836,
	U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_Reset_m0751DCDC13928AA5D907AC7B36516C65D707DF14,
	U3CdownloadTextU3Ed__6_t1FA42EBB0030D4C62CA3BE2D7D015B917F9C7588_CustomAttributesCacheGenerator_U3CdownloadTextU3Ed__6_System_Collections_IEnumerator_get_Current_m4DF213FF52A5A077F31FF7451F20153D024682C5,
	BigFileTransfer_t5B293FB7D88B7FFBFEA260C17A621E0635808A85_CustomAttributesCacheGenerator_BigFileTransfer_NowCheckForResourceUnload_m5EB71F677981F73F4F3775E68423D6059B0AF4FB,
	U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5__ctor_m4810B14070C5216BCA0F1F217A7B3653BC66F5B1,
	U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_IDisposable_Dispose_mDA95B5C816AEC937D165CF6D263C7085959E6A2F,
	U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD8597AAA4D7CC5CCC1EA88DCA8417825EAE7C2F,
	U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_Reset_m61B8907EC6A8728FEB290A78E71268521E2E7EAB,
	U3CNowCheckForResourceUnloadU3Ed__5_t214C71766C0C92CE110936837399F65153BFA1BB_CustomAttributesCacheGenerator_U3CNowCheckForResourceUnloadU3Ed__5_System_Collections_IEnumerator_get_Current_m47DEB1A7423358FEA2F80C95D805C376FB593CC6,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_RemoveQuotes_m9A717CF268B56CC0E5DE3D67272B856909B8E166,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_ToJSON_mCA4FCBE50520BBEE0533C4BB26710BAC73021462,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_Invoke_m0A97AA69FF14FC349F826EA8CB5E22F7E958A50D,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_InvokeRoutine_m68D13618A04F1101FC57EB9B7F6A6240EF47E027,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fetchText_m5BC095AD33A5648A3A1F5A8DCC2966C70EC96D45,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_getText_m1C9828B2FB479308B4245F59087EAF0C4232B7C2,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fetchText_m41CC23C150225080757EB0305E55CC9F747D79C3,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fetchImage_mFFAD95B615A556F8FAA456813FF7C0F4A4ECE4EC,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fit_m6CE160463F7C2D2A0E3CE704DB5D2A45E48768F9,
	ExtensionMethodes_t996ED41B44A841049BE980C4FAB280AD094646E8_CustomAttributesCacheGenerator_ExtensionMethodes_fitIn_m098C6C9472334448B87968472E68FC0E7EB7A7F7,
	U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3__ctor_mDE74617B2BB991AC518479ADBD4FC449F2CF8713,
	U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_IDisposable_Dispose_m6ADC93DE5909213D7180E3892F23EEF1391A085D,
	U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DB589A82DEB3980CD467F5210816729B6332E2E,
	U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_Reset_m1054613E0F54ADCCA9F9B985EEF2D99D23816070,
	U3CInvokeRoutineU3Ed__3_t5F1DAB05930C005BCCE8841E0EBF545A9850CCC1_CustomAttributesCacheGenerator_U3CInvokeRoutineU3Ed__3_System_Collections_IEnumerator_get_Current_mCD45E7F0EF779A679CC0267BB47747283483AD63,
	LeaderBoard_tD4CC602D4E66148EE1DCDE17472560F977122150_CustomAttributesCacheGenerator_LeaderBoard_Start_m662CE1902678089ED1725BD705C2D55569FA2717,
	U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1__ctor_m3678519FEB2A830B52694CC1A8F920BC498ACD37,
	U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_IDisposable_Dispose_mFAE344D106FD947608664C6EAA75C92211E6D1F8,
	U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B7D1CA1CFA697529F4FA22E71C7E4B74771A002,
	U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m6C97C235FF601436BEEE231C487C46F7E51D8E1F,
	U3CStartU3Ed__1_tA93A4712CABBF5E7638818F8CB61C745552942B1_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mAA56FDC02462F6064F0966239B3399EC2E3FE7CD,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_get_spawnedObject_m6E6DC83FA54E6A49680A69B3D087119A2F6010CF,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_set_spawnedObject_m101BBFC64960985CF1A5BC47C788741012A02728,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_add_onPlacedObject_m03FFE730BC4665310982D88817DD2F01CAFE4DB8,
	PlaceMultipleObjectsOnPlane_t86427E032FC2F29E059D9265C71050E372532C62_CustomAttributesCacheGenerator_PlaceMultipleObjectsOnPlane_remove_onPlacedObject_m903CF85547E4B7FA691C36447C003F5FC41A54A5,
	PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_PlaceOnPlane_get_spawnedObject_m3EEB260F2583438D88633A58DB2A0E7C9FC9853E,
	PlaceOnPlane_t3DF0B6A936DE2BB24AF3A85695A36F2D6048A9C3_CustomAttributesCacheGenerator_PlaceOnPlane_set_spawnedObject_m2FCF31C894AF24CB5E36FA21620911FC41270E7D,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_LoadAssetBundle_m2A8F39B091A6E2678A513F029728116BAD6B321A,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_load360InGameMode_mAC10C32153021F0DE28F29DAD37800D5DEC44469,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getAllTps_m63B27A953D9BBF10760CE224ED695E490CFD1050,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_download360_m0A0CEDC96DF65F9AC90CADE6C1BAC75D9D196E87,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_downloadForGameMode_mA8EF232F20FF4C0B9DE8F25580DC2185B7F6494A,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_download360ForGameMode_m2D430260CD0B1CE0E80849BDB2C623BDD57963F0,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_download_mE46059585947898E3F841CBF2E20DCF0CBCB3F42,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getPortalsJson_m36A6E9ECB959C23FE3F2AC3BDC4ED6738B842700,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getPortal_m44629A7892D19B1D30F933394D2E9C82A23211F9,
	cashing_t8A18A91301E2967B1D5A17B2D890A5D7D53480CF_CustomAttributesCacheGenerator_cashing_getGameModeJson_m6E1164B53A6B983E1CB7A91FB0254D141C878B68,
	U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11__ctor_m27DB2E4150ED40D54B6D7AC09A233248B2892300,
	U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_IDisposable_Dispose_m757415C4F7CCEF82DC8DD2BB460FFD3483BCF4A4,
	U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75089F44DE96500C0BF6967D14008FBF15EDDD24,
	U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mCC75770FBA53EB5CFA7D41DDD37C694BC01FBD84,
	U3CLoadAssetBundleU3Ed__11_tA721EF5984B799BE34D5492234E9DD9D40D3376E_CustomAttributesCacheGenerator_U3CLoadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m6C541003B1786239D2669D83C3568C39A97D13DB,
	U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12__ctor_mE83D806E0EE2815CAB01D8B62CF1E7FBB6A2BC36,
	U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_IDisposable_Dispose_mD73B9A03551E2E0B1DDE124768B4637E36901413,
	U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFE70ED4FAF646F8C999092EBA78F42076C88CEC,
	U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_Reset_m6108BAF5C6A492E6A5B33A9FE215256428726EFA,
	U3Cload360InGameModeU3Ed__12_t626F825FAE282E5B82E8691469278F66F31DC869_CustomAttributesCacheGenerator_U3Cload360InGameModeU3Ed__12_System_Collections_IEnumerator_get_Current_mCFAF5DAA51544C6913BA7C2CBB0CB047B5635D45,
	U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14__ctor_m51508F758A4EE470759F88A90E33C10CA651C4CA,
	U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_IDisposable_Dispose_m59A4443BEAFEC7E72871F04F73ABE41CBB20953D,
	U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3280A4DC4A2D656206EF0199368AA999FC3CA53D,
	U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_Reset_mC2E810E082B85571FA8F14A2EE0F9BEAF1020D10,
	U3CgetAllTpsU3Ed__14_t91AB1F852EC45FFCE9AF722AA9F86095F1AB96BC_CustomAttributesCacheGenerator_U3CgetAllTpsU3Ed__14_System_Collections_IEnumerator_get_Current_m94A816C63B45183CBAE8C2BFCB9A4AB1F458BAF1,
	U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15__ctor_mFB7F1D6BC6F63CC8129E1EDD9F042D86A2C247A4,
	U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_IDisposable_Dispose_m33E0DEAF9648839BD56911E0484ACFB603D4B02E,
	U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m691B920495544D18CEEFCE8127C290181909C4AA,
	U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_Collections_IEnumerator_Reset_m1B4C4BAB956CDCFB14DE8386FE2D17A088CF4463,
	U3Cdownload360U3Ed__15_t2256DDA1FB402287F653E6F5C8B4C259951F1461_CustomAttributesCacheGenerator_U3Cdownload360U3Ed__15_System_Collections_IEnumerator_get_Current_mA5E9CEDC5B9EB29BB4F38156CA7D4B4B07B5022C,
	U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16__ctor_m472E02B641AFDF72C75D3725D7126F85EFC9BD53,
	U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_IDisposable_Dispose_m0EA57873FAD8370AE52B273CA4EFE711BDF18933,
	U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m358F85E1553EB93E39939F33714E1BBE1136BB70,
	U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_Reset_mA8F421E36E6A9C9EB392365B7788722EC3B6AA08,
	U3CdownloadForGameModeU3Ed__16_t10B11437F58BEA13D6C4E1BDB1DA959951E0CF62_CustomAttributesCacheGenerator_U3CdownloadForGameModeU3Ed__16_System_Collections_IEnumerator_get_Current_mADB9FC2B5CD54319C4C552D6E54FCB88F6E25DEB,
	U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17__ctor_mE9557100C0BA840F6F03FE0EEEFDC692ED8A6011,
	U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_IDisposable_Dispose_m5DE4CA4615F57DF289C548A1631ACEE10BB11894,
	U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC17E8C51DAC6D1212F7BA907F67C8030B1E3CE36,
	U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_Reset_mBBDB9EC2A4D88EED8B85216A582E109A4BBD31C9,
	U3Cdownload360ForGameModeU3Ed__17_t046B258B981663CACDB4F0C7E9B2C06FF6B5E2D8_CustomAttributesCacheGenerator_U3Cdownload360ForGameModeU3Ed__17_System_Collections_IEnumerator_get_Current_m7FE0DBAF0EF8B6FD7A6D2D39C1BD3BAB76C6DF49,
	U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18__ctor_m43E1E27480854B1FE2DB6B6DEAE884B7FBEA7134,
	U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_IDisposable_Dispose_mA29A63F245B20473233D4D16D370D84417CF9617,
	U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB4E2FC6B956A6AF4568DE51640B3ACC4BABE482,
	U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_Collections_IEnumerator_Reset_m946ACD78A5CA4F8B7E98BCE82ED7E3D9C97CAD3B,
	U3CdownloadU3Ed__18_t0B15B69DAE67E809089B6809F184F07D2F8DCC8F_CustomAttributesCacheGenerator_U3CdownloadU3Ed__18_System_Collections_IEnumerator_get_Current_m81787FF7DF119D442C3C3291F65A91B2A1271E18,
	U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19__ctor_m0D8998FC0BB840571494BFCF5E5059EC16CB430B,
	U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_IDisposable_Dispose_m5C0DD30947E713AF6E1C124D915F9D9AF4D625CE,
	U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AF10A11810A70141E8CE7B926EEB339D650B226,
	U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_Reset_mE676442194F6699FC2B720F79D01A1F6EE85086A,
	U3CgetPortalsJsonU3Ed__19_tBF473DD003B25882B4C9A5300E99D36F62B2D633_CustomAttributesCacheGenerator_U3CgetPortalsJsonU3Ed__19_System_Collections_IEnumerator_get_Current_m530301696820C06A9957D466F8AD0B3FC86A74A1,
	U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20__ctor_m575CD131084A10603A0E610EF658BFB9091E420B,
	U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_IDisposable_Dispose_mA6058E0DB176DDBEF05040D7F4D9BA588BCB094E,
	U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E4907A063AF1F0C4A0A3F4DE4520126AB94AE79,
	U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_Collections_IEnumerator_Reset_m4095F38A748F61E9F7A2CAE7B8140FF0D95F408A,
	U3CgetPortalU3Ed__20_t0ED1A0C3127C36BD48A254F4BAFC176AFB85D5DD_CustomAttributesCacheGenerator_U3CgetPortalU3Ed__20_System_Collections_IEnumerator_get_Current_mDD7D091DBC38241704DCE7A16B08A60F7ACF54F6,
	U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21__ctor_m33BA62505B1495742109388FDD4AD47D59299713,
	U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_IDisposable_Dispose_m2BA528F86EB7740587B9DE8992F9ABEA4AE6F1E6,
	U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3682D310CCEBCC6227CEDEF4937C0FE58C4C0D15,
	U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_Reset_m5B4D96E2D62CA34C2DF12D1FB947F73C91C10D6B,
	U3CgetGameModeJsonU3Ed__21_tF3E96D5DC19C58A1229B892602FE020D5818B9DD_CustomAttributesCacheGenerator_U3CgetGameModeJsonU3Ed__21_System_Collections_IEnumerator_get_Current_m6C4AECAF32F3DF93C95D46238555A5F7671913DA,
	collapsibleList_t42A7A7596008BD989870E9A5720D7268093E0266_CustomAttributesCacheGenerator_collapsibleList_expandCollapse_mADFAD1573901F55E3251D47EA2ACAB29C6AC731E,
	U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5__ctor_mBFBDFACBF40F3A6AC86C6232A962D8D5A460733F,
	U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_IDisposable_Dispose_m426B430EA932152626ED4F5A729047211B853E06,
	U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B073DE4ACAC5B582EE2DC2358E6453237C630F2,
	U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_Reset_mF6D7DFDB099383114F4D246A1157C185A59D76CD,
	U3CexpandCollapseU3Ed__5_t387680554C717E4054A05110CFF2A82250149D1D_CustomAttributesCacheGenerator_U3CexpandCollapseU3Ed__5_System_Collections_IEnumerator_get_Current_m8D4E549664F10A3B58AFA4921925EB42C6F57E8C,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_galleryImporter_Start_m0C28E1792CF565FA829FFCDA4787A28F23C99D30,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_galleryImporter_spawnGallery_mB8E622DDCEBFAB6D3D1462282F3104D8D42EB698,
	galleryImporter_t8576920D646786E2DA0D3EBA1E3D698694D15C5C_CustomAttributesCacheGenerator_galleryImporter_setPortal_m03AA7011878B44152E20186188DD20F05379CF1B,
	U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27__ctor_m0B8811B30EB600364DF6C15D193DCAC339F6B626,
	U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_IDisposable_Dispose_mE8D87EB2F4514AEDAB6555362B72FC96A2E0EDB4,
	U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA2DBFD21ADBAF930E826FFD85CAB9195E8665E5,
	U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_Collections_IEnumerator_Reset_m4029AE5DF84AD76E8097B4F4B258F88374A49793,
	U3CStartU3Ed__27_t71DAE46B61B9C1D81628CEE8FDA49CA603404F32_CustomAttributesCacheGenerator_U3CStartU3Ed__27_System_Collections_IEnumerator_get_Current_m882972B1CA3F6ADD16C5533231020519B1F64E2C,
	U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29__ctor_m0193ADC12AEB1D701A614D2B10A9F265EE4A9992,
	U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_IDisposable_Dispose_m7FEBF588D867DC10A5585017993C635B8DAB45D7,
	U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4871A28B2322CB8FAB15A72BD66A58EEDD6B9629,
	U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_Reset_m32DC4030997459632CE3C216C330990BDF25503E,
	U3CspawnGalleryU3Ed__29_t0FBF2DB5ACE82129CA5637FCE84B646AB8E57001_CustomAttributesCacheGenerator_U3CspawnGalleryU3Ed__29_System_Collections_IEnumerator_get_Current_m96EFD1F62A0244F072445F09F66F821B22D8ED79,
	U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33__ctor_m58AD80E68A85B07032DC3D27D26BC188D3E43D5C,
	U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_IDisposable_Dispose_m21A4150C350D623D1C5F6F9DF5A90F622A82D797,
	U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34134D144A2E3435E70851F744F08B6F705AC206,
	U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_Collections_IEnumerator_Reset_mAA9424DFE9D8706A426B724705BC890DA5550A97,
	U3CsetPortalU3Ed__33_tBBAC62043DF10BDE7AA5517DCB12917FED9FCE82_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__33_System_Collections_IEnumerator_get_Current_mD5F908FBA910960F3A26A4BA1D9AC922314F8305,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_Start_m169616D522718461F2522FC60796972765513C0B,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_setPortal_mA087AAC5911FA190935E3F15A8E68127F2563FDC,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_setPortalNoReload_mC693A3202E474BD7084B3925328C1A45C44597B3,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_next_mADD0A4EA4D45B942DE04516A3311E13C6AC6BC14,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_sendLeaderBoard_mA350466B6838C990593636057E0F360174E3A11C,
	U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21__ctor_mEF2B87CF03BF250F61C1E22922C890B32917C866,
	U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_IDisposable_Dispose_m3BC66AD8F8AE7802BCFD4D98362AA5305A338CC7,
	U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5258F4F2C9E7DD1733DE60CE2DEC9597FF26A25,
	U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m145FD8392C86D200899A2C4599867A8D891F6DBC,
	U3CStartU3Ed__21_t835925D338AC79C4F8FC278C11F80724204536AF_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_m5D6592FF1D9F9E1BC91BAF232D12E12097694C82,
	U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22__ctor_m14C3F1881692CB63AC92768EC611CDEC88C8E3D2,
	U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_IDisposable_Dispose_m933F60E78613B8F77909F38934408D65DACC7212,
	U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D2B7D2355AC39A635454CAAEA2C9F29E38D34D0,
	U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_Collections_IEnumerator_Reset_m8194E63ADEDFADC0621E20A60D8968E09281954A,
	U3CsetPortalU3Ed__22_t24BF22C1EA93F76727D963CA9076854277E94B6D_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__22_System_Collections_IEnumerator_get_Current_mA9DE23C794BA2DEC3072A3E1B64A4624B705BB71,
	U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23__ctor_m1767EFC32956AAAF6F7D60E14EE15C19F411BF49,
	U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_IDisposable_Dispose_m48415DE3EB47BCF40947029C0FDA0AC0071B4C09,
	U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45321B203FA4AE4176FE9E7778990D99605467AC,
	U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_Reset_m36BC0AA0642EFFC8B82559416D679FD080578627,
	U3CsetPortalNoReloadU3Ed__23_t07E3329B29AF2DC7EC1781C5BF74C27E578DA5EC_CustomAttributesCacheGenerator_U3CsetPortalNoReloadU3Ed__23_System_Collections_IEnumerator_get_Current_mED9BFBAF66CB09E82ECA7BBE6F9E3F531487D95D,
	U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24__ctor_m17D0A0EC8F3115BFF62CB09AEB60A1FCB4EA556E,
	U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_IDisposable_Dispose_m4B20891703F191AB5B860B84215319B9F809E975,
	U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m919363BEC99F1629E5E9B975AFC36E11299A47FA,
	U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_Collections_IEnumerator_Reset_mD787BBEEDCF6BDA102082176386C6821C7BF4CA3,
	U3CnextU3Ed__24_t43705E1D52D8729350220DD768C49D8D52BDBDD7_CustomAttributesCacheGenerator_U3CnextU3Ed__24_System_Collections_IEnumerator_get_Current_m3EBF0F869F7F8E21C2FD3227D29BFEFB85A013CE,
	U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25__ctor_m3135E302BFD2C0C3D21A122EA1D2C1D9901B629D,
	U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_IDisposable_Dispose_m24270614E4514553D51DC65691B789672AA2A984,
	U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1064BB8870C65E12FCAF6E50F53AA409F9552585,
	U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_Reset_m66A7DDDD5C34A611D7AAB4166A61B80028048A4E,
	U3CsendLeaderBoardU3Ed__25_t25FBDD8856F52DA66169241C08C6436683E86C20_CustomAttributesCacheGenerator_U3CsendLeaderBoardU3Ed__25_System_Collections_IEnumerator_get_Current_m0A31FDCB2696350455C3F79BFDC9779F3B43F6DD,
	gameMode_t3482C86F16D1F4C44F0062A99B7A0FEEA60D9507_CustomAttributesCacheGenerator_gameMode_download_m3657B9D533F61FABB22FCD3EAAC82F1158E801E4,
	gameMode_t3482C86F16D1F4C44F0062A99B7A0FEEA60D9507_CustomAttributesCacheGenerator_gameMode_downloadHidden_m68D4C21E769238439ACE96C78F8F9070758F471D,
	U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5__ctor_m48EEE33F5E91228F9519B94A0F1A574DB8B87977,
	U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_IDisposable_Dispose_m45936B0F78F3A28CFEC857966FA9B4C0B5C11B05,
	U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BE5F967C2CF51C2616C0784F560DEC048EC9E4A,
	U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_Collections_IEnumerator_Reset_m457EFAE4B023CED9721AEB88FB9E758FC46B8DC0,
	U3CdownloadU3Ed__5_tC253E2C968F7660A8FF6678F54253F7AE105EE6C_CustomAttributesCacheGenerator_U3CdownloadU3Ed__5_System_Collections_IEnumerator_get_Current_m397E0633C764549FDAB0F322F0B1ACECE9443A91,
	U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6__ctor_m2810B2657357FEF21FC927248373AE1845FB39D6,
	U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_IDisposable_Dispose_mC3C65E624E782F46E034947EB42B4F362E764688,
	U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60E52325C5905BE8D6C05233270C82E5CB7805FC,
	U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_Reset_m46369049429C9A0C7EF2FE276441BAE764F80E6B,
	U3CdownloadHiddenU3Ed__6_tED675A9ECDC30F3EFC87AE1908C9C67930AA6E53_CustomAttributesCacheGenerator_U3CdownloadHiddenU3Ed__6_System_Collections_IEnumerator_get_Current_m6EF7A2146BA4DC6BC2877876AC97DE5FC20F5DFD,
	IListExtensions_t0E6B49245242D031CDD0CB4D73AAB9337BD3BBE4_CustomAttributesCacheGenerator_IListExtensions_Shuffle_m86A28B141E93AA1F2082B17F3FA18BB5078C927A,
	nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_setPortal_m2380CD9BD029464024B834CD3C723FA8ABB123DE,
	nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_0_m270B1D1CE3829824E010011625AF105A74A3AEFB,
	nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_1_mCD2790B37EE115F46C41F7D49FAA6C58710523C3,
	nativeGalleryImporter_tAB0954863BFF2522C94C1411FE01F797013EC424_CustomAttributesCacheGenerator_nativeGalleryImporter_U3CcustomizeSceneU3Eb__26_2_mA1A540D526EC3EACC52D115C32751D307497D7E0,
	U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30__ctor_m6C8E4E232B9E6DF5BF9CF28A383DF2B314A3142C,
	U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_IDisposable_Dispose_mB0CD9C5D764E231E71230E223E2D19C07EA09BDF,
	U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1EDC811A1D4AFAFCA0CDC184534C02C70A2699C,
	U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_Collections_IEnumerator_Reset_mA2BB601112A640FE30489C0E4E93B407E5075C22,
	U3CsetPortalU3Ed__30_t8BDD3BD9948CEDB32ED7B8BC37629D8D8DE1DB70_CustomAttributesCacheGenerator_U3CsetPortalU3Ed__30_System_Collections_IEnumerator_get_Current_m6D35ECB1F8B9011F1C5D29BE07833F759381CD0B,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
